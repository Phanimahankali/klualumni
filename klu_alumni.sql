-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 14, 2019 at 06:19 PM
-- Server version: 8.0.12
-- PHP Version: 7.1.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `klu_alumni`
--

-- --------------------------------------------------------

--
-- Table structure for table `academics`
--

CREATE TABLE `academics` (
  `email` varchar(300) NOT NULL,
  `reg` int(100) DEFAULT NULL,
  `program` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stream` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year_entry` int(100) DEFAULT NULL,
  `year_graduate` int(100) DEFAULT NULL,
  `addhar` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `academics`
--

INSERT INTO `academics` (`email`, `reg`, `program`, `stream`, `year_entry`, `year_graduate`, `addhar`) VALUES
('anandkrishna30227@gmail.com', 160030227, 'B.Tech', 'Computer Science and Engineering (CSE)', NULL, 2020, NULL),
('gunji.sairevanth@gmail.com', 160030462, 'B.Tech', 'Computer Science and Engineering (CSE)', 2016, 2020, '874525787943'),
('kteja197@gmail.com', 160030726, 'B.Tech', 'Computer Science and Engineering (CSE)', NULL, 2021, '78965413394555'),
('kujwalsai549@gmail.com', 160050098, 'B.Tech', 'Electrical and Electronics Engineering (EEE)', NULL, 2020, '65548484464858'),
('raghavendradevathi33@gmail.com', 160030307, 'B.Tech', 'Computer Science and Engineering (CSE)', NULL, 2020, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `broswer` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hash_key` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`email`, `password`, `broswer`, `ip`, `time`, `hash_key`) VALUES
('gunji.sairevanth@gmail.com', 'a154c807bec440540102000c25104e96', 'Opera', '175.101.104.28', '05-04-2019 22:53:32', '');

-- --------------------------------------------------------

--
-- Table structure for table `admin_secuirty`
--

CREATE TABLE `admin_secuirty` (
  `email` varchar(250) DEFAULT NULL,
  `hash_key` varchar(250) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status_key` varchar(150) DEFAULT NULL,
  `ip` varchar(150) DEFAULT NULL,
  `broswer` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_secuirty`
--

INSERT INTO `admin_secuirty` (`email`, `hash_key`, `timestamp`, `status_key`, `ip`, `broswer`) VALUES
('gunji.sairevanth@gmail.com', '10a2aaf849cee5d58e6dce6a8ea942c3d89edf703e18a599ee0fe603df990fe4', '2019-03-31 20:37:24', 'LOGOUT', '175.101.105.137', 'Opera'),
('gunji.sairevanth@gmail.com', '18d097cd00df25587451865dc9cf98c8bb4898fb01a124b9e2a68f60fa8da540', '2019-03-31 20:51:08', 'LOGOUT', '175.101.105.137', 'Opera'),
('gunji.sairevanth@gmail.com', '222126d1f8c2b7c5b130bf99a877d15b4982222cc20a25d1e7ae1364ce1b89d5', '2019-04-01 06:42:26', 'LOGOUT', '103.206.105.69', 'Opera'),
('gunji.sairevanth@gmail.com', 'efbbcee924a368cb3b3ba4b50c0675066ca6bee083eeb5518c0c4fa4bf71511f', '2019-04-05 01:46:00', NULL, '103.206.105.69', 'Opera'),
('gunji.sairevanth@gmail.com', '9da0e0ebd9fcdb35a3d50b20399b4fcba221414a403a376b955cd7a1b223aee1', '2019-04-05 17:22:17', 'LOGOUT', '175.101.104.28', 'Opera'),
('gunji.sairevanth@gmail.com', 'd9b1d7bdaadd0b5fa10b20b5269c9bcaa6d2cbcce6d516d58231efcbdd5ea694', '2019-04-05 17:23:32', 'LOGOUT', '175.101.104.28', 'Opera');

-- --------------------------------------------------------

--
-- Table structure for table `auth`
--

CREATE TABLE `auth` (
  `email` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `google` varchar(200) DEFAULT NULL,
  `microsoft` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `fb` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `linkedin` text,
  `twitter` text,
  `instagram` text,
  `status` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `auth`
--

INSERT INTO `auth` (`email`, `google`, `microsoft`, `password`, `fb`, `linkedin`, `twitter`, `instagram`, `status`) VALUES
('anandkrishna30227@gmail.com', NULL, NULL, '2b18b53cebf40b7f58a693513a209394', NULL, NULL, NULL, NULL, NULL),
('gunji.sairevanth@gmail.com', 'gunji.sairevanth@gmail.com', '160030462@kluniversity.in', '2ac93334baad1d814619692473a80cf2', 'https://www.facebook.com/gunji.sairevanth', '', 'https://twitter.com/GunjiRevanth', 'https://www.instagram.com/sairevanth_gunji/', 'VERIFIED'),
('kteja197@gmail.com', NULL, NULL, '897c8fde25c5cc5270cda61425eed3c8', NULL, NULL, NULL, NULL, 'VERIFIED'),
('kujwalsai549@gmail.com', NULL, NULL, 'ec6a6536ca304edf844d1d248a4f08dc', NULL, NULL, NULL, NULL, 'VERIFIED'),
('raghavendradevathi33@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `chat_messages`
--

CREATE TABLE `chat_messages` (
  `to_user_id` int(11) DEFAULT NULL,
  `from_user_id` int(11) DEFAULT NULL,
  `chat_message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `chat_message_id` int(11) NOT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chat_messages`
--

INSERT INTO `chat_messages` (`to_user_id`, `from_user_id`, `chat_message`, `chat_message_id`, `timestamp`) VALUES
(2, 1, 'Hi ra', 142, '2019-03-17 17:42:33'),
(3, 1, 'Hi', 143, '2019-03-17 17:49:22'),
(3, 1, 'Ra', 144, '2019-03-17 17:49:24'),
(3, 1, 'yela vunavu', 145, '2019-03-17 17:49:29'),
(2, 1, 'Hi', 146, '2019-03-17 18:43:11'),
(2, 1, 'Hi', 147, '2019-03-17 18:45:39'),
(1, 2, 'Hi', 148, '2019-03-18 03:52:55'),
(2, 1, 'Hi ra', 149, '2019-03-18 14:34:42'),
(3, 1, 'Ho', 150, '2019-03-19 03:57:46'),
(2, 1, 'Hi ra', 151, '2019-03-19 03:57:58'),
(2, 1, 'Hi', 152, '2019-03-19 03:58:02'),
(3, 1, 'Helli', 153, '2019-03-19 03:58:05'),
(3, 1, 'Hi', 154, '2019-03-19 05:04:51'),
(2, 1, 'Hi', 155, '2019-03-19 05:05:02'),
(2, 1, 'gg', 156, '2019-03-19 05:08:24'),
(3, 1, 'gg', 157, '2019-03-19 05:08:28'),
(2, 1, 's', 158, '2019-03-19 05:08:56'),
(2, 1, 'sssss', 159, '2019-03-19 05:08:59'),
(3, 1, 'fvfv', 160, '2019-03-19 05:09:02'),
(3, 1, 'ss', 161, '2019-03-19 05:10:01'),
(2, 1, 'jbl', 162, '2019-03-19 05:10:03'),
(3, 1, 's', 163, '2019-03-19 05:10:06'),
(3, 1, 'Hi', 164, '2019-03-19 05:18:44'),
(3, 1, 'Hi', 165, '2019-03-19 05:19:01'),
(3, 1, 'H', 166, '2019-03-19 05:19:04'),
(3, 1, 'sd', 167, '2019-03-19 05:21:30'),
(3, 1, 'dd', 168, '2019-03-19 05:21:34'),
(2, 1, 'effwr', 169, '2019-03-19 05:21:44'),
(3, 1, 'wfefew', 170, '2019-03-19 05:21:47'),
(3, 1, 'rglkheg', 171, '2019-03-19 05:25:22'),
(2, 1, 'Hi', 172, '2019-03-19 05:26:01'),
(3, 1, 'Hello', 173, '2019-03-19 05:26:05'),
(3, 1, 'ss', 174, '2019-03-19 05:28:24'),
(3, 1, 'Hi', 175, '2019-03-19 05:29:22'),
(3, 1, 'Hi', 176, '2019-03-19 05:29:30'),
(3, 1, 'Hello', 177, '2019-03-19 05:35:07'),
(2, 1, 'rgerke', 178, '2019-03-24 14:56:26'),
(2, 1, 'rfe', 179, '2019-03-24 14:56:30'),
(3, 1, 'Hi', 180, '2019-03-24 15:01:17'),
(2, 1, 'Hi', 181, '2019-03-24 15:09:37'),
(2, 1, 'd', 182, '2019-03-24 15:09:43'),
(3, 1, 'd', 183, '2019-03-24 15:10:31'),
(3, 1, 'd', 184, '2019-03-24 15:12:30'),
(3, 1, 'd', 185, '2019-03-24 15:12:32'),
(3, 1, 'd', 186, '2019-03-24 15:12:53'),
(3, 1, 'Hello', 187, '2019-03-24 15:13:51'),
(3, 1, 'dd', 188, '2019-03-24 15:28:13'),
(3, 1, 'dd', 189, '2019-03-24 15:28:46'),
(3, 1, 'a', 190, '2019-03-24 15:29:26'),
(3, 1, 's', 191, '2019-03-24 15:29:43'),
(3, 1, 's', 192, '2019-03-24 15:30:12'),
(3, 1, 'Ff', 193, '2019-03-24 15:30:22'),
(3, 1, 'ss', 194, '2019-03-24 15:31:16'),
(3, 1, 'Hello', 195, '2019-03-24 15:31:38'),
(3, 1, 'This is revanth', 196, '2019-03-24 15:31:51'),
(3, 1, 'Hejnf', 197, '2019-03-24 15:33:03'),
(3, 1, 'Hello', 198, '2019-03-24 15:33:16'),
(3, 1, 'Hi ra', 199, '2019-03-24 15:33:18'),
(3, 1, 'Ok', 200, '2019-03-24 15:33:22'),
(1, 2, 'Hi ra', 201, '2019-03-24 17:34:37'),
(2, 1, 'Hi ra', 202, '2019-03-24 17:35:14'),
(2, 1, 'Yam doing', 203, '2019-03-24 17:35:18'),
(1, 2, 'Ntg ra', 204, '2019-03-24 17:35:25'),
(1, 2, 'U?', 205, '2019-03-24 17:35:27'),
(1, 2, 'Yela vundhe', 206, '2019-03-24 17:35:43'),
(1, 2, 'Trip', 207, '2019-03-24 17:35:46'),
(2, 1, 'parla', 208, '2019-03-24 17:35:50'),
(2, 1, 'Avg', 209, '2019-03-24 17:35:53'),
(2, 1, 'Oh', 210, '2019-03-24 18:00:01'),
(14, 14, 'Hi', 211, '2019-03-27 09:06:41'),
(1, 14, 'Hello', 212, '2019-03-27 09:08:49'),
(15, 1, 'Hi', 213, '2019-03-28 05:28:00'),
(3, 1, 'Hi', 214, '2019-04-05 17:41:01'),
(3, 1, 'Hi', 215, '2019-04-05 17:42:58'),
(2, 1, 'Hi', 216, '2019-04-05 17:45:52'),
(2, 1, 'Hello', 217, '2019-04-05 17:46:04'),
(2, 1, 'Hi', 218, '2019-04-05 17:57:56'),
(2, 1, '4', 219, '2019-04-05 17:58:14'),
(2, 1, '154', 220, '2019-04-05 17:58:15'),
(2, 1, '454', 221, '2019-04-05 17:58:17'),
(3, 1, '44', 222, '2019-04-05 18:18:43'),
(3, 1, '564', 223, '2019-04-05 18:19:59'),
(3, 1, '545+65', 224, '2019-04-05 18:20:03'),
(3, 1, '64654', 225, '2019-04-05 18:20:08'),
(3, 1, 'Hello', 226, '2019-04-05 18:21:02'),
(2, 1, 'wrjngre', 227, '2019-04-05 18:22:27'),
(2, 1, ',nljnrgltkne', 228, '2019-04-05 18:22:30'),
(2, 1, 'rnj', 229, '2019-04-05 18:22:35'),
(2, 1, 'rjngr', 230, '2019-04-05 18:23:18'),
(2, 1, 'rjnrg', 231, '2019-04-05 18:23:22'),
(2, 1, 'rjkg', 232, '2019-04-05 18:23:56'),
(2, 1, 'jnegjntgkjntt', 233, '2019-04-05 18:24:00'),
(2, 1, 'Ok maa', 234, '2019-04-05 18:25:03'),
(2, 1, 'egh', 235, '2019-04-05 18:26:25'),
(2, 1, 'Hi', 236, '2019-04-05 18:28:06'),
(2, 1, 'weugr', 237, '2019-04-05 18:28:15'),
(2, 1, 'kjfnjkbn', 238, '2019-04-05 18:28:20'),
(2, 1, 'vkjfb', 239, '2019-04-05 18:28:36'),
(2, 1, 'vjnbnb', 240, '2019-04-05 18:28:49'),
(2, 1, 'njkt', 241, '2019-04-05 18:30:57'),
(2, 1, 'lnndgb', 242, '2019-04-05 18:31:02'),
(2, 1, 'bkjb', 243, '2019-04-05 18:31:13'),
(2, 1, 'fdblkbg', 244, '2019-04-05 18:31:15'),
(2, 1, 'vlnb', 245, '2019-04-05 18:31:20'),
(2, 1, 'vv', 246, '2019-04-05 18:31:26'),
(2, 1, 'dbkbfjng', 247, '2019-04-05 18:31:27'),
(2, 1, 'fslb', 248, '2019-04-05 18:31:32'),
(2, 1, 'svkhjdf', 249, '2019-04-05 18:31:46'),
(2, 1, 'sfnfb', 250, '2019-04-05 18:31:54'),
(2, 1, 'rgjkbte', 251, '2019-04-05 18:32:04'),
(2, 1, 'kdbkjdbgnjn', 252, '2019-04-05 18:32:06'),
(2, 1, 'kbngb', 253, '2019-04-05 18:32:39'),
(2, 1, 'flbng', 254, '2019-04-05 18:32:50'),
(2, 1, 'rnjlnbt', 255, '2019-04-05 18:32:54'),
(2, 1, 'blgbnngf', 256, '2019-04-05 18:33:05'),
(2, 1, 'flnbgb', 257, '2019-04-05 18:33:14'),
(2, 1, 'Testing', 258, '2019-04-05 18:35:01'),
(2, 1, 'Hello', 259, '2019-04-05 18:35:12'),
(2, 1, 'jfn', 260, '2019-04-05 18:35:26'),
(2, 1, ',vnjfnb', 261, '2019-04-05 18:35:28'),
(2, 1, 'Hello', 262, '2019-04-05 18:35:32'),
(2, 1, 'Ok ra', 263, '2019-04-05 18:35:39'),
(2, 1, 'hello mama', 264, '2019-04-05 18:35:47'),
(2, 1, 'Hello maa', 265, '2019-04-05 18:36:11'),
(2, 1, 'Hello', 266, '2019-04-05 18:40:24'),
(2, 1, 'Hello', 267, '2019-04-05 18:40:42'),
(2, 1, 'Ok', 268, '2019-04-05 18:40:48'),
(2, 1, 'sksjn', 269, '2019-04-05 18:41:23'),
(2, 1, 'lnflnblkgdbn4', 270, '2019-04-05 18:41:26'),
(2, 1, 'Hello', 271, '2019-04-05 18:41:45'),
(2, 1, 'Hello', 272, '2019-04-05 18:43:05'),
(2, 1, 'fljnbf', 273, '2019-04-05 18:43:08'),
(2, 1, 'fkhdf', 274, '2019-04-05 18:43:11'),
(2, 1, 'fljnf', 275, '2019-04-05 18:43:56'),
(2, 1, 'Hello', 276, '2019-04-05 18:45:01'),
(2, 1, 'rgoheg', 277, '2019-04-05 18:45:09'),
(2, 1, 'Hi mama', 278, '2019-04-05 18:45:12'),
(2, 1, 'Ok ra', 279, '2019-04-05 18:45:18'),
(2, 1, 'fkf', 280, '2019-04-05 18:45:43'),
(2, 1, 'Hi', 281, '2019-04-05 18:46:00'),
(1, 3, 'Hi ra', 282, '2019-04-05 18:48:31'),
(1, 3, 'Hi ra', 283, '2019-04-05 18:48:38'),
(3, 1, 'Hi yam doing', 284, '2019-04-05 18:48:51'),
(1, 3, 'Yam doing mama', 285, '2019-04-05 18:49:17'),
(1, 3, 'aaya', 286, '2019-04-05 18:49:26'),
(1, 3, 'aaya', 287, '2019-04-05 18:49:35'),
(3, 1, 'mamaaaa', 288, '2019-04-05 18:49:41'),
(1, 3, 'aaya rey', 289, '2019-04-05 18:49:51'),
(1, 3, 'mama', 290, '2019-04-05 18:50:10'),
(3, 1, 'gone aa tirupathi', 291, '2019-04-05 18:50:22'),
(1, 3, 'ha', 292, '2019-04-05 18:50:27'),
(1, 3, 'jsu now arrived ', 293, '2019-04-05 18:50:33'),
(3, 1, 'mama', 294, '2019-04-05 18:51:50'),
(3, 1, 'aaya', 295, '2019-04-05 18:51:54'),
(3, 1, 'why slowing system', 296, '2019-04-05 18:52:02'),
(1, 3, 'mama', 297, '2019-04-05 18:52:21'),
(3, 1, 'mamaa', 298, '2019-04-05 18:52:32'),
(3, 1, 'ok', 299, '2019-04-05 18:52:37'),
(3, 1, 'Aaya', 300, '2019-04-05 18:57:37'),
(2, 1, 'Hi', 301, '2019-04-06 06:22:23'),
(2, 1, 'Hello', 302, '2019-04-06 06:22:31'),
(2, 1, 'Hello', 303, '2019-04-06 06:24:27'),
(2, 1, 'Hello', 304, '2019-04-06 06:25:45'),
(2, 1, 'Hello', 305, '2019-04-06 06:26:34'),
(2, 1, 'Hello', 306, '2019-04-06 06:27:42'),
(2, 1, 'Hi', 307, '2019-04-06 06:28:10'),
(2, 1, 'Hi', 308, '2019-04-06 06:28:23'),
(3, 1, 'glnb', 309, '2019-04-06 06:32:34'),
(3, 1, 'fbfblnbdgd', 310, '2019-04-06 06:32:42'),
(1, 3, 'Hello guys', 311, '2019-04-06 06:33:06'),
(1, 3, 'mama', 312, '2019-04-06 06:33:12'),
(1, 3, 'yela vunavu', 313, '2019-04-06 06:33:16'),
(3, 1, 'yeho', 314, '2019-04-06 06:35:16'),
(3, 1, 'Yedho', 315, '2019-04-06 06:35:23'),
(3, 1, 'aaya', 316, '2019-04-06 06:36:22'),
(1, 3, 'avg', 317, '2019-04-06 06:36:28'),
(3, 1, 'ok', 318, '2019-04-06 06:37:01'),
(3, 1, 'hi', 319, '2019-04-06 06:37:05');

-- --------------------------------------------------------

--
-- Table structure for table `countrys`
--

CREATE TABLE `countrys` (
  `alpha2code` varchar(100) NOT NULL,
  `alpha3code` varchar(100) NOT NULL,
  `numeric_code` int(100) NOT NULL,
  `country_name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countrys`
--

INSERT INTO `countrys` (`alpha2code`, `alpha3code`, `numeric_code`, `country_name`) VALUES
('AF', 'AFG', 4, 'Afghanistan'),
('AL', 'ALB', 8, 'Albania'),
('DZ', 'DZA', 12, 'Algeria'),
('AS', 'ASM', 16, 'American Samoa'),
('AD', 'AND', 20, 'Andorra'),
('AO', 'AGO', 24, 'Angola'),
('AI', 'AIA', 660, 'Anguilla'),
('AQ', 'ATA', 10, 'Antarctica'),
('AG', 'ATG', 28, 'Antigua and Barbuda'),
('AR', 'ARG', 32, 'Argentina'),
('AM', 'ARM', 51, 'Armenia'),
('AW', 'ABW', 533, 'Aruba'),
('AU', 'AUS', 36, 'Australia'),
('AT', 'AUT', 40, 'Austria'),
('AZ', 'AZE', 31, 'Azerbaijan'),
('BS', 'BHS', 44, 'Bahamas'),
('BH', 'BHR', 48, 'Bahrain'),
('BD', 'BGD', 50, 'Bangladesh'),
('BB', 'BRB', 52, 'Barbados'),
('BY', 'BLR', 112, 'Belarus'),
('BE', 'BEL', 56, 'Belgium'),
('BZ', 'BLZ', 84, 'Belize'),
('BJ', 'BEN', 204, 'Benin'),
('BM', 'BMU', 60, 'Bermuda'),
('BT', 'BTN', 64, 'Bhutan'),
('BO', 'BOL', 68, 'Bolivia (Plurinational State of)'),
('BQ', 'BES', 535, 'Bonaire, Sint Eustatius and Saba'),
('BA', 'BIH', 70, 'Bosnia and Herzegovina'),
('BW', 'BWA', 72, 'Botswana'),
('BV', 'BVT', 74, 'Bouvet Island'),
('BR', 'BRA', 76, 'Brazil'),
('IO', 'IOT', 86, 'British Indian Ocean Territory'),
('BN', 'BRN', 96, 'Brunei Darussalam'),
('BG', 'BGR', 100, 'Bulgaria'),
('BF', 'BFA', 854, 'Burkina Faso'),
('BI', 'BDI', 108, 'Burundi'),
('CV', 'CPV', 132, 'Cabo Verde'),
('KH', 'KHM', 116, 'Cambodia'),
('CM', 'CMR', 120, 'Cameroon'),
('CA', 'CAN', 124, 'Canada'),
('KY', 'CYM', 136, 'Cayman Islands'),
('CF', 'CAF', 140, 'Central African Republic'),
('TD', 'TCD', 148, 'Chad'),
('CL', 'CHL', 152, 'Chile'),
('CN', 'CHN', 156, 'China'),
('CX', 'CXR', 162, 'Christmas Island'),
('CC', 'CCK', 166, 'Cocos (Keeling) Islands'),
('CO', 'COL', 170, 'Colombia'),
('KM', 'COM', 174, 'Comoros'),
('CG', 'COG', 178, 'Congo'),
('CD', 'COD', 180, 'Congo (Democratic Republic of the)'),
('CK', 'COK', 184, 'Cook Islands'),
('CR', 'CRI', 188, 'Costa Rica'),
('HR', 'HRV', 191, 'Croatia'),
('CU', 'CUB', 192, 'Cuba'),
('CY', 'CYP', 196, 'Cyprus'),
('CZ', 'CZE', 203, 'Czechia'),
('DK', 'DNK', 208, 'Denmark'),
('DJ', 'DJI', 262, 'Djibouti'),
('DM', 'DMA', 212, 'Dominica'),
('DO', 'DOM', 214, 'Dominican Republic'),
('EC', 'ECU', 218, 'Ecuador'),
('EG', 'EGY', 818, 'Egypt'),
('SV', 'SLV', 222, 'El Salvador'),
('GQ', 'GNQ', 226, 'Equatorial Guinea'),
('ER', 'ERI', 232, 'Eritrea'),
('EE', 'EST', 233, 'Estonia'),
('SZ', 'SWZ', 748, 'Eswatini'),
('ET', 'ETH', 231, 'Ethiopia'),
('FK', 'FLK', 238, 'Falkland Islands (Malvinas)'),
('FO', 'FRO', 234, 'Faroe Islands'),
('FJ', 'FJI', 242, 'Fiji'),
('FI', 'FIN', 246, 'Finland'),
('FR', 'FRA', 250, 'France'),
('GF', 'GUF', 254, 'French Guiana'),
('PF', 'PYF', 258, 'French Polynesia'),
('TF', 'ATF', 260, 'French Southern Territories'),
('GA', 'GAB', 266, 'Gabon'),
('GM', 'GMB', 270, 'Gambia'),
('GE', 'GEO', 268, 'Georgia'),
('DE', 'DEU', 276, 'Germany'),
('GH', 'GHA', 288, 'Ghana'),
('GI', 'GIB', 292, 'Gibraltar'),
('GR', 'GRC', 300, 'Greece'),
('GL', 'GRL', 304, 'Greenland'),
('GD', 'GRD', 308, 'Grenada'),
('GP', 'GLP', 312, 'Guadeloupe'),
('GU', 'GUM', 316, 'Guam'),
('GT', 'GTM', 320, 'Guatemala'),
('GG', 'GGY', 831, 'Guernsey'),
('GN', 'GIN', 324, 'Guinea'),
('GW', 'GNB', 624, 'Guinea-Bissau'),
('GY', 'GUY', 328, 'Guyana'),
('HT', 'HTI', 332, 'Haiti'),
('HM', 'HMD', 334, 'Heard Island and McDonald Islands'),
('VA', 'VAT', 336, 'Holy See'),
('HN', 'HND', 340, 'Honduras'),
('HK', 'HKG', 344, 'Hong Kong'),
('HU', 'HUN', 348, 'Hungary'),
('IS', 'ISL', 352, 'Iceland'),
('IN', 'IND', 356, 'India'),
('ID', 'IDN', 360, 'Indonesia'),
('IR', 'IRN', 364, 'Iran (Islamic Republic of)'),
('IQ', 'IRQ', 368, 'Iraq'),
('IE', 'IRL', 372, 'Ireland'),
('IM', 'IMN', 833, 'Isle of Man'),
('IL', 'ISR', 376, 'Israel'),
('IT', 'ITA', 380, 'Italy'),
('JM', 'JAM', 388, 'Jamaica'),
('JP', 'JPN', 392, 'Japan'),
('JE', 'JEY', 832, 'Jersey'),
('JO', 'JOR', 400, 'Jordan'),
('KZ', 'KAZ', 398, 'Kazakhstan'),
('KE', 'KEN', 404, 'Kenya'),
('KI', 'KIR', 296, 'Kiribati'),
('KP', 'PRK', 408, 'Korea (Democratic People\'s Republic of)'),
('KR', 'KOR', 410, 'Korea (Republic of)'),
('KW', 'KWT', 414, 'Kuwait'),
('KG', 'KGZ', 417, 'Kyrgyzstan'),
('LA', 'LAO', 418, 'Lao People\'s Democratic Republic'),
('LV', 'LVA', 428, 'Latvia'),
('LB', 'LBN', 422, 'Lebanon'),
('LS', 'LSO', 426, 'Lesotho'),
('LR', 'LBR', 430, 'Liberia'),
('LY', 'LBY', 434, 'Libya'),
('LI', 'LIE', 438, 'Liechtenstein'),
('LT', 'LTU', 440, 'Lithuania'),
('LU', 'LUX', 442, 'Luxembourg'),
('MO', 'MAC', 446, 'Macao'),
('MK', 'MKD', 807, 'Macedonia (the former Yugoslav Republic of)'),
('MG', 'MDG', 450, 'Madagascar'),
('MW', 'MWI', 454, 'Malawi'),
('MY', 'MYS', 458, 'Malaysia'),
('MV', 'MDV', 462, 'Maldives'),
('ML', 'MLI', 466, 'Mali'),
('MT', 'MLT', 470, 'Malta'),
('MH', 'MHL', 584, 'Marshall Islands'),
('MQ', 'MTQ', 474, 'Martinique'),
('MR', 'MRT', 478, 'Mauritania'),
('MU', 'MUS', 480, 'Mauritius'),
('YT', 'MYT', 175, 'Mayotte'),
('MX', 'MEX', 484, 'Mexico'),
('FM', 'FSM', 583, 'Micronesia (Federated States of)'),
('MD', 'MDA', 498, 'Moldova (Republic of)'),
('MC', 'MCO', 492, 'Monaco'),
('MN', 'MNG', 496, 'Mongolia'),
('ME', 'MNE', 499, 'Montenegro'),
('MS', 'MSR', 500, 'Montserrat'),
('MA', 'MAR', 504, 'Morocco'),
('MZ', 'MOZ', 508, 'Mozambique'),
('MM', 'MMR', 104, 'Myanmar'),
('NA', 'NAM', 516, 'Namibia'),
('NR', 'NRU', 520, 'Nauru'),
('NP', 'NPL', 524, 'Nepal'),
('NL', 'NLD', 528, 'Netherlands'),
('NC', 'NCL', 540, 'New Caledonia'),
('NZ', 'NZL', 554, 'New Zealand'),
('NI', 'NIC', 558, 'Nicaragua'),
('NE', 'NER', 562, 'Niger'),
('NG', 'NGA', 566, 'Nigeria'),
('NU', 'NIU', 570, 'Niue'),
('NF', 'NFK', 574, 'Norfolk Island'),
('MP', 'MNP', 580, 'Northern Mariana Islands'),
('NO', 'NOR', 578, 'Norway'),
('OM', 'OMN', 512, 'Oman'),
('PK', 'PAK', 586, 'Pakistan'),
('PW', 'PLW', 585, 'Palau'),
('PS', 'PSE', 275, 'Palestine, State of'),
('PA', 'PAN', 591, 'Panama'),
('PG', 'PNG', 598, 'Papua New Guinea'),
('PY', 'PRY', 600, 'Paraguay'),
('PE', 'PER', 604, 'Peru'),
('PH', 'PHL', 608, 'Philippines'),
('PN', 'PCN', 612, 'Pitcairn'),
('PL', 'POL', 616, 'Poland'),
('PT', 'PRT', 620, 'Portugal'),
('PR', 'PRI', 630, 'Puerto Rico'),
('QA', 'QAT', 634, 'Qatar'),
('RO', 'ROU', 642, 'Romania'),
('RU', 'RUS', 643, 'Russian Federation'),
('RW', 'RWA', 646, 'Rwanda'),
('SH', 'SHN', 654, 'Saint Helena, Ascension and Tristan da Cunha'),
('KN', 'KNA', 659, 'Saint Kitts and Nevis'),
('LC', 'LCA', 662, 'Saint Lucia'),
('MF', 'MAF', 663, 'Saint Martin (French part)'),
('PM', 'SPM', 666, 'Saint Pierre and Miquelon'),
('VC', 'VCT', 670, 'Saint Vincent and the Grenadines'),
('WS', 'WSM', 882, 'Samoa'),
('SM', 'SMR', 674, 'San Marino'),
('ST', 'STP', 678, 'Sao Tome and Principe'),
('SA', 'SAU', 682, 'Saudi Arabia'),
('SN', 'SEN', 686, 'Senegal'),
('RS', 'SRB', 688, 'Serbia'),
('SC', 'SYC', 690, 'Seychelles'),
('SL', 'SLE', 694, 'Sierra Leone'),
('SG', 'SGP', 702, 'Singapore'),
('SX', 'SXM', 534, 'Sint Maarten (Dutch part)'),
('SK', 'SVK', 703, 'Slovakia'),
('SI', 'SVN', 705, 'Slovenia'),
('SB', 'SLB', 90, 'Solomon Islands'),
('SO', 'SOM', 706, 'Somalia'),
('ZA', 'ZAF', 710, 'South Africa'),
('GS', 'SGS', 239, 'South Georgia and the South Sandwich Islands'),
('SS', 'SSD', 728, 'South Sudan'),
('ES', 'ESP', 724, 'Spain'),
('LK', 'LKA', 144, 'Sri Lanka'),
('SD', 'SDN', 729, 'Sudan'),
('SR', 'SUR', 740, 'Suriname'),
('SJ', 'SJM', 744, 'Svalbard and Jan Mayen'),
('SE', 'SWE', 752, 'Sweden'),
('CH', 'CHE', 756, 'Switzerland'),
('SY', 'SYR', 760, 'Syrian Arab Republic'),
('TW', 'TWN', 158, 'Taiwan, Province of China'),
('TJ', 'TJK', 762, 'Tajikistan'),
('TZ', 'TZA', 834, 'Tanzania, United Republic of'),
('TH', 'THA', 764, 'Thailand'),
('TL', 'TLS', 626, 'Timor-Leste'),
('TG', 'TGO', 768, 'Togo'),
('TK', 'TKL', 772, 'Tokelau'),
('TO', 'TON', 776, 'Tonga'),
('TT', 'TTO', 780, 'Trinidad and Tobago'),
('TN', 'TUN', 788, 'Tunisia'),
('TR', 'TUR', 792, 'Turkey'),
('TM', 'TKM', 795, 'Turkmenistan'),
('TC', 'TCA', 796, 'Turks and Caicos Islands'),
('TV', 'TUV', 798, 'Tuvalu'),
('UG', 'UGA', 800, 'Uganda'),
('UA', 'UKR', 804, 'Ukraine'),
('AE', 'ARE', 784, 'United Arab Emirates'),
('GB', 'GBR', 826, 'United Kingdom of Great Britain and Northern Ireland'),
('US', 'USA', 840, 'United States of America'),
('UM', 'UMI', 581, 'United States Minor Outlying Islands'),
('UY', 'URY', 858, 'Uruguay'),
('UZ', 'UZB', 860, 'Uzbekistan'),
('VU', 'VUT', 548, 'Vanuatu'),
('VE', 'VEN', 862, 'Venezuela (Bolivarian Republic of)'),
('VN', 'VNM', 704, 'Viet Nam'),
('VG', 'VGB', 92, 'Virgin Islands (British)'),
('VI', 'VIR', 850, 'Virgin Islands (U.S.)'),
('WF', 'WLF', 876, 'Wallis and Futuna'),
('EH', 'ESH', 732, 'Western Sahara'),
('YE', 'YEM', 887, 'Yemen'),
('ZM', 'ZMB', 894, 'Zambia'),
('ZW', 'ZWE', 716, 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `event_name`
--

CREATE TABLE `event_name` (
  `e_code` int(11) NOT NULL,
  `name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_date` varchar(150) DEFAULT NULL,
  `event_desc` text,
  `cover_photo_format` varchar(150) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `venue` varchar(150) DEFAULT NULL,
  `location` varchar(150) DEFAULT NULL,
  `duration` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event_photos`
--

CREATE TABLE `event_photos` (
  `code` int(11) NOT NULL,
  `image_type` varchar(150) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(150) DEFAULT NULL,
  `event_code` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `event_photos`
--

INSERT INTO `event_photos` (`code`, `image_type`, `timestamp`, `status`, `event_code`) VALUES
(78, 'jpg', '2019-03-10 18:58:39', 'OK', 38),
(79, 'png', '2019-03-10 18:58:39', 'OK', 38),
(80, 'png', '2019-03-10 18:58:39', 'OK', 38),
(81, 'png', '2019-03-10 18:58:39', 'OK', 38),
(82, 'png', '2019-03-10 18:58:39', 'OK', 38),
(83, '', '2019-03-10 19:00:21', 'OK', 39),
(84, '', '2019-03-10 19:06:29', 'OK', 40),
(85, '', '2019-03-10 19:16:20', 'OK', 41),
(86, '', '2019-03-10 19:37:02', 'OK', 42),
(87, 'jpg', '2019-03-15 10:58:39', 'OK', 43),
(88, 'jpg', '2019-03-15 10:58:40', 'OK', 43),
(89, 'jpg', '2019-03-15 10:58:40', 'OK', 43),
(90, 'jpg', '2019-03-15 10:58:40', 'OK', 43),
(91, 'jpg', '2019-03-15 10:58:40', 'OK', 43),
(92, 'jpg', '2019-03-15 10:58:40', 'OK', 43),
(93, 'jpg', '2019-03-15 10:58:40', 'OK', 43),
(94, 'jpg', '2019-03-27 07:43:52', 'OK', 44),
(95, 'jpg', '2019-03-27 07:43:52', 'OK', 44),
(96, 'jpg', '2019-03-27 07:43:52', 'OK', 44),
(97, 'jpg', '2019-03-27 07:43:52', 'OK', 44),
(98, 'jpg', '2019-03-27 07:43:53', 'OK', 44),
(99, 'jpg', '2019-03-27 07:43:53', 'OK', 44),
(100, 'jpg', '2019-03-27 07:43:53', 'OK', 44),
(101, 'jpg', '2019-03-27 07:43:53', 'OK', 44),
(102, 'jpg', '2019-03-27 07:43:53', 'OK', 44),
(103, 'jpg', '2019-03-27 07:43:53', 'OK', 44),
(104, 'jpg', '2019-03-27 07:43:53', 'OK', 44),
(105, 'jpg', '2019-03-27 07:43:53', 'OK', 44),
(106, 'jpg', '2019-03-27 07:43:53', 'OK', 44),
(107, 'jpg', '2019-03-27 07:43:53', 'OK', 44),
(108, 'jpg', '2019-03-27 07:43:53', 'OK', 44),
(109, 'jpg', '2019-03-27 07:43:53', 'OK', 44),
(110, 'jpg', '2019-03-27 07:43:53', 'OK', 44),
(111, 'jpg', '2019-03-27 07:43:53', 'OK', 44),
(112, 'jpg', '2019-03-27 07:43:53', 'OK', 44),
(113, 'jpg', '2019-03-27 07:46:03', 'OK', 44),
(114, 'jpg', '2019-03-27 07:46:03', 'OK', 44),
(115, 'jpg', '2019-03-27 07:46:03', 'OK', 44);

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `code` int(11) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `department` varchar(200) DEFAULT NULL,
  `department_designation` varchar(150) DEFAULT NULL,
  `alumni_designation` varchar(150) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `phone` varchar(150) DEFAULT NULL,
  `photo` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`code`, `name`, `department`, `department_designation`, `alumni_designation`, `email`, `phone`, `photo`) VALUES
(14, 'Er. K. Sathyanarayana', '', 'President, KLEF', 'Advisory Board', '', '', '14f9084e070bb29cbf8399fbc553530a.jpg'),
(16, 'Dr.L.S.S Reddy', '', 'Vice Chancellor, K L University', 'Advisory Board', '', '', 'dfbd8ca820a66b6ccb00b254c00ab164.jpg'),
(17, 'Dr.A.V.S.Prasad', '', 'Pro-Vice Chancellor, K L University', 'Advisory Board', '', '', 'b7558dc83d2a7ddc6e78e73ada9c04cf.jpg'),
(18, 'Prof. R.R.L. Kantam', '', 'Registrar, K L University', 'Advisory Board', '', '', '764776a478dfc9095582359f24bdb531.jpg'),
(19, 'Mr.Rajasekhar Kandepu', '', 'Director of Alumni Relations', 'Organisation', '', '', '9b30b3fbf2d4bbdc0a7bf372de3f4331.jpg'),
(22, 'Dr. P.V.R.D Prasada Rao', '', 'Assoc. Dean (Alumni, Placements & Progression)', 'Organisation', '', '', 'ac1d1df72b2d0842a8b95e5b8f7ff5cb.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `code` int(11) NOT NULL,
  `event_name` varchar(400) DEFAULT NULL,
  `date` varchar(150) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `location` varchar(150) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`code`, `event_name`, `date`, `timestamp`, `location`, `description`) VALUES
(70, 'Hello', '2019-04-04', '2019-04-01 06:44:05', 'Testing', 'Testing');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_photos`
--

CREATE TABLE `gallery_photos` (
  `code` int(11) NOT NULL,
  `g_code` int(11) DEFAULT NULL,
  `previous_name` varchar(150) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `file_type` varchar(150) DEFAULT NULL,
  `status` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallery_photos`
--

INSERT INTO `gallery_photos` (`code`, `g_code`, `previous_name`, `timestamp`, `file_type`, `status`) VALUES
(10, 50, 'IMG_5793.JPG', '2019-03-09 08:57:24', 'jpg', 'NO'),
(11, 50, 'IMG_5794.JPG', '2019-03-09 08:57:24', 'jpg', 'NO'),
(12, 50, 'IMG_5795.JPG', '2019-03-09 08:57:25', 'jpg', 'OK'),
(13, 50, 'IMG_5796.JPG', '2019-03-09 08:57:25', 'jpg', 'OK'),
(14, 50, 'IMG_5797.JPG', '2019-03-09 08:57:25', 'jpg', 'OK'),
(15, 50, 'IMG_5798.JPG', '2019-03-09 08:57:25', 'jpg', 'OK'),
(16, 50, 'IMG_5799.JPG', '2019-03-09 08:57:25', 'jpg', 'OK'),
(17, 50, 'IMG_5812.JPG', '2019-03-09 08:57:25', 'jpg', 'OK'),
(18, 50, 'IMG_5814.JPG', '2019-03-09 08:57:25', 'jpg', 'OK'),
(19, 50, 'IMG_5817.JPG', '2019-03-09 08:57:25', 'jpg', 'OK'),
(20, 50, 'IMG_5818.JPG', '2019-03-09 08:57:25', 'jpg', 'OK'),
(21, 50, 'IMG_5819.JPG', '2019-03-09 08:57:25', 'jpg', 'OK'),
(22, 50, 'IMG_5852.JPG', '2019-03-09 08:57:25', 'jpg', 'OK'),
(23, 50, 'IMG_6385.JPG', '2019-03-09 08:57:25', 'jpg', 'OK'),
(46, 50, 'IMG_6697.JPG', '2019-03-09 17:00:46', 'jpg', 'OK'),
(48, 50, 'anand-150x150.jpg', '2019-03-09 17:09:31', 'jpg', 'OK'),
(49, 50, 'black-friday-drones.png', '2019-03-09 17:09:47', 'png', 'NO'),
(50, 50, 'Capture.PNG', '2019-03-09 17:10:12', 'png', 'OK'),
(51, 50, 'games.JPG', '2019-03-09 17:12:46', 'jpg', 'OK'),
(52, 50, 'basit-linux-komutlari-1.png', '2019-03-09 17:13:06', 'png', 'OK'),
(53, 50, 'black-friday-drones.jpg', '2019-03-09 17:13:38', 'jpg', 'OK'),
(54, 50, 'dotted-pixel-india-map-vector-20543912.jpg', '2019-03-09 17:14:20', 'jpg', 'OK'),
(55, 50, 'IMG_6455.JPG', '2019-03-09 17:14:48', 'jpg', 'OK'),
(56, 50, 'IMG_6412.JPG', '2019-03-09 17:15:11', 'jpg', 'OK'),
(57, 50, 'IMG_5861.JPG', '2019-03-09 17:15:47', 'jpg', 'OK'),
(58, 50, 'IMG_6445.JPG', '2019-03-09 17:16:30', 'jpg', 'OK'),
(59, 50, 'IMG_5860.JPG', '2019-03-09 17:17:38', 'jpg', 'OK'),
(60, 50, 'IMG_5836.JPG', '2019-03-09 17:18:12', 'jpg', 'OK'),
(61, 50, 'IMG_6395.JPG', '2019-03-09 17:19:49', 'jpg', 'OK'),
(62, 50, 'IMG_6374.JPG', '2019-03-09 17:20:12', 'jpg', 'OK'),
(63, 50, 'IMG_5848.JPG', '2019-03-09 17:20:30', 'jpg', 'OK'),
(64, 50, '2.jpg', '2019-03-09 17:27:52', 'jpg', 'OK'),
(65, 50, '4.jpg', '2019-03-09 17:28:49', 'jpg', 'OK'),
(66, 50, '1.jpg', '2019-03-09 17:29:29', 'jpg', 'OK'),
(67, 50, '3.jpg', '2019-03-10 03:07:17', 'jpg', 'OK'),
(68, 50, 'IMG_6485.JPG', '2019-03-10 05:04:21', 'jpg', 'OK'),
(69, 50, '', '2019-03-10 05:59:42', '', 'NO'),
(75, 50, '4.jpg', '2019-03-10 08:22:52', 'jpg', 'NO'),
(76, 50, 'statecops.png', '2019-03-10 08:23:21', 'png', 'NO'),
(90, 68, '10014254465ac1d09e73cc4.JPG', '2019-03-10 08:35:10', 'jpg', 'NO'),
(91, 68, 'indian-army-helicopter-22838567.png', '2019-03-10 08:35:10', 'png', 'OK'),
(92, 68, 'maa.png', '2019-03-10 08:35:10', 'png', 'OK'),
(93, 68, 'statecops.png', '2019-03-10 08:35:10', 'png', 'OK'),
(94, 68, 'statecops_round.png', '2019-03-10 08:35:10', 'png', 'NO'),
(95, 69, 'DSC_0017.JPG', '2019-03-10 12:44:06', 'jpg', 'OK'),
(96, 70, '46221944_10155633593311254_5245779910378651648_o.jpg', '2019-04-01 06:44:05', 'jpg', 'OK'),
(97, 70, '46485557_10155642902091254_5324104046385037312_n.jpg', '2019-04-01 06:44:05', 'jpg', 'OK'),
(98, 70, '48379357_347663995819314_4478306891381866496_n.jpg', '2019-04-01 06:44:06', 'jpg', 'OK'),
(99, 70, '48395389_10155692088391254_1517044077432930304_o.jpg', '2019-04-01 06:44:06', 'jpg', 'OK'),
(100, 70, '48395389_10155707009841254_8178822371126280192_o.jpg', '2019-04-01 06:44:06', 'jpg', 'OK'),
(101, 70, '48396046_10155699567731254_6905600181178204160_o.jpg', '2019-04-01 06:44:06', 'jpg', 'NO'),
(102, 70, '48409620_10155695882256254_2695884365346373632_o.jpg', '2019-04-01 06:44:06', 'jpg', 'OK'),
(103, 70, '48415734_10155695882491254_6523782935798087680_o.jpg', '2019-04-01 06:44:07', 'jpg', 'OK');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `code` int(11) NOT NULL,
  `hash_key` varchar(200) DEFAULT NULL,
  `title` text,
  `file` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `url` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`code`, `hash_key`, `title`, `file`, `timestamp`, `url`) VALUES
(12, '75e67b77c1a76f53aa833593ccf45714', 'Farewell to Y7 Batch', '75e67b77c1a76f53aa833593ccf45714.', '2019-03-27 07:31:45', 'https://kluniversity.in/alumni/farewell.aspx'),
(13, '027a33fb477bc3484021da40f6675c5b', 'Alumni Meet held on 4<sup>th</sup> Dec, 2010 at Hotel Bansal Taj, Secunderabad', '027a33fb477bc3484021da40f6675c5b.pdf', '2019-03-27 07:32:45', ''),
(14, '9e3b1dc867e853607f93d347a2334101', 'KLUAA CUP - 2012, (Date : 28/02/2012 &amp; 29/02/2012)', '9e3b1dc867e853607f93d347a2334101.', '2019-03-27 07:33:52', ''),
(15, '545c534d318c412a5c82f285c1d5838b', 'SAMASANA (Togetherness) (Date : 08/04/2012)', '545c534d318c412a5c82f285c1d5838b.', '2019-03-27 07:34:16', 'https://picasaweb.google.com/114392431706604333137/KLUniversityAlumniAssociationCelebratesSAMASANATogetherness08042012?authuser=0&amp;feat=directlink'),
(16, 'acd747d9703989f993e04b5e17abef25', 'AlumniSilver Jubilee Celebrations for 1983-87 batch (11/08/2012)', 'acd747d9703989f993e04b5e17abef25.', '2019-03-27 07:34:37', 'https://picasaweb.google.com/114392431706604333137/KLUniversityAlumniAssociationSilverJubileeCelebrationsFor198387Batch11082012?authuser=0&amp;feat=directlink');

-- --------------------------------------------------------

--
-- Table structure for table `news_letters`
--

CREATE TABLE `news_letters` (
  `code` int(11) NOT NULL,
  `subject` varchar(300) DEFAULT NULL,
  `content_cover_page` varchar(150) DEFAULT NULL,
  `content_heading` varchar(250) DEFAULT NULL,
  `content` text,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `to_address` varchar(150) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `total` varchar(200) DEFAULT NULL,
  `send` varchar(200) DEFAULT NULL,
  `not_send` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_letters`
--

INSERT INTO `news_letters` (`code`, `subject`, `content_cover_page`, `content_heading`, `content`, `timestamp`, `to_address`, `status`, `total`, `send`, `not_send`) VALUES
(56, 'afwfeg', '', 'Hello world..', 'Hello world', '2019-03-15 08:28:11', 'all', 'SUCCESS', NULL, NULL, NULL),
(57, 'WFfw', '', 'aerrea', 'raggae', '2019-03-15 08:29:15', 'all', 'SUCCESS', NULL, NULL, NULL),
(58, 'wefwfw', '', 'efeff', 'RRAFREFG', '2019-03-15 08:32:36', 'all', 'SUCCESS', NULL, NULL, NULL),
(59, 'Hello', 'maa.png', 'Testing', 'fraegsrshsrrhhsr', '2019-03-15 09:02:12', 'alumnis', 'SUCCESS', NULL, NULL, NULL),
(60, 'WEFARGR', 'indian-army-helicopter-22838567.png', 'AGEGRA', 'AGRAGARGR', '2019-03-15 09:03:34', 'all', 'SUCCESS', NULL, NULL, NULL),
(61, 'Hello', '26818.jpg', 'Testing', 'Testing', '2019-03-21 10:30:42', 'all', 'SUCCESS', NULL, NULL, NULL),
(70, 'Testing', '840.jpg', 'Hello world..', 'Hello world..', '2019-03-30 18:29:55', 'all', 'SUCCESS', NULL, NULL, NULL),
(72, 'Testing', '9.png', 'Hello world..', 'wrgg', '2019-03-30 18:35:53', 'all', 'SUCCESS', NULL, NULL, NULL),
(73, 'Testing', '10.png', 'Hello world..', 'rwaer', '2019-03-30 18:36:51', 'all', 'SUCCESS', NULL, NULL, NULL),
(74, 'Testing', '10.png', 'Hello world..', 'raaer', '2019-03-30 18:37:31', 'all', 'SUCCESS', NULL, NULL, NULL),
(75, 'Testing', '16.png', 'Hello world..', 'aegt', '2019-03-30 18:38:16', 'all', 'SUCCESS', NULL, NULL, NULL),
(76, 'Testing', '10.png', 'Hello world..', 'Hello world..', '2019-03-30 18:46:04', 'all', 'SUCCESS', '', NULL, NULL),
(80, 'Testing', '3.png', 'Hello world..', 'Hello world..', '2019-03-30 19:04:29', 'alumnis', 'SUCCESS', '6', NULL, NULL),
(86, 'Testing', '9.png', 'Hello world..', 'Hello world..', '2019-03-30 19:20:06', 'all', 'SUCCESS', '6', '6', NULL),
(87, 'Testing', '9.png', 'Hello world..', 'Hello World', '2019-03-30 19:25:48', 'alumnis', 'SUCCESS', '6', '5', '1'),
(88, 'Testing', '2.png', 'Hello world..', 'Testing Sorry mama', '2019-03-30 19:27:53', 'all', 'SUCCESS', '6', '5', '1');

-- --------------------------------------------------------

--
-- Table structure for table `news_letters_files`
--

CREATE TABLE `news_letters_files` (
  `code` int(11) NOT NULL,
  `news_letters_code` int(11) DEFAULT NULL,
  `file_name` varchar(300) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_letters_files`
--

INSERT INTO `news_letters_files` (`code`, `news_letters_code`, `file_name`, `timestamp`, `status`) VALUES
(51, 56, '', '2019-03-15 08:28:11', NULL),
(52, 57, '', '2019-03-15 08:29:15', NULL),
(53, 58, '', '2019-03-15 08:32:37', NULL),
(54, 59, '', '2019-03-15 09:02:12', NULL),
(55, 60, '', '2019-03-15 09:03:34', NULL),
(56, 61, '48418954_10155701456616254_198406795922767872_o.jpg', '2019-03-21 10:30:42', NULL),
(57, 61, '10014254465ac1d09e73cc4.JPG', '2019-03-21 10:30:43', NULL),
(58, 62, '2.png', '2019-03-30 18:09:54', NULL),
(59, 63, '', '2019-03-30 18:14:21', NULL),
(60, 64, '840.jpg', '2019-03-30 18:21:40', NULL),
(61, 65, '840.jpg', '2019-03-30 18:23:05', NULL),
(62, 66, '840.jpg', '2019-03-30 18:24:05', NULL),
(63, 67, '840.jpg', '2019-03-30 18:25:20', NULL),
(64, 68, '840.jpg', '2019-03-30 18:26:20', NULL),
(65, 69, '840.jpg', '2019-03-30 18:28:22', NULL),
(66, 70, '840.jpg', '2019-03-30 18:29:55', NULL),
(67, 71, '10.png', '2019-03-30 18:34:05', NULL),
(68, 72, '4.png', '2019-03-30 18:35:53', NULL),
(69, 73, '8.png', '2019-03-30 18:36:51', NULL),
(70, 74, '3.png', '2019-03-30 18:37:31', NULL),
(71, 75, '#include Proposal Document (1).pdf', '2019-03-30 18:38:16', NULL),
(72, 76, '4.png', '2019-03-30 18:46:05', NULL),
(73, 77, '9.png', '2019-03-30 18:47:12', NULL),
(74, 78, '3.png', '2019-03-30 18:49:17', NULL),
(75, 79, '9.png', '2019-03-30 18:50:43', NULL),
(76, 80, '3.png', '2019-03-30 19:04:29', NULL),
(77, 81, '8.png', '2019-03-30 19:05:32', NULL),
(78, 82, '8.png', '2019-03-30 19:08:18', NULL),
(79, 83, '9.png', '2019-03-30 19:10:19', NULL),
(80, 84, '10.png', '2019-03-30 19:16:13', NULL),
(81, 85, '16.png', '2019-03-30 19:17:58', NULL),
(82, 86, '9.png', '2019-03-30 19:20:06', NULL),
(83, 87, '2.png', '2019-03-30 19:25:48', NULL),
(84, 88, '9.png', '2019-03-30 19:27:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `offer`
--

CREATE TABLE `offer` (
  `code` int(11) NOT NULL,
  `organization_name` varchar(250) DEFAULT NULL,
  `jobname` varchar(250) DEFAULT NULL,
  `jobtype` varchar(150) DEFAULT NULL,
  `degree` varchar(150) DEFAULT NULL,
  `lastdate` varchar(100) DEFAULT NULL,
  `joblocation` varchar(150) DEFAULT NULL,
  `compant_logo` varchar(150) DEFAULT NULL,
  `minimumqualification` text,
  `posted_by` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `hash` varchar(150) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offer`
--

INSERT INTO `offer` (`code`, `organization_name`, `jobname`, `jobtype`, `degree`, `lastdate`, `joblocation`, `compant_logo`, `minimumqualification`, `posted_by`, `status`, `hash`, `timestamp`) VALUES
(53, 'Google', 'Android Development', 'full-time', 'associate', '2019-03-12', 'Hyderabad', '50% off.png', 'Minimum Knowledge on programing in Java', 'kujwalsai549@gmail.com', 'APPROVED', '4533ec9d3fbd6e18341927c6859847aa', '2019-03-18 05:29:51');

-- --------------------------------------------------------

--
-- Table structure for table `offer_files`
--

CREATE TABLE `offer_files` (
  `code` int(11) NOT NULL,
  `filename` varchar(150) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `offer_code` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offer_files`
--

INSERT INTO `offer_files` (`code`, `filename`, `timestamp`, `offer_code`, `size`) VALUES
(31, '8af5e252-9b12-4646-b525-9bd925a55c15.csv', '2019-03-18 05:29:51', 53, 4769);

-- --------------------------------------------------------

--
-- Table structure for table `offer_response`
--

CREATE TABLE `offer_response` (
  `code` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `gender` varchar(100) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `university_name` varchar(200) DEFAULT NULL,
  `reg_id` varchar(200) DEFAULT NULL,
  `passing_out_year` varchar(10) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `status` varchar(150) DEFAULT NULL,
  `stream` varchar(200) DEFAULT NULL,
  `branch` varchar(150) DEFAULT NULL,
  `hash_id` varchar(150) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `response_to` int(11) DEFAULT NULL,
  `year` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offer_response`
--

INSERT INTO `offer_response` (`code`, `name`, `gender`, `email`, `university_name`, `reg_id`, `passing_out_year`, `phone`, `status`, `stream`, `branch`, `hash_id`, `size`, `timestamp`, `response_to`, `year`) VALUES
(47, 'G Sai Revanth', 'Male', 'gunji.sairevanth@gmail.com', 'KLU', '160030462', '2020', '9603354001', 'VERIFIED', 'B.Tech', 'CSE', '5c8f2d14d3547', 253208, '2019-03-18 05:31:00', 53, '3'),
(48, 'G Sai Revanth', 'Male', 'gunji.sairevanth@gmail.com', 'KL University', '1552', '2020', '9603354001', 'VERIFIED', 'B.Tech', 'CSE', '5c92c79344e77', 253208, '2019-03-20 23:06:59', 53, 'III');

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE `organization` (
  `code` int(11) NOT NULL,
  `organization_name` varchar(200) DEFAULT NULL,
  `logo` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`code`, `organization_name`, `logo`) VALUES
(1, 'Amazon', 'Find'),
(2, 'Flipkart', 'Find'),
(3, 'Koneru Lakshmaiah Education Foundation', 'Find'),
(4, 'K L University', 'Find'),
(5, 'Narayana Junior College', 'Find'),
(6, 'Sri Chaitanya Techno School', 'Find'),
(7, 'Sri Chaitanaya Educational Committee', NULL),
(8, 'Narayana Groups', NULL),
(9, 'Narayana e-Techno School', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset`
--

CREATE TABLE `password_reset` (
  `code` int(11) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `generated_by` varchar(150) DEFAULT NULL,
  `hash_key` varchar(100) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_reset`
--

INSERT INTO `password_reset` (`code`, `email`, `generated_by`, `hash_key`, `timestamp`, `status`) VALUES
(3, 'gunji.sairevanth@gmail.com', 'User', '72f97fbf6d326594353468dec7566c85', '2019-03-24 22:16:55', 'CLOSED'),
(4, 'gunji.sairevanth@gmail.com', 'User', '750be3cbc046b755be3803f59b54ceb2', '2019-03-24 22:27:37', 'CLOSED'),
(6, 'gunji.sairevanth@gmail.com', 'User', 'b5a504ebcd98ec9aa9e509b01f999cc9', '2019-03-24 22:30:27', 'CLOSED'),
(7, 'gunji.sairevanth@gmail.com', 'User', '3b3315a8ef5f07d1860e24fb3f36ef51', '2019-03-24 22:32:13', 'CLOSED'),
(8, 'gunji.sairevanth@gmail.com', 'User', '926edf43c173812e6af4d740de059514', '2019-03-24 22:33:26', 'CLOSED'),
(9, 'gunji.sairevanth@gmail.com', 'User', '073aaa651e3a73e1e6a7e859086c34a5', '2019-03-24 22:33:56', 'CLOSED'),
(10, 'gunji.sairevanth@gmail.com', 'User', '96708698473e5b7e2b87f3db686bad71', '2019-03-24 22:39:33', 'CLOSED'),
(11, 'gunji.sairevanth@gmail.com', 'User', '53046045fb3e50cda261d1fc20fa2994', '2019-03-24 22:40:04', 'CLOSED'),
(12, 'gunji.sairevanth@gmail.com', 'User', 'c385d4f20c8704338f031f68e17a4cb7', '2019-03-25 05:41:32', 'CLOSED'),
(23, 'sairevanth.gunji123@gmail.com', 'User', '8cb2e6a5a3933a76ed81062ba1122d53', '2019-03-25 09:12:32', 'CLOSED'),
(26, 'sairevanth.gunji123@gmail.com', 'User', '3b65a173fbb6aad1c44547d0f7088564', '2019-03-25 09:25:31', 'CLOSED'),
(28, 'sairevanth.gunji123@gmail.com', 'User', '15ac14dd58c0c82063dd5801e5f8b27e', '2019-03-25 17:01:47', 'CLOSED'),
(29, 'raghavendradevathi33@gmail.com', 'User', '26f24c73bcdc2a0584f9adbd48c84f40', '2019-03-28 04:02:31', NULL),
(30, 'gunji.sairevanth@gmail.com', 'User', '0b60d2cba62ab4e6fbdc54e3f6dfe059', '2019-03-28 07:32:40', 'CLOSED'),
(31, 'gunji.sairevanth@gmail.com', 'User', '0402e66be8af9f1fc48a933c8b605c07', '2019-03-28 07:36:28', 'CLOSED'),
(32, 'anandkrishna30227@gmail.com', 'User', 'f1f9c1f822c8a0f59bd9e0060b408938', '2019-03-28 07:43:48', NULL),
(33, 'anandkrishna30227@gmail.com', 'User', '18dd4a3c814746431a7c1234df6b3acf', '2019-03-28 07:43:48', 'CLOSED'),
(34, 'gunji.sairevanth@gmail.com', 'User', 'e1152654910dd01051a947129ed6c5d4', '2019-03-31 19:15:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `personal`
--

CREATE TABLE `personal` (
  `email` varchar(200) NOT NULL,
  `education_previous` text,
  `preference_profiles` text,
  `interests` varchar(400) DEFAULT NULL,
  `skills` varchar(600) DEFAULT NULL,
  `job` varchar(300) DEFAULT NULL,
  `company` varchar(150) DEFAULT NULL,
  `bio` text,
  `previous_company` text,
  `town` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `higher_education` varchar(150) DEFAULT NULL,
  `higher_education_completed` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal`
--

INSERT INTO `personal` (`email`, `education_previous`, `preference_profiles`, `interests`, `skills`, `job`, `company`, `bio`, `previous_company`, `town`, `country`, `higher_education`, `higher_education_completed`) VALUES
('anandkrishna30227@gmail.com', '{KL UNIVERSITY,B.Tech,CSE,,Jun,2016,JUN,2020}', NULL, NULL, NULL, '', '', NULL, NULL, '', '', '', ''),
('gunji.sairevanth@gmail.com', '{Sri Chaitanaya Educational Committee,10,SSC,Ongole,Jun,2013,Jul,2014},{Narayana Junior College,Intermediate,BIEAP,Gollapudi,Jun,2014,Jun,2016}', 'all', NULL, 'c,Java', 'Senior Software Engineer', 'Amazon', NULL, '{AO,Narayana Groups,Nov,2016,Nov,2020,Gollapudi,IN},{Web developer,Amazon,Jul,2016,Oct,2020,Banglore,India}', 'Vijayawada', 'India', 'Ph.D', 'K L University'),
('kteja197@gmail.com', '{sri vijanana vihara ,10,CBSE,Vijayawada,Apr,2006,apr,2014}', '', NULL, '', 'Senior Software Engineer', 'amazon', NULL, '{Senior Software Enginer,Sri Chaitanya Techno School,Nov,2017,Nov,2022,Vijayawada,IN},{SE,Amazon,Nov,2016,Oct,2020,Vijayawada,IN},{AO,Narayana Groups,Nov,2016,Nov,2020,Gollapudi,IN}', 'hyderabad', 'india', NULL, NULL),
('kujwalsai549@gmail.com', '{Sri Chaitanaya Educational Committee,10,SSC,Ongole,Jun,2013,Jul,2014},{Narayana Junior College,Intermediate,BIEAP,Gollapudi,Jun,2014,Jun,2016},{K L University,B.Tech,CSE,Vijayawada,Jun,2016,Jun,2020}', 'all', NULL, 'Web ', 'Student', 'KL University', NULL, '{Senior Software Enginer,Flipkart,Nov,2017,Nov,2022,Vijayawada,IN},{SE,Amazon,Nov,2016,Oct,2020,Vijayawada,IN},{AO,Narayana Groups,Nov,2016,Nov,2020,Gollapudi,HU}', 'Hyderabad', 'India', 'M.Tech', 'K L University'),
('raghavendradevathi33@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `code` int(11) NOT NULL,
  `description` text,
  `files` text,
  `status` varchar(150) DEFAULT NULL,
  `user` varchar(300) DEFAULT NULL,
  `likes` text,
  `likes_count` int(11) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `dis_files` text,
  `posted_time` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`code`, `description`, `files`, `status`, `user`, `likes`, `likes_count`, `timestamp`, `dis_files`, `posted_time`) VALUES
(105, 'Hello', NULL, 'POSTED', 'gunji.sairevanth@gmail.com', ',1', 1, '2019-04-03 14:34:40', NULL, '2019-04-03 20:04:40'),
(106, 'Hello World', NULL, 'POSTED', 'gunji.sairevanth@gmail.com', ',1', 1, '2019-04-04 19:27:03', NULL, '2019-04-05 00:57:03'),
(107, 'Hello world', NULL, 'POSTED', 'gunji.sairevanth@gmail.com', ',1', 1, '2019-04-04 19:27:22', NULL, '2019-04-05 00:57:22'),
(108, 'Hello world', NULL, 'POSTED', 'gunji.sairevanth@gmail.com', NULL, NULL, '2019-04-04 19:27:25', NULL, '2019-04-05 00:57:25'),
(109, '', 'IGTC004442.png', 'POSTED', 'gunji.sairevanth@gmail.com', NULL, NULL, '2019-04-04 19:27:41', 'IGTC004442.png', '2019-04-05 00:57:43'),
(110, '', 'IGTC004443.png', 'POSTED', 'gunji.sairevanth@gmail.com', NULL, NULL, '2019-04-04 19:27:51', 'IGTC004443.png', '2019-04-05 00:57:52'),
(111, '', 'IGTC004441.png', 'POSTED', 'gunji.sairevanth@gmail.com', NULL, NULL, '2019-04-04 19:28:00', 'IGTC004441.png', '2019-04-05 00:58:02'),
(112, '', 'IGTC004434.png', 'POSTED', 'gunji.sairevanth@gmail.com', NULL, NULL, '2019-04-04 19:28:13', 'IGTC004434.png', '2019-04-05 00:58:15'),
(113, 'Hello world', NULL, 'POSTED', 'gunji.sairevanth@gmail.com', NULL, NULL, '2019-04-04 19:28:18', NULL, '2019-04-05 00:58:18'),
(114, 'Hello world', NULL, 'POSTED', 'gunji.sairevanth@gmail.com', NULL, NULL, '2019-04-04 19:28:21', NULL, '2019-04-05 00:58:21'),
(115, 'Hello world', NULL, 'POSTED', 'gunji.sairevanth@gmail.com', NULL, NULL, '2019-04-04 19:28:24', NULL, '2019-04-05 00:58:24'),
(116, 'Hello world', NULL, 'POSTED', 'gunji.sairevanth@gmail.com', NULL, NULL, '2019-04-04 19:28:27', NULL, '2019-04-05 00:58:27'),
(117, 'Hello world', NULL, 'POSTED', 'gunji.sairevanth@gmail.com', NULL, NULL, '2019-04-04 19:28:30', NULL, '2019-04-05 00:58:30'),
(118, '', 'CSEWebsite.mp4', 'POSTED', 'gunji.sairevanth@gmail.com', ',3', 1, '2019-04-04 19:29:03', 'CSEWebsite.mp4', '2019-04-05 00:59:05'),
(127, '123', '6265156.JPG', 'POSTED', 'gunji.sairevanth@gmail.com', NULL, NULL, '2019-04-05 17:29:19', '6265156.JPG', '2019-04-05 22:59:23'),
(128, 'Testing', 'NSSsymbol.png', 'POSTED', 'gunji.sairevanth@gmail.com', NULL, NULL, '2019-04-05 17:29:36', 'NSSsymbol.png', '2019-04-05 22:59:38'),
(130, '', 'Alumni.ppt', 'POSTED', 'gunji.sairevanth@gmail.com', NULL, NULL, '2019-04-06 03:28:55', 'Alumni.ppt', '2019-04-06 08:58:57');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `code` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `profile_name` varchar(150) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `dob` varchar(100) DEFAULT NULL,
  `father` varchar(200) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `phone` varchar(150) DEFAULT NULL,
  `profile_pic` varchar(200) DEFAULT NULL,
  `profile_header` varchar(200) DEFAULT NULL,
  `category` varchar(150) DEFAULT NULL,
  `hometown` varchar(150) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `follower` text,
  `request` text,
  `following` text,
  `request_sent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `hash_key` varchar(35) DEFAULT NULL,
  `visitors` text,
  `visitors_count` int(11) DEFAULT NULL,
  `active_status` varchar(150) DEFAULT NULL,
  `geo_location` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`code`, `name`, `profile_name`, `gender`, `dob`, `father`, `email`, `phone`, `profile_pic`, `profile_header`, `category`, `hometown`, `country`, `follower`, `request`, `following`, `request_sent`, `hash_key`, `visitors`, `visitors_count`, `active_status`, `geo_location`) VALUES
(1, 'G,Sai Revanth', 'Sai Revanth', 'Male', '23/06/1999', 'G Suresh Babu', 'gunji.sairevanth@gmail.com', '9603354001', 'profile_pics/KLAU1.jpg', 'profile_banners/KLAU1.jpg', NULL, 'Mangalagiri', 'IN', '2', '', '2,3', '', '7d7ba1f899a81c7e9db98d206529df04', ',2,14', 2, 'LOGOUT', '16.4403927,80.62217849999999'),
(2, 'Ujwal', 'Ujwal Kandi', 'MALE', '05/07/99', 'Kandi', 'kujwalsai549@gmail.com', '9704566002', 'profile_pics/KLAU2.jpeg', 'profile_banners/KLAU2.jpeg', NULL, 'Vijayawada', 'IN', '1', '14', '1', '', '64774270b397ee3546c0dbd2b7b30b89', ',1', 1, 'LOGOUT', NULL),
(3, 'krishna teja', 'krishna teja', 'male', '24/10/1998', 'apparao', 'kteja197@gmail.com', '9032652329', 'profile_pics/KLAU3.jpg', 'profile_banners/KLAU3.jpg', NULL, 'Ongole', 'india', '1', '', '1,2', '', 'eb36974ab730ddbdc3d2d8f7b23ac395', ',2,1', 2, 'LOGOUT', NULL),
(15, 'Raghavendradevathi,Devathi', 'Raghavendradevathi Devathi', 'Male', NULL, NULL, 'raghavendradevathi33@gmail.com', '949430333', 'profile_pics/default.png', 'profile_banners/default.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1a8d9ab92a65a00b3ab35d885cf84efc', NULL, NULL, NULL, NULL),
(16, 'Anand,Krishna', 'Anand Krishna', 'Male', '25/08/1999', 'subramanyam', 'anandkrishna30227@gmail.com', '7702408373', 'profile_pics/default.png', 'profile_banners/default.png', NULL, 'yerragondapalem', 'IN', NULL, NULL, NULL, NULL, '88a8b3853c82b5b2f5e76da7d3546002', NULL, NULL, 'LOGOUT', '16.4404075,80.62167339999999');

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE `programs` (
  `sno` int(11) NOT NULL,
  `program` varchar(200) DEFAULT NULL,
  `stream` varchar(300) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`sno`, `program`, `stream`, `duration`) VALUES
(1, 'B.Tech', 'Bio-Technology (BT)', 4),
(2, 'B.Tech', 'Civil Engineering (CE)', 4),
(3, 'B.Tech', 'Computer Science and Engineering (CSE)', 4),
(4, 'B.Tech', 'Electronics and Communications Engineering (ECE)', 4),
(5, 'B.Tech', 'Electronics and Computer Engineering (ECSE)', 4),
(6, 'B.Tech', 'Electrical and Electronics Engineering (EEE)', 4),
(7, 'B.Tech', 'Mechanical Engineering (ME)', 4),
(8, 'B.Arch', '', 5),
(9, 'BHM', '', 3),
(10, 'BFA', 'Film Making', 3),
(11, 'BFA', 'Animation', 2),
(12, 'B.Sc(VC)', '', 3),
(13, 'BCA', 'Cloud Technology & Information Security', 3),
(14, 'B.Com(Hons)', 'Accounting and Finance', 3),
(15, 'BBA', 'MBA', 3),
(16, 'BBA', 'LLB', 3),
(17, 'B.Pharm', '', 4),
(18, 'BA', 'IAS', 3),
(19, 'M.Com', '', 2),
(20, 'M.Tech', 'Bio Technology', 2),
(21, 'M.Tech', 'Structural Engineering', 2),
(22, 'M.Tech', 'Geo Informatics', 2),
(23, 'M.Tech', 'Constructions Technology & Management', 2),
(24, 'M.Tech', 'Computer Science & Engineering', 2),
(25, 'M.Tech', 'Digital Forensic & Cyber Security', 2),
(26, 'M.Tech', 'Machine Learning and Computing', 2),
(27, 'M.Tech', 'VLSI', 2),
(28, 'M.Tech', 'RADAR & COMMUNICATION', 2),
(29, 'M.Tech', 'ATMOSPHERIC SCIENCE', 2),
(30, 'M.Tech', 'Embedded Systems', 2),
(31, 'M.Tech', 'Power Electronics & Drives', 2),
(32, 'M.Tech', 'Power Systems', 2),
(33, 'M.Tech', 'Thermal Engineering', 2),
(34, 'M.Tech', 'ROBOTICS AND MECHATRONICS', 2),
(35, 'M.Tech', 'MACHINE DESIGN', 2),
(36, 'MBA', 'HR', 2),
(37, 'MBA', 'Marketing', 2),
(38, 'MBA', 'Finance', 2),
(39, 'MBA', 'Digital Marketing', 2),
(40, 'MBA', 'Banking and Financial service', 2),
(41, 'MBA', 'Business Analytics', 2),
(42, 'MA', 'English ', 2),
(43, 'M.Sc', 'Chemistry', 2),
(44, 'M.Sc', 'Maths', 2),
(45, 'M.Sc', 'Physics', 2),
(46, 'LLB', '', 3),
(47, 'LLM', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `name` varchar(300) DEFAULT NULL,
  `description` text,
  `enrichment_fund` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `query`
--

CREATE TABLE `query` (
  `code` int(11) NOT NULL,
  `problem` text,
  `status` varchar(100) DEFAULT NULL,
  `type` varchar(150) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `hash` varchar(100) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `reply_message` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `query`
--

INSERT INTO `query` (`code`, `problem`, `status`, `type`, `email`, `mail`, `hash`, `timestamp`, `reply_message`) VALUES
(15, 'sstrhhstrhtsr', 'DELETE', 'General', 'gunji.sairevanth@gmail.com', 'SEND', 'c40906e55eb41a84f4ec960a687654e3', '2019-03-31 14:32:32', NULL),
(16, 'Hello World', 'DELETE', 'General', 'gunji.sairevanth@gmail.com', 'SEND', 'b577982da94368e7a1e22fc44f78980f', '2019-03-31 14:32:32', ''),
(17, 'Hello Testing', 'CLOSED', 'General', 'gunji.sairevanth@gmail.com', 'SEND', '2697141b17ecd13c73b03da37b137881', '2019-03-31 14:32:32', 'Please send the screen shorts '),
(18, 'Hello Testing', '', 'General', 'gunji.sairevanth@gmail.com', 'SEND', '0718c3d2625138f6b31276948e4ef937', '2019-03-31 14:32:32', ''),
(19, 'Testing', NULL, 'General', 'gunji.sairevanth@gmail.com', 'SEND', 'd321e8dc8ed297a33aa1d64353111a6c', '2019-04-05 01:51:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `success_stories`
--

CREATE TABLE `success_stories` (
  `code` int(11) NOT NULL,
  `title` text,
  `content` text,
  `hash_key` varchar(200) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ext` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `success_stories`
--

INSERT INTO `success_stories` (`code`, `title`, `content`, `hash_key`, `timestamp`, `ext`) VALUES
(34, 'Dr.K.V .D kiran with registered no y5mtcs217.', 'These awards have been presented at the 50th Golden Jubilee Annual Convention of CSI 2015 held at Bharati Vidyapeeth University, New Delhi from 02-12-2015 to 05-12-2015. The awards were presented by CSI President Prof.Bipin V. Mehta to our faculty on 05-12-15 on the last day of the Golden Jubilee Annual Convention.', '064a325ba20a3edf0597a37f69ae51b6', '2019-03-20 21:47:06', 'jpg'),
(35, 'M.srikanth with registered no 98EEE250 .', 'Achieved national award for outstanding excellence and remarable achievements in the field of TEACHING ,RESEARCH & PUBLICATIONS on 30th may 2018 at chennai.', '570177fbfaf8eca66da209f398ef860a', '2019-03-20 21:48:10', 'jpg'),
(37, 'KL Heartily Congratulates the Alumni on achieving 1st rank in 2018 APPSC Group-1', 'The topper of Group 1 services Venkata Ramana is the youngest son of the couple Akula Sreeramulu and Lakshmi Narasamma and is hailing from a humble agricultural background. He scored 460.5 marks in total - 408 marks in Mains examination and 52.5 in Interview.', '4d33ea5d74f3a6ab068d354e3b735922', '2019-03-20 22:14:04', 'jpg'),
(38, 'Patent for our Alumni', 'Our Alumni Dr PVRD Prasada Rao of 1990-94 Batch from CSE Dept filed a patent against his research work in the area of Data Science and it is accepted and published on 25/06/2018.', '7a473a6ddfae4176cdd725e8aa3c154b', '2019-03-20 22:14:29', 'jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user_security`
--

CREATE TABLE `user_security` (
  `email` varchar(150) DEFAULT NULL,
  `hash_key` varchar(100) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `code` int(11) NOT NULL,
  `status` varchar(150) DEFAULT NULL,
  `ip` varchar(100) DEFAULT NULL,
  `broswer` varchar(100) DEFAULT NULL,
  `mode` varchar(100) DEFAULT NULL,
  `id` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_security`
--

INSERT INTO `user_security` (`email`, `hash_key`, `timestamp`, `code`, `status`, `ip`, `broswer`, `mode`, `id`) VALUES
('gunji.sairevanth@gmail.com', '1474b7d01a5603c8dc4a191f76787f29', '2019-03-20 23:26:31', 108, 'CLOSED', '175.101.105.137', 'Opera', NULL, NULL),
('kujwalsai549@gmail.com', 'dd879e696284a1277ae10f31280e217d', '2019-03-20 23:30:32', 109, NULL, '175.101.105.137', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', '90f7914bc92472905764026f415f5308', '2019-03-21 10:00:44', 110, 'CLOSED', '103.206.105.69', 'Opera', NULL, NULL),
('kujwalsai549@gmail.com', '213bb803e7fb0b09901ee04e37ae2186', '2019-03-21 10:18:45', 111, NULL, '103.206.105.69', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', '247188c8418202640e93175b4b0454d8', '2019-03-24 05:23:37', 112, 'CLOSED', '175.101.105.137', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', 'c9bc0562f7bc7f576f84c89abd9fb963', '2019-03-24 14:54:42', 113, 'CLOSED', '175.101.105.137', 'Opera', NULL, NULL),
('kujwalsai549@gmail.com', '77978788aa94c46b38edc43798559b0a', '2019-03-24 17:34:27', 114, NULL, '175.101.105.137', 'Opera', NULL, NULL),
('kujwalsai549@gmail.com', '60258ad2550af846f94c41c73e715d50', '2019-03-24 18:47:14', 115, 'CLOSED', '175.101.105.137', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', '12072316a99deace8549b6aa8cba36ae', '2019-03-24 22:26:33', 116, 'CLOSED', '175.101.105.137', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', 'cd2fad58b16da554bdb9d4833e180944', '2019-03-24 22:40:29', 117, 'CLOSED', '175.101.105.137', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', '5e444d8a9c987c385a98afaeaade6567', '2019-03-25 05:42:18', 118, 'CLOSED', '103.206.105.69', 'Opera', NULL, NULL),
('sairevanth.gunji123@gmail.com', 'e05d6a5e6b9eff8092fc3ada511ceeb8', '2019-03-25 09:26:38', 119, NULL, '103.206.105.69', 'Opera', NULL, NULL),
('sairevanth.gunji123@gmail.com', '708ee23a455c40657410366fbb11847c', '2019-03-25 16:10:02', 120, 'CLOSED', '175.101.105.137', 'Opera', NULL, NULL),
('sairevanth.gunji123@gmail.com', '45fbc8cb102825be096df4143a5fa81f', '2019-03-25 17:02:31', 121, 'CLOSED', '175.101.105.137', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', 'e72fbb9f56b3e1b17942045034e9f5cd', '2019-03-26 06:19:07', 122, 'CLOSED', '103.206.105.69', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', 'ea1732226e67e049462b4c743b3a5a94', '2019-03-26 06:32:33', 123, 'CLOSED', '103.206.105.69', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', '1a4636fc795b8600d7c00abd805b138e', '2019-03-26 12:45:23', 124, 'CLOSED', '103.206.105.69', 'isBlink', NULL, NULL),
('gunji.sairevanth@gmail.com', '1b1b2f8acc70295fca530bc68cb4fba4', '2019-03-27 06:42:03', 125, 'CLOSED', '103.206.105.69', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', '0acfc7da52040f7202fd965238d2f39b', '2019-03-27 06:44:18', 126, 'CLOSED', '103.206.105.69', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', '002e82922885badaf84abdde770b8070', '2019-03-27 07:48:41', 127, 'CLOSED', '103.206.105.69', 'Opera', NULL, NULL),
('sairevanth.gunji123@gmail.com', 'f68d13cc2db8ef4259170edb9f7514f4', '2019-03-27 09:03:38', 128, 'CLOSED', '103.206.105.69', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', '58f2fb197c60d6df74c3c49b52cc12aa', '2019-03-28 05:04:48', 129, 'CLOSED', '103.206.105.71', 'Firefox', NULL, NULL),
('gunji.sairevanth@gmail.com', 'ea02a3f130c2c11a8d92df7c28fd3ece', '2019-03-28 05:27:19', 130, NULL, '103.206.105.69', 'Firefox', NULL, NULL),
('gunji.sairevanth@gmail.com', '43aacb1f8613aa282547c74260fa83b3', '2019-03-28 07:28:21', 131, 'CLOSED', '103.206.105.69', 'Firefox', NULL, NULL),
('gunji.sairevanth@gmail.com', '139b8300c9ce97757bdd9bc7c84def53', '2019-03-28 07:33:49', 132, 'CLOSED', '103.206.105.69', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', '46e83973de4cbcbf112ecc5f44b3c61c', '2019-03-28 07:34:01', 133, 'CLOSED', '103.206.105.69', 'Firefox', NULL, NULL),
('gunji.sairevanth@gmail.com', '3f1fe87529245be4426f0753846e2c64', '2019-03-28 07:38:09', 134, 'CLOSED', '103.206.105.69', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', '8c4a4f9fff4d6a00711831dc001e51b0', '2019-03-28 07:40:00', 135, 'CLOSED', '103.206.105.69', 'Opera', NULL, NULL),
('anandkrishna30227@gmail.com', '7b56d4ed573e73943b8a58ab46c8cb08', '2019-03-28 07:50:18', 136, 'CLOSED', '103.206.105.69', 'Opera', NULL, NULL),
('anandkrishna30227@gmail.com', 'f22b786a07c5df8325b8a399040c73f2', '2019-03-28 07:51:30', 137, 'CLOSED', '103.206.105.69', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', '4271190bcfe0cc147172accfe56b064e', '2019-03-29 02:10:06', 138, 'CLOSED', '175.101.105.137', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', 'cbbf061c6f2d441f3b04a5ac6778e92c', '2019-03-29 04:41:42', 139, 'CLOSED', '103.206.105.69', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', 'a3a2b8173b295e65ea2ebd313d7c70cd', '2019-03-29 04:44:40', 140, 'CLOSED', '::1', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', '7fdef8b672570973851faf9a303dfbc3', '2019-03-29 03:31:02', 142, 'CLOSED', '::1', 'Opera', 'Microsoft', 'e7fb39ec-b0c1-4574-86bd-c46ee223c20e'),
('gunji.sairevanth@gmail.com', '795c122002fde8583f3720818f4b5d30', '2019-03-29 03:31:38', 143, 'CLOSED', '103.206.105.69', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', 'c8831cc1ec6ecac86cac9ca173b0fa9d', '2019-03-29 03:46:33', 144, NULL, '::1', 'Opera', 'Microsoft', 'e7fb39ec-b0c1-4574-86bd-c46ee223c20e'),
('gunji.sairevanth@gmail.com', '49676dc8a040bc39a163cc6560b99849', '2019-03-29 03:46:33', 145, 'CLOSED', '::1', 'Opera', 'Microsoft', 'e7fb39ec-b0c1-4574-86bd-c46ee223c20e'),
('gunji.sairevanth@gmail.com', '57aa4290c3ff05a34d4a87232692b78b', '2019-03-29 12:21:39', 146, NULL, '::1', 'Opera', 'Microsoft', 'e7fb39ec-b0c1-4574-86bd-c46ee223c20e'),
('gunji.sairevanth@gmail.com', 'c4a0f349e0f80cae6c27dd8d4156e6d8', '2019-03-29 12:21:40', 147, 'CLOSED', '::1', 'Opera', 'Microsoft', 'e7fb39ec-b0c1-4574-86bd-c46ee223c20e'),
('gunji.sairevanth@gmail.com', '6a2cc93c08ff8da3274ccd9eaedf328c', '2019-03-29 19:01:04', 148, 'CLOSED', '::1', 'Opera', 'Google', '111249328392737698211'),
('gunji.sairevanth@gmail.com', '712410b5948e615fceba7c6df5386d92', '2019-03-29 19:01:42', 149, NULL, '::1', 'Opera', 'Google', '111249328392737698211'),
('gunji.sairevanth@gmail.com', 'b5df4a825a8fd4523a8cdc3ccdc10c23', '2019-03-29 19:09:27', 150, NULL, '::1', 'Opera', 'Google', '111249328392737698211'),
('gunji.sairevanth@gmail.com', 'acaf9445d59b949c9eb34edbca6510a6', '2019-03-29 19:10:22', 151, 'CLOSED', '::1', 'Opera', 'Google', '111249328392737698211'),
('sairevanth.gunji123@gmail.com', 'b7facd8bf3b2b7281ffd5a2375affd57', '2019-03-30 09:52:35', 152, 'CLOSED', '::1', 'Chrome', 'Google', '100341699180095834609'),
('sairevanth.gunji123@gmail.com', '3972443669d8435fae4ebf26d7a5cf1c', '2019-03-30 10:15:22', 153, 'CLOSED', '::1', 'Chrome', 'Google', '100341699180095834609'),
('gunji.sairevanth@gmail.com', '7366c1b424bfea522cfc4e70ac857092', '2019-03-30 15:24:12', 154, 'CLOSED', '::1', 'Opera', 'Google', '111249328392737698211'),
('gunji.sairevanth@gmail.com', '4f282a209613846c5b28d9cca64bbdaa', '2019-03-31 19:44:01', 155, 'CLOSED', '175.101.105.137', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', '7d37aceacc077e1f1ec29bae00439a96', '2019-03-31 19:44:40', 156, NULL, '::1', 'Opera', 'Microsoft', 'e7fb39ec-b0c1-4574-86bd-c46ee223c20e'),
('gunji.sairevanth@gmail.com', '60aa9946ce0b71ca4ef45d3cb6353a48', '2019-03-31 19:44:41', 157, 'CLOSED', '::1', 'Opera', 'Microsoft', 'e7fb39ec-b0c1-4574-86bd-c46ee223c20e'),
('gunji.sairevanth@gmail.com', 'c71725a2e035176cd68f52e6e4dbc8c9', '2019-04-02 11:04:49', 158, NULL, '::1', 'Chrome', 'Microsoft', 'e7fb39ec-b0c1-4574-86bd-c46ee223c20e'),
('gunji.sairevanth@gmail.com', 'd1faf904ca1d71b6dd70756410706874', '2019-04-02 11:04:49', 159, NULL, '::1', 'Chrome', 'Microsoft', 'e7fb39ec-b0c1-4574-86bd-c46ee223c20e'),
('gunji.sairevanth@gmail.com', 'e435d922d012f5b8359345fd57f8d0d5', '2019-04-02 11:04:49', 160, 'CLOSED', '::1', 'Chrome', 'Microsoft', 'e7fb39ec-b0c1-4574-86bd-c46ee223c20e'),
('gunji.sairevanth@gmail.com', '1bb406a894b7ab1727489ff0b54a8529', '2019-04-03 14:34:28', 161, 'CLOSED', '::1', 'Opera', 'Google', '111249328392737698211'),
('gunji.sairevanth@gmail.com', 'c96ca950b5ccac5b068b96fd60706fb1', '2019-04-03 14:35:35', 162, 'CLOSED', '::1', 'Opera', 'Google', '111249328392737698211'),
('kteja197@gmail.com', '0de30ee17569e884facef1f276400d00', '2019-04-05 01:56:13', 163, NULL, '103.206.105.69', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', '3c6bfbbdec2622e6838e62257ee4d9a8', '2019-04-05 01:58:33', 164, 'CLOSED', '::1', 'Opera', 'Google', '111249328392737698211'),
('gunji.sairevanth@gmail.com', '7dea1e92586f1ec907ce38c01207f548', '2019-04-05 17:27:16', 165, NULL, '::1', 'Opera', 'Google', '111249328392737698211'),
('kteja197@gmail.com', '0d0d74b9850c2eeee7709f905173d457', '2019-04-05 18:47:12', 166, NULL, '175.101.104.28', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', '42eed6cccca80defb8f383394df4b4bc', '2019-04-06 03:24:01', 167, 'CLOSED', '175.101.104.28', 'Opera', NULL, NULL),
('gunji.sairevanth@gmail.com', '90502350d953983623880cdea8d8163e', '2019-04-06 06:22:02', 168, 'CLOSED', '175.101.104.28', 'Opera', NULL, NULL),
('kteja197@gmail.com', 'd67994cc8bdcfb078bfbbef35505b5e1', '2019-04-06 06:31:05', 169, 'CLOSED', '175.101.104.28', 'Opera', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academics`
--
ALTER TABLE `academics`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `auth`
--
ALTER TABLE `auth`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `chat_messages`
--
ALTER TABLE `chat_messages`
  ADD PRIMARY KEY (`chat_message_id`);

--
-- Indexes for table `event_name`
--
ALTER TABLE `event_name`
  ADD PRIMARY KEY (`e_code`);

--
-- Indexes for table `event_photos`
--
ALTER TABLE `event_photos`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `gallery_photos`
--
ALTER TABLE `gallery_photos`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `news_letters`
--
ALTER TABLE `news_letters`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `news_letters_files`
--
ALTER TABLE `news_letters_files`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `offer`
--
ALTER TABLE `offer`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `offer_files`
--
ALTER TABLE `offer_files`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `offer_response`
--
ALTER TABLE `offer_response`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `organization`
--
ALTER TABLE `organization`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `password_reset`
--
ALTER TABLE `password_reset`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`sno`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `query`
--
ALTER TABLE `query`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `success_stories`
--
ALTER TABLE `success_stories`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `user_security`
--
ALTER TABLE `user_security`
  ADD PRIMARY KEY (`code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat_messages`
--
ALTER TABLE `chat_messages`
  MODIFY `chat_message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=320;

--
-- AUTO_INCREMENT for table `event_name`
--
ALTER TABLE `event_name`
  MODIFY `e_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `event_photos`
--
ALTER TABLE `event_photos`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `gallery_photos`
--
ALTER TABLE `gallery_photos`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `news_letters`
--
ALTER TABLE `news_letters`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `news_letters_files`
--
ALTER TABLE `news_letters_files`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `offer`
--
ALTER TABLE `offer`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `offer_files`
--
ALTER TABLE `offer_files`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `offer_response`
--
ALTER TABLE `offer_response`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `organization`
--
ALTER TABLE `organization`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `password_reset`
--
ALTER TABLE `password_reset`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `sno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `query`
--
ALTER TABLE `query`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `success_stories`
--
ALTER TABLE `success_stories`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `user_security`
--
ALTER TABLE `user_security`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
