<html>
<head>
<title>Forgot Password | KL Alumni Association</title>
<link href="https://fonts.googleapis.com/css?family=Lato|Noto+Sans" rel="stylesheet">
<style>
body
{
	margin: 0px;
	background-color: #f7f7f7;
}
.nav
{
	width: 100%;
	height: 70px;
	background-color: white;
	box-shadow: 0px 2px 8px 0px #ebebeb;
}
.nav_icon
{
	list-style-type: none;
}
.nav_icon li
{
	float: right;
	padding-top: 6px;
	font-family: 'Lato', sans-serif;
	padding-bottom: 5px;
	margin-left: 20px;
	cursor: pointer;
	color: black;
	margin-right: 30px;
	transition: 0.2s;
}
.nav_icon li:hover
{
	transition: 0.2s;
	border-bottom: 2px solid #b5b5b5;
}
.login
{
	width: 380px;
	height: 300px;
	margin: auto;
	margin-top: 100px;
	box-shadow: 0px 0px 1px 0px #8d8d8d;
	background-color: white;
}
input:focus,button:focus
{
	outline: none;
}
.signin_button
{
	width: 80%;
	font-family: arial;
	color: white;
	margin: 10px 40px 0px 40px;
	background-color: #cc3333;
	border: none;
	font-size: 18px;
	padding: 10px;
	cursor:pointer;
	
}
.input_fields_style
{
	width: 100%;
	margin-top: 3px;
	font-family: 'Lato', sans-serif;
	padding: 10px 5px 10px 5px;
	border-radius: 3px;
	border: 1px solid #bdbdbd;
	background-color: #fefefe;
}
*:focus
{
	outline: none;
}
</style>
<link href="https://fonts.googleapis.com/css?family=Lato|Roboto" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
<div class="nav">
<a href="index.php">
<img src="imags/klu.png" style="height: 50px; width: auto; margin-top: 10px; margin-left: 10px; float: left;"/>
<p style="margin: 0px; color: black; font-family: arial; padding-top: 38px; font-size: 18px; float:left;">Alumni Association</p>
</a>
<div style="width: auto; height: 70px;  float: right; margin: 0px;">
<ul class="nav_icon">
<a href="register.php"><li>REGISTRATION</li></a>
<a href="careers.php"><li>Carrers</li></a>
<a href="events.php"><li>Events & Calendar </li></a>
<a href="index.php"><li>Home</li></a>
</ul>
</div>
</div>

<div class="login">

<p style="text-align: left; padding-left: 40px; font-family: 'Lato', sans-serif; margin: 0px; font-size: 25px; letter-spacing: 1px; padding-top: 30px;"><strong>Forgot Password</strong></p>
<div style="width: 100%; height: 10px;">
<p id="not_exit" style="text-align: center; font-family: 'Roboto', cursive; font-size: 12px;  color: red; padding-top: 0px;"></p>
</div>
<div style="width: 100%; height: auto; margin-top: 10px; text-align: center;">
<form action="forgot.php"  method="post">
<fieldset style="margin: 0px 30px 0px 30px; text-align: left; border: none;">
<legend><p style="font-family: 'Lato', sans-serif; font-size: 13px; margin: 0px;"><b>Username</b></p></legend>
<input name="email" class="input_fields_style" placeholder="e.g., &nbsp; alumni@kluniversity.in"></input>
</fieldset>
<div style="width: 100%; height: 30px; ">
</div>
<button name="signin" id="signin" class="signin_button">Send password reset email</button>
<script src="js/get_ip.js"></script>
<script type="application/javascript" src="https://api.ipify.org?format=jsonp&callback=getIP"></script>
</form>

</div>
</div>
<p style="text-align: center; font-family: 'Noto Sans', sans-serif; font-size: 13px; color: #494949;">Copyright &copy;  KL University</p>
<?php
require("js/php/conn.php");
if(isset($_REQUEST['signin']))
{
	$email=$_REQUEST['email'];
	$email=mysqli_real_escape_string($conn,$email);
	$check=mysqli_query($conn,"select email from profile where email='$email';");
	if(mysqli_num_rows($check)==1)
	{
		$check1=mysqli_query($conn,"select email,hash_key from password_reset where email='$email' and status is NULL;");
		if(mysqli_num_rows($check1)==1)
		{
			$check1=mysqli_fetch_array($check1);
			$hash=$check1['hash_key'];
			
		}
		else
		{
			$hash=md5(uniqid($email,true));
			mysqli_query($conn,"insert into password_reset (email,generated_by,hash_key) values ('$email','User','$hash');");
		}
		
		include('cms/mailer/reset.php');	
		echo '
		<script>
		document.getElementById("not_exit").innerHTML="Verification Link sent to '.$email.'";
		document.getElementById("not_exit").style.color="green";
		</script>';
	}
	else
	{
		echo '
		<script>
		document.getElementById("not_exit").innerHTML="Account Not Found";
		</script>';
	}
	echo '
		<script>
	setTimeout(function () {
   window.location.href= "login.php"; // the redirect goes here
},3000);
		</script>';
}
mysqli_close($conn);
?>
</body>
</html>
