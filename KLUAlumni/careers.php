<html>
<head>
<title></title>
<link href="https://fonts.googleapis.com/css?family=Lato|Roboto" rel="stylesheet">
<style>
body
{
margin: 0px;
}
.icon_dis
{
	width: 100%;
	height: 70px;
	max-width: 1800px;
	margin: auto;
}
.main_icon
{
	width: 400px;
	height: 100%;
}
.main_icon img
{
	height: 80%;
	width: auto;
	float: left;
	margin-top: 6px;
	margin-left: 10px;
}
.main_icon p
{
	font-size: 20px;
	padding-top: 35px;
	font-family: 'Lato', sans-serif;
}
.nav_images
{
	width: 100%;
	height: 50px;
	background-color: red;
}
.events_display_box
{
	width: 450px;
	height: 130px;
	cursor: pointer;
	margin: 10px 10px 10px 10px;
	display: inline-block;
	border-radius: 10px;
	position: relative;
	background-color: white;
	overflow: hidden;
	transition: 0.5;
	box-shadow: 0px 0px 5px 0px #d6d6d6;
}
.events_display_box:hover
{
	box-shadow: 0px 0px 1px 0px #d6d6d6;
	transition: 0.5;
}
.dropdownmenu
{
	width: 100%;
	height: 40px;
	z-index: 10;
	background-color: #A81E24;
}
@charset "UTF-8";
.navigation {
  height: 70px;
}

.brand {
  position: absolute;
  padding-left: 20px;
  float: left;
  line-height: 70px;
  text-transform: uppercase;
  font-size: 1.4em;
}
.brand a,
.brand a:visited {
  color: #ffffff;
  text-decoration: none;
}

.nav-container {
  max-width: 1000px;
  margin: 0 auto;
}

nav {
  float: right;
}
nav ul {
  list-style: none;
  margin: 0;
  padding: 0;
}
nav ul li {
  float: left;
  font-family: 'Roboto', sans-serif;
  font-size: 15px;
  position: relative;
}
nav ul li a,
nav ul li a:visited {
  display: block;
  padding: 0 20px;
  line-height: 40px;
  background: #A81E24;
  color: #ffffff;
  text-decoration: none;
}
nav ul li a:hover,
nav ul li a:visited:hover {
  background: #bd3339;
  color: #ffffff;
}
nav ul li a:not(:only-child):after,
nav ul li a:visited:not(:only-child):after {
  padding-left: 4px;
  content: " ▾";
}
nav ul li ul li {
  min-width: 190px;
}
nav ul li ul li a {
  padding: 8px;
  font-size: 14px;
  line-height: 20px;
}

.nav-dropdown {
  position: absolute;
  display: none;
  z-index: 1;
  box-shadow: 0 3px 12px rgba(0, 0, 0, 0.15);
}

.nav-mobile {
  display: none;
  position: absolute;
  top: 0;
  right: 0;
  background: #262626;
  height: 70px;
  width: 70px;
}

@media only screen and (max-width: 798px) {
  .nav-mobile {
    display: block;
  }

  nav {
    width: 100%;
    padding: 70px 0 15px;
  }
  nav ul {
    display: none;
  }
  nav ul li {
    float: none;
  }
  nav ul li a {
    padding: 15px;
    line-height: 20px;
  }
  nav ul li ul li a {
    padding-left: 30px;
  }

  .nav-dropdown {
    position: static;
  }
}
@media screen and (min-width: 799px) {
  .nav-list {
    display: block !important;
  }
}
#nav-toggle {
  position: absolute;
  left: 18px;
  top: 22px;
  cursor: pointer;
  padding: 10px 35px 16px 0px;
}
#nav-toggle span,
#nav-toggle span:before,
#nav-toggle span:after {
  cursor: pointer;
  border-radius: 1px;
  height: 5px;
  width: 35px;
  background: #ffffff;
  position: absolute;
  display: block;
  content: "";
  transition: all 300ms ease-in-out;
}
#nav-toggle span:before {
  top: -10px;
}
#nav-toggle span:after {
  bottom: -10px;
}
#nav-toggle.active span {
  background-color: transparent;
}
#nav-toggle.active span:before, #nav-toggle.active span:after {
  top: 0;
}
#nav-toggle.active span:before {
  transform: rotate(45deg);
}
#nav-toggle.active span:after {
  transform: rotate(-45deg);
}
#main_content_display
{
	width: 100%;
	height: auto;
	position: relative;
}
#main_content_filter
{
	width: 350px;
	height: 100%; 
	float: left;
	position: absolute;
}
#main_c_content_display
{
	width: 100%;
	height: 100%; 
	margin-left: 350px;
	float: left;
	overflow-y:scroll;
	position: absolute;
	background-color: #f2f2f2;
}
.container {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 20px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 20px;
  width: 20px;
  border: 1.5px solid #c4c4c4;
  border-radius: 5px;
  background-color: white;
}



.container input:checked ~ .checkmark {
  background-color: #A81E24;
  border: 1.5px solid #A81E24;
}

.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
  display: block;
}

.container .checkmark:after {
  left: 6px;
  top: 2px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
.apply
{
	position: absolute;
	bottom: 15px;
	right: 30px;
	background-color: #A81E24;
	color: white;
	padding: 5px 15px 5px 15px;
	cursor: pointer;4
	font-size: 15px;
	transition: 0.3s;
	font-family: 'Open Sans', sans-serif;
	border: 1px solid #A81E24;
}
.apply:hover
{
	background-color: white;
	color: #A81E24;
	transition: 0.3s;
}
.Info
{
	position: absolute;
	bottom: 15px;
	right: 120px;
	background-color: white;
	color: #A81E24;
	padding: 5px 15px 5px 15px;
	cursor: pointer;4
	font-size: 15px;
	transition: 0.3s;
	font-family: 'Open Sans', sans-serif;
	border: 1px solid #A81E24;
}
</style>
<link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <script src="js/index_dropdown_menu.js"></script>
</head>
<body>
<div class="icon_dis">
<div class="main_icon" style="cursor: pointer;" onclick="window.location='index.php'">
<img src="icons/klu.png" />
<strong><p>Alumni Association</p></strong>
<div class="main_icon" style="float: rig">
</div>
</div>
</div>

<div class="dropdownmenu">
  <nav>
      <div class="nav-mobile"><a id="nav-toggle" href="#!"><span></span></a></div>
      <ul class="nav-list" >
        <li>
          <a href="#" style="text-decoration: none;">Carrers</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="careers.php" style="text-decoration: none;">Internship</a>
            </li>
            <li>
              <a href="careers.php" style="text-decoration: none;">Jobs</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="advisoryboard.php" style="text-decoration: none;">Board Members</a>
        </li>
        <li>
          <a href="#!" style="text-decoration: none;">Events</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="events.php" style="text-decoration: none;">Alumni Event Calender</a>
            </li>
			<li><a href="events.php" style="text-decoration: none;">All Events</a></li>
			<li>
			  <a href="https://kluniversity.in/site/acadcal.htm" style="text-decoration: none;">Academic Calender</a></li>
          </ul>
        </li>
		 <li>
              <a href="suess_stories.php" style="text-decoration: none;">Alumni Stories</a>
            </li>
            <li>
              <a href="gallery.php" style="text-decoration: none;">Gallery</a>
            </li>
         <li>
              <a href="updates.php" style="text-decoration: none;">News & Updates</a>
            </li>
        <li>
          <a href="aboutus.html" style="text-decoration: none;">About us</a>
        </li>
      </ul>
    </nav>
</div>

<div id="main_content_display">

<div id="main_c_content_display">



<?php
require_once('js/php/conn.php');
$get_data=mysqli_query($conn,"select code,jobname,minimumqualification,organization_name,joblocation,timestamp,hash,degree,jobtype from offer where status='APPROVED';");
while($get_c=mysqli_fetch_array($get_data))
{
	$offer_code=$get_c[0];
	$job_name=$get_c[1];
	$minimumqualification=$get_c[2];
	$organization_name=$get_c[3];
	$joblocation=$get_c[4];
	$timestamp=$get_c[5];
	$hash=$get_c[6];
	$degree=$get_c[7].' '.$get_c[8];
	$get_file=mysqli_query($conn,"select * from offer_files where offer_code='$offer_code';");
	$get_file=mysqli_fetch_array($get_file);
	$file_name=$get_file[0];
	$minimumqualification=explode("\n",$minimumqualification);
	echo '
	
	<div class="'.$degree.'" style="width: 95%; position: relative; margin: auto; cursor: pointer; margin-top: 20px; height: auto; border-radius: 10px; background-color: white;"> 
<p style="margin: 0px; color: #606060; font-family: '."'Roboto'".', sans-serif; font-size: 20px; padding-top: 20px; padding-left: 20px;">'.$job_name.'</p>
<div style="width: 100%; height: auto;">
<ul style="margin-top: 20px; margin-bottom: 30px;">';
for($i=0;$i<sizeof($minimumqualification);$i++)
{
	if($minimumqualification[$i]=='')
	{
		continue;
	}
	else
	{
	echo '
	<li style="font-family: '."'Roboto'".', sans-serif; font-size: 13px;">'.$minimumqualification[$i].'</li>
	';
	}
}

echo '</ul>
</div>
<div style="width: 100%; height: 20px;  margin-bottom: 10px;">
<table style="margin-top: 15px;">
<tr>
<td><img src="company.png" style="width: auto; height: 15px; margin-left: 20px; margin-right: 5px;"/></td>
<td><p style="margin: 0px; font-family: '."'Roboto'".', sans-serif; font-size: 13px;">'.$organization_name.'</p></td>
<td><img src="location.png" style="width: auto; height: 15px; margin-left: 40px; margin-right: 5px;"/></td>
<td><p style="margin: 0px; font-family: '."'Roboto'".', sans-serif; font-size: 13px;">'.$joblocation.'</p></td>
</tr>
</table>
</div>
<div style="width: 100%; height: 50px; position: relative; ">
<p style="position: absolute; font-family: '."'Roboto'".', sans-serif; color: #606060; bottom: 20px; left: 20px; margin: 0px;">1 Hour</p>
<a href="o_attachment_download.php?code='.$offer_code.'"><button class="Info">More</button></a>
<a href="offer_response.php?h='.$hash.'"><button class="apply">Apply</button></a>
</div>
</div>
	
	';
	
}
?>






</div>




<div id="main_content_filter">
<p style="font-family: 'Roboto', sans-serif; font-size: 15px;  color: #545454; margin: 0px; padding-top: 10px; padding-left: 10px;">Job types</p>
<div style="width: 100%; height: auto; margin-top: 10px; text-align: center;">
<label class="container" style="display: inline-block; margin: 20px 30px 0px 30px;"><p style="margin: 0px; padding-top: 3px; font-family: 'Roboto', sans-serif; font-size: 15px; ">Full-Time</p>
  <input onclick="check('full-time');" type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container" style="display: inline-block; margin: 20px 30px 0px 30px;"><p style="margin: 0px;font-family: 'Roboto', sans-serif;padding-top: 3px; font-size: 15px;">Part-Time</p>
  <input onclick="check('part-time');" type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container" style="display: inline-block; margin: 20px 45px 0px 30px;"><p style="margin: 0px; padding-top: 3px; font-family: 'Roboto', sans-serif; font-size: 15px; ">Intern</p>
  <input onclick="check('intern');" type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container" style="display: inline-block; margin: 20px 25px 0px 40px;"><p style="margin: 0px; font-family: 'Roboto', sans-serif;padding-top: 3px; font-size: 15px;">Temporary</p>
  <input onclick="check('temporary');" type="checkbox">
  <span class="checkmark"></span>
</label>
</div>
<p style="font-family: 'Roboto', sans-serif; font-size: 15px;  color: #545454; margin: 0px; padding-top: 10px; padding-left: 10px;">Degree</p>
<table style="width: 100%; margin-top: 20px;">
<tr>
<td>
<label class="container" style="margin-left: 30px;"><p style="margin: 0px; padding-top: 3px; font-family: 'Roboto', sans-serif; font-size: 15px; ">Associate</p>
  <input onclick="check('associate');" type="checkbox">
  <span class="checkmark"></span>
</label>
</td>
<td>
<label class="container"><p style="margin: 0px; padding-top: 3px; font-family: 'Roboto', sans-serif; font-size: 15px; ">Bachelor's</p>
  <input onclick="check('bachelors');" type="checkbox">
  <span class="checkmark"></span>
</label>
</td>
</tr>
<tr>
<td>
<label class="container" style="margin-left: 30px;"><p style="margin: 0px; padding-top: 3px; font-family: 'Roboto', sans-serif; font-size: 15px; ">Master's</p>
  <input onclick="check('masters');"  type="checkbox">
  <span class="checkmark"></span>
</label>
</td>
<td>
<label class="container"><p style="margin: 0px; padding-top: 3px; font-family: 'Roboto', sans-serif; font-size: 15px; ">Ph.D.</p>
  <input onclick="check('ph.d');"  type="checkbox">
  <span class="checkmark"></span>
</label>
</td>
</tr>
<tr>
<td>
<label class="container" style="margin-left: 30px;"><p style="margin: 0px; padding-top: 3px; font-family: 'Roboto', sans-serif; font-size: 15px; ">Pursuing Degree</p>
  <input onclick="check('pursuingdegree');"  type="checkbox">
  <span class="checkmark"></span>
</label>
</td>
<td>
</td>
</tr>
</table>
</div>
</div>

<script>
var width=window.innerWidth;
var height=window.innerHeight;
document.getElementById('main_content_display').style.height=height-110;
document.getElementById('main_c_content_display').style.width=width-370;
$(window).on('resize', function(){
   if($(this).height() != height || $(this).width() != width){
      height = $(this).height();
	  width = $(this).width();
document.getElementById('main_content_display').style.height=height-110;
document.getElementById('main_c_content_display').style.width=width-350;
   }
});
var display=[];
var total_array=['full-time','part-time','intern','temporary','associate','bachelors','masters','ph.d','pursuingdegree'];
function check(a)
{
	var c=0;
	var k=0;
	for(var i=0;i<display.length;i++)
	{
		if(display[i]==a)
		{
			c=1;
			k=i;
		}
	}
	if(c==1)
	{
		for(var j=k;j<display.length-1;j++)
		{
			display[j]=display[j+1];
		}
		display.pop();
	}
	else
	{
		display.push(a);
	}
	ok(display);
}
function ok(a)
{
		if(a.length==0)
		{
			location.reload();
		}
		else
		{
			for(var i=0;i<total_array.length;i++)
			{
				var x=document.getElementsByClassName(""+total_array[i]);
				for(var j=0;j<x.length;j++)
				{
					x[j].style.display="none";
				}
			}
			for(var i=0;i<a.length;i++)
			{
				var x=document.getElementsByClassName(""+a[i]);
				for(var j=0;j<x.length;j++)
				{
					x[j].style.display="block";
				}
			}
		}
}
</script>


</body>
</html>