<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Geo Location | KL Alumni Association</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
	
	<title>Geo Locations | KL Alumni Association | K L University</title>
<meta http-equiv="expires" content="0">
<?php
clearstatcache();
?>
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<link rel="stylesheet" type="text/css" href="css/index.css">
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">	
<link href="https://fonts.googleapis.com/css?family=Noto+Sans|Roboto" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Baloo+Chettan|Montserrat" rel="stylesheet">
	
  </head>
  <body>
  <div class="icon_dis" style="cursor: pointer;">
<div class="main_icon" onclick="window.location='index.php'">
<img src="imags/klu.png" />
<strong><p style="text-decoration: none;">Alumni Association</p></strong>
</div>
</div>
<div class="dropdownmenu">
  <nav>
      <div class="nav-mobile"><a id="nav-toggle" href="#!"><span></span></a></div>
      <ul class="nav-list" >
        <li>
          <a href="#" style="text-decoration: none;">Carrers</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="careers.php" style="text-decoration: none;">Internship</a>
            </li>
            <li>
              <a href="careers.php" style="text-decoration: none;">Jobs</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="advisoryboard.php" style="text-decoration: none;">Board Members</a>
        </li>
        <li>
          <a href="#!" style="text-decoration: none;">Events</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="events.php" style="text-decoration: none;">Alumni Event Calender</a>
            </li>
			<li><a href="events.php" style="text-decoration: none;">All Events</a></li>
			<li>
			  <a href="https://kluniversity.in/site/acadcal.htm" style="text-decoration: none;">Academic Calender</a></li>
          </ul>
        </li>
		 <li>
              <a href="suess_stories.php" style="text-decoration: none;">Alumni Stories</a>
            </li>
            <li>
              <a href="gallery.php" style="text-decoration: none;">Gallery</a>
            </li>
         <li>
              <a href="updates.php" style="text-decoration: none;">News & Updates</a>
            </li>
        <li>
          <a href="aboutus.html" style="text-decoration: none;">About us</a>
        </li>
      </ul>
    </nav>
 </div>
  
  <?php
require('js/php/conn.php');
$get=mysqli_query($conn,"select geo_location,profile_name from profile where geo_location is not null;");
$name='';
$location='';
while($g=mysqli_fetch_assoc($get))
{
	$temp=$g['geo_location'];
	$temp=explode(",",$temp);
	if($name=='')
	{
		$name=$name.'"'.$g['profile_name'].'"';
	}
	if($location=='')
	{
		$location=$location.'{ lat:'.$temp[0].', lng:'.$temp[1].'}';
	}
	else
	{
		$name=$name.',"'.$g['profile_name'].'"';
		$location=$location.',{ lat:'.$temp[0].', lng:'.$temp[1].'}';
	}
}
?>
  
    <div id="map"></div>
    <script>
      function initMap() {

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 3,
          center: {lat: 20.5937, lng: 78.9629}
        });

        // Create an array of alphabetical characters used to label the markers.
    var labels= [<?php echo $location; ?>];

        // Add some markers to the map.
        // Note: The code uses the JavaScript Array.prototype.map() method to
        // create an array of markers based on a given "locations" array.
        // The map() method here has nothing to do with the Google Maps API.
        var markers = locations.map(function(location, i) {
          return new google.maps.Marker({
            position: location,
            label: labels[i % labels.length]
          });
        });

        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
      }
      var locations = [<?php echo $location; ?>
      ]
    </script>
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCL0oDAtYCFwLuN18rAExqrsZ6nRVz1GYA&callback=initMap">
    </script>
  </body>
</html>
