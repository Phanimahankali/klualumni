<html>
<head>
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
  <meta charset="utf-8">
<?php
session_start();
if($_SESSION['alu_auth']==null)
{
	header("location:index.php");
}
?>
<title>Search | KL Alumni Association</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
  body
{
	margin: 0px;
	background: #f7f7f7;
	position: relative;
}
#messanger_down_box
{
	width:100%;
	height: 10px;
	position: absolute;
	bottom: 0px;
	z-index: 2;
	position: fixed;
}
.message_box
{
	width: auto;
	height: 350px;
	position: absolute;
	bottom: 0px;
	right: 0px;
}
.message_sub_box
{
	width: 280px;
	height: 350px;
	display: inline-block;
	visibility: hidden;
	border-top-left-radius: 10px;
	border-top-right-radius: 10px;
	margin: 0px 20px 0px 20px;
	overflow: hidden;
	background-color: white;
	box-shadow: 0px 0px 5px 1px #d9d9d9;
}
.message_top_details
{
	background-color:#df3a3a;
	width: 100%;
	height: 40px;
}
.message_send_box
{
	width: 280px;
	height: 35px;
	border-top: 1px solid #a6a6a6;
	position: absolute;
	bottom: 0px;
}
.message_active_status
{
	width: 30px; 
	height: 40px;
	float: left;
	position: relative;
}
.message_active_person_name
{
	width: 200px;
	height: 40px;
	float: left;
	overflow: hidden;
}
.message_active_person_name p
{
	font-family: 'Noto Sans', sans-serif;
	font-size: 13px;
	margin: 0px;
	padding-top: 10px;
	color: white;
}
.message_box_close
{
	width: auto;
	height: 40px;
	float: right;
}
.message_box_close p
{
	font-family: 'Noto Sans', sans-serif;
	font-size: 18px;
	margin: 0px;
	cursor: pointer;
	margin: 8px 10px 0px 0px;
    padding: 0px;
	padding: 0px 4px 0px 4px;
	width: auto;
	color: white;
}
.message_box_close p:hover
{
	background-color: #e14848;
}
*:focus
{
	outline: none;
}
.message_display_content
{
	width:280px;
	height: 274px;
	background-color: white;
	position: absolute;
}
.name_diplay
{
	width: auto;
	height: auto;
	margin-left: 350px;
	float: left;
	display: inline-block;
}

.banner_main
{
	width: 100%;
	height:400px;
	max-width: 1800px;
	margin: auto;
	position: relative;
}
.profile_pic
{
	width: 200px;
	height: 200px;
	position: absolute;
	border: 3px solid white;
	border-radius: 50%;
	top: 190px;
	left: 130px;
	right: 0px;
	overflow: hidden;
}
#follow_button
{
margin-top: 30px;
font-size: 20px;
color: #fe6e60;
cursor: pointer;
padding: 5px 20px 5px 20px;
border-radius: 20px;	
background: white;
border: 1.5px solid #fe6e60;
transition: .5s;
}
#follow_button:hover
{
color: white;	
background: #fe6e60;
transition: .5s;
}
.main
{
	width: 100%;
	max-width: 1800px;
	margin: auto;
}
.side_nav_left
{
	width: 30%;
	height: auto;
	float: left;
}
.side_nav
{
	width: 400px;
	height: auto;
	background: white;
	box-shadow: 0px 0px 10px 0px #cfcfcf;
	float: left;
	margin-left: 10px;
	margin-top: 10px;
	padding-bottom: 20px;
	border-radius: 10px;
}
.side_content
{
	width: 70%;
	height: auto; 
	float: left;
}
.content
{
	width: 900px;
	height: auto;
	margin: auto;
}
.post_new
{
	width: 650px;
	height: 200px;
	border-radius: 20px;
	box-shadow: 0px 0px 10px 0px #cfcfcf; 
	margin: auto;
	background: white;
}
.postedby
{
	width: 100%;
	height: 50px;
	box-shadow:  0px 1px 0px 0px #cfcfcf; 
}
.postedby_photo
{
	width: 40px;
	height: 40px;
	position: relative;
	top:5px;
	left: 15px;
	float: left;
	overflow: hidden;
	border-radius: 50px;
	background: yellow;
}
.postedby_name
{
	width: auto;
	height: 100%;
	margin-left: 25px;
	float: left;
}
.attachment
{
	width: 100%;
	height: 47px;
	border-bottom-left-radius: 20px;
	border-bottom-right-radius: 20px;
}
.photo_upload
{
	width: 120px;
	height: 40px;
	border-radius: 18px;
	float: left;
	cursor: pointer;
	margin-top: 3px;
	margin-left: 10px;
	background: #f4f4f4;
}
.post
{
	width: 80px;
	padding: 5px;
	background:#62c5c5;
	color: white;
	margin-right: 10px;
	margin-top: 10px;
	border: 1px solid #62c5c5;
	border-radius:10px;
	font-size: 15px;
	cursor: pointer;
}
.top_nav
{
	width: 100%;
	height: 50px;
	max-width: 1600px;
	margin: auto;
	text-align: center;
	background: white;
	box-shadow: 0px 1px 5px #cccccc;
}
input:focus
{
	outline: none;
}
.icons
{
	width: 40px;
	height: 100%;
	float: right;
}
#profile_content_click
{
	position: absolute;
	margin-top: 50px;
	z-index: 1;
	width: auto;
	height: auto;
}
.profile_pic:hover > .change_photo
{
	position: absolute;
	bottom:0px;
	width: 100%;
	height: 35%;
	cursor: pointer;
	transition: 0.5s;
	background-color: rgb(0,0,0,0.5);
}
.profile_pic:hover > .change_photo:hover
{
	background-color: black;
}
.dropdown_profile
{
	position: relative; 
	z-index: 1;
	float: right; 
	text-align: left;
	height: auto;
	margin: 0px; 
	display: none;
	cursor:pointer;	
	background-color: white;
}
.icons:hover > .dropdown_profile
{
	display: block;
}
.view_profile
{
	width: 95%; 
	font-size: 15px; 
	color: black;
	margin: 0px;
	text-decoration: none;
	margin: auto; 
	padding: 3px 0px 3px 0px;
	text-align: center; 
	font-family: arial;
	border-radius: 5px;
}
.view_profile:hover
{
	background-color: #f1f1f1;
}
.experience
{
	width: 800px;
	height: auto;
	float: left;
	border-radius: 5px;
	margin-top: 15px;
	margin-left: 30px;
	padding-bottom: 10px;
	background-color: white;
	box-shadow: 0px 1px 5px #cccccc;
}
.education
{
	width: 800px;
	height: auto;
	float: left;
	margin-top: 10px;
	border-radius: 5px;
	margin-left: 30px;
	padding-bottom: 10px;
	background-color: white;
	box-shadow: 0px 1px 5px #cccccc;
}
.skills_show
{
	width: 95%;
	height: auto;
	margin-left: 5%;
}
.skills_show p
{
	float: left;
	display: block;
	font-family: 'Roboto', sans-serif;
	padding: 5px;
	color: #4d4d4d;
}
.suggest_peoples
{
	width: 400px;
	height: auto;
	background: white;
	box-shadow: 0px 0px 10px 0px #cfcfcf;
	float: left;
	margin-left: 10px;
	margin-top: 10px;
	border-radius: 10px;
}
.suggest_people_show
{
	width: 100%;
	height: 40px;
	cursor: pointer;
	margin-top: 10px;
}
.social_accounts
{
	width: 400px;
	height: auto;
	background: white;
	padding-bottom: 10px;
	box-shadow: 0px 0px 10px 0px #cfcfcf;
	float: left;
	margin-left: 10px;
	margin-top: 10px;
	border-radius: 10px;
}
.fa {
  padding: 4px;
  font-size: 20px;
  width: 20px;
  text-align: center;
  text-decoration: none;
  margin: 5px 2px;
}

.fa:hover {
    opacity: 0.7;
}
.fa-twitter {
  margin-left: 10px;
  background: #55ACEE;
  color: white;
}
.fa-linkedin {
  margin-left: 10px;
  background: #007bb5;
  color: white;
}
.fa-facebook {
  margin-left: 10px;
  background: #3B5998;
  color: white;
}
.fa-instagram {
  margin-left: 10px;
  background: #3f729b;
  color: white;
}
.nav_icons
{
	width: auto;
	height: 100%;
	float: left;
	padding-right: 10px;
	cursor: pointer;
}
.nav_icons:hover
{
	background-color: #f4f4f4;
}
.nav_friends_search_box
{
	width: 500px;
	height: 380px;
	position: absolute;
	z-index: 1;
	display: none;
	margin: auto;
	top: 0px;
	left: 0px;
	right: 0px;
	bottom: 0px;
	position: fixed;
	box-shadow: 0px 0px 10px 0px #f4f4f4;
	background-color: white;
	top: 0px;
}
#nav_friends_display
{
	width: 100%;
	height: 330px;
	overflow-y: scroll;
}
#nav_search_friends
{
	width: 100%;
	height: 330px;
	overflow-y: scroll;
	display: none;
}
#nav_unread_message_status
{
	width: 15px;
	height: 15px;
	background-color: #ff5757;
	position: absolute;
	top: 8px; 
	right: 5px;
	border-radius: 20px;
}
.result_show
{
	width: 100%;
	height: 40px;
	cursor: pointer;
	background-color: white;
}
.result_show:hover
{
	background-color: #f8f8f8
}
.post_style
{
	width: 600px;
	height: auto;
	box-shadow: 0px 0px 3px #cccccc;
	border-top-left-radius: 5px;
	border-top-right-radius: 5px;
	background: white;
	margin-left: auto;
	display:block;
	margin-right: auto;
	margin-top: 40px;
	margin: 10px 50px 10px 50px;	
}
.post_top
{
	width: 100%;
	height: 50px;
	border-bottom: 1px solid #cccccc;
	border-top-left-radius: 20px;
	border-top-right-radius: 20px;
	background: white;
}
.profile_logo
{
	width: 35px;
	height: 35px;
	position: relative;
	top: 5px;
	float: left;
	border-radius: 35px;
	left: 10px;
	overflow: hidden;
}
.profile_details
{
	width: auto;
	height: 100%;
	float: left;
	margin-left: 15px;
}
#profile_display
{
	width: 100%; 
	height: auto;
}
.profile_data_box
{
	width: 300px; 
	height: 110px;
	margin: 10px;
	display: inline-block;
	cursor: pointer;
	box-shadow: 0px 0px 2px 0px #d9d9d9;
}
.input_filed
{
	font-family: arial;
	font-size: 15px;
	padding: 6px 0px  6px 10px;
	border: 1px solid #d9d9d9;
	margin-left: 20px;
	border-radius: 5px;
}
  </style>
  <?php require('js/php/get_profile_data.php'); ?>
  <link href="https://fonts.googleapis.com/css?family=Noto+Sans|Open+Sans" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<div class="top_nav">
<img src="imags/klu.png" style="height: 90%; width: auto; margin-left: 10px; margin-top: 3px; float: left;">
<p style="margin: 0px; font-family: 'Lato', sans-serif; font-size: 20px; padding-top: 12px; float: left;"><strong>Alumni Association</strong></p>
<div style="width: auto; height:100%; float: right;">
<div class="icons">
<div style="width: auto; height: 100%;float: left;">
<a href="http://localhost/KLUAlumni/logout.php">
<div onclick="profile_content_click();" style="width: 30px;  height: 30px; cursor: pointer;  overflow: hidden; border-radius: 30px; margin-top: 10px;">
<img src="logout_c.png" style="width: 100%; height: auto;">
</div>
</a>
</div>
</div>
<div class="icons">
<div style="width: auto; height: 100%;float: left;">
<a href="main_p.php">
<div onclick="profile_content_click();" style="width: 30px;  height: 30px; cursor: pointer; background: pink; overflow: hidden; border-radius: 30px; margin-top: 10px;">
<img src="<?php echo $profile_pic; ?>" style="width: 100%; height: auto;">
<ul>
<li>Profile</li>
</ul>
</div>
</a>
</div>
<div class="dropdown_profile">
<div style="width: 250px; height: 60px; background-color: white;">
<div style="width: 50px; margin: 5px; height: 50px; border-radius: 60px; background-color: black; overflow: hidden; float:left;">
<img src="<?php echo $profile_pic; ?>" style="width: 100%; height: auto;">
</div>
<div style="height: 50px; width: auto; float: left;">
<p style=" margin: 0px; padding-left: 5px; padding-top: 10px; font-family: arial; font-size: 15px;"><?php echo $profile_name; ?></p>
</div>
</div>
<a style="text-decoration: none;" href="edit.php"><p class="view_profile">View Profile</p></a>
<div style="width: 100%; height: 0.5px; background-color: #bebebe; margin-top: 5px;">
</div>
<a href="news.php" style="text-decoration: none;"><p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; padding: 7px 0px 2px 7px; font-size: 15px; ">News</p></a>
<a href="offer.php" style="text-decoration: none;"><p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; padding: 7px 0px 2px 7px; font-size: 15px; ">Job postings</p></a>
<p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; padding: 7px 0px 2px 7px; font-size: 15px; ">Change Password</p>
<p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; padding: 7px 0px 2px 7px; font-size: 15px;">Privacy policy</p>

</div>
</div>
</div>
<div style="width: auto; height: 100%; float: right; margin-right: 30px;">
<a href="news.php">
<div class="nav_icons">
<div style="width: auto; height: 100%; float: right;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; margin: 0px; padding-top: 13.5px; color: #8d8d8d;">Home</p>
</div>
<div style="width: 50px; height: 100%; float: right; position: relative;">
<img src="imags/home.png" style="width: 35px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;">
</div>
</div>
</a>
<a href="mypost.php">
<div class="nav_icons">
<div style="width: auto; height: 100%; float: right;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; margin: 0px; padding-top: 13.5px; color: #8d8d8d;">My Posts</p>
</div>
<div style="width: 50px; height: 100%; float: right; position: relative;">
<img src="imags/my_post.png" style="width: 35px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;">
</div>
</div>
</a>
<a href="network.php">
<div class="nav_icons">
<div style="width: auto; height: 100%; float: right;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; margin: 0px; padding-top: 13.5px; color: #8d8d8d;">My Network</p>
</div>
<div style="width: 50px; height: 100%; float: right; position: relative;">
<img src="imags/mynetwork.png" style="width: 35px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;">
</div>
</div>
</a>
<div class="nav_icons" onclick="document.getElementById('nav_friends_search_box').style.display='block'">
<div style="width: auto; height: 100%; float: right;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; margin: 0px; padding-top: 13.5px; color: #8d8d8d;">Messaging</p>
</div>
<div style="width: 50px; height: 100%; float: right; position: relative;">
<img src="imags/message.png" style="width: 35px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;">
</div>
</div>
</div>
</div>

<div style="width: 100%; max-width: 1600px; height: auto; background-color: red; margin: auto;  margin-top: 10px;">

<div id="filters" style="width: 300px; height: 1000px; background-color: white; float: left;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; padding-left: 5px;">Graduated year</p>
<input class="input_filed" id="year" onkeyup="set_query();" placeholder="Ex: 2020"></input>
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; padding-left: 5px;">Location</p>
<input class="input_filed" onkeyup="set_query();" id="location" placeholder="Ex: Vijayawada"></input>
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; padding-left: 5px;">Stream</p>
<select class="input_filed"  onchange="set_query();" id="stream1" style="max-width: 200px; margin-top: 20px;">
<option value="">Select Stream</option>
<?php
require("js/php/conn.php");
$get_det=mysqli_query($conn,"select * from programs where stream!='';");
while($g=mysqli_fetch_assoc($get_det))
{
	$s=$g['stream'];
	echo '<option value="'.$s.'">'.$s.'</option>';
}
?>
</select>
<select class="input_filed"  onchange="set_query();" id="stream2" style="max-width: 200px; margin-top: 20px;">
<option value="">Select Stream</option>
<?php
require("js/php/conn.php");
$get_det=mysqli_query($conn,"select * from programs where stream!='';");
while($g=mysqli_fetch_assoc($get_det))
{
	$s=$g['stream'];
	echo '<option value="'.$s.'">'.$s.'</option>';
}
?>
</select>
<select class="input_filed" onchange="set_query();" id="stream3" style="max-width: 200px; margin-top: 20px;">
<option value="">Select Stream</option>
<?php
require("js/php/conn.php");
$get_det=mysqli_query($conn,"select * from programs where stream!='';");
while($g=mysqli_fetch_assoc($get_det))
{
	$s=$g['stream'];
	echo '<option value="'.$s.'">'.$s.'</option>';
}
?>
</select>
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; padding-left: 5px;">Gender</p>
<div style="width: 100%; height: 60px; text-align: center;">
<div onclick="document.getElementById('gender').value=''; set_query();" style="width: 60px; height: 60px; margin: 0px 5px 0px 5px;  display: inline-block;">
<input type="radio" name="ok_r"><p  style="font-family: 'Noto Sans', sans-serif;margin: 0px; font-size: 14px;">All</p></input>
</div>
<div onclick="document.getElementById('gender').value='Male'; set_query();" style="width: 60px; height: 60px; margin: 0px 5px 0px 5px; display: inline-block;">
<input type="radio" name="ok_r"> <p  style="font-family: 'Noto Sans', sans-serif;margin: 0px; font-size: 14px;">Male</p></input>
</div>
<div onclick="document.getElementById('gender').value='Female'; set_query();" style="width: 100px; height: 60px; margin: 0px 5px 0px 5px; display: inline-block;">
<input type="radio" name="ok_r"><p  style="font-family: 'Noto Sans', sans-serif; margin: 0px; font-size: 14px;">Female</p></input>
</div>
</div>
<input style="visibility: hidden;" id="gender"></input>
</div>



<div id="content_dis" style="height: auto; margin-left: 10px; float: left; background-color: white;">
<div style="width: 95%; margin: 5px 0px 0px 5px; height: 60px; box-shadow: 0px 0px 2px 0px #d9d9d9;">
<div style="width: 60px; height: 60px; float: left; position: relative;">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="25" height="25" style=" position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;" viewBox="0 0 20 20">
                  <path d="M18.869 19.162l-5.943-6.484c1.339-1.401 2.075-3.233 2.075-5.178 0-2.003-0.78-3.887-2.197-5.303s-3.3-2.197-5.303-2.197-3.887 0.78-5.303 2.197-2.197 3.3-2.197 5.303 0.78 3.887 2.197 5.303 3.3 2.197 5.303 2.197c1.726 0 3.362-0.579 4.688-1.645l5.943 6.483c0.099 0.108 0.233 0.162 0.369 0.162 0.121 0 0.242-0.043 0.338-0.131 0.204-0.187 0.217-0.503 0.031-0.706zM1 7.5c0-3.584 2.916-6.5 6.5-6.5s6.5 2.916 6.5 6.5-2.916 6.5-6.5 6.5-6.5-2.916-6.5-6.5z"></path>
                </svg>
</div>
<div style="width: auto; height: 60px; float: left;">
<input onkeyup="set_query();" id="query" type="text" placeholder="Search" style=" width: 300px;padding: 5px 0px 5px 0px; margin-top: 10px; margin-left: 10px; font-family: 'Open Sans', sans-serif; font-size: 17px; border: none;"></input>
</div>

</div>
<div id="profile_display">

</div>


</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
var width=$(window).width();
document.getElementById('content_dis').style.width=width-320;
$(window).resize(function() {
  width=$(window).width(); 
  document.getElementById('content_dis').style.width=width-320;
});
function set_query()
{
	var query=document.getElementById('query').value;
	var year=document.getElementById('year').value;
	var location=document.getElementById('location').value;
	var stream1=document.getElementById('stream1').value;
	var stream2=document.getElementById('stream2').value;
	var stream3=document.getElementById('stream3').value;
	var gender=document.getElementById('gender').value;
	$.ajax({
	 type: 'post',
	 url: 'js/php/advance_search.php',
	 data : {query:query, year:year, location:location, stream1:stream1, stream2:stream2, stream3:stream3, gender:gender},
	 success: function(code){
	 $('#profile_display').html(code);
	 }
	 });
}
</script>

</body>
</html>