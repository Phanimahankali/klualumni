<?php
error_reporting(0);
session_start();
$session=$_SESSION['alu_auth'];
if($session=='')
{
	header("Location: index.php");
	exit;
}
else
{
require_once('js/php/conn.php');
$get_details=mysqli_query($conn,"select * from user_security where hash_key='$session';");
if(mysqli_num_rows($get_details)==0)
{
	header("Location: index.php");
	exit;
}
else
{
	$get_details=mysqli_fetch_array($get_details);
	$session=$get_details[0];
}
}
require('js/php/new_offer.php');
require('js/php/get_profile_data.php');
?>
<html>
<head>
<title>Job offer || KL Alumni Association</title>
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
<style>
body
{
	margin: 0px;
	background: #f7f7f7;
}
.top_nav
{
	width: 100%;
	height: 50px;
	max-width: 1600px;
	margin: auto;
	text-align: center;
	background: white;
	box-shadow: 0px 1px 5px #cccccc;
}
.icons
{
	width: 40px;
	height: 100%;
	float: right;
	position: relative;
}
.offerpage
{
	width: 98%; 
	height: auto; 
	position: relative;
	border-radius: 15px;
	box-shadow: 0px 1px 5px #cccccc;
	background: white;
	margin: auto;
	margin-top: 15px;
	padding-bottom: 50px;
}
fieldset
{
	border: none;
	font-size: 13px;
	display: inline-block;
	text-align: left;
	margin-left: 40px; 
	margin-right: 40px;
	font-family: 'Roboto', sans-serif;
}
fieldset input
{
	border: none;
	border-bottom: 1.5px solid black;
}
fieldset select
{
	width: auto;
	border: none;
	border-bottom: 1.5px solid black;
}
*:focus {
    outline: none;
}
[type="date"] {
  background:#fff url(https://cdn1.iconfinder.com/data/icons/cc_mono_icon_set/blacks/16x16/calendar_2.png)  97% 50% no-repeat ;
}
[type="date"]::-webkit-inner-spin-button {
  display: none;
}
[type="date"]::-webkit-calendar-picker-indicator {
  opacity: 0;
}


label {
  display: block;
}
#dateofbirth {
  border: 1px solid #c4c4c4;
  border-radius: 5px;
  background-color: #fff;
  padding: 3px 5px;
  box-shadow: inset 0 3px 6px rgba(0,0,0,0.1);
  width: 190px;
}
.submit
{
	padding: 5px;
	position: absolute;
	right: 20px;
	cursor: pointer;
	font-size: 18px;
	color: white;
	border-radius: 5px;
	border: 1.5px solid #227f8f;
	bottom: 10px;
	padding-left: 10px;
	padding-right: 10px;
	background-color: #227f8f;
}
.applications
{
	float: right;
	font-family: arial;
	margin: 0px;
	margin-top: 9px;
	border-radius: 5px;
	margin-right: 15px;
	padding: 7px;
	cursor: pointer;
}
.applications:hover
{
	color: rgba(0,0,0,0.8);
	background-color: rgba(153,204,204,0.1);
}
#customers {
  font-family: 'Noto Sans', sans-serif;
  border-collapse: collapse;
  width: 95%;
  margin: auto;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}



#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #222e39;
  color: white;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body>
<div class="top_nav">
<img src="imags/klu.png" style="height: 90%; width: auto; margin-left: 10px; margin-top: 3px; float: left;" />
<p style="margin: 0px; font-family: 'Lato', sans-serif; font-size: 20px; padding-top: 12px; float: left;"><strong>Alumni Association</strong></p>
<div style="width: auto; height:100%; float: right;">
<div class="icons">
<div style="width: auto; height: 100%;float: left;">
<a href="logout.php">
<div onclick="profile_content_click();" style="width: 30px;  height: 30px; cursor: pointer;  overflow: hidden; border-radius: 30px; margin-top: 10px;">
<img src="logout_c.png" style="width: 100%; height: auto;">
</div>
</a>
</div>
</div>
<div class="icons">
<div style="width: auto; height: 100%;float: left;">
<a href="main_p.php">
<div onclick="profile_content_click();" style="width: 30px;  height: 30px; cursor: pointer; background: pink; overflow: hidden; border-radius: 30px; margin-top: 10px;">
<img src="<?php echo $profile_pic; ?>" style="width: 100%; height: auto;">
</div>
</a>
</div>
</div>
</div>
</div>


<div class="offerpage">
<p style="font-family: arial; text-align: center; padding-top: 10px; font-size: 20px;">Job Offers Application</p>
<div style="width: 100%; height: auto; margin-top: 50px; text-align: center;">
<form action="offer.php" enctype="multipart/form-data" method="post">
<fieldset>
<legend>Orgination Name * </legend>
<input required type="text" name="orgination_name" id="orgination_name" style="padding: 5px; font-size: 15px;"></input>
</fieldset>
<fieldset>
<legend>Job Name *</legend>
<input required type="text" name="job_name" id="job_name" style="padding: 5px; font-size: 15px;"></input>
</fieldset>
<fieldset>
<legend>Job Type *</legend>
<select required name="jobtype" id="jobtype" type="text" style="padding: 5px; font-size: 15px;">
<option value="">Select Job Type</option>
<option value="full-time">Full-Time</option>
<option value="part-time">Part-Time</option>
<option value="intern">Intern</option>
<option value="temporary">Temporary</option>
<option value="pursuing-degree">Pursuing Degree</option>
</select>
</fieldset>
<fieldset>
<legend>Degree *</legend>
<select required name="degree" id="degree" type="text" style="padding: 5px; font-size: 15px;">
<option value="">Select Degree</option>
<option value="associate">Associate</option>
<option value="bachelors">Bachelor's</option>
<option value="masters">Master's</option>
<option value="ph.d">Ph.D</option>
<option value="pursuingdegree">Pursuing Degree</option>
</select>
</fieldset>
<fieldset>
<legend>Last Date *</legend>
<input required type="date" name="lastdate" id="lastdate">
</fieldset>
</div>
<p style="font-family: 'Roboto', sans-serif; font-size: 15px; padding-left: 40px; padding-top: 20px;">Minimum Qualification :</p>
<textarea  name="minimumqualification"  id="minimumqualification" style="padding: 10px; font-family: arial; min-width: 90%; max-width: 90%; max-height: 120px; min-height: 120px; margin-left: 5%; background-color: white;">
</textarea>
<fieldset style="margin-top: 40px; margin-left: 50px; font-size: 18px;">
<legend>Job Location *</legend>
<input required name="joblocation" id="joblocation" type="text" style="padding: 5px; font-size: 15px;">
</fieldset>
<fieldset style="margin-top: 40px; margin-left: 50px; font-size: 18px;">
<legend>Attachments *</legend>
<input required  name="attachments[]" id="attachments" type="file" style="padding: 5px; border: none; font-size: 15px;">
</fieldset>
<fieldset style="margin-top: 40px; border: none; margin-left: 50px; font-size: 18px;">
<legend>Company Logo *</legend>
<input required name="companylogo" id="companylogo" type="file" style="padding: 5px; border: none; font-size: 15px;">
</fieldset>
<button class="submit" name="submit">Submit</button>
</form>
</div><div style="width: 100%; height: auto; margin-top: 10px; padding-bottom: 20px; background-color: white;">
<div style="width: 100%; height: 50px;">
<p style="font-family: 'Lato', sans-serif; font-size: 15px; padding-top: 10px; padding-left: 20px;">Previous job offer record list</p>
</div>
<table id="customers">
  <tr>
    <th>Code</th>
    <th>Orgination Name</th>
    <th>Job Name</th>
	<th>Type</th>
	<th>Last date</th>
    <th>Posted date</th>
    <th>Status</th>
    <th>Response</th>
    <th>Action</th>
  </tr>
  
  <?php
  $get_offer_details=mysqli_query($conn,"select * from offer where posted_by='$session' order by code DESC;");
  
  while($get_o=mysqli_fetch_assoc($get_offer_details))
  {
	  $code=$get_o['code'];
	  $org_name=$get_o['organization_name'];
	  $job_name=$get_o['organization_name'];
	  $job_type=$get_o['jobtype'];
	  $last_date=$get_o['lastdate'];
	  $posted_date=$get_o['timestamp'];
	  $count_res=mysqli_query($conn,"select * from offer_response where response_to='$code' and status='VERIFIED';");
	  $response=mysqli_num_rows($count_res);
	  $status=$get_o['status'];
	  echo '
	  
	   <tr>
    <td>'.$code.'</td>
    <td>'.$org_name.'</td>
    <td>'.$session.'</td>
	<td>'.$job_type.'</td>
    <td>'.$last_date.'</td>
	<td>'.$posted_date.'</td>
    <td>
	<div style="width: 10px; height: 10px; margin-top: 8px; border-radius: 10px; float: left; background-color:
';
if($status=='Under Verification')
{
	echo '#ffde59';
}
else
{
	echo '#c9e265';
}
	echo '
	;">
	</div>
	<p style="float: left; margin: 0px; padding-left: 10px;">'.$status.'</p>
	</td>
    <td>
	<p style="float: left; margin: 0px; padding-left: 10px;">'.$response.'</p>';
	if($response>=1)
	{
	 echo '<p id="'.$code.'" onclick="download_report('.$code.');" style="float: left; margin: 0px; padding-left: 10px; cursor: pointer;">(Download)</p>';
	}
	echo '
	</td>
	<td><p id="'.$code.'" onclick="delete_record('.$code.');" style="text-align: center; cursor: pointer; color: red;">Delete</p></td>
  </tr>
	  
	  ';
  }
  ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="js/send_offer_request_data.js"></script>

</table>
</div>
</body>
</html>