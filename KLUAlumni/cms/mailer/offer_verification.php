<?php
require('../php/conn.php');
$code=$_REQUEST['cod'];
$hash=$_REQUEST['hash'];
$get_details1=mysqli_query($conn,"select email from offer_response where hash_id='$hash';");
if(mysqli_num_rows($get_details1)==1)
{
$get_details1=mysqli_fetch_array($get_details1);
$email=$get_details1[0];
$get_details=mysqli_query($conn,"select organization_name,jobtype from offer where code='$code';");
$get_details=mysqli_fetch_array($get_details);
$organization_name=$get_details[0];
$jobtype=$get_details[1];
require ('phpmailer/PHPMailerAutoload.php');
$mail = new PHPMailer();
$mail->isSMTP();
include('main_setting.php');
$mail->Subject = "Verification || KL Alumni Association";
$link="http://103.206.105.88/overify.php?hash=".$hash."&c=".enc_function();
$html='
<html>
<head>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">
<style>
.mail_main_box
{
width: 600px;
height: auto;
margin: auto;
border: 1px solid #ebebeb;
}

a {
    text-decoration: none;
}
p > a:hover{
    text-decoration:  underline;
}
h1,
h2,
h3,
h4,
h5,
h6 {
    margin:  1% 0 1% 0;
}
._12 {
    font-size: 1.2em;
}
._14 {
    font-size: 1.4em;
}
ul {
    padding:0;
    list-style: none;
}
.footer-social-icons {
    width: 350px;
    display:block;
    margin: 0 auto;
}
.social-icon {
    color: #fff;
}
ul.social-icons {
    margin-top: 10px;
}
.social-icons li {
    vertical-align: top;
    display: inline;
    height: 100px;
}
.social-icons a {
    color: #fff;
    text-decoration: none;
}
.fa-facebook {
    padding:10px 14px;
    -o-transition:.5s;
    -ms-transition:.5s;
    -moz-transition:.5s;
    -webkit-transition:.5s;
    transition: .5s;
	border-radius: 50%;
    background-color: #322f30;
}

.fa-twitter {
    padding:10px 12px;
    -o-transition:.5s;
    -ms-transition:.5s;
    -moz-transition:.5s;
    -webkit-transition:.5s;
    transition: .5s;
	border-radius: 50%;
    background-color: #322f30;
}



.fa-youtube {
    padding:10px 14px;
    -o-transition:.5s;
    -ms-transition:.5s;
    -moz-transition:.5s;
    -webkit-transition:.5s;
    transition: .5s;
	border-radius: 50%;
    background-color: #322f30;
}


.fa-instagram {
    padding:10px 9px;
	padding-left: 12px;
	padding-right: 12px;
    -o-transition:.5s;
    -ms-transition:.5s;
    -moz-transition:.5s;
    -webkit-transition:.5s;
    transition: .5s;
	border-radius: 50%;
    background-color: #322f30;
}
.verify_button
{
	padding: 5px 13px 5px 13px;
	font-size: 15px;
	font-family: arial;
	color: white;
	background-color: #729af4;
	border: 1px solid #729af4;
	cursor: pointer;
}
.social_icons
{
width: 30px;
 height: 30px; 
 border-radius:30px;
 background-color: black; 
 display: inline-block;
 position: relative;
}
.social_icons img
{
width: 20px;
height: auto; 
margin: auto; 
margin-top: 5px;
}
</style>
<link href="https://fonts.googleapis.com/css?family=Lato|Roboto" rel="stylesheet">
</head>
<body>
<div class="mail_main_box">
<div style="width: 100%; height: 80px; text-align: center;">
<div style="width: auto; height: 40px;  margin-top: 10px; display: inline-block; position: relative;">
<img src="https://klfocus.in/css/images/klu.png" style="width: auto; margin: 0px; height: 40px; float: left;" />
<strong><p style="font-family: '."'Roboto'".', sans-serif; float: left; padding-left: 10px;">Alumni Association</p></strong>
</div>
</div>
<div style="width: 580px; height: auto; padding-bottom: 20px; border: 10px solid #f7f7f7;">
<strong><p style="text-align: center;font-family: '."'Lato'".', sans-serif; font-size: 18px;">VERIFICATION</p></strong>
<p style="font-family: '."'Lato'".',Arial,sans-serif; font-size: 13px; padding: 30px 10px 0px 10px;">You have been applied for '.$jobtype.' in '.$organization_name.'. To approve your application. Please verify your email by clicking th below link.</p>
<div style="width: 100%; height: auto; text-align: center;">
<p>'.$link.'</p>
<a href="'.$link.'"><button class="verify_button">Click to verify</button></a>
</div>
</div>
<div style="width: 600px; height: 110px; background-color: #f7f7f7;">
<div style="width: 100%; height: 50px;">
<div style="width: 50%; height: 50px; float: left; text-align: center;">
 <ul class="social-icons" style="display: inline-block; margin-top: 5px;">
         <li><a href="https://www.facebook.com/KLUniversity/" target="_blank" ><div class="social_icons"><img src="https://i.ibb.co/6wP5vsT/facebook.png" /></div></a></li>
        <li><a href="https://twitter.com/kluniversity?lang=en" target="_blank" class="social-icon"><div class="social_icons"><img src="https://i.ibb.co/vwXJZpM/twitter.png" /></div></a></li>
        <li><a href="https://www.youtube.com/channel/UCDpAsRnAnV6ey0r-BaIfzkA" target="_blank" class="social-icon"><div class="social_icons"><img style="width: 16px; height: auto;"  src="https://i.ibb.co/VHPQQFK/youtube.png" /></div></a></li>
		<li><a href="https://www.instagram.com/kluniversityofficial/?hl=en" target="_blank" class="social-icon"> <div class="social_icons"><img src="https://i.ibb.co/2dshfnW/insta.png" /></div></a></li>
  </ul>
</div>
<div style="width: 50%; height: 50px; float: left;">
<p style="font-family: '."'Lato'".', sans-serif; font-size: 13px; text-align: center; color: #2e2e2e;">&copy; KL Alumni Association</p>
</div>
</div>
<div style="width: 100%; height: 60px;">
<p style="font-family: '."'Roboto'".', sans-serif; color: #5b5b5b; line-height: 15px; word-spacing: 1px; margin: 0px; font-size: 11px; padding: 10px 10px 0px 10px;">If you have any further queries, please contact: contact@alumni.kluniversity.in, our IT department will answer your questions within one working day. </p>
</div>
</div>
</div>
</body>
</html>';

$mail->Body = $html;
$mail->isHTML(true);
$mail->addAddress($email);
 if($mail->send())
 {
	 echo "ok";
 }
	 
 else
 {
 }
echo $mail->ErrorInfo;
$mail->ClearAllRecipients();
$mail->ClearAttachments();
}
function enc_function()
{
date_default_timezone_set('Asia/Kolkata'); 
$code=date('Hisdmy');
$temp='';
for($i=0;$i<strlen($code);$i=$i+2)
{
	$temp=$temp.''.$code[$i];
}
for($i=1;$i<strlen($code);$i=$i+2)
{
	$temp=$temp.''.$code[$i];
}
$code=$temp;
$temp='';
for($i=0;$i<strlen($code);$i=$i+2)
{
	$temp=$temp.''.$code[$i];
}
for($i=1;$i<strlen($code);$i=$i+2)
{
	$temp=$temp.''.$code[$i];
}
$code=$temp;
$temp='';
$c=0;
for($i=0;$i<strlen($code);$i++)
{
	if($c==2)
	{
		$i=$i+1;
		$c=0;
	}
	else
	{
	$temp=$temp.''.$code[$i];
	$c++;	
	}
}
$c=0;
for($i=2;$i<strlen($code);$i++)
{
	if($c==2)
	{
		$i=$i+1;
		$c=0;
	}
	else
	{
	$temp=$temp.''.$code[$i];
	$c++;	
	}
}
$code=$temp;
$temp='';
$c=0;
for($i=0;$i<strlen($code);$i++)
{
	if($c==3)
	{
		$i=$i+2;
		$c=0;
	}
	else
	{
	$temp=$temp.''.$code[$i];
	$c++;	
	}
}
$c=0;
for($i=3;$i<strlen($code);$i++)
{
	if($c==3)
	{
		$i=$i+2;
		$c=0;
	}
	else
	{
	$temp=$temp.''.$code[$i];
	$c++;	
	}
}
$code=$temp;
$temp='';
$c=0;
for($i=0;$i<strlen($code);$i++)
{
	if($c==4)
	{
		$i=$i+3;
		$c=0;
	}
	else
	{
	$temp=$temp.''.$code[$i];
	$c++;	
	}
}
$c=0;
for($i=4;$i<strlen($code);$i++)
{
	if($c==4)
	{
		$i=$i+3;
		$c=0;
	}
	else
	{
	$temp=$temp.''.$code[$i];
	$c++;	
	}
}
$code=$temp;
return($code);
}
?>
