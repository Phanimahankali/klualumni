<?php

require('../php/conn.php');
$get=mysqli_query($conn,"select * from news_letters where status='PENDING';");
while($temp=mysqli_fetch_assoc($get))
{
	$t=$temp['code'];
	$m=send_mail($t);
}



function send_mail($code)
{
	$send_c=0;
	$not_send_c=0;
require('../php/conn.php');
require ('phpmailer/PHPMailerAutoload.php');
$mail = new PHPMailer();
$mail->isSMTP();
include('main_setting.php');
$get_details=mysqli_query($conn,"select * from news_letters where code='$code';");
$get_d=mysqli_fetch_assoc($get_details);
$subject=$get_d['subject'];
$content_cover_page=$get_d['content_cover_page'];
$content_heading=$get_d['content_heading'];
$content_mail=$get_d['content'];
$to_address=$get_d['to_address'];
$mail->Subject = $subject." || KL Alumni Association";
$email="gunji.sairevanth@gmail.com";
$get_attach=mysqli_query($conn,"select * from news_letters_files where news_letters_code='$code';");
if(mysqli_num_rows($get_attach)!=0)
{
	while($get_f=mysqli_fetch_assoc($get_attach))
	{
		$attach_file_name=$get_f['file_name'];
		$attach_file_url="../news_letters/".$code."/".$get_f['file_name'];
		for($i=0;$i<mysqli_num_rows($get_attach);$i++)
		{
			$mail->addStringAttachment(file_get_contents($attach_file_url),$attach_file_name);
		}
	}
}
$head='<html>
<head>
<style>

.mail_main_box
{
width: 600px;
height: auto;
margin: auto;
border: 1px solid #ebebeb;
}

a {
    text-decoration: none;
}
p > a:hover{
    text-decoration:  underline;
}
h1,
h2,
h3,
h4,
h5,
h6 {
    margin:  1% 0 1% 0;
}
._12 {
    font-size: 1.2em;
}
._14 {
    font-size: 1.4em;
}
ul {
    padding:0;
    list-style: none;
}
.footer-social-icons {
    width: 350px;
    display:block;
    margin: 0 auto;
}
.social-icon {
    color: #fff;
}
ul.social-icons {
    margin-top: 10px;
}
.social-icons li {
    vertical-align: top;
    display: inline;
    height: 100px;
}

.social_icons
{
width: 30px;
 height: 30px; 
 border-radius:30px;
 background-color: black; 
 display: inline-block;
 position: relative;
}
.social_icons img
{
width: 20px;
height: auto; 
margin: auto; 
margin-top: 5px;
}
</style>
<link href="https://fonts.googleapis.com/css?family=Lato|Roboto" rel="stylesheet">
</head>
<body>
<div class="mail_main_box">
<div style="width: 100%; height: 80px; text-align: center;">
<div style="width: auto; height: 40px;  margin-top: 10px; display: inline-block; position: relative;">
<img src="https://klfocus.in/css/images/klu.png" style="width: auto; margin: 0px; height: 40px; float: left;" />
<strong><p style="font-family: '."'Roboto'".', sans-serif; float: left; padding-left: 10px;">Alumni Association</p></strong>
</div>
</div>';
if($content_cover_page!='')
{
	$main_photo='
	<img src="http://192.168.137.1/KLUAlumni/cms/news_letters/'.$code.'/'.$content_cover_page.'" style="width: 100%; height: auto;"/>
	';
	$head=$head.''.$main_photo;
}
$content='
<div style="width: 580px; height: auto; padding-bottom: 20px; border: 10px solid #f7f7f7;">
<strong><p style="text-align: center;font-family: '."'Lato'".', sans-serif; font-size: 18px;">'.$content_heading.'</p></strong>
';

if($to_address=='all')
{
	$content=$content.'<p style="font-family: '."'Lato'".', sans-serif; font-size: 15px; padding-top: 20px; padding-left: 10px;">Dear Alumnis/Staff members,</p>';
}
if($to_address=='alumnis')
{
	$content=$content.'<p style="font-family: '."'Lato'".', sans-serif; font-size: 15px; padding-top: 20px; padding-left: 10px;">Dear Alumnis,</p>';
}
if($to_address=='staff')
{
	$content=$content.'<p style="font-family: '."'Lato'".', sans-serif; font-size: 15px; padding-top: 20px; padding-left: 10px;">Dear Staff members,</p>';
}

$content_mail=explode("\r\n",$content_mail);
for($i=0;$i<sizeof($content_mail);$i++)
{
	$content=$content.'<p style="font-family: '."'Lato'".',Arial,sans-serif; font-size: 13px; padding: 0px 10px 0px 10px;">'.$content_mail[$i].'</p>';
}
$content=$content.'
</div>
<div style="width: 600px; height: 110px; background-color: #f7f7f7;">
<div style="width: 100%; height: 50px;">
<div style="width: 50%; height: 50px; float: left; text-align: center;">
 <ul class="social-icons" style="display: inline-block; margin-top: 5px; text-align: center;">
        <li><a href="https://www.facebook.com/KLUniversity/" target="_blank" ><div class="social_icons"><img src="https://i.ibb.co/6wP5vsT/facebook.png" /></div></a></li>
        <li><a href="https://twitter.com/kluniversity?lang=en" target="_blank" class="social-icon"><div class="social_icons"><img src="https://i.ibb.co/vwXJZpM/twitter.png" /></div></a></li>
        <li><a href="https://www.youtube.com/channel/UCDpAsRnAnV6ey0r-BaIfzkA" target="_blank" class="social-icon"><div class="social_icons"><img style="width: 16px; height: auto;"  src="https://i.ibb.co/VHPQQFK/youtube.png" /></div></a></li>
		<li><a href="https://www.instagram.com/kluniversityofficial/?hl=en" target="_blank" class="social-icon"> <div class="social_icons"><img src="https://i.ibb.co/2dshfnW/insta.png" /></div></a></li>
    </ul>
</div>
<div style="width: 50%; height: 50px; float: left;">
<p style="font-family: '."'Lato'".', sans-serif; font-size: 13px; text-align: center; color: #2e2e2e;">&copy; KL Alumni Association</p>
</div>
</div>
<div style="width: 100%; height: 60px;">
<p style="font-family: '."'Roboto'".', sans-serif; color: #5b5b5b; line-height: 15px; word-spacing: 1px; margin: 0px; font-size: 11px; padding: 10px 10px 0px 10px;">If you have any further queries, please contact: contact@alumni.kluniversity.in, our IT department will answer your questions within one working day. </p>
</div>
</div>
</div>
</body>
</html>';

		if($to_address=='all')
		{
			$m_m=mysqli_query($conn,"select * from auth");
			while($k_k=mysqli_fetch_assoc($m_m))
			{
				$email=$k_k['email'];
				$mail->addAddress($email);
				$html=$head.''.$content;
				$mail->Body = $html;
				$mail->isHTML(true);
				 if($mail->send())
				 {
					 $send_c++;
					 mysqli_query($conn,"update news_letters set send='$send_c' where code='$code';");
				 }
					 
				 else
				 {
					$not_send_c++;
					mysqli_query($conn,"update news_letters set not_send='$not_send_c' where code='$code';");
				 }
				echo $mail->ErrorInfo;
				$mail->ClearAllRecipients();
			}
			$m_m=mysqli_query($conn,"select * from faculty where email!='';");
			while($k_k=mysqli_fetch_assoc($m_m))
			{
				$email=$k_k['email'];
				$mail->addAddress($email);
				$html=$head.''.$content;
				$mail->Body = $html;
				$mail->isHTML(true);
				 if($mail->send())
				 {
					 $send_c++;
					 mysqli_query($conn,"update news_letters set send='$send_c' where code='$code';");
				 }
					 
				 else
				 {
					$not_send_c++;
					mysqli_query($conn,"update news_letters set not_send='$not_send_c' where code='$code';");
				 }
				echo $mail->ErrorInfo;
				$mail->ClearAllRecipients();
			}
		}
		if($to_address=='alumnis')
		{
			$m_m=mysqli_query($conn,"select * from auth");
			while($k_k=mysqli_fetch_assoc($m_m))
			{
				$email=$k_k['email'];
				$mail->addAddress($email);
				$html=$head.''.$content;
				$mail->Body = $html;
				$mail->isHTML(true);
				 if($mail->send())
				 {
					 $send_c++;
					 mysqli_query($conn,"update news_letters set send='$send_c' where code='$code';");
				 }
					 
				 else
				 {
					$not_send_c++;
					mysqli_query($conn,"update news_letters set not_send='$not_send_c' where code='$code';");
				 }
				echo $mail->ErrorInfo;
				$mail->ClearAllRecipients();
			}
		}
		if($to_address=='staff')
		{
			$m_m=mysqli_query($conn,"select * from faculty where email!='';");
			while($k_k=mysqli_fetch_assoc($m_m))
			{
				
				$email=$k_k['email'];
				$mail->addAddress($email);
				$html=$head.''.$content;
				$mail->Body = $html;
				$mail->isHTML(true);
				 if($mail->send())
				 {
					 $send_c++;
					 mysqli_query($conn,"update news_letters set send='$send_c' where code='$code';");
				 }
					 
				 else
				 {
					$not_send_c++;
					mysqli_query($conn,"update news_letters set not_send='$not_send_c' where code='$code';");
				 }
				echo $mail->ErrorInfo;
				$mail->ClearAllRecipients();
			}
		}
		$mail->ClearAttachments();
mysqli_query($conn,"update news_letters set status='SUCCESS' where code='$code';");
return 0;
}
?>

