<?php
require ('phpmailer/PHPMailerAutoload.php');
$mail = new PHPMailer();
$mail->isSMTP();
include('main_setting.php');
$mail->Subject = "Ticket closed || KL Alumni Association";
$html='
<html>
<head>
<style>
.mail_main_box
{
width: 600px;
height: auto;
margin: auto;
border: 1px solid #ebebeb;
}

a {
    text-decoration: none;
}
p > a:hover{
    text-decoration:  underline;
}
h1,
h2,
h3,
h4,
h5,
h6 {
    margin:  1% 0 1% 0;
}
._12 {
    font-size: 1.2em;
}
._14 {
    font-size: 1.4em;
}
ul {
    padding:0;
    list-style: none;
}
.footer-social-icons {
    width: 350px;
    display:block;
    margin: 0 auto;
}
.social-icon {
    color: #fff;
}
ul.social-icons {
    margin-top: 10px;
}
.social-icons li {
    vertical-align: top;
    display: inline;
    height: 100px;
}

.social_icons
{
width: 30px;
 height: 30px; 
 border-radius:30px;
 background-color: black; 
 display: inline-block;
 position: relative;
}
.social_icons img
{
width: 20px;
height: auto; 
margin: auto; 
margin-top: 5px;
}
.notme
{
font-size: 17px;
cursor: pointer;
border-radius: 5px;
padding: 2px 18px 2px 18px;
background-color: #5dadec;
color: white;
border: 1px solid #5dadec;
}
</style>
<link href="https://fonts.googleapis.com/css?family=Lato|Roboto" rel="stylesheet">
</head>
<body>
<div class="mail_main_box">
<div style="width: 100%; height: 80px; text-align: center;">
<div style="width: auto; height: 40px;  margin-top: 10px; display: inline-block; position: relative;">
<img src="https://klfocus.in/css/images/klu.png" style="width: auto; margin: 0px; height: 40px; float: left;" />
<strong><p style="font-family: '."'Roboto'".', sans-serif; float: left; padding-left: 10px;">Alumni Association</p></strong>
</div>
</div>
<img src="https://i.ibb.co/zbqCVY2/closed.png" style="width: 100%; height: auto;"/>
<div style="width: 580px; height: auto; padding-bottom: 20px; border: 10px solid #f7f7f7;">
<p style="font-family: '."'Lato'".',Arial,sans-serif; font-size: 13px; padding: 0px 10px 0px 10px;"><b>Query Status: Closed</b></p>

<div style="width: 100%; height: auto;">
<p style="font-family: '."'Lato'".',Arial,sans-serif; font-size: 13px; padding: 0px 10px 0px 10px;">'.$content.'</p>
</div>
<div style="width: 100%; height: auto; text-align: center;">

</div>
</div>
<div style="width: 600px; height: 110px; background-color: #f7f7f7;">
<div style="width: 100%; height: 50px;">
<div style="width: 50%; height: 50px; float: left; text-align: center;">
 <ul class="social-icons" style="display: inline-block; margin-top: 5px; text-align: center;">
        <li><a href="https://www.facebook.com/KLUniversity/" target="_blank" ><div class="social_icons"><img src="https://i.ibb.co/6wP5vsT/facebook.png" /></div></a></li>
        <li><a href="https://twitter.com/kluniversity?lang=en" target="_blank" class="social-icon"><div class="social_icons"><img src="https://i.ibb.co/vwXJZpM/twitter.png" /></div></a></li>
        <li><a href="https://www.youtube.com/channel/UCDpAsRnAnV6ey0r-BaIfzkA" target="_blank" class="social-icon"><div class="social_icons"><img style="width: 16px; height: auto;" src="https://i.ibb.co/VHPQQFK/youtube.png" /></div></a></li>
		<li><a href="https://www.instagram.com/kluniversityofficial/?hl=en" target="_blank" class="social-icon"> <div class="social_icons"><img src="https://i.ibb.co/2dshfnW/insta.png" /></div></a></li>
    </ul>
</div>
<div style="width: 50%; height: 50px; float: left;">
<p style="font-family: '."'Lato'".', sans-serif; font-size: 13px; text-align: center; color: #2e2e2e;">&copy; KL Alumni Association</p>
</div>
</div>
<div style="width: 100%; height: 60px;">
<p style="font-family: '."'Roboto'".', sans-serif; color: #5b5b5b; line-height: 15px; word-spacing: 1px; margin: 0px; font-size: 11px; padding: 10px 10px 0px 10px;">If you have any further queries, please contact: contact@alumni.kluniversity.in, our IT department will answer your questions within one working day. </p>
</div>
</div>
</div>
</body>
</html>
';
$mail->Body = $html;
$mail->isHTML(true);
$mail->addAddress($email);
$mail->send();
echo $mail->ErrorInfo;
$mail->ClearAllRecipients();
$mail->ClearAttachments();
?>

