<html>
<head>
<?php
error_reporting(0);
require("js/php/conn.php");
session_start();
if($_SESSION['alu_auth']==null)
{
	header('location: login.php');
}
$session=$_SESSION['alu_auth'];
$get_d=mysqli_query($conn,"select * from user_security where hash_key='$session';");
$get_d=mysqli_fetch_assoc($get_d);
$session=$get_d['email'];
date_default_timezone_set('Asia/Kolkata');
$date=date('Y-m-d H:i:s');
mysqli_query($conn,"update profile set active_status='$date' where email='$session';");

?>
<?php require('js/php/get_profile_data.php'); ?>
<title>KL Alumni Association | KL University</title>
<style>
.top_nav
{
	width: 100%;
	height: 50px;
	max-width: 1800px;
	text-align: center;
	background: white;
	z-index:1;
	margin: auto;
	box-shadow: 0px 1px 5px #cccccc;
}
.gap
{
	width: 100%;
	height: 50px;
}
.main_posts
{
	width: 1200px;
	margin: auto;
	height: auto;
}
.profile
{
	width: 350px;
	height: 500px;
	position: absolute;
	right: 150px;
	position: fixed;
	margin-top: 40px;
	background: blue;
}
.posts
{
	width: 850px;
	height: auto;
	float: left;
}
.post_style
{
	width: 650px;
	height: auto;
	box-shadow: 0px 0px 3px #cccccc;
	border-top-left-radius: 5px;
	border-top-right-radius: 5px;
	background: white;
	margin-left: auto;
	margin-right: auto;
	margin-top: 40px;
}
.post_top
{
	width: 100%;
	height: 50px;
	border-bottom: 1px solid #cccccc;
	border-top-left-radius: 20px;
	border-top-right-radius: 20px;
	background: white;
}
.profile_logo
{
	width: 35px;
	height: 35px;
	position: relative;
	top: 5px;
	float: left;
	border-radius: 35px;
	left: 10px;
	overflow: hidden;
}
.profile_details
{
	width: auto;
	height: 100%;
	float: left;
	margin-left: 15px;
}
.post_body
{
	width: 100%;
	height: auto;
}
.post_image
{
	width: 100%;
	height: auto;
}
.post_text
{
	width: 100%;
	padding-bottom: 5px;
	height: auto;
}
.post_like
{
	width: 100%;
	height: 35px;
}
.search
{
	width: 300px;
	height: 100%;
	text-align: center;
	float: left;
	margin-left: 60px;
}
input:focus
{
	outline: none;
}
.icons
{
	width: 100px;
	height: 100%;
	margin-left: 20px;
	float: right;
}
.post_new
{
	width: 650px;
	height: auto;
	margin: auto;
	border-radius: 5px;
	margin-top: 10px;
	box-shadow: 0px 0px 3px 0px #cfcfcf;
	background: white;
}
.postedby
{
	width: 100%;
	height: 50px;
	display: inline-block;
	box-shadow:  0px 1px 0px 0px #cfcfcf;
}
.postedby_photo
{
	width: 40px;
	height: 40px;
	position: relative;
	top:5px;
	left: 15px;
	float: left;
	overflow: hidden;
	border-radius: 50px;
}
.postedby_name
{
	width: auto;
	height: 100%;
	margin-left: 25px;
	float: left;
}
.attachment
{
	width: 100%;
	height: 47px;
	display: inline-block;
	border-bottom-left-radius: 20px;
	border-bottom-right-radius: 20px;
}
.photo_upload
{
	width: 120px;
	height: 40px;
	border-radius: 18px;
	float: left;
	cursor: pointer;
	margin-top: 3px;
	margin-left: 10px;
	background: #f4f4f4;
}
.post
{
	width: 80px;
	padding: 5px;
	background:#62c5c5;
	color: white;
	margin-right: 10px;
	margin-top: 10px;
	border: 1px solid #62c5c5;
	border-radius:10px;
	font-size: 15px;
	cursor: pointer;
}
#search_show
{
	box-shadow: 0px 0px 5px 0px #ededed;
}
.search_filter_list:hover
{
	background-color: #d5d5d5;
}
*:focus
{
	outline: none;
}
.status_upload_box
{
	width: 250px;
	height: 20px;
	margin: 10px 0px 10px 10px;
	box-shadow: 0px 0px 5px 0px #e1e1e1;
	background-color: white;
}
#upload_status_box
{
	width: 100%;
	height: auto;
}
.upload_file_name
{
	width: 230px;
	height: 20px;
	float: left;
}
.upload_cancel
{
	width: 20px; 
	height: 20px;  
	float: right;
	cursor: pointer;
}
.upload_cancel:hover
{
	background-color: #e1e1e1;
}
.side_nav
{
	width:340px;
	height: 500px;
	margin-top: 50px;
	display: inline-block;
	background-color: #ffffff;
	box-shadow: 0px 0px 2px 0px #d4d4d4;
}
.popular_profile_box_profile
	{
	    width: 100%;
		height: 60px;
		background-color: white;
		margin: 2px 0px 2px 0px;
		cursor: pointer;
	}
#nav_unread_message_status
{
	width: 15px;
	height: 15px;
	background-color: #ff5757;
	position: absolute;
	top: 8px; 
	right: 5px;
	border-radius: 20px;
}
.like_style:hover
{
background-color: #f4f4f4;
}
</style>
  <meta charset="utf-8">
  <link href="https://fonts.googleapis.com/css?family=Baloo+Chettan" rel="stylesheet">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <style>
  #myCarousel
  {
  width: 100%;
  height: auto;
  }
  .loader{
  width: 100px;
  height: 100px;
  border-radius: 100%;
  position: relative;
  margin: 0 auto;
}


#loader-4 span{
  display: inline-block;
  width: 20px;
  height: 20px;
  border-radius: 100%;
  background-color: #3498db;
  margin: 35px 5px;
  opacity: 0;
}

#loader-4 span:nth-child(1){
  animation: opacitychange 1s ease-in-out infinite;
}

#loader-4 span:nth-child(2){
  animation: opacitychange 1s ease-in-out 0.33s infinite;
}

#loader-4 span:nth-child(3){
  animation: opacitychange 1s ease-in-out 0.66s infinite;
}

@keyframes opacitychange{
  0%, 100%{
    opacity: 0;
  }

  60%{
    opacity: 1;
  }
}

  </style>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript">
$(document).ready(function (e) {
	$("#uploadForm").on('submit',(function(e) {
		document.getElementById("file_loading").style.display="block";
		e.preventDefault();
		$.ajax({
        	url: "js/php/upload_posts.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				document.getElementById('final_upload_file_code').value=data;
				if(data!="")
				{
				document.getElementById("file_loading").style.display="none";
				}
		    },
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
	$("#uploadForm1").on('submit',(function(e) {
		document.getElementById("file_loading").style.display="block";
		e.preventDefault();
		$.ajax({
        	url: "js/php/upload_posts.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				document.getElementById('final_upload_file_code').value=data;
				if(data!="")
				{
				document.getElementById("file_loading").style.display="none";
				}
		    },
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});

</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<style>
body
{
background-color: #fcfcfc;
}
</style>
<body>
<div style="width: 100%; height: 100%; background-color: rgba(0,0,0,0.5); z-index: 5; position: fixed; display: none;" id="file_loading">
<div style="width: 500px; height: 280px; background-color: white; position: absolute; margin: auto; top: 0px;left:0px; bottom: 0px; right: 0px;">
<div style="width: 100%; height: 80px; text-align: center;">
<div style="width: 250px; height: 50px; margin: auto;">
<img src="imags/klu.png" style="height:40px; width: auto; margin-top: 10px; float: left;"/>
<p style=" float: left; padding-top: 30px; font-size: 15px;">Alumni Association</p>
</div>
<div class="loader" id="loader-4" style="margin-top: 50px;">
          <span></span>
          <span></span>
          <span></span>
        </div>
		<p>Please wait while loading ..</p>
</div>
</div>
</div>
<div style="width: 100%; height: 50px; position: fixed; z-index: 1;">
<div class="top_nav">
<img onclick="window.location='news.php'" src="imags/klu.png" style="height: 90%; cursor: pointer; width: auto; margin-left: 10px; margin-top: 3px; float: left;" />
<p onclick="window.location='news.php'" style="margin: 0px; cursor: pointer; font-family: 'Lato', sans-serif; font-size: 20px; padding-top: 12px; float: left;"><strong>Alumni Association</strong></p>
<div class="search">
<input type="text" onkeyup="search_result(this);" id="alumni_search" style="width:300px;font-size: 16px; padding: 5px; border: 1px solid #d7d7d7; padding-left: 10px; border-radius: 5px;  margin-top: 10px;" placeholder="Search"></input>
<div id="search_show" style="width: auto; height: auto; background-color: red; margin-top: 3px;">
</div>
</div>
<div style="width: auto; height: 100%; float: left;">
</div>
<div class="icons">
<div style="width: auto; height: 100%;float: left;">
<a href="main_p.php">
<div style=" width: 30px;  height: 30px; cursor:pointer; background: pink; overflow: hidden; border-radius: 30px; margin-top: 10px;">
<img src="<?php echo $profile_pic;?>" style="width: 100%; height: auto;">
</div>
</a>
</div>
<a href="logout.php">
<div style="width: 50px; height: 100%; float: right; position: relative;">
<img src="imags/logout_c.png" style="width: auto; margin-top: 10px; height: 30px;" />
</div>
</a>
</div>
<a href="mypost.php">
<div class="nav_icons">
<div style="width: auto; height: 100%; float: right;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; margin: 0px; padding-top: 13.5px; color: #8d8d8d;">My Posts</p>
</div>
<div style="width: 50px; height: 100%; float: right; position: relative;">
<img src="imags/my_post.png" style="width: 35px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;" />
</div>
</div>
</a>
<a href="network.php">
<div class="nav_icons">
<div style="width: auto; height: 100%; float: right;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; margin: 0px; padding-top: 13.5px; color: #8d8d8d;">My Network</p>
</div>
<div style="width: 50px; height: 100%; float: right; position: relative;">
<img src="imags/mynetwork.png" style="width: 35px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;">
<?php
$temp_r=$request;
$temp_r=explode(",",$temp_r);
if(sizeof($temp_r)>=1 and $temp_r[0]!='')
{
	if(sizeof($temp_r)>9)
	{
		echo '
	<div id="nav_unread_message_status">
	<p style="margin: 0px; text-align: center; font-size: 9px; color: white; font-family: arial; padding-top: 1px;"><b>9+</b></p>
	</div>
	';
	}
	else
	{
		echo '
	<div id="nav_unread_message_status">
	<p style="margin: 0px; text-align: center; font-size: 9px; color: white; font-family: arial; padding-top: 1px;"><b>'.sizeof($temp_r).'</b></p>
	</div>
	';
	}
}
?>
</div>
</div>
</a>
<a href="news.php">
<div class="nav_icons">
<div style="width: auto; height: 100%; float: right;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; margin: 0px; padding-top: 13.5px; color: #8d8d8d;">Home</p>
</div>
<div style="width: 50px; height: 100%; float: right; position: relative;">
<img src="imags/home.png" style="width: 35px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;">
</div>
</div>
</a>
</div>
</div>


<div class="gap">
</div>

<div class="main_posts">
<div class="side_nav">
<p style="padding-top: 10px; font-size: 15px; padding-left: 10px;">Popular Profiles</p>
<div style="width: 95%; height: 2px; background-color: #d4d4d4; margin: auto;">
</div>
<div style="width: 100%; height: 450px; margin-top: 10px;">
<?php
require('js/php/conn.php');
$get_popular_profiles=mysqli_query($conn,"select profile_pic,profile_name,town,personal.country,hash_key from profile 
inner join auth on auth.email=profile.email 
inner join personal on personal.email=profile.email 
where auth.status='VERIFIED' order by profile.visitors_count DESC limit 6;");
while($get_popular=mysqli_fetch_array($get_popular_profiles))
{
	$profile_pi=$get_popular[0];
	$get_name=$get_popular[1];
	$get_town=$get_popular[2];
	$get_country=$get_popular[3];
	$get_hash=$get_popular[4];
	echo '
	<a href="profile.php?getdetails='.$get_hash.'">
	<div class="popular_profile_box_profile">
<div style="width: 35px; height: 35px; margin-top: 10px; position: relative; float:left; border-radius: 60px; overflow: hidden; margin-left: 20px;">
<img src="'.$profile_pi.'" style="width: 100%; height: auto; position: absolute; top:0px; left: 0px; right: 0px; bottom: 0px; margin: auto;"/>
</div>
<div style="width:280px; float:left; height: 100%;">
<div style="width: 100%; height: 25px; float: left; margin-top: 5px;">
<p style="font-family: '."'Open Sans'".', sans-serif; float: left; margin: 0px; margin-left: 10px; border: none; color: black; font-size: 16px;">'.$get_name.'</p>
<img src="imags/verify.png" style="width: auto; margin-top: 5px; float: left; margin-left: 5px; height: 60%;" />
</div>
<p style="font-family: '."'Open Sans'".', sans-serif; float: left; margin: 0px; margin-left: 10px; border: none; color: black; font-size: 12px;">'.$get_town.', '.$get_country.'</p>
</div>
</div>
</a>
	';
}
?>
</div>
</div>
<div class="posts">
<div class="post_new">
<div class="postedby">
<div class="postedby_photo">
<img src="<?php echo $profile_pic;?>" style="width: 100%; height: auto;"/>
</div>
<div class="postedby_name">
<p style="font-family: 'Noto Sans', sans-serif; margin: 0px; font-size: 12px; padding-top: 11px;"><?php echo $profile_name; ?></p>
<p style="font-family: 'Noto Sans', sans-serif; margin: 0px; font-size: 9px; color: #989898;"><?php echo $current_job.', '.$current_company; ?></p>
</div>
</div>
<textarea placeholder="Write Someting Here" name="upload_post_desc" id="upload_post_desc" style="margin-top: 3px; display: inline-block; font-family: 'Noto Sans', sans-serif; padding-left: 20px; padding-top:10px; border: none; min-width: 100%;max-width: 100%; min-height: 90px; max-height: 90px;"></textarea>
<div style="width: 650px; height: auto; display: inline-block;" id="file_upload_status">

<div id="upload_status_box">
<div class="status_upload_box" id="status_upload_box" style="visibility: hidden;">
<div class="upload_file_name">
<strong><p style="margin: 0px; font-size: 11px; padding-top: 2px; padding-left: 10px; font-family: 'Lato', sans-serif;"></p></strong>
</div>
<div class="upload_cancel" onclick="close_upload(this);">
<p style="text-align: center; font-size: 10px; font-family: 'Lato', sans-serif; margin: 0px; padding-top: 3px;">X</p>
</div>
</div>
</div>

</div>
<div class="attachment">
<div class="photo_upload" style="position: relative;">
<form id="uploadForm" action="news.php" method="post" style="width: 100%; height: 100%; opacity: 0; cursor: pointer; z-index: 1; position: absolute; top: 0px;" >
<input name="userImage" id="upload_files" onchange="upload(this);" type="file" class="inputFile" accept="image/*" style="width: 100%; height: 100%; opacity: 0; cursor: pointer; z-index: 1; position: absolute; top: 0px;"/>
<input type="submit" style="visibility: hidden;" id="submit" value="Submit" class="btnSubmit" style="width: 100%; height: 100%; opacity: 0; cursor: pointer; z-index: 1; position: absolute; top: 0px;" />
</form>

<img src="imags/camera.png" style="height: auto; float: left; width: 25px; margin-top: 13px; margin-left: 12px;"/>
<p style="color: #565656; margin: 0px; padding-top: 9px; padding-left: 45px; font-family: 'Noto Sans', sans-serif;">Photos</p>

</div>
<div class="photo_upload" style="position: relative">
<form id="uploadForm1" action="news.php" method="post" style="width: 100%; height: 100%; opacity: 0; cursor: pointer; z-index: 1; position: absolute; top: 0px;" >
<input name="userImage" id="upload_files1" onchange="upload(this);" type="file" class="inputFile" accept="files/*" style="width: 100%; height: 100%; opacity: 0; cursor: pointer; z-index: 1; position: absolute; top: 0px;"/>
<input type="submit" style="visibility: hidden;" id="submit1" value="Submit" class="btnSubmit" style="width: 100%; height: 100%; opacity: 0; cursor: pointer; z-index: 1; position: absolute; top: 0px;" />
</form>
<img src="imags/file.png" style="height: 22px; float: left; width: auto; margin-top: 9px; margin-left: 12px;"/>
<p style="color: #565656; margin: 0px; padding-top: 9px; padding-left: 45px; font-family: 'Noto Sans', sans-serif;">Add file</p>
</div>
<input name="final_upload_file" id="final_upload_file" style="visibility: hidden; position: absolute; bottom: 0px;"></input>
<input name="final_upload_file_code" id="final_upload_file_code" style="visibility: hidden; position: absolute; bottom: 0px;"></input>
<button onclick="final_upload_file();" style="float: right;" class="post">POST</button>
</div>
</div>


<script>
var upload_push=[];
var c=0;
function final_upload_file()
{
	var final_upload_file_names=document.getElementById('final_upload_file').value;
	var final_submit_code=document.getElementById('final_upload_file_code').value;
	var upload_post_desc=document.getElementById('upload_post_desc').value;
	$.ajax({
		type: 'post',
		 url: 'js/php/final_submit_post.php',
		 data : {final_upload_file_names:final_upload_file_names, final_submit_code: final_submit_code, upload_post_desc: upload_post_desc},
		 success: function(code){
		 location.reload();
		 }
		 });
		
	
}
function search_result(m)
{
var m=m.value
$.ajax({
type: 'post',
 url: 'js/php/search.php',
 data : {m:m},
 success: function(code){
 $('#search_show').html(code);
 }
 });
 if(m=="")
 {
document.getElementById('search_show').style.display="none";
 }
 else
 {
	document.getElementById('search_show').style.display="block";
 }
}
function upload(a)
{
	var file_name=a.files[0].name;
	var file_size=a.files[0].size;
	file_name=file_name.replace(/[^A-Z0-9.]/ig,"");
	var copy=document.getElementById('status_upload_box');
	var m=copy.cloneNode(true);
	m.id=file_name;
	upload_push.push(file_name);
	m.style.visibility="visible";
	document.getElementById('upload_status_box').appendChild(m);
	var file_n=document.getElementById(''+file_name);
	file_n.querySelector('.upload_file_name p').innerHTML=file_name+' ('+file_size+')';
	document.getElementById('final_upload_file').value=upload_push;
	document.getElementById('submit').click();
	document.getElementById('submit1').click();
}
function close_upload(a)
{
	var temp=a.parentElement.id;
	var c=0;
	document.getElementById(""+temp).style.display='none';
	for(var i=0;i<upload_push.length;i++)
	{
		if(upload_push[i]==temp)
		{
			c=i;
			break;
		}
	}
	for(var j=c;j<upload_push.length;j++)
	{
		upload_push[i]=upload_push[i+1];
	}
	upload_push.pop();
	document.getElementById('final_upload_file').value=upload_push;
}

</script>





<div id="post_dis" style="width: 100%; height: auto;">
</div>

</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
var limit_s=0;
var limit_e=20;
var pix=2000;
function display_post(limit_s,limit_e)
{
	 $.ajax({
	 type: 'post',
	 url: 'js/php/news_update.php',
	 data : {limit_s:limit_s, limit_e:limit_e},
	 success: function(code){
	 var add=document.getElementById("post_dis");
	 add.innerHTML+=code;
	 limit_s=limit_e;
	 }
	 });
}
display_post(limit_s,limit_e);

$(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
   if(scroll>=pix)
   {
	   limit_s=limit_e;
	   limit_e=limit_e+20;
	   display_post(limit_s,limit_e);
	   pix=pix+1000;
	   
   }
});

function like(a)
{
  var cod=a.id;	
  var like=a.style.WebkitFilter;
  var m=0;
  if(like=="grayscale(100%)")
  {
  a.style.filter = "grayscale(0%)";
  a.style.WebkitFilter = "grayscale(0%)";
  m=1;
  }
  else
  {
  a.style.filter = "grayscale(100%)";
  a.style.WebkitFilter = "grayscale(100%)";
  m=0;
  }
  
    $.ajax({
	 type: 'post',
	 url: 'js/php/like.php',
	 data : {cod:cod},
	 success: function(code){
	 document.getElementById(cod+"_count").innerHTML=code+' Likes';
	 }
	 });
  
}
</script>



</body>
</html>
