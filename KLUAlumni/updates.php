<html>
<head>
<title>KL Alumni Association | K L University</title>
<meta http-equiv="expires" content="0">
<?php
clearstatcache();
?>
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<link rel="stylesheet" type="text/css" href="css/index.css">
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">	
<link href="https://fonts.googleapis.com/css?family=Noto+Sans|Roboto" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Baloo+Chettan|Montserrat" rel="stylesheet">
<script src="js/index_dropdown_menu.js"></script>
</head>
<body>
<div class="icon_dis" style="cursor: pointer;">
<div class="main_icon" onclick="window.location='index.php'">
<img src="imags/klu.png" />
<strong><p style="text-decoration: none;">Alumni Association</p></strong>
</div>
</div>
<div class="dropdownmenu">
 <nav>
      <div class="nav-mobile"><a id="nav-toggle" href="#!"><span></span></a></div>
      <ul class="nav-list" >
        <li>
          <a href="#" style="text-decoration: none;">Carrers</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="careers.php" style="text-decoration: none;">Internship</a>
            </li>
            <li>
              <a href="careers.php" style="text-decoration: none;">Jobs</a>
            </li>
            <li>
              <a href="careers.php" style="text-decoration: none;">Mentorship</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="advisoryboard.php" style="text-decoration: none;">Board Members</a>
        </li>
        <li>
          <a href="#!" style="text-decoration: none;">Events</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="events.php" style="text-decoration: none;">Alumni Event Calender</a>
            </li>
			<li><a href="events.php" style="text-decoration: none;">All Events</a></li>
			<li>
			  <a href="https://kluniversity.in/site/acadcal.htm" style="text-decoration: none;">Academic Calender</a></li>
          </ul>
        </li>
		 <li>
              <a href="#" style="text-decoration: none;">Alumni Stories</a>
            </li>
            <li>
              <a href="gallery.php" style="text-decoration: none;">Gallery</a>
            </li>
         <li>
              <a href="updates.php" style="text-decoration: none;">News & Updates</a>
            </li>
        <li>
          <a href="aboutus.html" style="text-decoration: none;">About us</a>
        </li>
      </ul>
    </nav>
</div>
<p style=" margin-left: 20px; padding-top: 10px; font-size: 20px; width: 200px; padding-bottom: 5px; border-bottom: 2px solid #9b9b9b;">News and Updates</p>
<?php
require('js/php/conn.php');
$get_data=mysqli_query($conn,"select file,title,url from news order by timestamp DESC limit 6;");
while($get_news_data=mysqli_fetch_array($get_data))
{
	$title=$get_news_data[1];
	$file="cms/news/".$get_news_data[0];
	$url=$get_news_data[2];
	if($url=='' or $url==null)
	{
	echo '
	<a href="'.$file.'" style="text-decoration: none;"><li style="color: black; list-style: none; margin: 0px 5px 5px 20px;">'.$title.'</li></a>
	';
	}
	else
	{
		echo '
		<a href="'.$url.'" style="text-decoration: none"><li style="color: black; list-style: none; margin: 0px 5px 5px 20px;">'.$title.'</li></a>
		';
	}
}
?>
</head>
</html>