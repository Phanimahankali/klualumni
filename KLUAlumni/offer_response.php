<?php
require('js/php/conn.php');
$hash=$_REQUEST['h'];
error_reporting(0);
if($hash=='')
{
	header("Location:careers.php");
}
$get_hash_details=mysqli_query($conn,"select * from offer where hash='$hash';");
$get_hash_details=mysqli_fetch_array($get_hash_details);
$offer_code=$get_hash_details[0];
$job_name=$get_hash_details[2];
$org_name=$get_hash_details[1];
$location=$get_hash_details[6];
$com_logo=$get_hash_details[7];
$check=mysqli_query($conn,"select code from offer where hash='$hash';");
if(mysqli_num_rows($check)==1)
{
	$name=$_REQUEST['name'];
	$gender=$_REQUEST['gender'];
	$email=$_REQUEST['email'];
	$university=$_REQUEST['university'];
	$regno=$_REQUEST['register_no'];
	$year=$_REQUEST['year'];
	$stream=$_REQUEST['stream'];
	$branch=$_REQUEST['branch'];
	$passing_year=$_REQUEST['passing'];
	$phone=$_REQUEST['phone'];
	$resume=$_FILES['resume'];
	$size=$_FILES['resume']['size'];
	$file_name=$_FILES['resume']['name'];
	$ext = end((explode(".",$file_name)));
	if($name!='' and $gender!='' and $email!='' and $_FILES['resume']['error']==0 and $university!='' and $_FILES['resume']['name']!='' and $regno!='' and $year!='' and $stream!='' and $branch!='' and $passing_year!='' and $phone!='' and $ext=="pdf")
	{
		$check=mysqli_fetch_array($check);
		$code=$check[0];
		$hash_id=uniqid();
		mysqli_query($conn,"insert into offer_response (name,gender,email,university_name,reg_id,year,passing_out_year,phone,stream,branch,hash_id,size,response_to) 
		values ('$name','$gender','$email','$university','$regno','$year','$passing_year','$phone','$stream','$branch','$hash_id','$size','$code');");
		$getcode=mysqli_query($conn,"select response_to from offer_response where hash_id='$hash_id';");
		$getcode=mysqli_fetch_array($getcode);
		$getcode=$getcode[0];
		$file_temp_name=$_FILES['resume']['tmp_name'];
		move_uploaded_file($file_temp_name,"offers/".$code."/resumes/".$getcode.'.'.$ext);
				echo "
		<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
		<script>
		function send_main()
		{
		var hash='$hash_id';
		var cod='$getcode';
		$.ajax({
			type: 'post',
			 url: 'cms/mailer/offer_verification.php',
			 data : {hash:hash,cod:cod},
			 success: function(code){
			 
			 }
			 });
		}
		send_main();
		window.location = 'careers.php';
		</script>
		";
	}
	else if(($ext!='pdf' and $_FILES['resume']!='') or ($_FILES['resume']['error']==1))
	{
		if($_FILES['resume']['error']==1)
		{
			echo '
		<script>
		alert("Error in uploaded file");
		</script>
		';
		}
		else
		{
		echo '
		<script>
		alert("Resume will be in PDF format");
		</script>
		';
		}
	}
	else
	{
		
	}
}

else
{
	header("Location: careers.php");
}
?>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<style>
body
{
	margin: 0px;
	background-color: #f4f4f4;
}
.main_box
{
	width: 700px;
	height: auto;
	box-shadow: 0px 0px 3px 0px #b2b2b2;
	background-color: white;
	margin: auto;
	margin-top: 20px;
	margin-bottom: 50px;
}

.input-field {
  position: relative;
  width: 250px;
  height: 44px;
  line-height: 44px;
}
label {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  color: #3b3b3b;
  transition: 0.2s all;
  cursor: text;
}
input {
  width: 100%;
  border: 0;
  outline: 0;
  padding: 0.5rem 0;
  border-bottom: 2px solid #d3d3d3;
  box-shadow: none;
  color: #111;
}
input:invalid {
  outline: 0;
  // color: #ff2300;
  //   border-color: #ff2300;
}
input:focus,
input:valid {
  border-color: #70bbe6;
}
input:focus~label,
input:valid~label {
  font-size: 12px;
  top: -24px;
  color: black;
}
.upload_resume
{
	background-color: #dddddd;
	border: 1px solid #dddddd;
	cursor: pointer;
	color: #3d3d3d;
	margin-top: 10px;
	padding: 5px 13px 5px 13px;
	border-radius: 5px;
	font-family: arial;
	font-size: 15px;
}
*:focus
{
	outline: none;
}
.submit_application
{
	margin: 20px;
	font-family: arial;
	font-size: 16px;
	background-color: #ffbd59;
	color: white;
	padding: 4px 20px 4px 20px;
	border: 1px solid #ffbd59;
	cursor: pointer;
	box-shadow: 0px 0px 15px 0px #9f9f9f;
	transition: 0.5s;
}
.submit_application:hover
{
	box-shadow: 0px 0px 3px 0px #9f9f9f;
	transition: 0.5s;
}
ol
{
	padding-left: 40px;
	padding-right: 40px;
	font-family: 'Roboto', sans-serif;
	font-size: 13px;
}
li
{
	margin-top: 5px;
}
</style>
</head>
<body>
<div style="width: 100%; height: 50px; background-color: white; box-shadow: 0px 0px 5px 0px #a2a2a2;">
<a href="careers.php"><p style="font-family: 'Noto Sans', sans-serif; font-size: 25px; cursor: pointer;  color: #646464; padding-top: 6px; margin: 0px; padding-left: 15px; float: left;">X</p></a>
<p style="font-family: 'Noto Sans', sans-serif; font-size: 17px; padding-top: 10px; margin: 0px; padding-left: 30px; float: left;">Application Form</p>
</div>
<div class="main_box">
<div style="width: 100%; height: 100px; border-bottom: 5px solid  #f1f1f1;">
<div style="width: 100px; height: 100px; position: relative; float: left; ">
<div style="width: 70px; height: 70px;  position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px; margin: auto;">
<div style="width: 100%; height: 100%; position: relative; top: 0px;">
<img src="offers/<?php echo $offer_code.'/'.$com_logo; ?>" style="width: 100%; height: auto; position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px; margin: auto;" />
</div>
</div>
</div>

<div style="float: left; width: auto; height: 100%; ">
<p style="font-family: arial; font-size: 15px; margin: 0px; padding-left: 15px; padding-top: 22px;"><?php echo $job_name; ?></p>
<p style="font-family: arial; font-size: 13px; margin: 0px; padding-left: 15px; padding-top: 10px;"><?php echo $org_name; ?></p>
<p style="font-family: arial; font-size: 13px; margin: 0px; padding-left: 15px; padding-top: 5px;"><?php echo $location; ?></p>
</div>
</div>
<form action="offer_response.php" method="post" enctype="multipart/form-data">
<div style="width: 100%; height: auto;">
  <div class="input-field" style="margin-top: 25px; margin-left: 20px;">
    <input type="text" name="name" id="name" style="padding-top: 15px; color: #898989; font-size: 15px;" required />
    <label for="name" style="font-family: arial; font-size: 14px;">Name: *</label>
  </div>
</div>
<div style="width: 100%; height: auto; margin-top: 40px;">
  <div class="input-field" style="margin-top: 35px; margin-left: 20px;">
    <input type="text" name="gender" list="gender_list" id="gender" style="padding-top: 15px; color: #898989; font-size: 15px;" required />
    <label for="gender" style="font-family: arial; font-size: 14px;">Gender: *</label>
  </div>
</div>
<div style="width: 100%; height: auto;">
  <div class="input-field" style="margin-top: 35px; margin-left: 20px;">
    <input type="email" id="email" name="email" style="padding-top: 15px; color: #898989; font-size: 15px;" required />
    <label for="email" style="font-family: arial; font-size: 14px;">Email: *</label>
  </div>
</div>
<datalist id="gender_list">
<option value="Male">Male</option>
<option value="Female">Female</option>
<option value="Others">Others</option>
</datalist>
<div style="width: 100%; height: auto;">
  <div class="input-field" style="margin-top: 35px; margin-left: 20px;">
    <input type="text" id="University" name="university" style="padding-top: 15px; color: #898989; font-size: 15px;" required />
    <label for="University" style="font-family: arial; font-size: 14px;">University Name: *</label>
  </div>
</div>
<div style="width: 100%; height: auto;">
  <div class="input-field" style="margin-top: 35px; margin-left: 20px;">
    <input type="text" id="Register_no" name="register_no" style="padding-top: 15px; color: #898989; font-size: 15px;" required />
    <label for="Register_no" style="font-family: arial; font-size: 14px;">University Register Number: *</label>
  </div>
</div>
<div style="width: 100%; height: auto;">
  <div class="input-field" style="margin-top: 35px; margin-left: 20px;">
    <input list="year_list" name="year" type="text"  id="Year" style="padding-top: 15px; color: #898989; font-size: 15px;" required />
    <label for="Year" style="font-family: arial; font-size: 14px;">Year: *</label>
  </div>
</div>
<datalist id="year_list">
<option value="II">II</option>
<option value="III">III</option>
<option value="IV">IV</option>
</datalist>
<div style="width: 100%; height: auto;">
  <div class="input-field" style="margin-top: 35px; margin-left: 20px;">
    <input type="text" id="stream" name="stream" style="padding-top: 15px; color: #898989; font-size: 15px;" required />
    <label for="stream" style="font-family: arial; font-size: 14px;">Stream: *</label>
  </div>
</div>
<div style="width: 100%; height: auto;">
  <div class="input-field" style="margin-top: 35px; margin-left: 20px;">
    <input type="text" id="Branch" name="branch" style="padding-top: 15px; color: #898989; font-size: 15px;" required />
    <label for="Branch" style="font-family: arial; font-size: 14px;">Branch: *</label>
  </div>
</div>
<div style="width: 100%; height: auto;">
  <div class="input-field" style="margin-top: 35px; margin-left: 20px;">
    <input type="number" name="passing" id="Passing" style="padding-top: 15px; color: #898989; font-size: 15px;" required />
    <label for="Passing" style="font-family: arial; font-size: 14px;">Passing out year: *</label>
  </div>
</div>
<div style="width: 100%; height: auto;">
  <div class="input-field" style="margin-top: 35px; margin-left: 20px;">
    <input type="text" name="phone" id="Phone" style="padding-top: 15px; color: #898989; font-size: 15px;" required />
    <label for="Phone" style="font-family: arial; font-size: 14px;">Phone Number: *</label>
  </div>
</div>
<div style="width: 100%; height: auto; margin-top: 30px;">
<fieldset style="border: none; position: relative;">
<legend><p style="margin: 0px; font-family: arial; font-size: 14px; float: left; color: red;">*</p><p style="font-family: arial; font-size: 14px;margin: 0px; float: left;"> Resume</p></legend>
<button  type="file" class="upload_resume" style="float: left;">Upload file</button>
<p id="resume_name" style="float: left; padding-left: 10px; font-family: arial; font-size: 15px;"></p>
<input required onchange="resume_name(this);" name="resume" type="file" style="z-index: 10; width: 100px; cursor: pointer; position: absolute; left: 17px; top: 29px; opacity: 0; "></input>
</fieldset>
</div>
<p style="font-family: 'Roboto', sans-serif; font-size: 15px; padding-left: 20px;">Note :</p>
<ol>
<li>Please enter the details above that appears on your official documents, such as your driver’s license or passport.</li>
<li>After submitting the application form. please verfiy the link send to your email address.</li>
</ol>
<input name="h" value="<?php echo $hash;?>" style="opacity: 0;"></input>
<div style="width: 100%; height: auto; text-align: right;">
<button class="submit_application">Submit</button>
</div>
</form>
</div>
<script>
function resume_name(a)
{
	var name=a.files[0].name;
	document.getElementById("resume_name").innerHTML=name;
}
</script>
</body>
</html>