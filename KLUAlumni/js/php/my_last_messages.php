<html>
<head>
<style>
body
{
	margin: 0px;
}
</style>
<link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
</head>
<body>
<?php
require('conn.php');
session_start();
$session=$_SESSION['alu_auth'];
$verify=mysqli_query($conn,"select * from user_security where hash_key='$session';");
$verify=mysqli_fetch_assoc($verify);
$session=$verify['email'];
date_default_timezone_set('Asia/Kolkata');
$date=date('Y-m-d H:i:s');
mysqli_query($conn,"update profile set active_status='$date' where email='$session';");
$mycode=mysqli_query($conn,"select code from profile where email='$session';");
$mycode=mysqli_fetch_array($mycode);
$mycode=$mycode[0];
$get_last_messages=mysqli_query($conn,"select * from chat_messages where from_user_id='$mycode' group by to_user_id;");
while($mdata=mysqli_fetch_assoc($get_last_messages))
{
	$to_id=$mdata['to_user_id'];
	$get_message=mysqli_query($conn,"select * from chat_messages where from_user_id='$mycode' and to_user_id='$to_id' order by timestamp DESC;");
	$chat_d=mysqli_fetch_assoc($get_message);
	$chat_message=$chat_d['chat_message'];
	$time=$chat_d['timestamp'];
	$time=get_time_d($time);
	$get_to_user_details=mysqli_query($conn,"select profile_name,profile_pic from profile where code='$to_id';");
	$get_to_user_details=mysqli_fetch_array($get_to_user_details);
	$get_to_user_photo=$get_to_user_details[1];
	$get_to_user_name=$get_to_user_details[0];
	
	echo '
	
	<div onclick="';
	echo "get_new_message_box('".$to_id."');";
	echo'
	"; style="width: 100%; cursor: pointer; height: 50px; display: block;">
	<div style="width: 45px; height: 45px; border-radius: 45px; margin-top: 3px; position: relative; overflow: hidden; float: left;">
	<img src="'.$get_to_user_photo.'" style="width: 100%; height: auto; position: absolute; margin: auto; position: absolute; top: 0; left: 0; right: 0; bottom: 0;" />
	</div>
	<div style="width: auto; margin: 0px; float: left; height: 100%;">
	<p style="margin: 0px; font-family: '."'Noto Sans'".', sans-serif; padding-left: 15px; font-family: arial; padding-top: 3px; font-size: 13px;"><strong>'.$get_to_user_name.'</strong></p>
	<p style="margin: 0px; font-family: '."'Noto Sans'".', sans-serif; padding-left: 15px; font-family: arial; padding-top: 7px; font-size: 13px;">'.$chat_message.'</p>
	</div>
	<div style="width: auto; margin: 0px; float: right; height: 100%;">
	<p style="margin: 0px; font-family: '."'Noto Sans'".', sans-serif; padding-left: 15px; padding-top: 5px; padding-right: 5px; font-size: 13px;">'.$time.'</p>
	</div>
	</div>
	<div style="width: 100%; height: 1px; background-color: #ebebeb;">
	</div>
	
	
	';
	
}
function get_time_d($time)
{
date_default_timezone_set('Asia/Calcutta'); 
$t=date('Y-m-d H:i:s');
$datetime1 = new DateTime($t);
$datetime2 = new DateTime($time);
$interval = $datetime1->diff($datetime2);
$years=$interval->format('%y');
$months=$interval->format('%m');
$days=$interval->format('%d');
$hours=$interval->format('%h');
$minutes=$interval->format('%i');
	if($years>0)
	{
		if($months>=1)
		{
			return ("$years years ago");
		}
		else
		{
			return ("$years years ago");
		}
	}
	else if ($months>=1)
	{
		if($days>=1)
		{
			return ("$months months ago");
		}
		else
		{
			return ("$months months ago");
		}
	}
	else if($days>=1)
	{
		return("$days days");
	}
	else if($hours>=1)
	{
		return("$hours minutes ago");
	}
	else
	{
		return("$minutes minutes ago");
	}
}
?>
</body>
</html>
