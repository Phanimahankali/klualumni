
function onSignIn(googleUser) {
	var id_token = googleUser.getAuthResponse().id_token;
	$.ajax({
 type: 'post',
 url: 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='+id_token,
 success: function(code){
	var iss= code.iss;
	var aud=code.aud;
	var email_verified=code.email_verified;
	var email=code.email;
	var name=code.given_name+" "+code.family_name;
	var picture=code.picture;
	sent(iss,aud,email_verified,email,name,picture);
 }
 });
}
function sent(iss,aud,email_verified,email,name,picture)
{
	$.ajax({
	 type: 'post',
	 url: 'php/google_auth.php',
	 data: { iss :iss, aud:aud, email_verified:email_verified, email:email, name:name, picture:picture},
	success: function(code){
	document.getElementById('token').innerHTML=code;
 }
 });
}
