<!DOCTYPE html>
<?php
require_once('js/php/conn.php');
$code=$_GET['c'];
if($code=='')
{
	header('Location:gallery.php');
}
$get=mysqli_query($conn,"select * from gallery_photos where g_code='$code' and status='OK';");
if(mysqli_num_rows($get)==0)
{
	header('Location:gallery.php');
}
?>
<html lang="en">
  <head>
   
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">

    <link rel="stylesheet" href="gallery/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="gallery/css/animate.css">
    
    <link rel="stylesheet" href="gallery/css/owl.carousel.min.css">
    <link rel="stylesheet" href="gallery/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="gallery/css/magnific-popup.css">

    <link rel="stylesheet" href="gallery/css/aos.css">
<link rel="stylesheet" type="text/css" href="css/index.css">

 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<script src="js/index_dropdown_menu.js"></script>

    <link rel="stylesheet" href="gallery/css/ionicons.min.css">

    <link rel="stylesheet" href="gallery/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="gallery/css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="gallery/css/flaticon.css">
    <link rel="stylesheet" href="gallery/css/icomoon.css">
    <link rel="stylesheet" href="gallery/css/style.css">
  </head>
  <body>
<div class="icon_dis" style="cursor: pointer;">
<div class="main_icon" onclick="window.location='index.php'">
<img src="imags/klu.png" />
<strong><p style="text-decoration: none;">Alumni Association</p></strong>
</div>
</div>
<div class="dropdownmenu">
 <nav>
      <div class="nav-mobile"><a id="nav-toggle" href="#!"><span></span></a></div>
      <ul class="nav-list" >
        <li>
          <a href="#" style="text-decoration: none;">Carrers</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="careers.php" style="text-decoration: none;">Internship</a>
            </li>
            <li>
              <a href="careers.php" style="text-decoration: none;">Jobs</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="advisoryboard.php" style="text-decoration: none;">Board Members</a>
        </li>
        <li>
          <a href="#!" style="text-decoration: none;">Events</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="events.php" style="text-decoration: none;">Alumni Event Calender</a>
            </li>
			<li><a href="events.php" style="text-decoration: none;">All Events</a></li>
			<li>
			  <a href="https://kluniversity.in/site/acadcal.htm" style="text-decoration: none;">Academic Calender</a></li>
          </ul>
        </li>
		 <li>
              <a href="suess_stories.php" style="text-decoration: none;">Alumni Stories</a>
            </li>
            <li>
              <a href="gallery.php" style="text-decoration: none;">Gallery</a>
            </li>
         <li>
              <a href="updates.php" style="text-decoration: none;">News & Updates</a>
            </li>
        <li>
          <a href="aboutus.html" style="text-decoration: none;">About us</a>
        </li>
      </ul>
    </nav>
</div>
	<div id="colorlib-page">
		

		<div id="colorlib-main">
		
			<section class="ftco-section-2">
				<div class="photograhy">
					<div class="row no-gutters">
						<?php
						while($g=mysqli_fetch_assoc($get))
						{
							$p_code=$g['code'];
							$p_code=$p_code.'.'.$g['file_type'];
							$p_code="cms/gallery/".$code.'/'.$p_code;
						echo '
						<div class="col-md-4 ftco-animate">
							<a href="'.$p_code.'" class="photography-entry img image-popup d-flex justify-content-center align-items-center" style="background-image: url('.$p_code.');">
								
							</a>
						</div>
						
						';
						}
						?>
					</div>
				</div>
			</section>
	    
		</div><!-- END COLORLIB-MAIN -->
	</div><!-- END COLORLIB-PAGE -->

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="gallery/js/jquery.min.js"></script>
  <script src="gallery/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="gallery/js/popper.min.js"></script>
  <script src="gallery/js/bootstrap.min.js"></script>
  <script src="gallery/js/jquery.easing.1.3.js"></script>
  <script src="gallery/js/jquery.waypoints.min.js"></script>
  <script src="gallery/js/jquery.stellar.min.js"></script>
  <script src="gallery/js/owl.carousel.min.js"></script>
  <script src="gallery/js/jquery.magnific-popup.min.js"></script>
  <script src="gallery/js/aos.js"></script>
  <script src="gallery/js/jquery.animateNumber.min.js"></script>
  <script src="gallery/js/bootstrap-datepicker.js"></script>
  <script src="gallery/js/jquery.timepicker.min.js"></script>
  <script src="gallery/js/scrollax.min.js"></script>
 <script src="gallery/js/google-map.js"></script>
  <script src="gallery/js/main.js"></script>
    
  </body>
</html>