<?php
session_start();
if($_SESSION['alu_auth']=="")
{
	header("Location:login.php");
}

?>
<?php include('js/php/get_profile_data.php'); ?>
<?php include('profile_image_chage.php'); ?>
<?php
clearstatcache();
?>
<html>
<head>
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style>
body
{
	margin: 0px;
	background: #f7f7f7;
	position: relative;
}
#messanger_down_box
{
	width:100%;
	height: 10px;
	position: absolute;
	bottom: 0px;
	z-index: 2;
	position: fixed;
}
.message_box
{
	width: auto;
	height: 350px;
	position: absolute;
	bottom: 0px;
	right: 0px;
}
.message_sub_box
{
	width: 280px;
	height: 350px;
	display: inline-block;
	visibility: hidden;
	border-top-left-radius: 10px;
	border-top-right-radius: 10px;
	margin: 0px 20px 0px 20px;
	overflow: hidden;
	background-color: white;
	box-shadow: 0px 0px 5px 1px #d9d9d9;
}
.message_top_details
{
	background-color:#df3a3a;
	width: 100%;
	height: 40px;
}
.message_send_box
{
	width: 280px;
	height: 35px;
	border-top: 1px solid #a6a6a6;
	position: absolute;
	bottom: 0px;
}
.message_active_status
{
	width: 30px; 
	height: 40px;
	float: left;
	position: relative;
}
.message_active_person_name
{
	width: 200px;
	height: 40px;
	float: left;
	overflow: hidden;
}
.message_active_person_name p
{
	font-family: 'Noto Sans', sans-serif;
	font-size: 13px;
	margin: 0px;
	padding-top: 10px;
	color: white;
}
.message_box_close
{
	width: auto;
	height: 40px;
	float: right;
}
.message_box_close p
{
	font-family: 'Noto Sans', sans-serif;
	font-size: 18px;
	margin: 0px;
	cursor: pointer;
	margin: 8px 10px 0px 0px;
    padding: 0px;
	padding: 0px 4px 0px 4px;
	width: auto;
	color: white;
}
.message_box_close p:hover
{
	background-color: #e14848;
}
*:focus
{
	outline: none;
}
.message_display_content
{
	width:280px;
	height: 274px;
	background-color: white;
	position: absolute;
}
.banner
{
	width: 100%;
	height: 300px;
	max-width: 1800px;
	margin: 0px;
	position: relative;
	background-image: url('<?php echo $profile_header; ?>');
	background-size: 100% auto;
	background-position: center;
	background-repeat: no-repeat;
}
.banner_change
{
	display: none;
}
.banner:hover > .banner_change
{
	width: 100%;
	height: 50px;
	display: block;
	position: absolute;
	bottom: 0px;
}
.banner_bottom_profile
{
	width: 100%;
	max-width: 1800px;
	height: 100px;
	background: white;
	border-bottom-left-radius: 25px;
	border-bottom-right-radius: 25px;
	box-shadow: 0px 1px 10px 0px #cfcfcf;
}
.name_diplay
{
	width: auto;
	height: auto;
	margin-left: 350px;
	float: left;
	display: inline-block;
}

.banner_main
{
	width: 100%;
	height:400px;
	max-width: 1800px;
	margin: auto;
	position: relative;
}
.profile_pic
{
	width: 200px;
	height: 200px;
	position: absolute;
	border: 3px solid white;
	border-radius: 50%;
	top: 190px;
	left: 130px;
	right: 0px;
	overflow: hidden;
}
#follow_button
{
margin-top: 30px;
font-size: 20px;
color: #fe6e60;
cursor: pointer;
padding: 5px 20px 5px 20px;
border-radius: 20px;	
background: white;
border: 1.5px solid #fe6e60;
transition: .5s;
}
#follow_button:hover
{
color: white;	
background: #fe6e60;
transition: .5s;
}
.main
{
	width: 100%;
	max-width: 1800px;
	margin: auto;
}
.side_nav_left
{
	width: 30%;
	height: auto;
	float: left;
}
.side_nav
{
	width: 400px;
	height: auto;
	background: white;
	box-shadow: 0px 0px 10px 0px #cfcfcf;
	float: left;
	margin-left: 10px;
	margin-top: 10px;
	padding-bottom: 20px;
	border-radius: 10px;
}
.side_content
{
	width: 70%;
	height: auto; 
	float: left;
}
.content
{
	width: 900px;
	height: auto;
	margin: auto;
}
.post_new
{
	width: 650px;
	height: 200px;
	border-radius: 20px;
	box-shadow: 0px 0px 10px 0px #cfcfcf; 
	margin: auto;
	background: white;
}
.postedby
{
	width: 100%;
	height: 50px;
	box-shadow:  0px 1px 0px 0px #cfcfcf; 
}
.postedby_photo
{
	width: 40px;
	height: 40px;
	position: relative;
	top:5px;
	left: 15px;
	float: left;
	overflow: hidden;
	border-radius: 50px;
	background: yellow;
}
.postedby_name
{
	width: auto;
	height: 100%;
	margin-left: 25px;
	float: left;
}
.attachment
{
	width: 100%;
	height: 47px;
	border-bottom-left-radius: 20px;
	border-bottom-right-radius: 20px;
}
.photo_upload
{
	width: 120px;
	height: 40px;
	border-radius: 18px;
	float: left;
	cursor: pointer;
	margin-top: 3px;
	margin-left: 10px;
	background: #f4f4f4;
}
.post
{
	width: 80px;
	padding: 5px;
	background:#62c5c5;
	color: white;
	margin-right: 10px;
	margin-top: 10px;
	border: 1px solid #62c5c5;
	border-radius:10px;
	font-size: 15px;
	cursor: pointer;
}
.top_nav
{
	width: 100%;
	height: 50px;
	max-width: 1800px;
	margin: auto;
	text-align: center;
	background: white;
	box-shadow: 0px 1px 5px #cccccc;
}
input:focus
{
	outline: none;
}
.icons
{
	width: 40px;
	height: 100%;
	float: right;
}
#profile_content_click
{
	position: absolute;
	margin-top: 50px;
	z-index: 1;
	width: auto;
	height: auto;
}
.profile_pic:hover > .change_photo
{
	position: absolute;
	bottom:0px;
	width: 100%;
	height: 35%;
	cursor: pointer;
	transition: 0.5s;
	background-color: rgb(0,0,0,0.5);
}
.profile_pic:hover > .change_photo:hover
{
	background-color: black;
}
.dropdown_profile
{
	position: relative; 
	z-index: 1;
	float: right; 
	text-align: left;
	height: auto;
	margin: 0px; 
	display: none;
	cursor:pointer;	
	background-color: white;
}
.icons:hover > .dropdown_profile
{
	display: block;
}
.view_profile
{
	width: 95%; 
	font-size: 15px; 
	color: black;
	margin: 0px;
	text-decoration: none;
	margin: auto; 
	padding: 3px 0px 3px 0px;
	text-align: center; 
	font-family: arial;
	border-radius: 5px;
}
.view_profile:hover
{
	background-color: #f1f1f1;
}
.experience
{
	width: 800px;
	height: auto;
	float: left;
	border-radius: 5px;
	margin-top: 15px;
	margin-left: 30px;
	padding-bottom: 10px;
	background-color: white;
	box-shadow: 0px 1px 5px #cccccc;
}
.education
{
	width: 800px;
	height: auto;
	float: left;
	margin-top: 10px;
	border-radius: 5px;
	margin-left: 30px;
	padding-bottom: 10px;
	background-color: white;
	box-shadow: 0px 1px 5px #cccccc;
}
.skills_show
{
	width: 95%;
	height: auto;
	margin-left: 5%;
}
.skills_show p
{
	float: left;
	display: block;
	font-family: 'Roboto', sans-serif;
	padding: 5px;
	color: #4d4d4d;
}
.suggest_peoples
{
	width: 400px;
	height: auto;
	background: white;
	box-shadow: 0px 0px 10px 0px #cfcfcf;
	float: left;
	margin-left: 10px;
	margin-top: 10px;
	border-radius: 10px;
}
.suggest_people_show
{
	width: 100%;
	height: 40px;
	cursor: pointer;
	margin-top: 10px;
}
.social_accounts
{
	width: 400px;
	height: auto;
	background: white;
	padding-bottom: 10px;
	box-shadow: 0px 0px 10px 0px #cfcfcf;
	float: left;
	margin-left: 10px;
	margin-top: 10px;
	border-radius: 10px;
}
.fa {
  padding: 4px;
  font-size: 20px;
  width: 20px;
  text-align: center;
  text-decoration: none;
  margin: 5px 2px;
}

.fa:hover {
    opacity: 0.7;
}
.fa-twitter {
  margin-left: 10px;
  background: #55ACEE;
  color: white;
}
.fa-linkedin {
  margin-left: 10px;
  background: #007bb5;
  color: white;
}
.fa-facebook {
  margin-left: 10px;
  background: #3B5998;
  color: white;
}
.fa-instagram {
  margin-left: 10px;
  background: #3f729b;
  color: white;
}
.nav_icons
{
	width: auto;
	height: 100%;
	float: left;
	padding-right: 10px;
	cursor: pointer;
}
.nav_icons:hover
{
	background-color: #f4f4f4;
}
.nav_friends_search_box
{
	width: 500px;
	height: 380px;
	position: absolute;
	z-index: 1;
	display: none;
	margin: auto;
	top: 0px;
	left: 0px;
	right: 0px;
	bottom: 0px;
	position: fixed;
	box-shadow: 0px 0px 10px 0px #f4f4f4;
	background-color: white;
	top: 0px;
}
#nav_friends_display
{
	width: 100%;
	height: 330px;
	overflow-y: scroll;
}
#nav_search_friends
{
	width: 100%;
	height: 330px;
	overflow-y: scroll;
	display: none;
}
#nav_unread_message_status
{
	width: 15px;
	height: 15px;
	background-color: #ff5757;
	position: absolute;
	top: 8px; 
	right: 5px;
	border-radius: 20px;
}
.result_show
{
	width: 100%;
	height: 40px;
	cursor: pointer;
	background-color: white;
}
.result_show:hover
{
	background-color: #f8f8f8
}
.post_style
{
	width: 600px;
	height: auto;
	box-shadow: 0px 0px 3px #cccccc;
	border-top-left-radius: 5px;
	border-top-right-radius: 5px;
	background: white;
	margin-left: auto;
	display:block;
	margin-right: auto;
	margin-top: 40px;
	margin: 10px 50px 10px 50px;	
}
.post_top
{
	width: 100%;
	height: 50px;
	border-bottom: 1px solid #cccccc;
	border-top-left-radius: 20px;
	border-top-right-radius: 20px;
	background: white;
}
.profile_logo
{
	width: 35px;
	height: 35px;
	position: relative;
	top: 5px;
	float: left;
	border-radius: 35px;
	left: 10px;
	overflow: hidden;
}
.profile_details
{
	width: auto;
	height: 100%;
	float: left;
	margin-left: 15px;
}
.post_body
{
	width: 100%;
	height: auto;
}
.post_image
{
	width: 100%;
	height: auto;
}
.post_text
{
	width: 100%;
	padding-bottom: 5px;
	height: auto;
}
.post_like
{
	width: 100%;
	height: 30px;
}
.delete_post
{
	width: 80px; 
	height: 30px;
	box-shadow: 0px 0px 7px 0px #c8c8c8;
	float: right; 
	margin-top: 5px; 
	background-color: white
}
.delete_post_mod
{
	width: 30px;
	height: 100%;
	float: right;
	cursor: pointer;
}
.delete_post_mod:active  .delete_post
{
	background-color: red;
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
</head>
<body>
<div id="messanger_down_box">
<div class="message_box" id="message_box">
<div class="message_sub_box" id="message_sub_box" value="cc">
<div class="message_top_details">
<div class="message_active_status">
<div id="messages_aactive_status" style="width: 10px; height: 10px; display: none; border-radius: 10px; background-color: #7ed957; position:absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;">
</div>
</div>
<div class="message_active_person_name">
<p></p>
</div>
<div class="message_box_close">
<b><p onclick="close_m(this);">X</p></b>
</div>
</div>
<div class="message_display_content" style="overflow-y: scroll; ">

</div>
<div class="message_send_box">
<div style="width: 85%; height: 100%; float: left;">
<input onkeydown="update_message_into_db(this)" style="width: 100%; height: 100%; font-family: 'Noto Sans', sans-serif; margin: 0px; border: none; padding-left: 10px;" placeholder="Type a message"></input>
</div>
<div style="width: 15%; height: 100%; float: left;">
<p style="font-family: 'Noto Sans', sans-serif; margin: 0px; font-size: 13px; text-align: center; padding-top: 9px; cursor: pointer;"><b>Send</b></p>
</div>
</div>
</div>
</div>
</div>
<div id="test_display">
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
var i=0;
var num_box=0;
var active_box=[];
    var h=$(window).height();
	var h=h-10;
function get_new_message_box(id_num)
{
	var c1=0;
	for(var i=0;i<active_box.length;i++)
	{
		if(active_box[i]==id_num)
		{
			c1++;
		}
	}
	if(c1==0)
	{
		active_box.push(id_num);
	    var w=320+320;
		num_box=num_box+320;
		if(num_box<=w-320)
		{
			var message_box=document.getElementById('message_sub_box');
			var m=message_box.cloneNode(true);
			m.id="messagebox|"+id_num;
			m.value="messagebox|"+id_num;
			m.style.visibility="visible";

			document.getElementById('message_box').appendChild(m);
		}
		else
		{
			num_box=num_box-320;
			alert("Resolution Not Support");
		}
	}
	document.getElementById('nav_friends_search_box').style.display='none';
}
function close_m(m)
{
num_box=num_box-320;
var mk=0;
for(var i=0;i<active_box.length;i++)
{
	if(active_box[i]==m)
	{
		mk=i;
		break;
	}
}
for(var j=mk;j<active_box.length;j++)
{
	active_box[j]=active_box[j+1];
}
active_box.pop();
var k=m.closest('[id]')
k.remove();
}
var time_d=0;
function get_active_box_list()
{
	if(active_box.length==1)
	{
for(var i=0;i<1;i++)
{
var get_active_box_details=active_box[i];
 $.ajax({	
type: 'post',
 url: 'js/php/get_active_message_box_update.php',
 data : {get_active_box_details:get_active_box_details},
 success: function(code){
	 
 $('#test_display').html(code);
   $.ajax({	
			type: 'post',
			 url: 'js/php/messages_all_display.php',
			 data : {get_active_box_details:get_active_box_details},
			 success: function(code){
			  var x = document.getElementById("messagebox|"+get_active_box_details);
			  var time=code.split("|");
			  x.querySelector(".message_display_content ").innerHTML =time[0];
			  if(time_d!=time[1])
			  {
				  time_d=time[1];
				  down();
				  status=time[2];
				  status=status.trim();
				  if(status=='YES')
				  {
				  x.querySelector(".message_active_status #messages_aactive_status").style.display='block';
				  }
				  else
				  {
				  x.querySelector(".message_active_status #messages_aactive_status").style.display='none'; 
				  }
			  }
			  else
			  {
				    status=time[2];
				  status=status.trim();
				  if(status=='YES')
				  {
				  x.querySelector(".message_active_status #messages_aactive_status").style.display='block';
				  }
				  else
				  {
				  x.querySelector(".message_active_status #messages_aactive_status").style.display='none'; 
				  }
			  }
			 }
			 });
 
 }
 });
}
	}

}
setInterval("get_active_box_list();",100);
function down()
{
	for(var i=0;i<active_box.length;i++)
	{
	var get_active_box_details=active_box[i];
		var x = document.getElementById("messagebox|"+get_active_box_details);
		x.querySelector(".message_display_content").scrollTop = x.querySelector(".message_display_content").scrollHeight+10;
	}		
}


function update_message_into_db(ele)
{
	if(event.key === 'Enter') {
        var message=ele.value; 
		ele.value="";
        var parent_div=ele.closest('[id]');
	    parent_div=parent_div.value;
	    $.ajax({	
			type: 'post',
			 url: 'js/php/message_get_update_db.php',
			 data : {parent_div:parent_div, message:message},
			 success: function(code){
				
				  }
			 });
    }
}
</script>



<div class="nav_friends_search_box" id="nav_friends_search_box">
<p onclick="document.getElementById('nav_friends_search_box').style.display='none'" style="font-family: arial; background-color: rgba(0,0,0,0.5); cursor: pointer; color: white; padding: 5px 6px 5px 6px; border-radius: 10px; font-size: 15px; position: absolute; top: -50px; right: 0px;">X</p>
<div style="width: 100%; height: 50px; background-color: #224162;">
<div onclick="nav_friends_search_box('nav_friends_display');" id="nav_message_status1" style="width: 50%; height: 50px; cursor: pointer; background-color:#102842; float: left;">
<div style="width: 200px; height: 100%; margin: auto;">
<img src="imags/friend.png" style="width: auto; margin-top: 15px; height: 50%; float: left;" />
<p style="margin: 0px;font-family: 'Noto Sans', sans-serif; float: left; font-size: 15px; padding-top: 15px; color: white; padding-left: 10px;">Friends</p>
</div>
</div>
<div onclick="nav_friends_search_box('nav_search_friends');" id="nav_message_status2" style="width: 50%; height: 50px; float: left; cursor: pointer;">
<div style="width: 200px; height: 100%; margin: auto;">
<img src="imags/search.png" style="width: auto; margin-top: 15px; height: 50%; float: left;" />
<p style="margin: 0px;font-family: 'Noto Sans', sans-serif; float: left; font-size: 15px; padding-top: 15px; color: white; padding-left: 10px;">Search friends</p>
</div>
</div>
</div>
<script>
function nav_friends_search_box(m,color)
{
	if(m=='nav_friends_display')
	{
		document.getElementById('nav_friends_display').style.display="block";
		document.getElementById('nav_search_friends').style.display="none";
		nav_message_status1.style.backgroundColor='#102842';
		nav_message_status2.style.backgroundColor='#224162';
	}
	else
	{
		document.getElementById('nav_friends_display').style.display="none";
		document.getElementById('nav_search_friends').style.display="block";
		nav_message_status1.style.backgroundColor='#224162';
		nav_message_status2.style.backgroundColor='#102842';
	}
}
</script>
<div id="nav_friends_display">
</div>

<script>
window.setInterval(function(){
 	$.ajax({
	 type: 'post',
	 url: 'js/php/my_last_messages.php',
	 success: function(code){
	 $('#nav_friends_display').html(code);
	 }
	 });
}, 2000);
</script>

<div id="nav_search_friends">
<div style="width: 100%; height: 35px;">
<input type="text" onkeyup="friend_search_for_message(this);" placeholder="Search friend by name"  style="width: 100%; color: #505050; border: none; border-bottom: 1.5px solid #b4b4b4; font-size: 15px; padding: 7px;"/>
<div id="nav_search_display_box" style="width: 100%; height: 293px;">
</div>
</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
function friend_search_for_message(a)
{
	var nav_friend_search=a.value;
	$.ajax({
type: 'post',
 url: 'js/php/friend_search_for_send_message.php',
 data : {nav_friend_search:nav_friend_search},
 success: function(code){
 $('#nav_search_display_box').html(code);
 }
 });
}
</script>


<div class="top_nav">
<img src="imags/klu.png" style="height: 90%; width: auto; margin-left: 10px; margin-top: 3px; float: left;" />
<p style="margin: 0px; font-family: 'Lato', sans-serif; font-size: 20px; padding-top: 12px; float: left;"><strong>Alumni Association</strong></p>
<div style="width: auto; height:100%; float: right;">
<div class="icons">
<div style="width: auto; height: 100%;float: left;">
<a href="logout.php">
<div onclick="profile_content_click();" style="width: 30px;  height: 30px; cursor: pointer;  overflow: hidden; border-radius: 30px; margin-top: 10px;">
<img src="logout_c.png" style="width: 100%; height: auto;">
</div>
</a>
</div>
</div>
<div class="icons">
<div style="width: auto; height: 100%;float: left;">
<a href="main_p.php">
<div onclick="profile_content_click();" style="width: 30px;  height: 30px; cursor: pointer;  overflow: hidden; border-radius: 30px; margin-top: 10px;">
<img src="<?php echo $profile_pic; ?>" style="width: 100%; height: auto;">
<ul>
<li>Profile</li>
</ul>
</div>
</a>
</div>
<div class="dropdown_profile">
<div style="width: 250px; height: 60px; background-color: white;">
<div style="width: 50px; margin: 5px; height: 50px; border-radius: 60px; background-color: black; overflow: hidden; float:left;">
<img src="<?php echo $profile_pic; ?>" style="width: 100%; height: auto;" />
</div>
<div style="height: 50px; width: auto; float: left;">
<p style=" margin: 0px; padding-left: 5px; padding-top: 10px; font-family: arial; font-size: 15px;"><?php echo $profile_name; ?></p>
</div>
</div>
<a style="text-decoration: none;" href="edit.php"><p class="view_profile">View Profile</p></a>
<div style="width: 100%; height: 0.5px; background-color: #bebebe; margin-top: 5px;">
</div>
<a href="news.php" style="text-decoration: none;"><p style="margin: 0px; text-decoration: none; color: #595959; font-family: 'Roboto', sans-serif; padding: 7px 0px 2px 7px; font-size: 15px; ">News</p></a>
<a href="offer.php" style="text-decoration: none;"><p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; padding: 7px 0px 2px 7px; font-size: 15px; ">Job postings</p></a>
<p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; padding: 7px 0px 2px 7px; font-size: 15px;">Change Password</p>
<p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; padding: 7px 0px 2px 7px; font-size: 15px;">Privacy policy</p>

</div>
</div>
</div>
<div style="width: auto; height: 100%; float: right; margin-right: 30px;">
<a href="news.php">
<div class="nav_icons">
<div style="width: auto; height: 100%; float: right;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; margin: 0px; padding-top: 13.5px; color: #8d8d8d;">Home</p>
</div>
<div style="width: 50px; height: 100%; float: right; position: relative;">
<img src="imags/home.png" style="width: 35px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;" />
</div>
</div>
</a>
<a href="mypost.php">
<div class="nav_icons">
<div style="width: auto; height: 100%; float: right;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; margin: 0px; padding-top: 13.5px; color: #8d8d8d;">My Posts</p>
</div>
<div style="width: 50px; height: 100%; float: right; position: relative;">
<img src="imags/my_post.png" style="width: 35px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;" />
</div>
</div>
</a>
<a href="network.php">
<div class="nav_icons">
<div style="width: auto; height: 100%; float: right;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; margin: 0px; padding-top: 13.5px; color: #8d8d8d;">My Network</p>
</div>
<div style="width: 50px; height: 100%; float: right; position: relative;">
<img src="imags/mynetwork.png" style="width: 35px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;" />
<?php 
$temp_r=$request;
$temp_r=explode(",",$temp_r);
if(sizeof($temp_r)>=1 and $temp_r[0]!='')
{
	if(sizeof($temp_r)>9)
	{
		echo '
	<div id="nav_unread_message_status">
	<p style="margin: 0px; text-align: center; font-size: 9px; color: white; font-family: arial; padding-top: 1px;"><b>9+</b></p>
	</div>
	';
	}
	else
	{
		echo '
	<div id="nav_unread_message_status">
	<p style="margin: 0px; text-align: center; font-size: 9px; color: white; font-family: arial; padding-top: 1px;"><b>'.sizeof($temp_r).'</b></p>
	</div>
	';
	}
}
?>

</div>
</div>
</a>
<div class="nav_icons" onclick="document.getElementById('nav_friends_search_box').style.display='block'" >
<div style="width: auto; height: 100%; float: right;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; margin: 0px; padding-top: 13.5px; color: #8d8d8d;">Messaging</p>
</div>
<div style="width: 50px; height: 100%; float: right; position: relative;">
<img src="imags/message.png" style="width: 35px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;" />

</div>
</div>
</div>
</div>

<div class="banner_main">
<div class="banner">
<div class="banner_change">
<a href="edit.php">

</a>
</div>
</div>
<div class="profile_pic" id="profile_pic">
<img src="<?php echo $profile_pic; ?>" style="width: 100%; height: auto; margin: auto; position: absolute; top: 0px; bottom: 0px; left: 0px; right: 0px;"/>
</div>
<div class="banner_bottom_profile" id="banner_bottom_profile">
<div class="name_diplay" id="name_diplay">
<div style="width: 100%; height: 35px;">
<p style="font-family: 'Noto Sans', sans-serif; float: left; margin: 0px; font-size: 25px;" id="name"><?php echo $profile_name; ?></p>
<?php
if($verified_status=="VERIFIED")
{
	echo '
	<div id="verified_account" style=" width: 18px; height: 18px; margin-left: 7px; margin-top: 10px; float: left; ">
<img src="imags/verify.png" alt="Verified" style="width: 100%; height: auto;"/>
</div>
	';
}
?>
</div>
<p style="font-family: 'Noto Sans', sans-serif; margin: 0px; font-size: 15px; color: #989898; padding-top: 2px;"><?php echo $current_company." ,".$current_job; ?></p>
<p style="font-family: 'Noto Sans', sans-serif; margin: 0px; font-size: 13px; color: #989898;padding-top: 1px;">(<?php echo $batch; ?> Batch)</p>
</div>
</div>
</div>






<div class="main">
<div class="side_nav_left">
<div class="side_nav">
<div style="width: 100%; height: 25px; margin-top: 20px; background-color: white;">
<img src="company.png" style="margin-left: 50px; margin-right: 8px; height: 90%; margin-top:5px; width: auto; float: left;">
<p style="margin: 0px; font-family: 'Noto Sans', sans-serif; font-size: 11px; padding-top: 11px;">Working at &nbsp;<strong id="working"><?php echo $current_company; ?></strong></p>
</div>
<div style="width: 100%; height: 25px; margin-top: 5px; background-color: white;">
<img src="job.png" style="margin-left: 50px; margin-right: 14px; height: 15px; margin-top:11px; width: auto; float: left;">
<p style="margin: 0px; font-family: 'Noto Sans', sans-serif; font-size: 11px; padding-top: 12px; word-spacing:1px;">Role &nbsp;<strong id="working"><?php echo $current_job; ?></strong></p>
</div>
<div style="width: 100%; height: 25px; margin-top: 5px; background-color: white;">
<img src="education.png" style="margin-left: 50px; margin-right: 8px; height: 90%; margin-top:8px; width: auto; float: left;">
<p style="margin: 0px; font-family: 'Noto Sans', sans-serif; font-size: 11px; padding-top: 12px; word-spacing:1px;">Studied &nbsp;<strong id="working"><?php echo $higher_education.' at '.$higher_education_completed; ?></strong></p>
</div>
<div style="width: 100%; height: 25px; margin-top: 6px; background-color: white;">
<img src="home.png" style="margin-left: 50px; margin-right: 8px; height: 90%; margin-top:8px; width: auto; float: left;">
<p style="margin: 0px; font-family: 'Noto Sans', sans-serif; font-size: 11px; padding-top: 12px; word-spacing:1px;">Current City &nbsp;<strong id="working"><?php echo $current_town.' ,'.$current_country; ?></strong></p>
</div>
<div style="width: 100%; height: 25px; margin-top: 6px; background-color: white;">
<img src="location.png" style="margin-left: 50px; margin-right: 8px; height: 90%; margin-top:8px; width: auto; float: left;">
<p style="margin: 0px; font-family: 'Noto Sans', sans-serif; font-size: 11px; padding-top: 12px; word-spacing:1px;">Home Town &nbsp;<strong id="working"><?php echo $hometown.' , '.$country_display; ?></strong></p>
</div>

</div>
<div class="suggest_peoples">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 15px; padding-left: 10px;"><strong>Suggested Peoples</strong></p>

<?php
require('js/php/conn.php');
$get_data=mysqli_query($conn,"select profile_name,profile_pic,hash_key,job,company,status from profile inner join auth on profile.email=auth.email inner join personal on personal.email=profile.email order by visitors_count DESC limit 3;");
while($get_t=mysqli_fetch_assoc($get_data))
{
	$pic=$get_t['profile_pic'];
	$profile_name=$get_t['profile_name'];
	$hask_key=$get_t['hash_key'];
	$job=$get_t['job'];
	$company=$get_t['company'];
	$status=$get_t['status'];
	
	echo '
	<a href="profile.php?getdetails='.$hask_key.'">
	<div class="suggest_people_show">
	<div style="overflow: hidden; width: 40px; height: 40px; border-radius: 40px; margin-left: 40px; float: left;">
	<img src="'.$pic.'" style="width: 100%; height: auto">
	</div>
	<div style="width: auto; height: 100%; float: left;">
	<p style=" margin: 0px; font-family: arial; padding-top: 5px; font-size: 13px; color: black; padding-left: 10px;"><strong>'.$profile_name.' <img src="imags/verify.svg" style="width: 13px; height: auto; padding-bottom: 5px;"></strong></p>
	<p style=" margin: 0px; font-family: arial; padding-top: 2px; font-size: 10px; color: black; padding-left: 10px;">'.$job.', '.$company.'</p>
	</div>
	</div>
	</a>
	';
}
?>

<a href="as.php" style="text-decoration: none;">
<div style="width: 100%; height: 20px; padding-bottom: 30px; padding-top: 10px; cursor: pointer;">
<p style="text-align: center; margin: 0px; font-size: 15px; color: black; font-family: arial;">Show more</p>
</div>
</div>
</a>

<div class="social_accounts">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 15px; padding-left: 10px;"><strong>Social Media</strong></p>
<div style="width: auto; display: inline-block; height: 30px;">
<a href="gunji.sairevanth@outlook.com" class="fa fa-facebook"></a>
</div><div style="width: auto; display: inline-block; height: 30px;">
<a href="https://www.instagram.com/sairevanth_gunji/" class="fa fa-instagram"></a>
</div><div style="width: auto; display: inline-block; height: 30px;">
<a href="https://twitter.com/GunjiRevanth" class="fa fa-twitter"></a>
</div></div>

</div>
<div class="side_content">
<div class="content">
<?php
require('js/php/conn.php');
$get_post_data=mysqli_query($conn,"select * from posts where user='$session' and status='POSTED' order by posted_time DESC;");
while($get_post_d=mysqli_fetch_assoc($get_post_data))
{
	$cod=$get_post_d['code'];
	$desc=$get_post_d['description'];
	$desc=explode("\n",$desc);
	$posted_time=$get_post_d['posted_time'];
	$disfiles=$get_post_d['dis_files'];
	$time=get_time_d($posted_time);
	$disfiles=explode(",",$disfiles);
	if(sizeof($disfiles)>1)
	{
			echo '
				
				<div class="post_style">
				<div class="post_top">
				<div class="profile_logo">
				<img src="'.$profile_pic.'" style="width: 100%; height: auto;">
				</div>
				<div class="profile_details">
				<a style="text-decoration: none;" href="profile.php?getdetails='.$hash_key.'">
				<p style="margin: 0px; color: black; padding-left: 8px; text-align: left; font-family: '."'Noto Sans'".', sans-serif; font-size: 13px; padding-top: 8px;">'.$profile_name.'</p>
				<p style="margin: 0px; color: black; padding-left: 8px; font-family: '."'Noto Sans'".', sans-serif; font-size: 10px;">@ '.$current_company.' ,'.$current_job.'</p>
				</a>
				</div>

				</div>
				<div class="post_body">
				<div class="post_text">';
				
				
				for($i=0;$i<sizeof($desc);$i++)
				{
					echo '<p style="font-family: '."'Open Sans'",', sans-serif; font-size: 13px; text-align: left; margin: 0px; padding: 5px 5px 0px 10px;">'.$desc[$i].'</p>';
				}
				echo '
				</div>
				<div class="post_image">
				  <div id="myCarousel'.$cod.'" class="carousel slide" data-ride="carousel">
				   <ol class="carousel-indicators">
				';
				for($i=0;$i<sizeof($disfiles);$i++)
				{
					if($i==0)
					{
						echo '<li data-target="#myCarousel'.$cod.'" data-slide-to="'.$i.'" class="active"></li>';
					}
					else
					{
						echo '<li data-target="#myCarousel'.$cod.'" data-slide-to="'.$i.'"></li>';
					}
				}
				echo '
				    </ol>
					<div class="carousel-inner">
				';
				$k=0;
				for($i=0;$i<sizeof($disfiles);$i++)
				{
					$ext=$disfiles[$i];
					$ext=explode('.',$ext);
					$ext=end($ext);
					$ext=strtolower($ext);
					if($ext=="jpg" or $ext=="jpeg" or $ext=="bmp" or $ext=="gif" or $ext=="png")
					{
					  if($i==0)
					  {
						  echo '<div class="item active">
							<img src="js/php/posts/'.$cod.'/';
							echo $disfiles[$i];
							echo'"alt="Los Angeles" style="width:100%;">
							</div>';
					  }
					  else
					  {
						  echo '<div class="item">
								<img src="js/php/posts/'.$cod.'/';
								echo $disfiles[$i];
								echo'"alt="Los Angeles" style="width:100%;">
								</div>';
					  }
					}
					else if($ext=='mp4')
					{
						if($i==0)
					 {
							echo '
							<div class="item active">
							<video controls="autoplay" style="width:100%;height:auto;" poster="poster.png">
							<source src="devstories.webm" type="video/webm;codecs=&quot;vp8, vorbis&quot;">
							<source src="js/php/posts/'.$cod.'/'.$disfiles[0].'" type="video/mp4;codecs=&quot;avc1.42E01E, mp4a.40.2&quot;">
							<track src="devstories-en.vtt" label="English subtitles" kind="subtitles" srclang="en" default="">
						    </video>	
						    </div>';
								
								
						  }
					  else
					  {
						  	echo '
							<div class="item">
							<video controls="autoplay" style="width:100%;height:auto;" poster="poster.png">
							<source src="devstories.webm" type="video/webm;codecs=&quot;vp8, vorbis&quot;">
							<source src="js/php/posts/'.$cod.'/'.$disfiles[0].'" type="video/mp4;codecs=&quot;avc1.42E01E, mp4a.40.2&quot;">
							<track src="devstories-en.vtt" label="English subtitles" kind="subtitles" srclang="en" default="">
						    </video>	
						    </div>';
					  }
	
					}
					else
					{
						$k=$k+1;
						echo '
					<div style="width: 95%; height: 40px; background-color: #fcfcfc; margin: auto;">
					<div style="width: auto; height: 40px; float: left;">
					<p style="font-family: arial; font-size: 14px; padding-top: 10px; padding-left: 10px; margin:0px;">'.$disfiles[$i].'</p>
					</div>
					<a href="post_file_download.php?code='.$cod.'&file='.$disfiles[$i].'">
					<div style="width: 20px; cursor: pointer; height: 100%; position: relative; float: right;">
					<img src="imags/download.png" style="width: 20px; height: auto; cursor: pointer; margin: auto; position: absolute; bottom:0px; left: 0px; right:0px; top: 0px;" />
					</div>
					</a>
					</div>
					';
					
					}
				}
				echo '
				 </div>';
				 if($k==0)
				 {
					 echo'
					 <a class="left carousel-control" href="#myCarousel'.$cod.'" data-slide="prev" style="background-image: none; height: 50%; margin-top: 15%;">
				  <span class="glyphicon glyphicon-chevron-left"></span>
				  <span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#myCarousel'.$cod.'" data-slide="next" style="background-image: none; height: 50%; margin-top: 15%;">
				  <span class="glyphicon glyphicon-chevron-right"></span>
				  <span class="sr-only">Next</span>
				</a>';
				 }
				 
				echo '</div></div>
				</div>
				<p style="font-family: '."'Noto Sans'".', sans-serif; margin: 0px; text-align: left; font-size: 12px; padding: 5px;">'.$time.' ago</p>
				<div class="post_like" style="text-align: right;">
				<button onclick=delete_post('.$cod.'); style="background-color: #224162; border:1px solid #224162; color: white; font-family: arial; font-size: 13px; cursor: pointer; border-radius: 5px; padding: 3px 10px 3px 10px; margin: 0px 10px 30px 0px;">Delete Post</button>
				</div>
				</div>';
	}
	else
	{
		$ext=$disfiles[0];
		$ext=explode('.',$ext);
		$ext=end($ext);
		$ext=strtolower($ext);
		
		
		echo '
				
				<div class="post_style">
				<div class="post_top">
				<div class="profile_logo">
				<img src="'.$profile_pic.'" style="width: 100%; height: auto;">
				</div>
				<div class="profile_details">
				<a style="text-decoration: none;" href="profile.php?getdetails='.$hash_key.'">
				<p style="margin: 0px; color: black; padding-left: 8px; text-align: left; font-family: '."'Noto Sans'".', sans-serif; font-size: 13px; padding-top: 8px;">'.$profile_name.'</p>
				<p style="margin: 0px; color: black; padding-left: 8px; font-family: '."'Noto Sans'".', sans-serif; font-size: 10px;">@ '.$current_company.' ,'.$current_job.'</p>
				</a>
				</div>

				</div>
				<div class="post_body">
				<div class="post_text">
				';
				for($i=0;$i<sizeof($desc);$i++)
				{
					echo '<p style="font-family: '."'Open Sans'",', sans-serif; font-size: 13px; text-align: left; margin: 0px; padding: 5px 5px 0px 10px;">'.$desc[$i].'</p>';
				}
				echo '
				</div>
				<div class="post_image">';
			     if($ext=="mp4")
				{
					echo '<video controls="autoplay" style="width:100%;height:auto;" poster="poster.png">
					<source src="devstories.webm" type="video/webm;codecs=&quot;vp8, vorbis&quot;">
					<source src="js/php/posts/'.$cod.'/'.$disfiles[0].'" type="video/mp4;codecs=&quot;avc1.42E01E, mp4a.40.2&quot;">
					<track src="devstories-en.vtt" label="English subtitles" kind="subtitles" srclang="en" default="">
				  </video>	';
				}
				else if($ext=="jpg" or $ext=="jpeg" or $ext=="bmp" or $ext=="gif" or $ext=="png")
				{
					echo '<img src="js/php/posts/'.$cod.'/'.$disfiles[0].'" style="width: 100%; height: auto;">';
				}
				else
				{
					echo '
					<div style="width: 95%; height: 40px; background-color: #fcfcfc; margin: auto;">
					<div style="width: auto; height: 40px; float: left;">
					<p style="font-family: arial; font-size: 14px; padding-top: 10px; padding-left: 10px; margin:0px;">'.$disfiles[0].'</p>
					</div>
					<a href="post_file_download.php?code='.$cod.'&file='.$disfiles[0].'">
					<div style="width: 20px; cursor: pointer; height: 100%; position: relative; float: right;">
					<img src="imags/download.png" style="width: 20px; height: auto; cursor: pointer; margin: auto; position: absolute; bottom:0px; left: 0px; right:0px; top: 0px;" />
					</div>
					</a>
					</div>
					';
				}
				echo'
				</div>
				</div>
				<p style="font-family: '."'Noto Sans'".', sans-serif; margin: 0px; text-align: left; font-size: 12px; padding: 5px;">'.$time.' ago</p>
				<div class="post_like" style="text-align: right;">
				<button onclick=delete_post('.$cod.'); style="background-color: #224162; border:1px solid #224162; color: white; font-family: arial; font-size: 13px; cursor: pointer; border-radius: 5px; padding: 3px 10px 3px 10px; margin: 0px 10px 30px 0px;">Delete Post</button>
				</div>
				</div>
				
				';
	}
}
?>
<?php
function get_time_d($time)
{
date_default_timezone_set('Asia/Calcutta'); 
$t=date('Y-m-d H:i:s');
$datetime1 = new DateTime($t);
$datetime2 = new DateTime($time);
$interval = $datetime1->diff($datetime2);
$years=$interval->format('%y');
$months=$interval->format('%m');
$days=$interval->format('%d');
$hours=$interval->format('%h');
$minutes=$interval->format('%i');
	if($years>0)
	{
		if($months>=1)
		{
			return ("$years years $months months");
		}
		else
		{
			return ("$years years");
		}
	}
	else if ($months>=1)
	{
		if($days>=1)
		{
			return ("$months months $days days");
		}
		else
		{
			return ("$months months");
		}
	}
	else if($days>=1)
	{
		return("$days days");
	}
	else if($hours>=1)
	{
		return("$hours hours $minutes minutes");
	}
	else
	{
		return("$minutes minutes");
	}
}
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
function delete_post(a)
{
	var c=a;
	if(confirm("Confirm to Delete Post"))
	{
		$.ajax({
	 type: 'post',
	 url: 'js/php/delete_post.php',
	 data : {c:c},
	 success: function(code){
	 location.reload();
	 }
	 });
	}
	else
	{
	}
	 
}
</script>

</div>
</div>
</div>






</body>
</html>
