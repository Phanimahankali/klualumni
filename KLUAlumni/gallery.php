<!DOCTYPE html>
<html lang="en">
<head>
   <style>
body
{
	margin: 0px;
	background-color: white;
}
.icon_dis
{
	width: 100%;
	height: 70px;
	max-width: 1800px;
	margin: auto;
}
.main_icon
{
	width: 400px;
	height: 100%;
}
.main_icon img
{
	height: 80%;
	width: auto;
	float: left;
	margin-top: 6px;
	margin-left: 10px;
}
.main_icon p
{
	font-size: 20px;
	padding-top: 35px;
	font-family: 'Lato', sans-serif;
}
.nav_images
{
	width: 100%;
	height: 50px;
	background-color: red;
}
.events_display_box
{
	width: 450px;
	height: 130px;
	cursor: pointer;
	margin: 10px 10px 10px 10px;
	display: inline-block;
	border-radius: 10px;
	position: relative;
	background-color: white;
	overflow: hidden;
	transition: 0.5;
	box-shadow: 0px 0px 5px 0px #d6d6d6;
}
.events_display_box:hover
{
	box-shadow: 0px 0px 1px 0px #d6d6d6;
	transition: 0.5;
}
.dropdownmenu
{
	width: 100%;
	height: 40px;
	background-color: #A81E24;
}
@charset "UTF-8";
.navigation {
  height: 70px;
}

.brand {
  position: absolute;
  padding-left: 20px;
  float: left;
  line-height: 70px;
  text-transform: uppercase;
  font-size: 1.4em;
}
.brand a,
.brand a:visited {
  color: #ffffff;
  text-decoration: none;
}

.nav-container {
  max-width: 1000px;
  margin: 0 auto;
}

nav {
  float: right;
}
nav ul {
  list-style: none;
  margin: 0;
  padding: 0;
}
nav ul li {
  float: left;
  font-family: 'Roboto', sans-serif;
  font-size: 15px;
  position: relative;
}
nav ul li a,
nav ul li a:visited {
  display: block;
  padding: 0 20px;
  line-height: 40px;
  background: #A81E24;
  color: #ffffff;
  text-decoration: none;
}
nav ul li a:hover,
nav ul li a:visited:hover {
  background: #bd3339;
  color: #ffffff;
}
nav ul li a:not(:only-child):after,
nav ul li a:visited:not(:only-child):after {
  padding-left: 4px;
  content: " ▾";
}
nav ul li ul li {
  min-width: 190px;
}
nav ul li ul li a {
  padding: 8px;
  font-size: 14px;
  line-height: 20px;
}

.nav-dropdown {
  position: absolute;
  display: none;
  z-index: 1;
  box-shadow: 0 3px 12px rgba(0, 0, 0, 0.15);
}

/* Mobile navigation */
.nav-mobile {
  display: none;
  position: absolute;
  top: 0;
  right: 0;
  background: #262626;
  height: 70px;
  width: 70px;
}

@media only screen and (max-width: 798px) {
  .nav-mobile {
    display: block;
  }

  nav {
    width: 100%;
    padding: 70px 0 15px;
  }
  nav ul {
    display: none;
  }
  nav ul li {
    float: none;
  }
  nav ul li a {
    padding: 15px;
    line-height: 20px;
  }
  nav ul li ul li a {
    padding-left: 30px;
  }

  .nav-dropdown {
    position: static;
  }
}
@media screen and (min-width: 799px) {
  .nav-list {
    display: block !important;
  }
}
#nav-toggle {
  position: absolute;
  left: 18px;
  top: 22px;
  cursor: pointer;
  padding: 10px 35px 16px 0px;
}
#nav-toggle span,
#nav-toggle span:before,
#nav-toggle span:after {
  cursor: pointer;
  border-radius: 1px;
  height: 5px;
  width: 35px;
  background: #ffffff;
  position: absolute;
  display: block;
  content: "";
  transition: all 300ms ease-in-out;
}
#nav-toggle span:before {
  top: -10px;
}
#nav-toggle span:after {
  bottom: -10px;
}
#nav-toggle.active span {
  background-color: transparent;
}
#nav-toggle.active span:before, #nav-toggle.active span:after {
  top: 0;
}
#nav-toggle.active span:before {
  transform: rotate(45deg);
}
#nav-toggle.active span:after {
  transform: rotate(-45deg);
}
.gallery_block
{
	width: 300px;
	height: 150px;
	display: inline-block;
	margin: 20px 20px 20px 20px;
	background-color: white;
	position: relative;
	cursor: pointer;
	box-shadow: 0px 0px 5px 0px #d9d9d9;
}
</style>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<script src="js/index_dropdown_menu.js"></script>
</head>
<body>
<div class="icon_dis" style="cursor: pointer;">
<div class="main_icon" onclick="window.location='index.php'">
<img src="imags/klu.png" />
<strong><p style="text-decoration: none;">Alumni Association</p></strong>
</div>
</div>
<div class="dropdownmenu">
  <nav>
      <div class="nav-mobile"><a id="nav-toggle" href="#!"><span></span></a></div>
      <ul class="nav-list" >
        <li>
          <a href="#" style="text-decoration: none;">Carrers</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="careers.php" style="text-decoration: none;">Internship</a>
            </li>
            <li>
              <a href="careers.php" style="text-decoration: none;">Jobs</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="advisoryboard.php" style="text-decoration: none;">Board Members</a>
        </li>
        <li>
          <a href="#!" style="text-decoration: none;">Events</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="events.php" style="text-decoration: none;">Alumni Event Calender</a>
            </li>
			<li><a href="events.php" style="text-decoration: none;">All Events</a></li>
			<li>
			  <a href="https://kluniversity.in/site/acadcal.htm" style="text-decoration: none;">Academic Calender</a></li>
          </ul>
        </li>
		 <li>
              <a href="suess_stories.php" style="text-decoration: none;">Alumni Stories</a>
            </li>
            <li>
              <a href="gallery.php" style="text-decoration: none;">Gallery</a>
            </li>
         <li>
              <a href="updates.php" style="text-decoration: none;">News & Updates</a>
            </li>
        <li>
          <a href="aboutus.html" style="text-decoration: none;">About us</a>
        </li>
      </ul>
    </nav>
</div>


<?php
require('js/php/conn.php');
$check=mysqli_query($conn,"select * from gallery;");
while($g=mysqli_fetch_assoc($check))
{
	$gallery_code=$g['code'];
	$event_name=$g['event_name'];
	$date=$g['date'];
	$cop=mysqli_query($conn,"select code,file_type from gallery_photos where status='OK' and g_code='$gallery_code' limit 1;");
	$cop=mysqli_fetch_assoc($cop);
	$photo="cms/gallery/".$gallery_code.'/'.$cop['code'].'.'.$cop['file_type'];
	
echo '

<div class="gallery_block" onclick="window.location='."'gallery_s.php?c=$gallery_code'".'">
<div style="width: 100%; overflow: hidden; height: 100%; position: absolute; top: 0px;">
<img src="'.$photo.'" style=" width: 100%; height: auto;"/>
<div style="width: 100%; height: 60px; background-color: rgba(0,0,0,0.5); position: absolute; bottom: 0px;">
<p style="margin: 0px; color: white; font-family: '."'Lato'".', sans-serif; font-size: 15px; padding-left: 5px; padding-top: 10px;">'.$event_name.'</p>
<p style="margin: 0px; color: white; font-family:  sans-serif; font-size: 15px; padding-left: 5px; padding-top: 2px;">'.$date.'</p>
</div>
</div>
</div>


';
}


?>



</body>
</html>