<html>
<head>
<?php
error_reporting(0);
$get_profile_details=$_REQUEST['getdetails'];
if($get_profile_details=="")
{
	header('location:news.php');
}
include('js/php/get_third_party_profile_details.php');
echo '<title>'.$third_profile_name.' | KL Alumni Association</title>';
session_start();
$session=$_SESSION['alu_auth'];
$che=mysqli_query($conn,"select * from user_security where hash_key='$session';");
if(mysqli_num_rows($che)==0)
{
	header('Location: index.php');
}
else
{
$che=mysqli_fetch_assoc($che);
$session=$che['email'];
date_default_timezone_set('Asia/Kolkata');
$date=date('Y-m-d H:i:s');
mysqli_query($conn,"update profile set active_status='$date' where email='$session';");

}
?>
<script src="js/boot.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style>

body
{
	margin: 0px;
	background: #f7f7f7;
}
#myCarousel
  {
  width: 100%;
  height: auto;
  }
  .loader{
  width: 100px;
  height: 100px;
  border-radius: 100%;
  position: relative;
  margin: 0 auto;
}


#loader-4 span{
  display: inline-block;
  width: 20px;
  height: 20px;
  border-radius: 100%;
  background-color: #3498db;
  margin: 35px 5px;
  opacity: 0;
}

#loader-4 span:nth-child(1){
  animation: opacitychange 1s ease-in-out infinite;
}

#loader-4 span:nth-child(2){
  animation: opacitychange 1s ease-in-out 0.33s infinite;
}

#loader-4 span:nth-child(3){
  animation: opacitychange 1s ease-in-out 0.66s infinite;
}

@keyframes opacitychange{
  0%, 100%{
    opacity: 0;
  }

  60%{
    opacity: 1;
  }
}
.posts
{
	width: 850px;
	height: auto;
	float: left;
}
.post_style
{
	width: 650px;
	height: auto;
	box-shadow: 0px 0px 3px #cccccc;
	border-top-left-radius: 5px;
	border-top-right-radius: 5px;
	background: white;
	margin-left: auto;
	margin-right: auto;
	margin-top: 40px;
}
.post_top
{
	width: 100%;
	height: 50px;
	border-bottom: 1px solid #cccccc;
	border-top-left-radius: 20px;
	border-top-right-radius: 20px;
	background: white;
}
.profile_logo
{
	width: 35px;
	height: 35px;
	position: relative;
	top: 5px;
	float: left;
	border-radius: 35px;
	left: 10px;
	overflow: hidden;
}
.profile_details
{
	width: auto;
	height: 100%;
	float: left;
	margin-left: 15px;
}
.post_body
{
	width: 100%;
	height: auto;
}
.post_image
{
	width: 100%;
	height: auto;
}
.post_text
{
	width: 100%;
	padding-bottom: 5px;
	height: auto;
}
.post_like
{
	width: 100%;
	height: 35px;
}
.search
{
	width: 300px;
	height: 100%;
	text-align: center;
	float: left;
	margin-left: 60px;
}
input:focus
{
	outline: none;
}
.icons
{
	width: 100px;
	height: 100%;
	margin-left: 20px;
	float: right;
}
.post_new
{
	width: 650px;
	height: auto;
	margin: auto;
	border-radius: 5px;
	margin-top: 10px;
	box-shadow: 0px 0px 3px 0px #cfcfcf;
	background: white;
}
.postedby
{
	width: 100%;
	height: 50px;
	display: inline-block;
	box-shadow:  0px 1px 0px 0px #cfcfcf;
}
.postedby_photo
{
	width: 40px;
	height: 40px;
	position: relative;
	top:5px;
	left: 15px;
	float: left;
	overflow: hidden;
	border-radius: 50px;
}
.postedby_name
{
	width: auto;
	height: 100%;
	margin-left: 25px;
	float: left;
}
.attachment
{
	width: 100%;
	height: 47px;
	display: inline-block;
	border-bottom-left-radius: 20px;
	border-bottom-right-radius: 20px;
}
.photo_upload
{
	width: 120px;
	height: 40px;
	border-radius: 18px;
	float: left;
	cursor: pointer;
	margin-top: 3px;
	margin-left: 10px;
	background: #f4f4f4;
}
.post
{
	width: 80px;
	padding: 5px;
	background:#62c5c5;
	color: white;
	margin-right: 10px;
	margin-top: 10px;
	border: 1px solid #62c5c5;
	border-radius:10px;
	font-size: 15px;
	cursor: pointer;
}
#search_show
{
	box-shadow: 0px 0px 5px 0px #ededed;
}
.search_filter_list:hover
{
	background-color: #d5d5d5;
}
*:focus
{
	outline: none;
}
.status_upload_box
{
	width: 250px;
	height: 20px;
	margin: 10px 0px 10px 10px;
	box-shadow: 0px 0px 5px 0px #e1e1e1;
	background-color: white;
}
#upload_status_box
{
	width: 100%;
	height: auto;
}
.upload_file_name
{
	width: 230px;
	height: 20px;
	float: left;
}
.upload_cancel
{
	width: 20px; 
	height: 20px;  
	float: right;
	cursor: pointer;
}
.upload_cancel:hover
{
	background-color: #e1e1e1;
}
.side_nav
{
	width:340px;
	height: 500px;
	margin-top: 50px;
	display: inline-block;
	background-color: #ffffff;
	box-shadow: 0px 0px 2px 0px #d4d4d4;
}
.popular_profile_box_profile
	{
	    width: 100%;
		height: 60px;
		background-color: white;
		margin: 2px 0px 2px 0px;
		cursor: pointer;
	}
#nav_unread_message_status
{
	width: 15px;
	height: 15px;
	background-color: #ff5757;
	position: absolute;
	top: 8px; 
	right: 5px;
	border-radius: 20px;
}
.like_style:hover
{
background-color: #f4f4f4;
}

.banner
{
	width: 100%;
	height: 300px;
	max-width: 1800px;
	margin: 0px;
	position: relative;
	background-image: url('<?php echo $get_details['profile_header']; ?>');
	background-size: 100% auto;
	background-position: center;
	background-repeat: no-repeat;
}
.banner_change
{
	display: none;
}
.banner:hover > .banner_change
{
	width: 100%;
	height: 50px;
	display: block;
	position: absolute;
	bottom: 0px;
}
.banner_bottom_profile
{
	width: 100%;
	max-width: 1800px;
	height: 100px;
	background: white;
	border-bottom-left-radius: 25px;
	border-bottom-right-radius: 25px;
	box-shadow: 0px 1px 10px 0px #cfcfcf;
}
.name_diplay
{
	width: auto;
	height: auto;
	margin-left: 350px;
	float: left;
	display: inline-block;
}

.follow
{
	width: auto;
	height: auto;
	float: right;
	margin-right: 50px;
	display: inline-block;
	text-align: center;
}
.banner_main
{
	width: 100%;
	height:400px;
	max-width: 1800px;
	margin: auto;
	position: relative;
}
.profile_pic
{
	width: 200px;
	height: 200px;
	background: white;
	position: absolute;
	border: 3px solid white;
	border-radius: 50%;
	top: 190px;
	left: 130px;
	right: 0px;
	overflow: hidden;
}
#follow_button
{
margin-top: 30px;
font-size: 20px;
color: #fe6e60;
cursor: pointer;
padding: 5px 20px 5px 20px;
border-radius: 20px;	
background: white;
border: 1.5px solid #fe6e60;
transition: .5s;
}
#follow_button:hover
{
color: white;	
background: #fe6e60;
transition: .5s;
}
.main
{
	width: 100%;
	max-width: 1800px;
	margin: auto;
}
.side_nav_left
{
	width: 30%;
	height: auto;
	float: left;
}
.side_nav
{
	width: 400px;
	height: auto;
	background: white;
	box-shadow: 0px 0px 10px 0px #cfcfcf;
	float: left;
	margin-left: 10px;
	margin-top: 10px;
	padding-bottom: 20px;
	border-radius: 10px;
}
.side_content
{
	width: 70%;
	height: auto; 
	float: left;
}
.content
{
	width: 900px;
	height: auto;
	margin: auto;
}
.post_new
{
	width: 650px;
	height: 200px;
	border-radius: 20px;
	box-shadow: 0px 0px 10px 0px #cfcfcf; 
	margin: auto;
	background: white;
}
.postedby
{
	width: 100%;
	height: 50px;
	box-shadow:  0px 1px 0px 0px #cfcfcf; 
}
.postedby_photo
{
	width: 40px;
	height: 40px;
	position: relative;
	top:5px;
	left: 15px;
	float: left;
	overflow: hidden;
	border-radius: 50px;
	background: yellow;
}
.postedby_name
{
	width: auto;
	height: 100%;
	margin-left: 25px;
	float: left;
}
.attachment
{
	width: 100%;
	height: 47px;
	border-bottom-left-radius: 20px;
	border-bottom-right-radius: 20px;
}
.photo_upload
{
	width: 120px;
	height: 40px;
	border-radius: 18px;
	float: left;
	cursor: pointer;
	margin-top: 3px;
	margin-left: 10px;
	background: #f4f4f4;
}
.post
{
	width: 80px;
	padding: 5px;
	background:#62c5c5;
	color: white;
	margin-right: 10px;
	margin-top: 10px;
	border: 1px solid #62c5c5;
	border-radius:10px;
	font-size: 15px;
	cursor: pointer;
}
.top_nav
{
	width: 100%;
	height: 50px;
	max-width: 1600px;
	margin: auto;
	text-align: center;
	background: white;
	box-shadow: 0px 1px 5px #cccccc;
}
input:focus
{
	outline: none;
}
.icons
{
	width: 40px;
	height: 100%;
	float: right;
}
#profile_content_click
{
	position: absolute;
	margin-top: 50px;
	z-index: 1;
	width: auto;
	height: auto;
	background-color: red;	
}
.profile_pic:hover > .change_photo
{
	position: absolute;
	bottom:0px;
	width: 100%;
	height: 35%;
	cursor: pointer;
	transition: 0.5s;
	background-color: rgb(0,0,0,0.5);
}
.profile_pic:hover > .change_photo:hover
{
	background-color: black;
}
.dropdown_profile
{
	position: relative; 
	z-index: 1;
	float: right; 
	text-align: left;
	height: auto;
	margin: 0px; 
	display: none;
	cursor:pointer;	
	background-color: white;
}
.icons:hover > .dropdown_profile
{
	display: block;
}
.view_profile
{
	width: 95%; 
	font-size: 15px; 
	color: black;
	margin: 0px;
	text-decoration: none;
	margin: auto; 
	padding: 3px 0px 3px 0px;
	text-align: center; 
	font-family: arial;
	border-radius: 5px;
}
.view_profile:hover
{
	background-color: #f1f1f1;
}
.experience
{
	width: 800px;
	height: auto;
	float: left;
	border-radius: 5px;
	margin-top: 15px;
	margin-left: 30px;
	padding-bottom: 10px;
	background-color: white;
	box-shadow: 0px 1px 5px #cccccc;
}
.education
{
	width: 800px;
	height: auto;
	float: left;
	margin-top: 10px;
	border-radius: 5px;
	margin-left: 30px;
	padding-bottom: 10px;
	background-color: white;
	box-shadow: 0px 1px 5px #cccccc;
}
.skills_show
{
	width: 95%;
	height: auto;
	margin-left: 5%;
	background-color: yellow;
}
.skills_show p
{
	float: left;
	display: block;
	font-family: 'Roboto', sans-serif;
	padding: 5px;
	color: #4d4d4d;
}
.suggest_peoples
{
	width: 400px;
	height: auto;
	background: white;
	box-shadow: 0px 0px 10px 0px #cfcfcf;
	float: left;
	margin-left: 10px;
	margin-top: 10px;
	border-radius: 10px;
}
.suggest_people_show
{
	width: 100%;
	height: 40px;
	cursor: pointer;
	margin-top: 10px;
}
.social_accounts
{
	width: 400px;
	height: auto;
	background: white;
	padding-bottom: 10px;
	box-shadow: 0px 0px 10px 0px #cfcfcf;
	float: left;
	margin-left: 10px;
	margin-top: 10px;
	border-radius: 10px;
}
.fa {
  padding: 4px;
  font-size: 20px;
  width: 20px;
  text-align: center;
  text-decoration: none;
  margin: 5px 2px;
}

.fa:hover {
    opacity: 0.7;
}
.fa-twitter {
  margin-left: 10px;
  background: #55ACEE;
  color: white;
}
.fa-linkedin {
  margin-left: 10px;
  background: #007bb5;
  color: white;
}
.fa-facebook {
  margin-left: 10px;
  background: #3B5998;
  color: white;
}
.fa-instagram {
  margin-left: 10px;
  background: #3f729b;
  color: white;
}
</style>
<?php
require('js/php/get_profile_data.php');
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
</head>
<body>
<div class="top_nav">
<a href="news.php">
<img src="imags/klu.png" style="height: 90%; width: auto; margin-left: 10px; margin-top: 3px; float: left;" />
<p style="margin: 0px; color: black; font-family: 'Lato', sans-serif; font-size: 20px; padding-top: 12px; float: left;"><strong>Alumni Association</strong></p>
</a>
<div style="width: auto; height:100%; float: right;">
<div class="icons">
<div style="width: auto; height: 100%;float: left;">
<a href="logout.php">
<div onclick="profile_content_click();" style="width: 30px;  height: 30px; cursor: pointer;  overflow: hidden; border-radius: 30px; margin-top: 10px;">
<img src="logout_c.png" style="width: 100%; height: auto;">
</div>
</a>
</div>
</div>
<div class="icons">
<div style="width: auto; height: 100%;float: left;">
<a href="main_p.php">
<div onclick="profile_content_click();" style="width: 30px;  height: 30px; cursor: pointer; overflow: hidden; border-radius: 30px; margin-top: 10px;">
<img src="<?php echo $profile_pic; ?>" style="width: 100%; height: auto;">
<ul>
<li>Profile</li>
</ul>
</div>
</a>
</div>
<div class="dropdown_profile">
<div style="width: 250px; height: 60px; background-color: white;">
<div style="width: 50px; margin: 5px; height: 50px; border-radius: 60px; background-color: black; overflow: hidden; float:left;">
<img src="<?php echo $profile_pic; ?>" style="width: 100%; height: auto;" />
</div>
<div style="height: 50px; width: auto; float: left;">
<p style=" margin: 0px; padding-left: 5px; padding-top: 10px; font-family: arial; font-size: 15px;"><?php echo $profile_name; ?></p>
</div>
</div>
<a style="text-decoration: none;" href="edit.php"><p class="view_profile">View Profile</p></a>
<div style="width: 100%; height: 0.5px; background-color: #bebebe; margin-top: 5px;">
</div>
<a href="news.php" style="text-decoration: none;"><p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; padding: 7px 0px 2px 7px; font-size: 15px; ">News</p></a>
<a href="offer.php" style="text-decoration: none;"><p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; padding: 7px 0px 2px 7px; font-size: 15px; ">Job postings</p></a>
<a href="password_change.php" style="text-decoration: none;"><p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; padding: 7px 0px 2px 7px; font-size: 15px; ">Change Password</p></a>
<p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; padding: 7px 0px 2px 7px; font-size: 15px;">Privacy policy</p>
<a href="edit.php" style="text-decoration: none;"><p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; padding: 7px 0px 2px 7px; font-size: 15px;">Settings</p></a>

</div>
</div>
</div>
</div>
<div class="banner_main">
<div id="banner" class="banner">

</div>
<div class="profile_pic" id="profile_pic">
<img src="<?php echo $get_details['profile_pic']; ?>" style="width: 100%; height: auto; margin: auto; position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px;"/>
</div>
<div class="banner_bottom_profile" id="banner_bottom_profile">
<div class="name_diplay" id="name_diplay">
<div style="width: 100%; height: 35px; padding-right: 100px;">
<p style="font-family: 'Noto Sans', sans-serif; float: left; margin: 0px; font-size: 25px;" id="name"><?php echo $get_details['profile_name']; ?></p>
<?php
if($verified_status=="VERIFIED")
{
	echo '
	<div id="verified_account" style=" width: 18px; height: 18px; margin-left: 7px; margin-top: 10px; float: left; ">
<img src="imags/verify.png" alt="Verified" style="width: 100%; height: auto;"/>
</div>
	';
}
?>

</div>
<p style="font-family: 'Noto Sans', sans-serif; margin: 0px; font-size: 15px; color: #989898; padding-top: 2px;"><?php echo $get_personal_details['job']." ,".$get_personal_details['company']; ?></p>
<p style="font-family: 'Noto Sans', sans-serif; margin: 0px; font-size: 13px; color: #989898;padding-top: 1px;">(<?php echo $get_details['year_graduate']; ?> Batch)</p>
</div>
<div class="follow" id="follow">
<button onclick="follow_button(this);" value="<?php echo $visitors_profile_code; ?>"  id="follow_button" style="float: right;">Follow</button>
<div style="width: 100px; height: 70px; margin-right: 20px; margin-top: 10px; float: right;">
<div style="width: 100%; height: 40px;">
<p style="margin: 0px; font-family: 'Noto Sans', sans-serif; padding-top: 14px; font-size: 14px;">Followers</p>
</div>
<p style="margin: 0px; font-family: arial;"><?php echo $third_follower; ?></p>
</div>
<div style="width: 100px; height: 70px;  margin-right: 10px; margin-top: 10px; float: right;">
<div style="width: 100%; height: 40px;">
<p style="margin: 0px; font-family: 'Noto Sans', sans-serif; padding-top: 14px; font-size: 14px;">Following</p>
</div>
<p style="margin: 0px; font-family: arial;"><?php echo $third_following; ?></p>
</div>
</div>
<?php
error_reporting(0);
require_once('js/php/conn.php');
$get=mysqli_query($conn,"select following,request_sent,code from profile where email='$session';");
$get=mysqli_fetch_assoc($get);
$follower=$get['following'];
$follower=explode(",",$follower);
$request_sent=$get['request_sent'];
$request_sent=explode(",",$request_sent);
for($i=0;$i<sizeof($request_sent);$i++)
{
	$t=$visitors_profile_code;
	if($request_sent[$i]==$visitors_profile_code)
	{
		echo '
		<script>
		document.getElementById("follow_button").innerHTML="Requested";
	document.getElementById("follow_button").style.backgroundColor="#fe6e60";
	document.getElementById("follow_button").style.color="white";
	</script>
		';
		break;
	}
}
for($i=0;$i<sizeof($follower);$i++)
{
	$t=$visitors_profile_code;
	if($follower[$i]==$visitors_profile_code)
	{
		echo '
		<script>
		document.getElementById("follow_button").innerHTML="Following";
	document.getElementById("follow_button").style.backgroundColor="#fe6e60";
	document.getElementById("follow_button").style.color="white";
	</script>
		';
		break;
	}
}
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
function follow_button(v)
{
	var value=v.value;
	$.ajax({
type: 'post',
 url: 'js/php/follow_button.php',
 data : {value:value},
 success: function(code){
var m=code;
if(m=="following")
{
	document.getElementById("follow_button").innerHTML="Following";
	document.getElementById("follow_button").style.backgroundColor="#fe6e60";
	document.getElementById("follow_button").style.color="white";
}
else if(m=="Requested")
{
	document.getElementById("follow_button").innerHTML="Requested";
	document.getElementById("follow_button").style.backgroundColor="#fe6e60";
	document.getElementById("follow_button").style.color="white";
}
else if(m=='unfollowed')
{
	document.getElementById("follow_button").innerHTML="Follow";
	document.getElementById("follow_button").style.backgroundColor="white";
	document.getElementById("follow_button").style.color="#fe6e60";
}
else
{
	document.getElementById("follow_button").innerHTML="Requested";
	document.getElementById("follow_button").style.backgroundColor="#fe6e60";
	document.getElementById("follow_button").style.color="white";
}
 }
 });
}
</script>
</div>
</div>
<div class="main">
<div class="side_nav_left">
<div class="side_nav">
<?php
if($get_personal_details['company']!='')
{
	$temp=$get_personal_details['company'];
	
	echo '
	<div style="width: 100%; height: 25px; margin-top: 20px; background-color: white;">
<img src="company.png" style="margin-left: 50px; margin-right: 8px; height: 90%; margin-top:5px; width: auto; float: left;" />
<p style="margin: 0px; font-family: '."'Noto Sans'".', sans-serif; font-size: 11px; padding-top: 11px;">Working at &nbsp;<strong id="working">'.$temp.'</strong></p>
</div>
	
	';
}

?>

<?php
if($get_personal_details['job']!='')
{
	$temp=$get_personal_details['job'];
	
	echo '
<div style="width: 100%; height: 25px; margin-top: 5px; background-color: white;">
<img src="job.png" style="margin-left: 50px; margin-right: 14px; height: 15px; margin-top:11px; width: auto; float: left;" />
<p style="margin: 0px; font-family: '."'Noto Sans'".', sans-serif; font-size: 11px; padding-top: 12px; word-spacing:1px;">Role &nbsp;<strong id="working">'.$temp.'</strong></p>
</div>
	';
}
?>

<?php
if($higher_education_t!='')
{
	
	echo '
<div style="width: 100%; height: 25px; margin-top: 5px; background-color: white;">
<img src="education.png" style="margin-left: 50px; margin-right: 8px; height: 90%; margin-top:8px; width: auto; float: left;" />
<p style="margin: 0px; font-family: '."'Noto Sans'".', sans-serif; font-size: 11px; padding-top: 12px; word-spacing:1px;">Studied &nbsp;<strong id="working">'.$higher_education_t.' at '.$higher_education_completed_t.'</strong></p>
</div>
	';
}
?>

<div style="width: 100%; height: 25px; margin-top: 6px; background-color: white;">
<img src="home.png" style="margin-left: 50px; margin-right: 8px; height: 90%; margin-top:8px; width: auto; float: left;" />
<p style="margin: 0px; font-family: 'Noto Sans', sans-serif; font-size: 11px; padding-top: 12px; word-spacing:1px;">Current City &nbsp;<strong id="working"><?php echo $get_personal_details['town'].', '.$get_personal_details['country']; ?></strong></p>
</div>
<div style="width: 100%; height: 25px; margin-top: 6px; background-color: white;">
<img src="location.png" style="margin-left: 50px; margin-right: 8px; height: 90%; margin-top:8px; width: auto; float: left;" />
<p style="margin: 0px; font-family: 'Noto Sans', sans-serif; font-size: 11px; padding-top: 12px; word-spacing:1px;">Home Town &nbsp;<strong id="working"><?php echo $get_details['hometown'].', '.$get_details['country'];  ?></strong></p>
</div>
<p style="font-size: 13px; padding-top: 15px; padding-left: 15px; font-family: 'Lato', sans-serif;"><strong>Contact Information</strong></p>
<div style="width: 100%; height: 25px; margin-top: 6px; background-color: white;">
<img src="email.png" style="margin-left: 50px; margin-right: 8px; height: 90%; margin-top:8px; width: auto; float: left;" />
<p style="margin: 0px; font-family: 'Noto Sans', sans-serif; font-size: 11px; padding-top: 12px; word-spacing:1px;">Email &nbsp;<strong id="email" style="cursor: pointer;" onclick="request(this);">Request Email</strong></p>
</div>
<div style="width: 100%; height: 25px; margin-top: 6px; background-color: white;">
<img src="phone.png" style="margin-left: 50px; margin-right: 8px; height: 90%; margin-top:8px; width: auto; float: left;" />
<p style="margin: 0px; font-family: 'Noto Sans', sans-serif; font-size: 11px; padding-top: 12px; word-spacing:1px;">Phone &nbsp;<strong id="phone" style="cursor: pointer;" onclick="request(this);">Request Number</strong></p>
</div>


</div>
<div class="suggest_peoples">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 15px; padding-left: 10px;"><strong>Suggested Peoples</strong></p>

<?php
require('js/php/conn.php');
$get_data=mysqli_query($conn,"select profile_name,profile_pic,hash_key,job,company,status from profile inner join auth on profile.email=auth.email inner join personal on personal.email=profile.email order by visitors_count DESC limit 3;");
while($get_t=mysqli_fetch_assoc($get_data))
{
	$pic=$get_t['profile_pic'];
	$profile_name=$get_t['profile_name'];
	$hask_key=$get_t['hash_key'];
	$job=$get_t['job'];
	$company=$get_t['company'];
	$status=$get_t['status'];
	
	echo '
	<a href="profile.php?getdetails='.$hask_key.'">
	<div class="suggest_people_show">
	<div style="overflow: hidden; width: 40px; height: 40px; border-radius: 40px; margin-left: 40px; float: left;">
	<img src="'.$pic.'" style="width: 100%; height: auto">
	</div>
	<div style="width: auto; height: 100%; float: left;">
	<p style=" margin: 0px; font-family: arial; padding-top: 5px; font-size: 13px; color: black; padding-left: 10px;"><strong>'.$profile_name.' <img src="imags/verify.svg" style="width: 13px; height: auto; padding-bottom: 5px;"></strong></p>
	<p style=" margin: 0px; font-family: arial; padding-top: 2px; font-size: 10px; color: black; padding-left: 10px;">'.$job.', '.$company.'</p>
	</div>
	</div>
	</a>
	';
}
?>


<a href="as.php" style="text-decoration: none;">
<div style="width: 100%; height: 20px; padding-bottom: 10px; padding-top: 15px; cursor: pointer;">
<p style="text-align: center; margin: 0px; font-size: 15px; color: black; font-family: arial;">Show more</p>
</div>
</div>
</a>

<div class="social_accounts">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 15px; padding-left: 10px;"><strong>Social Media</strong></p>
<?php
$c=0;
if($get_details['facebook']!="")
{
	echo '<div style="width: auto; display: inline-block; height: 30px;">
<a href="'.$get_details['facebook'].'" class="fa fa-facebook"></a>
</div>';
$c++;
}
if($get_details['linkedin']!="")
{
	echo '<div style="width: auto; display: inline-block; height: 30px;">
<a href="'.$get_details['linkedin'].'" class="fa fa-linkedin"></a>
</div>';
$c++;
}
if($get_details['twitter']!="")
{
	echo '<div style="width: auto; display: inline-block; height: 30px;">
<a href="'.$get_details['twitter'].'" class="fa fa-twitter"></a>
</div>';
$c++;
}
if($get_details['instagram']!="")
{
	echo '<div style="width: auto; display: inline-block; height: 30px;">
<a href="'.$get_details['instagram'].'" class="fa fa-instagram"></a>
</div>';
$c++;
}

if($c==0)
{
	echo "<p style='font-family: arial; text-align: center;'>No Accounts Found</p>";
}
?>

</div>

</div>
<div class="side_content">
<div class="content">

<div class="experience">
<p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; font-size: 23px; padding: 10px; padding-left: 15px;">Experience</p>

<?php 
require('js/php/conn.php');
for($i=0;$i<sizeof($previous_company);$i=$i+8)
{
	$check=$previous_company[$i+1];
	$check=mysqli_query($conn,"select * from organization where organization_name='$check';");
	if(mysqli_num_rows($check)==1)
	{
		$check=mysqli_fetch_assoc($check);
		$check=$check['code'];
	if($i==0)
	{
		echo '<div style="width: 100%; height: 120px; margin-top: 40px;">
<div style="width: 20%; height: 120px;  text-align: center;  float: left;">
<img src="organization_images/'.$check.'.png" style="width:80px; height: auto;" />
</div>
<div style="width: 80%; height: 120px; float: left; ">
<p style="margin: 0px; font-family: '.'Roboto'.', sans-serif; font-size: 18px; ">'.$previous_company[$i].'</p>
<p style="margin: 0px; font-family: '.'Roboto'.', sans-serif; font-size: 14px; padding-top: 10px;">'.$previous_company[$i+1].'</p>
<p style="margin: 0px; font-family: '.'Roboto'.', sans-serif; font-size: 14px; padding-top: 5px;">'.$previous_company[$i+2].' '.$previous_company[$i+3].' - '.$previous_company[$i+4].' '.$previous_company[$i+5].'</p>
<p style="margin: 0px; color: #6d6d6d; font-family: '.'Roboto'.', sans-serif; font-size: 12px; padding-top: 5px;">'.$previous_company[$i+6].', '.$previous_company[$i+7].'</p>
</div>
</div>';
	}
	else
	{
		echo '<div style="width: 90%; margin: auto; height: 1px; background-color: #6d6d6d;">
</div>
		<div style="width: 100%; height: 120px; margin-top: 40px;">
<div style="width: 20%; height: 120px;  text-align: center;  float: left;">
<img src="organization_images/'.$check.'.png" style="width:80px; height: auto;" />
</div>
<div style="width: 80%; height: 120px; float: left; ">
<p style="margin: 0px; font-family: '.'Roboto'.', sans-serif; font-size: 18px; ">'.$previous_company[$i].'</p>
<p style="margin: 0px; font-family: '.'Roboto'.', sans-serif; font-size: 14px; padding-top: 10px;">'.$previous_company[$i+1].'</p>
<p style="margin: 0px; font-family: '.'Roboto'.', sans-serif; font-size: 14px; padding-top: 5px;">'.$previous_company[$i+2].' '.$previous_company[$i+3].' - '.$previous_company[$i+4].' '.$previous_company[$i+5].'</p>
<p style="margin: 0px; color: #6d6d6d; font-family: '.'Roboto'.', sans-serif; font-size: 12px; padding-top: 5px;">'.$previous_company[$i+6].', '.$previous_company[$i+7].'</p>
</div>
</div>';
	}
	}
	else
	{
		if($i==0)
	{
		echo '<div style="width: 100%; height: 120px; margin-top: 40px;">
<div style="width: 20%; height: 120px;  text-align: center;  float: left;">
<img src="company_p.png" style="width:80px; height: auto;" />
</div>
<div style="width: 80%; height: 120px; float: left; ">
<p style="margin: 0px; font-family: '.'Roboto'.', sans-serif; font-size: 18px; ">'.$previous_company[$i].'</p>
<p style="margin: 0px; font-family: '.'Roboto'.', sans-serif; font-size: 14px; padding-top: 10px;">'.$previous_company[$i+1].'</p>
<p style="margin: 0px; font-family: '.'Roboto'.', sans-serif; font-size: 14px; padding-top: 5px;">'.$previous_company[$i+2].' '.$previous_company[$i+3].' - '.$previous_company[$i+4].' '.$previous_company[$i+5].'</p>
<p style="margin: 0px; color: #6d6d6d; font-family: '.'Roboto'.', sans-serif; font-size: 12px; padding-top: 5px;">'.$previous_company[$i+6].', '.$previous_company[$i+7].'</p>
</div>
</div>';
	}
	else
	{
		echo '<div style="width: 90%; margin: auto; height: 1px; background-color: #6d6d6d;">
</div>
		<div style="width: 100%; height: 120px; margin-top: 40px;">
<div style="width: 20%; height: 120px;  text-align: center;  float: left;">
<img src="company_p.png" style="width:80px; height: auto;" />
</div>
<div style="width: 80%; height: 120px; float: left; ">
<p style="margin: 0px; font-family: '.'Roboto'.', sans-serif; font-size: 18px; ">'.$previous_company[$i].'</p>
<p style="margin: 0px; font-family: '.'Roboto'.', sans-serif; font-size: 14px; padding-top: 10px;">'.$previous_company[$i+1].'</p>
<p style="margin: 0px; font-family: '.'Roboto'.', sans-serif; font-size: 14px; padding-top: 5px;">'.$previous_company[$i+2].' '.$previous_company[$i+3].' - '.$previous_company[$i+4].' '.$previous_company[$i+5].'</p>
<p style="margin: 0px; color: #6d6d6d; font-family: '.'Roboto'.', sans-serif; font-size: 12px; padding-top: 5px;">'.$previous_company[$i+6].', '.$previous_company[$i+7].'</p>
</div>
</div>';
	}
	}
}

?>

</div>

<div class="education">
<p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; font-size: 23px; padding: 10px; padding-left: 15px;">Education</p>


<?php 
require("js/php/conn.php");
for($i=0;$i<sizeof($education_previous);$i=$i+8)
{
	if($i==0)
	{
		echo '<div style="width: 100%; height: 120px; margin-top: 40px;">
<div style="width: 20%; height: 120px;  text-align: center;  float: left;">
<img src="company_p.png" style="width:80px; height: auto;" />
</div>
<div style="width: 80%; height: 120px; float: left; ">
<p style="margin: 0px; font-family: '.'Roboto'.', sans-serif; font-size: 18px; ">'.$education_previous[$i].'</p>
<p style="margin: 0px; font-family: '.'Roboto'.', sans-serif; font-size: 14px; padding-top: 10px;">'.$education_previous[$i+1].' '.$education_previous[$i+2].'</p>
<p style="margin: 0px; font-family: '.'Roboto'.', sans-serif; font-size: 14px; padding-top: 5px;">'.$education_previous[$i+4].' '.$education_previous[$i+5].' - '.$education_previous[$i+6].' '.$education_previous[$i+7].'</p>
<p style="margin: 0px; color: #6d6d6d; font-family: '.'Roboto'.', sans-serif; font-size: 12px; padding-top: 5px;">'.$education_previous[$i+3].'</p>
</div>
</div>';
	}
	else
	{
		echo '<div style="width: 90%; margin: auto; height: 1px; background-color: #6d6d6d;">
</div>
		<div style="width: 100%; height: 120px; margin-top: 40px;">
<div style="width: 20%; height: 120px;  text-align: center;  float: left;">
<img src="company_p.png" style="width:80px; height: auto;" />
</div>
<div style="width: 80%; height: 120px; float: left; ">
<p style="margin: 0px; font-family: '.'Roboto'.', sans-serif; font-size: 18px; ">'.$education_previous[$i].'</p>
<p style="margin: 0px; font-family: '.'Roboto'.', sans-serif; font-size: 14px; padding-top: 10px;">'.$education_previous[$i+1].' '.$education_previous[$i+2].'</p>
<p style="margin: 0px; font-family: '.'Roboto'.', sans-serif; font-size: 14px; padding-top: 5px;">'.$education_previous[$i+4].' '.$education_previous[$i+5].' - '.$education_previous[$i+6].' '.$education_previous[$i+7].'</p>
<p style="margin: 0px; color: #6d6d6d; font-family: '.'Roboto'.', sans-serif; font-size: 12px; padding-top: 5px;">'.$education_previous[$i+3].'</p>
</div>
</div>';
	}
	
}

?>


</div>
<div id="education" class="education">
<p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; font-size: 23px; padding: 10px; padding-left: 15px;">Skills</p>
<div class="skills_show">

<?php
$third_skills=explode(",",$third_skills);
for($i=0;$i<sizeof($third_skills);$i++)
{
	echo '<p>• '.$third_skills[$i].'</p>';
}
?>
</div>
</div>

<div id="education" class="education">
<p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; font-size: 23px; padding: 10px; padding-left: 15px;">Posts</p>

</div>
<div style="width: 800px; height: auto; float: left; margin-left:30px;">
<?php
require_once('js/php/conn.php');
$check=mysqli_query($conn,"select * from posts where status='POSTED' and user='$third_email' ORDER BY code DESC;");
while($c=mysqli_fetch_assoc($check))
{
	$files=$c['dis_files'];
	$content=$c['description'];
	$user=$c['user'];
	$likes=$c['likes'];
	$liked=0;
	$likes=explode(",",$likes);
	for($i=0;$i<sizeof($likes);$i++)
	{
	  if($likes[$i]==$profile_code)
	  {
		 $liked=1; 
	  }		  
	}
	$like_count=$c['likes_count'];
	$files=explode(",",$files);
	$code=$c['code'];
	$time=get_time_d($c['posted_time']);
	$content_c=preg_split("/\R/", $content);
	
		$get_data=mysqli_query($conn,"select profile.profile_name,profile_pic,hash_key,job,company from profile 
		inner join personal on personal.email=profile.email 
		where personal.email='$user';");
		$get_data=mysqli_fetch_array($get_data);
		$profile_pic=$get_data[1];
		$profile_name=$get_data[0];
		$st=$get_data[3].', '.$get_data[4];
		$hash_key="profile.php?getdetails=".$get_data[2];
	
	
	if(sizeof($files)>1)
	{
			echo '
				
				<div class="post_style">
				<div class="post_top">
				<div class="profile_logo">
				<img src="'.$profile_pic.'" style="width: 100%; height: auto;">
				</div>
				<div class="profile_details">
				<a style="text-decoration: none;" href="'.$hash_key.'">
				<p style="margin: 0px; color: black; padding-left: 8px; text-align: left; font-family: '."'Noto Sans'".', sans-serif; font-size: 13px; padding-top: 8px;">'.$profile_name.'</p>
				<p style="margin: 0px; color: black; padding-left: 8px; font-family: '."'Noto Sans'".', sans-serif; font-size: 10px;">@ '.$st.'</p>
				</a>
				</div>

				</div>
				<div class="post_body">
				<div class="post_text">';
				
				
				for($i=0;$i<sizeof($content_c);$i++)
				{
					echo '<p style="font-family: '."'Open Sans'",', sans-serif; font-size: 13px; text-align: left; margin: 0px; padding: 5px 5px 0px 10px;">'.$content_c[$i].'</p>';
				}
				echo '
				</div>
				<div class="post_image">
				  <div id="myCarousel'.$code.'" class="carousel slide" data-ride="carousel">
				   <ol class="carousel-indicators">
				';
				for($i=0;$i<sizeof($files);$i++)
				{
					if($i==0)
					{
						echo '<li data-target="#myCarousel'.$code.'" data-slide-to="'.$i.'" class="active"></li>';
					}
					else
					{
						echo '<li data-target="#myCarousel'.$code.'" data-slide-to="'.$i.'"></li>';
					}
				}
				echo '
				    </ol>
					<div class="carousel-inner">
				';
				$k=0;
				for($i=0;$i<sizeof($files);$i++)
				{
					$ext=$files[$i];
					$ext=explode('.',$ext);
					$ext=end($ext);
					$ext=strtolower($ext);
					if($ext=="jpg" or $ext=="jpeg" or $ext=="bmp" or $ext=="gif" or $ext=="png")
					{
					  if($i==0)
					  {
						  echo '<div class="item active">
							<img src="js/php/posts/'.$code.'/';
							echo $files[$i];
							echo'"alt="Los Angeles" style="width:100%;">
							</div>';
					  }
					  else
					  {
						  echo '<div class="item">
								<img src="js/php/posts/'.$code.'/';
								echo $files[$i];
								echo'"alt="Los Angeles" style="width:100%;">
								</div>';
					  }
					}
					else if($ext=='mp4')
					{
						
						 if($i==0)
						  {
							echo '
							<div class="item active">
							<video controls="autoplay" style="width:100%;height:auto;" ">
							<source src="devstories.webm" type="video/webm;codecs=&quot;vp8, vorbis&quot;">
							<source src="js/php/posts/'.$code.'/'.$files[$i].'" type="video/mp4;codecs=&quot;avc1.42E01E, mp4a.40.2&quot;">
							<track src="devstories-en.vtt" label="English subtitles" kind="subtitles" srclang="en" default="">
						  </video>	
						  </div>';
								
								
						  }
					  else
					  {
						  	echo '
							<div class="item">
							<video controls="autoplay" style="width:100%;height:auto;" ">
							<source src="devstories.webm" type="video/webm;codecs=&quot;vp8, vorbis&quot;">
							<source src="js/php/posts/'.$code.'/'.$files[$i].'" type="video/mp4;codecs=&quot;avc1.42E01E, mp4a.40.2&quot;">
							<track src="devstories-en.vtt" label="English subtitles" kind="subtitles" srclang="en" default="">
						  </video>	
						  </div>';
					  }						
					}
					else
					{
						$k=$k+1;
								echo '
							<div style="width: 95%; height: 40px; background-color: #fcfcfc; margin: auto;">
							<div style="width: auto; height: 40px; float: left;">
							<p style="font-family: arial; font-size: 14px; padding-top: 10px; padding-left: 10px; margin:0px;">'.$files[$i].'</p>
							</div>
							<a href="post_file_download.php?code='.$code.'&file='.$files[$i].'">
							<div style="width: 20px; cursor: pointer; height: 100%; position: relative; float: right;">
							<img src="imags/download.png" style="width: 20px; height: auto; cursor: pointer; margin: auto; position: absolute; bottom:0px; left: 0px; right:0px; top: 0px;" />
							</div>
							</a>
							</div>
							';
					}
				}
				echo '</div>';
				 if($k==0)
				 {
					 echo'
					 <a class="left carousel-control" href="#myCarousel'.$code.'" data-slide="prev" style="background-image: none; height: 50%; margin-top: 15%;">
				  <span class="glyphicon glyphicon-chevron-left"></span>
				  <span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#myCarousel'.$code.'" data-slide="next" style="background-image: none; height: 50%; margin-top: 15%;">
				  <span class="glyphicon glyphicon-chevron-right"></span>
				  <span class="sr-only">Next</span>
				</a>';
				 }
				echo '
				</div>
				</div>
				</div>
				<div class="post_like" style="text-align: right;">';
				
				if($liked==1)
				{
					echo '<div style="width: 100px; float: left; height: 25px;   border-radius: 5px; cursor: pointer; margin-top: 10px; margin-left: 10px;" class="like_style" onclick="like(this);" id="'.$code.'">';
				}
				else
				{
					echo '<div style="width: 100px; float: left; height: 25px;  filter: grayscale(100%); border-radius: 5px;  -webkit-filter: grayscale(100%); cursor: pointer; margin-top: 10px; margin-left: 10px;" class="like_style" onclick="like(this);" id="'.$code.'">';
				}
				echo'
				
				<div style="width: 25px; height: 25px; position: relative; float: left;" >
				<img src="imags/like.png" style="width: 20px; height: 20px; position: absolute; top: 0px; bottom: 0px; left:5px; right: 0px; margin: auto;" />
				</div>
				<p style="font-family: '."'Baloo Chettan'".', cursive; float: left; margin: 0px; padding-left: 10px;padding-top: 1px; font-size: 17px; color: #D32D30;">Like</p>
				</div>
			    <p id="'.$code.'_count" style="font-size: 13px; font-family: '."'Noto Sans'".', sans-serif; padding-right: 10px; padding-top: 15px;  float: right;">'.$like_count.' Likes</p>
			    </div>
				<p style="font-family: '."'Noto Sans'".', sans-serif; margin: 0px; text-align: left; font-size: 12px; padding: 5px;">'.$time.' ago</p>
				
				</div>';
	}
	else
	{
		$ext=$files[0];
		$ext=explode('.',$ext);
		$ext=end($ext);
		$ext=strtolower($ext);
		
		
		echo '
				
				<div class="post_style">
				<div class="post_top">
				<div class="profile_logo">
				<img src="'.$profile_pic.'" style="width: 100%; height: auto;">
				</div>
				<div class="profile_details">
				<a style="text-decoration: none;" href="'.$hash_key.'">
				<p style="margin: 0px; color: black; padding-left: 8px; text-align: left; font-family: '."'Noto Sans'".', sans-serif; font-size: 13px; padding-top: 8px;">'.$profile_name.'</p>
				<p style="margin: 0px; color: black; padding-left: 8px; font-family: '."'Noto Sans'".', sans-serif; font-size: 10px;">@ '.$st.'</p>
				</a>
				</div>

				</div>
				<div class="post_body">
				<div class="post_text">
				';
				for($i=0;$i<sizeof($content_c);$i++)
				{
					echo '<p style="font-family: '."'Open Sans'",', sans-serif; font-size: 13px; text-align: left; margin: 0px; padding: 5px 5px 0px 10px;">'.$content_c[$i].'</p>';
				}
				echo '
				</div>
				<div class="post_image">';
			     if($ext=="mp4")
				{
					echo '<video controls="autoplay" style="width:100%;height:auto;" ">
					<source src="devstories.webm" type="video/webm;codecs=&quot;vp8, vorbis&quot;">
					<source src="js/php/posts/'.$code.'/'.$files[0].'" type="video/mp4;codecs=&quot;avc1.42E01E, mp4a.40.2&quot;">
					<track src="devstories-en.vtt" label="English subtitles" kind="subtitles" srclang="en" default="">
				  </video>	';
				}
				else if($ext=="jpg" or $ext=="jpeg" or $ext=="bmp" or $ext=="gif" or $ext=="png")
				{
					echo '<img src="js/php/posts/'.$code.'/'.$files[0].'" style="width: 100%; height: auto;">';
				}
				else if($ext!='')
				{
					echo '
					<div style="width: 95%; height: 40px; background-color: #fcfcfc; margin: auto;">
					<div style="width: auto; height: 40px; float: left;">
					<p style="font-family: arial; font-size: 14px; padding-top: 10px; padding-left: 10px; margin:0px;">'.$files[0].'</p>
					</div>
					<a href="post_file_download.php?code='.$code.'&file='.$files[0].'">
					<div style="width: 20px; cursor: pointer; height: 100%; position: relative; float: right;">
					<img src="imags/download.png" style="width: 20px; height: auto; cursor: pointer; margin: auto; position: absolute; bottom:0px; left: 0px; right:0px; top: 0px;" />
					</div>
					</a>
					</div>
					';
				}
				echo'
				</div>
				</div>
			
				<div class="post_like" style="text-align: right;">';
				
				if($liked==1)
				{
					echo '<div style="width: 100px; height: 25px; float: left;  border-radius: 5px; cursor: pointer; margin-top: 10px; margin-left: 10px;" class="like_style" onclick="like(this);" id="'.$code.'">';
				}
				else
				{
					echo '<div style="width: 100px; height: 25px; float: left;  filter: grayscale(100%); border-radius: 5px;  -webkit-filter: grayscale(100%); cursor: pointer; margin-top: 10px; margin-left: 10px;" class="like_style" onclick="like(this);" id="'.$code.'">';
				}
				echo '
				<div style="width: 25px; height: 25px; position: relative; float: left;" >
				<img src="imags/like.png" style="width: 20px; height: 20px; position: absolute; top: 0px; bottom: 0px; left:5px; right: 0px; margin: auto;" />
				</div>
				<p style="font-family: '."'Baloo Chettan'".', cursive; float: left; margin: 0px; padding-left: 10px;padding-top: 1px; font-size: 17px; color: #D32D30;">Like</p>
				</div>
				<p id="'.$code.'_count" style="font-size: 13px; font-family: '."'Noto Sans'".', sans-serif; padding-right: 10px; padding-top: 15px;  float: right;">'.$like_count.' Likes</p>
			    </div>
				<p style="font-family: '."'Noto Sans'".', sans-serif; margin: 0px; text-align: left; font-size: 12px; padding: 5px;">'.$time.' ago</p>
				</div>
				
				';
	}
	
}
?>
<?php
function get_time_d($time)
{
date_default_timezone_set('Asia/Calcutta'); 
$t=date('Y-m-d H:i:s');
$datetime1 = new DateTime($t);
$datetime2 = new DateTime($time);
$interval = $datetime1->diff($datetime2);
$years=$interval->format('%y');
$months=$interval->format('%m');
$days=$interval->format('%d');
$hours=$interval->format('%h');
$minutes=$interval->format('%i');
	if($years>0)
	{
		if($months>=1)
		{
			return ("$years years $months months");
		}
		else
		{
			return ("$years years");
		}
	}
	else if ($months>=1)
	{
		if($days>=1)
		{
			return ("$months months $days days");
		}
		else
		{
			return ("$months months");
		}
	}
	else if($days>=1)
	{
		return("$days days");
	}
	else if($hours>=1)
	{
		return("$hours hours $minutes minutes");
	}
	else
	{
		return("$minutes minutes");
	}
}
?>

<script>
function like(a)
{
  var cod=a.id;	
  var like=a.style.WebkitFilter;
  var m=0;
  if(like=="grayscale(100%)")
  {
  a.style.filter = "grayscale(0%)";
  a.style.WebkitFilter = "grayscale(0%)";
  m=1;
  }
  else
  {
  a.style.filter = "grayscale(100%)";
  a.style.WebkitFilter = "grayscale(100%)";
  m=0;
  }
  
    $.ajax({
	 type: 'post',
	 url: 'js/php/like.php',
	 data : {cod:cod},
	 success: function(code){
	 document.getElementById(cod+"_count").innerHTML=code+' Likes';
	 }
	 });
  
}
</script>
</div>

</div>
</div>
</div>
</body>
</html>