<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Lato|Roboto" rel="stylesheet">
<style>
body
{
margin: 0px;
}
.icon_dis
{
	width: 100%;
	height: 70px;
	max-width: 1800px;
	margin: auto;
}
.main_icon
{
	width: 400px;
	height: 100%;
}
.main_icon img
{
	height: 80%;
	width: auto;
	float: left;
	margin-top: 6px;
	margin-left: 10px;
}
.main_icon p
{
	font-size: 20px;
	padding-top: 35px;
	font-family: 'Lato', sans-serif;
}
.nav_images
{
	width: 100%;
	height: 50px;
	background-color: red;
}
.events_display_box
{
	width: 450px;
	height: 130px;
	cursor: pointer;
	margin: 10px 10px 10px 10px;
	display: inline-block;
	border-radius: 10px;
	position: relative;
	background-color: white;
	overflow: hidden;
	transition: 0.5;
	box-shadow: 0px 0px 5px 0px #d6d6d6;
}
.events_display_box:hover
{
	box-shadow: 0px 0px 1px 0px #d6d6d6;
	transition: 0.5;
}
.dropdownmenu
{
	width: 100%;
	height: 40px;
	background-color: #A81E24;
}
@charset "UTF-8";
.navigation {
  height: 70px;
}

.brand {
  position: absolute;
  padding-left: 20px;
  float: left;
  line-height: 70px;
  text-transform: uppercase;
  font-size: 1.4em;
}
.brand a,
.brand a:visited {
  color: #ffffff;
  text-decoration: none;
}

.nav-container {
  max-width: 1000px;
  margin: 0 auto;
}

nav {
  float: right;
}
nav ul {
  list-style: none;
  margin: 0;
  padding: 0;
}
nav ul li {
  float: left;
  font-family: 'Roboto', sans-serif;
  font-size: 15px;
  position: relative;
}
nav ul li a,
nav ul li a:visited {
  display: block;
  padding: 0 20px;
  line-height: 40px;
  background: #A81E24;
  color: #ffffff;
  text-decoration: none;
}
nav ul li a:hover,
nav ul li a:visited:hover {
  background: #bd3339;
  color: #ffffff;
}
nav ul li a:not(:only-child):after,
nav ul li a:visited:not(:only-child):after {
  padding-left: 4px;
  content: " ▾";
}
nav ul li ul li {
  min-width: 190px;
}
nav ul li ul li a {
  padding: 8px;
  font-size: 14px;
  line-height: 20px;
}

.nav-dropdown {
  position: absolute;
  display: none;
  z-index: 1;
  box-shadow: 0 3px 12px rgba(0, 0, 0, 0.15);
}

/* Mobile navigation */
.nav-mobile {
  display: none;
  position: absolute;
  top: 0;
  right: 0;
  background: #262626;
  height: 70px;
  width: 70px;
}

@media only screen and (max-width: 798px) {
  .nav-mobile {
    display: block;
  }

  nav {
    width: 100%;
    padding: 70px 0 15px;
  }
  nav ul {
    display: none;
  }
  nav ul li {
    float: none;
  }
  nav ul li a {
    padding: 15px;
    line-height: 20px;
  }
  nav ul li ul li a {
    padding-left: 30px;
  }

  .nav-dropdown {
    position: static;
  }
}
@media screen and (min-width: 799px) {
  .nav-list {
    display: block !important;
  }
}
#nav-toggle {
  position: absolute;
  left: 18px;
  top: 22px;
  cursor: pointer;
  padding: 10px 35px 16px 0px;
}
#nav-toggle span,
#nav-toggle span:before,
#nav-toggle span:after {
  cursor: pointer;
  border-radius: 1px;
  height: 5px;
  width: 35px;
  background: #ffffff;
  position: absolute;
  display: block;
  content: "";
  transition: all 300ms ease-in-out;
}
#nav-toggle span:before {
  top: -10px;
}
#nav-toggle span:after {
  bottom: -10px;
}
#nav-toggle.active span {
  background-color: transparent;
}
#nav-toggle.active span:before, #nav-toggle.active span:after {
  top: 0;
}
#nav-toggle.active span:before {
  transform: rotate(45deg);
}
#nav-toggle.active span:after {
  transform: rotate(-45deg);
}

</style>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <script src="js/index_dropdown_menu.js"></script>
</head>
<body>
<div class="icon_dis">
<div class="main_icon" onclick="window.location='index.php'">
<img src="imags/klu.png" />
<strong><p>Alumni Association</p></strong>
</div>
</div>

<div class="dropdownmenu">
  <nav>
      <div class="nav-mobile"><a id="nav-toggle" href="#!"><span></span></a></div>
      <ul class="nav-list" >
        <li>
          <a href="#" style="text-decoration: none;">Carrers</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="careers.php" style="text-decoration: none;">Internship</a>
            </li>
            <li>
              <a href="careers.php" style="text-decoration: none;">Jobs</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="advisoryboard.php" style="text-decoration: none;">Board Members</a>
        </li>
        <li>
          <a href="#!" style="text-decoration: none;">Events</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="events.php" style="text-decoration: none;">Alumni Event Calender</a>
            </li>
			<li><a href="events.php" style="text-decoration: none;">All Events</a></li>
			<li>
			  <a href="https://kluniversity.in/site/acadcal.htm" style="text-decoration: none;">Academic Calender</a></li>
          </ul>
        </li>
		 <li>
              <a href="suess_stories.php" style="text-decoration: none;">Alumni Stories</a>
            </li>
            <li>
              <a href="gallery.php" style="text-decoration: none;">Gallery</a>
            </li>
         <li>
              <a href="updates.php" style="text-decoration: none;">News & Updates</a>
            </li>
        <li>
          <a href="aboutus.html" style="text-decoration: none;">About us</a>
        </li>
      </ul>
    </nav>
</div>


<div style="width: 100%; height: auto; ">


<?php

require('js/php/conn.php');
$get=mysqli_query($conn,"select * from event_name;");
while($k=mysqli_fetch_assoc($get))
{
	$code=$k['e_code'];
	$name=$k['name'];
	$date=$k['event_date'];
	$venue=$k['venue'];
	$location=$k['location'];
	$weekday = date('l', strtotime($date)); 
	$month=date('M', strtotime($date));
	$date=explode("-",$date);
	$dd=$date[2];
	$mm=$date[1];
	$yy=$date[0];
	$duration=$k['duration'];
	$temp='';
	if(strlen($name)<=43)
	{
		$temp=$name;
	}
	else
	{
		for($i=0;$i<43;$i++)
		{
			$temp=$temp.''.$name[$i];
		}
	}
	$temp=$temp.'....';
	echo '
	
<div class="events_display_box" onclick="window.location = '."'event_s.php?eve=$code'".'">
<div style="position: absolute; top: 0px; width:100%; height: 100%;">
<div style="width: 130px; height: 130px; background-color: #a6a6a6;float: left; text-align: center;">
<div style="width:100%; height: 20px; margin-top: 10px; ">
<p style="margin: 0px; font-size: 16px; font-family: '."'Lato'".', sans-serif; color: white;">'.$weekday.'</p>
</div>
<div style="width: 100%; height: 45px; margin-top: 10px; ">
<p style="margin: 0px; font-size: 35px; font-family: '."'Lato'".', sans-serif; color: white;">'.$dd.'</p>
</div>
<div style="width:100%; height: 20px; margin-top: 10px; ">
<p style="margin: 0px; font-size: 15px; font-family: '."'Lato'".', sans-serif; color: white;">'.$month.', '.$yy.'</p>
</div>
</div>
<div style="width: auto; height: 100%; float: left;">
<div style="width: 320px; height: 40px; ">
<p style="margin: 0px; font-family: '."'Lato'".', sans-serif; font-size: 15px; padding-top: 13px; text-align: left; padding-left: 6px;">'.$temp.'</p>
</div>
<div style="width: 320px; height: 30px; ">
<p style="margin: 0px; font-family: '."'Roboto'".', sans-serif; font-size: 13px; padding-top: 13px; text-align: left; padding-left: 6px;">'.$location.'</p>
</div>
<div style="width: 320px; height: 40px; ">
<p style="margin: 0px;  font-family: '."'Roboto'".', sans-serif; font-size: 13px; padding-top: 5px; text-align: left; padding-left: 6px;">Venue: '.$venue.'</p>
</div>
<div style="width: 320px; height: 20px; ">
<p style="margin: 0px;  font-family: '."'Roboto'".', sans-serif; font-size: 12px; color: #494949; padding-top: 0px; text-align: left; padding-left: 6px;">'.$duration.' Day Event</p>
</div>
</div>
</div>
</div>
	
	';
	
}
?>





</div>
</body>
</html>