<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<?php
require('js/php/conn.php');
$hash=$_GET['hash'];
if($hash==null)
{
	header("Location:suess_stories.php");
}
$hash=mysqli_real_escape_string($conn,$hash);
$get_data_s=mysqli_query($conn,"select * from success_stories where hash_key='$hash';");
if(mysqli_num_rows($get_data_s)==0)
{
	header("Location:suess_stories.php");
}
$get_data_s=mysqli_fetch_assoc($get_data_s);
$ss_title=$get_data_s['title'];
$ss_content=$get_data_s['content'];
$cc=$get_data_s['hash_key'];
$ss_ext="cms/sucess/".$get_data_s['hash_key'].'.'.$get_data_s['ext'];
?>
<title>Sucess stories | KL Alumni Association</title>
<style>
body
{
	margin: 0px;
	background-color: white;
}
.icon_dis
{
	width: 100%;
	height: 70px;
	max-width: 1800px;
	margin: auto;
}
.main_icon
{
	width: 400px;
	height: 100%;
}
.main_icon img
{
	height: 80%;
	width: auto;
	float: left;
	margin-top: 6px;
	margin-left: 10px;
}
.main_icon p
{
	font-size: 20px;
	padding-top: 35px;
	font-family: 'Lato', sans-serif;
}
.sucess_story_box
{
width: 300px; 
height: 300px; 
background-color: white;
margin: 20px;
display: inline-block;
position: relative;
cursor: pointer;
box-shadow: 0px 0px 2px 0px #8d8d8d;
}
.dropdownmenu
{
	width: 100%;
	height: 40px;
	background-color: #A81E24;
}
@charset "UTF-8";
.navigation {
  height: 70px;
}

.brand {
  position: absolute;
  padding-left: 20px;
  float: left;
  line-height: 70px;
  text-transform: uppercase;
  font-size: 1.4em;
}
.brand a,
.brand a:visited {
  color: #ffffff;
  text-decoration: none;
}

.nav-container {
  max-width: 1000px;
  margin: 0 auto;
}

nav {
  float: right;
}
nav ul {
  list-style: none;
  margin: 0;
  padding: 0;
}
nav ul li {
  float: left;
  font-family: 'Roboto', sans-serif;
  font-size: 15px;
  position: relative;
}
nav ul li a,
nav ul li a:visited {
  display: block;
  padding: 0 20px;
  line-height: 40px;
  background: #A81E24;
  color: #ffffff;
  text-decoration: none;
}
nav ul li a:hover,
nav ul li a:visited:hover {
  background: #bd3339;
  color: #ffffff;
}
nav ul li a:not(:only-child):after,
nav ul li a:visited:not(:only-child):after {
  padding-left: 4px;
  content: " ▾";
}
nav ul li ul li {
  min-width: 190px;
}
nav ul li ul li a {
  padding: 8px;
  font-size: 14px;
  line-height: 20px;
}

.nav-dropdown {
  position: absolute;
  display: none;
  z-index: 1;
  box-shadow: 0 3px 12px rgba(0, 0, 0, 0.15);
}

/* Mobile navigation */
.nav-mobile {
  display: none;
  position: absolute;
  top: 0;
  right: 0;
  background: #262626;
  height: 70px;
  width: 70px;
}

@media only screen and (max-width: 798px) {
  .nav-mobile {
    display: block;
  }

  nav {
    width: 100%;
    padding: 70px 0 15px;
  }
  nav ul {
    display: none;
  }
  nav ul li {
    float: none;
  }
  nav ul li a {
    padding: 15px;
    line-height: 20px;
  }
  nav ul li ul li a {
    padding-left: 30px;
  }

  .nav-dropdown {
    position: static;
  }
}
@media screen and (min-width: 799px) {
  .nav-list {
    display: block !important;
  }
}
#nav-toggle {
  position: absolute;
  left: 18px;
  top: 22px;
  cursor: pointer;
  padding: 10px 35px 16px 0px;
}
#nav-toggle span,
#nav-toggle span:before,
#nav-toggle span:after {
  cursor: pointer;
  border-radius: 1px;
  height: 5px;
  width: 35px;
  background: #ffffff;
  position: absolute;
  display: block;
  content: "";
  transition: all 300ms ease-in-out;
}
#nav-toggle span:before {
  top: -10px;
}
#nav-toggle span:after {
  bottom: -10px;
}
#nav-toggle.active span {
  background-color: transparent;
}
#nav-toggle.active span:before, #nav-toggle.active span:after {
  top: 0;
}
#nav-toggle.active span:before {
  transform: rotate(45deg);
}
#nav-toggle.active span:after {
  transform: rotate(-45deg);
}

article {
  max-width: 1000px;
  margin: 0 auto;
  padding: 10px;
}
.title
{
	width: 100%; 
	height: auto;
	text-align: center;
	font-family: 'Roboto', sans-serif;
}
.title p
{
	padding-top: 40px;
	font-size: 20px;
}
.main_p
{
	width: 100%; 
	height: auto;
	margin-top: 20px;
	text-align: center;
}
.main_con
{
	font-family: 'Roboto', sans-serif;
	font-size: 15px;
}
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<script src="js/index_dropdown_menu.js"></script>
</head>
<body>
<div class="icon_dis" style="cursor: pointer;">
<div class="main_icon" onclick="window.location='index.php'">
<img src="imags/klu.png" />
<strong><p style="text-decoration: none;">Alumni Association</p></strong>
</div>
</div>
<div class="dropdownmenu">
 <nav>
      <div class="nav-mobile"><a id="nav-toggle" href="#!"><span></span></a></div>
      <ul class="nav-list" >
        <li>
          <a href="#" style="text-decoration: none;">Carrers</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="careers.php" style="text-decoration: none;">Internship</a>
            </li>
            <li>
              <a href="careers.php" style="text-decoration: none;">Jobs</a>
            </li>
            <li>
              <a href="careers.php" style="text-decoration: none;">Mentorship</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="advisoryboard.php" style="text-decoration: none;">Board Members</a>
        </li>
        <li>
          <a href="#!" style="text-decoration: none;">Events</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="events.php" style="text-decoration: none;">Alumni Event Calender</a>
            </li>
			<li><a href="events.php" style="text-decoration: none;">All Events</a></li>
			<li>
			  <a href="https://kluniversity.in/site/acadcal.htm" style="text-decoration: none;">Academic Calender</a></li>
          </ul>
        </li>
		 <li>
              <a href="#" style="text-decoration: none;">Alumni Stories</a>
            </li>
            <li>
              <a href="gallery.php" style="text-decoration: none;">Gallery</a>
            </li>
         <li>
              <a href="updates.php" style="text-decoration: none;">News & Updates</a>
            </li>
        <li>
          <a href="aboutus.html" style="text-decoration: none;">About us</a>
        </li>
      </ul>
    </nav>
</div>
<div style="width: 80%; height: auto; margin: auto;">
<div class="title">
<p><?php echo $ss_title; ?></p>
</div>
<div style="width: 100%; height: 1px; background-color: #c2c2c2;">
</div>
<div class="main_p">
<?php
if($get_data_s['ext']=="mp4")
{
	echo '
	<video width="100%" height="auto" controls>
  <source src="'.$ss_ext.'" type="video/mp4">
  <source src="'.$ss_ext.'" type="video/ogg">
  Your browser does not support the video tag.
   </video>
	';
}
else
{
	echo '<img src="'.$ss_ext.'" style="max-height: 500px;" />';
}
?>
</div>
<div class="main_con">
<?php
$ss_content=explode("\n",$ss_content);
for($i=0;$i<sizeof($ss_content);$i++)
{
	echo '<p style="padding-top: 14px;">'.$ss_content[$i].'</p>';
}
?>

</div>
</div>
</body>
</html>