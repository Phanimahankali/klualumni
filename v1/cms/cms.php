<?php
clearstatcache();
session_start();
require('php/conn.php');
$session=$_SESSION['auth'];
$check=mysqli_query($conn,"select * from admin_secuirty where hash_key='$session' and status_key is NULL");
$ch1=mysqli_fetch_assoc($check);
$ch1_ee=$ch1['email'];
$check1=mysqli_query($conn,"select * from admin where hash_key='$session' and email='$ch1_ee';");
if(mysqli_num_rows($check1)==0 or mysqli_num_rows($check)==0)
{
	header('location:login.php');
}
?>

<html>
<head>
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />	
<link rel="stylesheet" type="text/css" href="css/cms.css"/>
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<script type="text/javascript" src="js/loader.js"></script>
<title>CMS | KL Alumni Association</title>
<style>
#verification_popup
{
	width: 100%;
	height: 100%;
	background-color: rgba(0,0,0,0.5);
	position: fixed;
	z-index: 1;
}
.verification_popup_verify
{
	width: 60%;
	height: 100%;
	margin: 0px 10px 0px 10px;
	background-color: white;
	display: inline-block;
}
.verification_popup_search_filter
{
	width: 30%;
	height: 100%;
	margin: 0px 10px 0px 10px;
	background-color: white;
	display: inline-block;
	position: relative;
}
*:focus
{
	outline: none;
}
.search_filter_input
{
	font-family: 'Open Sans', sans-serif;
	font-size: 15px;
	width: 65%;
	margin-left: 10px;
	border: none;
	border-radius: 2px;
	box-shadow: 0px 0px 2px 0px #e1e1e1;
	padding: 5px 7px 7px 15px;
}

#profile_verify_table tr td
{
	padding-top: 10px;
	font-size: 15px;
	font-family: 'Lato', sans-serif;
}
#verify_profile_verification_button
{
	margin-right: 50px;
	font-family: arial;
	font-size: 15px;
	padding: 5px 15px 5px 15px;
	background-color: #5cc1bf;
	color: white;
	border: 1px solid #5cc1bf;
	border-radius: 5px;
	cursor: pointer;
}
#verify_profile_verification_button:hover
{
	background-color: #008b88;
}
.exist_gallery_display
{
	width: 230px;
	height: 125px;
	display: inline-block;
	cursor: pointer;
	overflow: hidden;
	position: relative;
	margin: 10px 5px 10px 10px;
	background-color: #f6f6f6;
}
.delete_photo
{
	font-size: 13px;
	margin-top: 5px;
	margin-left: 5px;
	cursor: pointer;
	color: #ff5757;
	border: 1px solid #ff5757;
	border-radius: 2px;
    background-color: white;
	font-family: arial;
}
#customers {
  font-family: 'Lato', sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
#respose_request {
  border-collapse: collapse;
  width: 100%;
}

#respose_request th {
  padding: 8px;
  text-align: left;
  font-size: 13px;
  font-family: 'Roboto', sans-serif;
  border-bottom: 1px solid #ddd;
}
#respose_request td
{
  padding: 8px;
  text-align: left;
  font-family: 'Roboto', sans-serif;
  font-size: 13px;
  border-bottom: 1px solid #ddd;
}
.sucess_story_box
{
width: 300px; 
height: 300px; 
background-color: white;
margin: 20px;
display: inline-block;
position: relative;
cursor: pointer;
box-shadow: 0px 0px 2px 0px #8d8d8d;
}
.add_staff_record
{
	font-size: 15px;
	margin-left: 15px;
	margin-top: 10px;
	background-color: #e4e4e4;
	border: 1px solid #c6c6c6;
	cursor: pointer;
	padding: 2px 15px 2px 15px;
}
.add_staff_record:hover
{
	background-color: #c6c6c6;
}
#admin_logs {
  border-collapse: collapse;
  width: 100%;
}

#admin_logs th,#admin_logs td {
  text-align: left;
  padding: 8px;
}

#admin_logs tr:nth-child(even){background-color: #f2f2f2}

#admin_logs th {
  background-color: #4CAF50;
  color: white;
}
</style>
<script src="js/cms.js"></script>
<script src="js/edit_add_image.js"></script>
</head>
<body>



<?php require('php/new_gallery_images_upload.php'); ?>
<?php require('php/new_events_created.php'); ?>
<?php require('php/news_letter_send.php'); ?>
<a href="cms.php">
<div class="nav_main">
<img src="icons/klu_white.png" style="height: 90%; width: auto; float: left; margin-left: 10px; margin-top: 2px;" />
<p style="float: left; color: white; font-size: 16px; margin: 0px; font-family: 'Lato', sans-serif; padding-top: 30px;"><strong>Alumni Association</strong></p>
<p style="color: white; font-size: 20px;padding-top: 15px; margin: 0px; text-align: center; font-family: 'Lato', sans-serif; "><strong>CMS Panel</strong></p>
<a href="logout.php">
<div class="logout">
<img src="icons/logout.png" style="width: 70%; height: auto; margin-top: 5px;"/>
</div>
</a>
</div>
</a>
<div class="icons_main">
<div class="icon_items">
<div class="icons">
</div>
</div>
<a href="?show=galleryimage">
<div class="icon_items">
<div class="icons">
<img src="icons/gallery.png" style="width: auto; margin-top: 5%; height: 90%;"/>
</div>
<p style="float: left; margin: 0px;font-family: 'Lato', sans-serif; font-size: 13px; padding-top: 5px; padding-left: 5px; color: white; ">Gallery Images</p>
</div>
</a>
<a href="?show=alumniprofile_verification">
<div class="icon_items">
<div class="icons">

<?php
require('php/conn.php');
$verification_count=mysqli_query($conn,"select * from auth where status is NULL;");
$verification_count=mysqli_num_rows($verification_count);
if($verification_count>=1)
{
	echo '
	<div class="notify">
<p style="font-size: 10px; padding-top: 2px; margin: 0px; text-align: center; color: white;">'.$verification_count.'<p>
</div>
	';
}
?>

<img src="icons/alumni_student.png" style="width: auto; margin-top: 5%; height: 90%;"/>
</div>
<p style="float: left; margin: 0px;font-family: 'Lato', sans-serif; font-size: 13px; padding-top: 5px; padding-left: 5px; color: white; ">Alumni Profile Verification</p>
</div>
</a>

<a href="?show=alumnistaffrecord">
<div class="icon_items">
<div class="icons">
<img src="icons/alumni_staff.png" style="width: auto; margin-top: 5%; margin-left: 5px; height: 70%;"/>
</div>
<p style="float: left; margin: 0px;font-family: 'Lato', sans-serif; font-size: 13px; padding-top: 5px; padding-left: 5px; color: white; ">Alumni Staff Records</p>
</div>
</a>
<a href="?show=newsletters">
<div class="icon_items">
<div class="icons">
<img src="icons/news_letters.png" style="width: auto; margin-top: 10%; margin-left: 8px; height: 60%;"/>
</div>
<p style="float: left; margin: 0px;font-family: 'Lato', sans-serif; font-size: 13px; padding-top: 5px; padding-left: 5px; color: white; ">News Letters</p>

</div>
</a>
<a href="?show=alumniprofiles">
<div class="icon_items">
<div class="icons">
<img src="icons/alumni_profiles.png" style="width: auto;  height: 90%;"/>
</div>
<p style="float: left; margin: 0px;font-family: 'Lato', sans-serif; font-size: 13px; padding-top: 5px; padding-left: 5px; color: white; ">Alumni Profiles</p>
</div>
</a>
<a href="?show=offers">
<div class="icon_items">
<div class="icons">
<img src="icons/placement.png" style="width: auto; margin-top: 10%; margin-left: 8px; height: 60%;"/>

<div class="notify">
<p style="font-size: 10px; padding-top: 2px; margin: 0px; text-align: center; color: white;">9<p>
</div>
</div>
<p style="float: left; margin: 0px;font-family: 'Lato', sans-serif; font-size: 13px; padding-top: 5px; padding-left: 5px; color: white; ">Offers</p>
</div>
</a>
<a href="?show=studentprojects">
<div class="icon_items">
<div class="icons">
<img src="icons/idea.png" style="width: auto; margin-top: 10%; margin-left: 8px; height: 60%;"/>
</div>
<p style="float: left; margin: 0px;font-family: 'Lato', sans-serif; font-size: 13px; padding-top: 5px; padding-left: 5px; color: white; ">Sucess stories</p>
</div>
</a>
<a href="?show=events">
<div class="icon_items">
<div class="icons">
<img src="icons/events.png" style="width: auto; margin-top: 10%; margin-left: 8px; height: 60%;"/>
</div>
<p style="float: left; margin: 0px;font-family: 'Lato', sans-serif; font-size: 13px; padding-top: 5px; padding-left: 5px; color: white; ">Events</p>
</div>
</a>
<a href="?show=news">
<div class="icon_items">
<div class="icons">
<img src="icons/news.png" style="width: auto; margin-top: 10%; margin-left: 8px; height: 60%;"/>
</div>
<p style="float: left; margin: 0px;font-family: 'Lato', sans-serif; font-size: 13px; padding-top: 5px; padding-left: 5px; color: white; ">News</p>
</div>
</a>
<a href="?show=donation">
<div class="icon_items">
<div class="icons">
<img src="icons/rupee.png" style="width: auto; margin-top: 10%; margin-left: 8px; height: 60%;"/>

<div class="notify">
<p style="font-size: 10px; padding-top: 2px; margin: 0px; text-align: center; color: white;">9<p>
</div>
</div>
<p style="float: left; margin: 0px;font-family: 'Lato', sans-serif; font-size: 13px; padding-top: 5px; padding-left: 5px; color: white; ">Donations</p>
</div>
</a>
<a href="?show=feedback">
<div class="icon_items">
<div class="icons">
<img src="icons/feedback.png" style="width: auto; margin-top: 10%; margin-left: 8px; height: 60%;"/>
<?php
require('php/conn.php');
$feedback_count=mysqli_query($conn,"select * from query where status is NULL;");
$feedback_count=mysqli_num_rows($feedback_count);
if($feedback_count>=1)
{
	echo '
	<div class="notify">
<p style="font-size: 10px; padding-top: 2px; margin: 0px; text-align: center; color: white;">'.$feedback_count.'<p>
</div>
	';
}
?>


</div>
<p style="float: left; margin: 0px;font-family: 'Lato', sans-serif; font-size: 13px; padding-top: 5px; padding-left: 5px; color: white; ">Feedback</p>
</div>
</a>
</div>
<div id="galleryimage"  style="overflow-y: scroll;">
<p style="text-align: center; font-family: 'Open Sans', sans-serif; font-size: 15px;">Gallery Image Page</p>
<p style="font-family: 'Open Sans', sans-serif; margin: 0px; font-size: 15px; padding-left: 20px;">Create gallery</p>
<?php require('php/update_gallery_image.php'); ?>

<div onclick="gallery_pop_upload('1');" style="width: 200px; cursor: pointer; position: relative; height: 110px; border-radius: 10px; margin-left: 40px; margin-top: 20px; background-color: #f6f6f6;">
<div style="width: 60px; margin: auto; border-radius: 60px; position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px; height: 80px; background-color: #f6f6f6;">
<div style="width: 60px; height: 5px; background-color: #d9d9d9; position: relative; top: 38px;">
</div>
<div style="width: 60px; height: 5px; background-color: #d9d9d9; position: relative; top: 33px; transform: rotate(90deg);">
</div>
</div>
</div>

<div id="gallery_pop_upload" style="width: 100%; height: 100%; display: none; background-color: rgba(0,0,0,0.5); position: absolute; z-index:10; margin: auto; right: 0px; bottom: 0px; left: 0px; top: 0px;">
<div style="width: 100%; height: 100%; position: relative;">
<div style="width: 600px; height: 400px; background-color: white; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;">
<p onclick="gallery_pop_upload('2');" style="position: absolute; font-family: arial; font-size: 20px; cursor: pointer; color: white; margin: 0px; top: -30px; right: 0px;">X</p>
<form method="POST" enctype="multipart/form-data" id="new_gallery_upload_submit">
<fieldset style="border: none; margin-top: 10px;">
<legend style="font-family: arial; font-size: 13px; color: #525252;">Event Name</legend>
<input name="new_gallery_event_name" id="new_gallery_event_name" style="width: 90%; margin-left: 4px; font-size: 15px; font-family: arial; padding-top: 5px; border: none; border-bottom: 1px solid black;" type="text"></input>
</fieldset>
<div style="width: 100%; height: 130px; ">
<div style="width: 60%; height: 130px; float: left; ">
<fieldset style="border: none; margin-top: 10px;">
<legend style="font-family: arial; font-size: 13px; color: #525252;">Place</legend>
<input name="new_gallery_place" id="new_gallery_place" style="width: 90%; margin-left: 4px; font-size: 15px; font-family: arial; padding-top: 5px; border: none; border-bottom: 1px solid black;" type="text"></input>
</fieldset>
<fieldset style="border: none; margin-top: 10px;">
<legend style="font-family: arial; font-size: 13px; color: #525252;">Date</legend>
<input name="new_gallery_date" id="new_gallery_date" type="date" style="width: 90%; margin-left: 4px; font-size: 15px; font-family: arial; padding-top: 5px; border: none; border-bottom: 1px solid black;" type="text"></input>
</fieldset>
</div>
<div style="width: 40%; height: 130px; position: relative; float: left;">
<div  style="width: 150px; cursor: pointer; position: relative; height: 90px; border-radius: 10px; margin: auto; margin-top: 10px; background-color: #f6f6f6;">
<input type="file" name="upload_gallery_files[]" onchange="update_upload_gallery_files_count(this);" id="upload_gallery_files" style="width: 100%; height: 100%; opacity: 0; z-index: 5; position: relative; top: 0px;" multiple>
<div style="width: 60px; margin: auto; border-radius: 60px; position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px; height: 80px; background-color: #f6f6f6;">
<div style="width: 60px; height: 5px; background-color: #d9d9d9; position: relative; top: 38px;">
</div>
<div style="width: 60px; height: 5px; background-color: #d9d9d9; position: relative; top: 33px; transform: rotate(90deg);">
</div>
</div>
</input>
</div>
<p id="upload_gallery_files_count" style="font-family: arial;  font-size: 15px; padding-top: 5px;">Images upload count : 0</p>
</div>
</div>
<fieldset style="border: none; margin-top: 10px;">
<legend style="font-family: arial; font-size: 13px; color: #525252;">Desc</legend>
<textarea name="new_gallery_desc" id="new_gallery_desc" style="width: 95%; max-width: 95%; height: 100px; max-height: 100px; background-color: #eceeef; margin-left: 4px; font-size: 15px; font-family: arial; padding-top: 5px; border: none;" type="text"></textarea>
</fieldset>
</form>
<button id="new_gallery_upload" onclick="new_gallery_upload();" style="position: absolute; bottom: 10px; right: 10px; cursor: pointer; font-family: arial; background-color: #43a4dd; color: white; border: 1px solid #43a4dd; font-size: 14px; padding: 4px 10px 4px 10px; border-radius: 5px;">Upload</button>
</div>
</div>
</div>



<p style="font-family: 'Open Sans', sans-serif; margin: 0px; padding-top: 50px; font-size: 15px; padding-left: 20px;">Gallery</p>

<div style="width: 100%; height: auto; margin-top: 40px;">

<?php
require('php/conn.php');
$check=mysqli_query($conn,"select * from gallery;");
while($g=mysqli_fetch_assoc($check))
{
	$gallery_code=$g['code'];
	$event_name=$g['event_name'];
	$date=$g['date'];
	$cop=mysqli_query($conn,"select code,file_type from gallery_photos where status='OK' and g_code='$gallery_code' limit 1;");
	$cop=mysqli_fetch_assoc($cop);
	$photo="gallery/".$gallery_code.'/'.$cop['code'].'.'.$cop['file_type'];

echo '
<div onclick="edit_gallery('."'1'".','."'$gallery_code'".');" class="exist_gallery_display">
<img src="'.$photo.'" style="width: 100%; height: auto; position: absolute; margin: auto; bottom: 0px; left: 0px; right: 0px; top: 0px;" />
<div style="width: 100%; height: 50px; background-color: rgba(0,0,0,0.8); position: absolute; bottom: 0px; overflow: hidden;">
<p style="color: white; width: 100%; height: 15px; margin: 0px; overflow: hidden; font-family: arial; font-size: 15px; padding: 5px 10px 0px 5px;">'.$event_name.'</p>
<p style="color: white; margin: 0px; font-family: arial; font-size: 13px; padding-top: 5px; padding-left: 5px;">'.$date.'</p>
</div>
</div>
';
}


?>

</div>



<div id="gallery_edit_pop_upload" style="width: 100%; height: 100%; display: none; background-color: rgba(0,0,0,0.5); position: absolute; margin: auto; right: 0px; bottom: 0px; left: 0px; top: 0px;">
<div style="width: 100%; height: 100%; position: relative;">
<div style="width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; left: 0px; right: 0px; margin: auto; background-color: #f4f4f4;">
<p onclick="edit_gallery('2','');" style="font-family:'Open Sans', sans-serif; color: black; cursor: pointer; font-size: 28px; margin: 0px; position: absolute; top: 10px; right: 30px;">X</p>
<div style="width: 20%; height: 100%; position: relative; float: left; background-color: white;">
<fieldset style="border: none; margin-top: 50px;">
<legend style="font-family: arial; font-size: 15px;">Event Name</legend>
<input id="edit_gallery_event_name" style="width: 90%; font-family: arial; padding: 5px 0px 2px 0px; border: none; border-bottom:1px solid black; height: auto; border:"></input>
</fieldset>
<fieldset style="border: none; margin-top: 30px;">
<legend style="font-family: arial; font-size: 15px;">Date</legend>
<input id="edit_gallery_event_date" style="width: 90%; font-family: arial; padding: 5px 0px 2px 0px; border: none; border-bottom:1px solid black; height: auto; border:"></input>
</fieldset>
<fieldset style="border: none; margin-top: 30px;">
<legend style="font-family: arial; font-size: 15px;">Location</legend>
<input id="edit_gallery_event_location" style="width: 90%; font-family: arial; padding: 5px 0px 2px 0px; border: none; border-bottom:1px solid black; height: auto; border:"></input>
</fieldset>
<fieldset style="border: none; margin-top: 50px;">
<legend style="font-family: arial; font-size: 15px;">Desc</legend>
<textarea id="edit_gallery_event_desc" style="width: 95%; max-width: 95%; font-family: arial; background-color: #f4f4f4; font-family: arial; padding: 5px 0px 2px 0px; border: none; height: 300px; max-height: 300px; border:"></textarea>
</fieldset>
<input id="edit_gallery_event_code" style="visibility: hidden;"></input>
<button onclick="update_event('D');" style="position: absolute; bottom: 20px; font-family: arial; font-size: 15px; cursor:pointer; padding: 5px 13px 5px 13px; border: 1px solid #ff5757; left: 20px; background-color: #ff5757; color: white;">Delete</button>
<button onclick="update_event('U');" style="position: absolute; bottom: 20px; font-family: arial; font-size: 15px; right: 20px; cursor:pointer; padding: 5px 13px 5px 13px; border: 1px solid #38b6ff; background-color: #38b6ff; color: white;">Update</button>
</div>
<div id="edit_gallery_images_show" style="width: 80%; height: 100%; overflow-y: scroll; float: left;">
</div>
</div>
</div>
</div>
</div>

<div id="alumniprofile_verification"  style="overflow-y: scroll;">
<div id="verification_popup" style="display: none;">
<div class="verification_popup_verify">
<div style="width: 100%; height: 50px;  position: relative; z-index: 5px; top: 0px;">
<div style="width: 100%; height: 100%; text-align: center;  position: absolute;">
<p style="position: absolute; top: 0px; font-family: arial; margin: 0px; z-index: 10px; top: 10px; left: 20px; font-size: 18px; cursor: pointer;" onclick="close_popup('verification_popup');">X</p>
<p style="font-family: 'Open Sans', sans-serif;">Profile Verification</p>
<div style=" box-shadow: 0px 0px 5px 0px #dbdbdb; width: 90%; margin: auto; padding-bottom: 30px; border-radius: 10px; padding-top: 20px;">
<table style="width: 90%; margin: auto;" id="profile_verify_table">
<tr>
<td>Name</td>
<td>:</td>
<td id="profile_verify_name"></td>
<td>Gender</td>
<td>:</td>
<td id="profile_verify_gender"></td>
</tr>
<tr>
<td>Email</td>
<td>:</td>
<td id="profile_verify_email"></td>
<td>Phone</td>
<td>:</td>
<td id="profile_verify_phone"></td>
</tr>
<tr>
<td>Job</td>
<td>:</td>
<td id="profile_verify_job"></td>
<td>Company</td>
<td>:</td>
<td id="profile_verify_company"></td>
</tr>
<tr>
<td>Reg No</td>
<td>:</td>
<td id="profile_verify_regno"></td>
<td>Program</td>
<td>:</td>
<td id="profile_verify_program"></td>
</tr>
</table>
</div>
<div style="width:100%; height: 40px; text-align: right; margin-top: 40px;">
<button id="verify_profile_verification_button" onclick="verify_profile_verification_button();">Verify</button>
</div>
</div>
</div>
</div>
<div class="verification_popup_search_filter">
<div style="position: absolute; width: 100%; height: 100%; top: 0px;">
<p style="text-align: center; font-family: 'Open Sans', sans-serif;">Search Filter</p>
<div style="width: 100%; height: 8%;">
<input onkeyup='verification_filter_check();' id="search_filter_input" class="search_filter_input" type="text" placeholder="Search"></input>
</div>
<p style="text-align: left; margin: 0px; font-size: 14px; padding: 0px 0px 10px 10px; color: #9b9b9b; font-family: 'Open Sans', sans-serif;">Filter result</p>
<div style="width: 100%; height: 80%; overflow-y: scroll; " id="verification_filter">
</div>
</div>
</div>
</div>
<script>
function verification_filter_check()
{
	var search=document.getElementById('search_filter_input').value;
$.ajax({
type: 'post',
 url: 'php/profile_verification_filter.php',
 data : {search:search},
 success: function(code){
 $('#verification_filter').html(code);
 }
 });
}
</script>
<?php
require('php/conn.php');
$list=mysqli_query($conn,"select name,gender,profile.email,phone,job,company,reg,program from profile inner join personal on profile.email=personal.email inner join academics on profile.email=academics.email inner join auth on  profile.email=auth.email where auth.status is NULL;");
while($ok=mysqli_fetch_array($list))
{
echo "<div class='alumni_verification_box'>";
echo "<div class='alumni_verification_box_image'>";
echo "<img src='icons/user.png' style='width: 100%; height: auto;'/>";
echo "</div>";
echo "<div class='alumni_verification_box_1'>";
echo "<table style='margin-top:15px;'>";
echo "<tr>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>Name</td>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>:</td>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>".$ok['name']."</td>";
echo "</tr>";
echo "<tr>";
echo "<td style='font-family:  sans-serif; font-size: 15px;padding:4px;'>Gender</td>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>:</td>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>".$ok['gender']."</td>";
echo "</tr>";
echo "<tr>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>Email</td>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px; '>:</td>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>".$ok['email']."</td>";
echo "</tr>";
echo "<tr>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>Phone</td>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>:</td>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>".$ok['phone']."</td>";
echo "</tr>";
echo "</table>";
echo "</div>";
echo "<div class='alumni_verification_box_1'>";
echo "<table style='margin-top:15px;'>";
echo "<tr>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>Job</td>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px; '>:</td>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>".$ok['job']."</td>";
echo "</tr>";
echo "<tr>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>Company</td>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>:</td>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>".$ok['company']."</td>";
echo "</tr>";
echo "<tr>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>Reg No</td>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>:</td>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>".$ok['reg']."</td>";
echo "</tr>";
echo "<tr>";
echo "<td style='font-family:  sans-serif; font-size: 15px;padding:4px;'>Program</td>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>:</td>";
echo "<td style='font-family:  sans-serif; font-size: 15px; padding:4px;'>".$ok['program']."</td>";
echo "</tr>";
echo "</table>";
echo "</div>";
echo "<div style='width:80px; margin-right: 10px; float: right; height: 100%;'>";
echo "<div style='width: 80px; height: 50%;'>";
echo "<p style='margin: 0px; padding-top: 25px; font-family: arial; color: green;' onclick='".'open_popup_verify("'.$ok['name'].'","'.$ok['gender'].'","'.$ok['email'].'","'.$ok['phone'].'","'.$ok['job'].'","'.$ok['company'].'","'.$ok['reg'].'","'.$ok['program'].'","verification_popup");'."'>View</p>";
echo "</div>";
echo "<div style='width: 80px; height: 50%;'>";
echo "<p style='margin: 0px; padding-top: 25px; font-family: arial; color: red;'>Ignore</p>";
echo "</div>";
echo "</div>";
echo "</div>";
}
?>
</div>
<?php require('php/new_faculty_record.php'); ?>
<div id="alumnistaffrecord" style="overflow-y: scroll;">
<p style="font-family: 'Noto Sans', sans-serif; padding-left: 20px; font-size: 16px;">Add Record</p>
<div style="width: 100%; height: auto; padding-top: 20px;">
<form method="post" action=""  enctype="multipart/form-data">
<fieldset style="text-align: left; border: none; display: inline-block;">
<legend style="font-size: 15px; font-family: arial;">Faculty Name *</legend>
<input required name="faculty_name" style="width: 250px; font-size: 15px; padding: 5px; margin-top: 5px; background-color: #f4f4f4; border: 1px solid #f4f4f4; " type="text"></input>
</fieldset>

<fieldset style="text-align: left; border: none; display: inline-block;">
<legend style="font-size: 15px; font-family: arial;">Department</legend>
<select name="faculty_designation_department" style="width: 250px; font-size: 15px; padding: 5px; margin-top: 5px; background-color: #f4f4f4; border: 1px solid #f4f4f4; " type="text">
<?php
require('php/conn.php');
$stream=mysqli_query($conn,"select sno,stream from programs;");
echo '<option value="">Select Department</option>';
while($get_stream=mysqli_fetch_array($stream))
{
	$st=$get_stream[1];
	$sno=$get_stream[0];
	echo '<option value='.$sno.'>'.$st.'</option>';
}
?>
</select>
</fieldset>

<fieldset style="text-align: left; border: none; display: inline-block;">
<legend style="font-size: 15px; font-family: arial;">Department Post</legend>
<input name="faculty_designation_department_post" style="width: 250px; font-size: 15px; padding: 5px; margin-top: 5px; background-color: #f4f4f4; border: 1px solid #f4f4f4; " type="text"></input>
</fieldset>

<fieldset style="text-align: left; border: none; display: inline-block;">
<legend style="font-size: 15px; font-family: arial;">Designation in alumni *</legend>
<select required name="faculty_designation_alumni" style="width: 250px; font-size: 15px; padding: 5px; margin-top: 5px; background-color: #f4f4f4; border: 1px solid #f4f4f4; " type="text">
<option value="Advisory Board">Advisory Board</option>
<option value="Executive Board">Executive Board</option>
<option value="Organisation">Organisation</option>
<option value="Alumni Coordinators">Alumni Coordinators</option>
<option value="Alumni Staff">Alumni Staff</option>
<option value="Alumni Chapters & Presidents">Alumni Chapters & Presidents</option>
</select>
</fieldset>

<fieldset style="text-align: left; border: none; display: inline-block;">
<legend style="font-size: 15px; font-family: arial;">Email</legend>
<input  name="faculty_email" type="email" style="width: 250px; font-size: 15px; padding: 5px; margin-top: 5px; background-color: #f4f4f4; border: 1px solid #f4f4f4; " type="text"></input>
</fieldset>

<fieldset style="text-align: left; border: none; display: inline-block;">
<legend style="font-size: 15px; font-family: arial;">Phone Number</legend>
<input  name="faculty_phone" style="width: 250px; font-size: 15px; padding: 5px; margin-top: 5px; background-color: #f4f4f4; border: 1px solid #f4f4f4; " type="text"></input>
</fieldset>

<fieldset style="text-align: left; border: none; display: inline-block;">
<legend style="font-size: 15px; font-family: arial;">Photo *</legend>
<input required name="faculty_photo" type="file" style="width: 250px; font-size: 15px; padding: 5px; margin-top: 5px; background-color: #f4f4f4; border: 1px solid #f4f4f4; " type="text"></input>
</fieldset>

</div>
<button name="add_staff_record" class="add_staff_record">Add</button>
</form>

<table id="customers" style="margin-top: 20px;">
<tbody>
  <tr style="background-color: red;">
  <th style="background-color: #224162; color: white;">Name</th>
	<th style="background-color: #224162; color: white;">Department</th>
    <th style="background-color: #224162; color: white;">Department Post</th>
	<th style="background-color: #224162; color: white;">Designation in alumni</th>
	<th style="background-color: #224162; color: white;">Photo</th>
	<th style="background-color: #224162; color: white;">Action</th>
	<?php
	require('php/conn.php');
	$get_faculty_data=mysqli_query($conn,"select * from faculty;");
	while($get_f_data=mysqli_fetch_assoc($get_faculty_data))
	{
		$code=$get_f_data['code'];
		$name=$get_f_data['name'];
		$department=$get_f_data['department'];
		$department_designation=$get_f_data['department_designation'];
		$alumni_designation=$get_f_data['alumni_designation'];
		$email=$get_f_data['email'];
		$phone=$get_f_data['phone'];
		$photo=$get_f_data['photo'];
		echo '
		<tr>
    <td>'.$name.'</td>
    <td>'.$department.'</td>
	<td>'.$department_designation.'</td>
	<td>'.$alumni_designation.'</td>
	<td style="text-align: center;"><img src="faculty/'.$photo.'" style="width: 50px;  height: auto;" /></td>
	<td style="color: red; cursor: pointer;" onclick="delete_faculty_record('.$code.');">DELETE</td>
	</tr>
		';
	}
	?>
  </tr>
	</tbody>
</table>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
function delete_faculty_record(a)
{
	if(confirm("Are you confirm to delete ?"))
	{
		$.ajax({
		 type: 'post',
		 url: 'php/delete_faculty_record.php',
		 data : {a:a},
		 success: function(code){
			 window.location.replace("cms.php?show=alumnistaffrecord");
		 }
		 });
	}
	else
	{
		
	}
}
</script>
</div>













<div id="newsletters" style="overflow-y: scroll; position: relative;">
<p style="text-align: center; font-family: 'Open Sans', sans-serif;">News letters</p>


<p style=" padding-left: 30px; margin: 0px; font-size: 15px; font-family: 'Open Sans', sans-serif;">Create New</p>

<div onclick="document.getElementById('new_new_letter_popup').style.display='block';"  style="width: 200px; cursor: pointer; position: relative; height: 110px; border-radius: 10px; margin-left: 40px; margin-top: 20px; background-color: #f6f6f6;">
<div style="width: 60px; margin: auto; border-radius: 60px; position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px; height: 80px; background-color: #f6f6f6;">
<div style="width: 60px; height: 5px; background-color: #d9d9d9; position: relative; top: 38px;">
</div>
<div style="width: 60px; height: 5px; background-color: #d9d9d9; position: relative; top: 33px; transform: rotate(90deg);">
</div>
</div>
</div>
<p style=" padding-left: 10px; padding-top: 20px; margin: 0px; font-size: 15px; font-family: 'Open Sans', sans-serif;">Recent News letters</p>
<div style="width: 100%; height: auto; margin-top: 20px;">
<table id="customers">
<tbody id="display_recent_news_letters">
</tbody>
</table>
</div>
<script>
function display_recent_news_letters()
{
	$.ajax({
	type: 'post',
	 url: 'php/recent_news_letter.php',
	 success: function(code){
	 $('#display_recent_news_letters').html(code);
	 }
	 });
}
display_recent_news_letters();
setInterval("display_recent_news_letters();",1000);
</script>

<div id="new_new_letter_popup" style="width: 100%; display: none; height: 100%;  background-color: white; position: absolute; top: 0px;">
<div style="width: 100%; height: 40px;  margin-top: 20px;">
<div style="width: 40px; cursor: pointer; height: 40px; background-color: #808080; float: right; margin-right: 10px; border-radius: 40px;">
<p onclick="document.getElementById('new_new_letter_popup').style.display='none';" style="text-align: center; color: white; margin: 0px; padding-top: 5px; font-size: 25px; font-family: 'Noto Sans', sans-serif; ">X</p>
</div>
</div>
<form action="" method="post" enctype="multipart/form-data">
<fieldset style="border: none;">
<legend><p style="margin: 0px; font-family: 'Noto Sans', sans-serif; font-size: 14px;">* To</p></legend>
<select name="news_letters_to" id="news_letters_to" style="color: #707070; font-family: 'Noto Sans', sans-serif; margin-top: 5px; font-size: 15px;">
<option value="all">All members in Alumni Association</option>
<option value="alumnis">Alumnis</option>
<option value="staff">Alumnis staff</option>
</select>
</fieldset>
<fieldset style="border: none; margin-top: 30px;">
<legend><p style="margin: 0px; font-family: 'Noto Sans', sans-serif; font-size: 14px;">* Email Subject</p></legend>
<input  id="news_subject_to" name="news_subject_to" type="text" style="font-family: 'Noto Sans', sans-serif; font-size: 15px; color: #707070; margin-top: 5px; padding: 2px 0px 2px 0px; border: none; border-bottom: 1px solid black; width: 50%;"></input> 
</fieldset>
<fieldset style="border: none; margin-top: 30px;">
<legend><p style="margin: 0px; font-family: 'Noto Sans', sans-serif; color: #707070; margin-top: 5px; font-size: 15px;">Content Cover page</p></legend>
<input type="file" id="news_letters_cover_photo" name="news_letters_cover_photo"></input> 
</fieldset>
<fieldset style="border: none; margin-top: 30px; ">
<legend><p style="margin: 0px; font-family: 'Noto Sans', sans-serif; font-size: 15px;">* Content heading</p></legend>
<input id="news_letters_content_heading" name="news_letters_content_heading" type="text" style="font-family: 'Noto Sans', sans-serif; color: #707070; margin-top: 5px; font-size: 15px; padding: 2px 0px 2px 0px; border: none; border-bottom: 1px solid black; width: 50%;"></input> 
</fieldset>
<fieldset style="border: none; margin-top: 30px; ">
<legend><p style="margin: 0px; font-family: 'Noto Sans', sans-serif; font-size: 15px;">* Content</p></legend>
<textarea  name="news_letters_content" id="news_letters_content" style="font-family: 'Noto Sans', sans-serif; font-size: 15px; color: #707070; margin-top: 5px; padding: 2px 5px 2px 5px; border: none; background-color:#f4f4f4; width: 70%; max-width: 70%; height: 300px; max-height: 300px;"></textarea> 
</fieldset>
<fieldset style="border: none; margin-top: 30px;">
<legend><p style="margin: 0px; font-family: 'Noto Sans', sans-serif; color: #707070; margin-top: 5px; font-size: 15px;">Attachment</p></legend>
<input type="file" id="news_letters_attachments" name="news_letters_attachments[]" multiple></input> 
</fieldset>
<input name="show" style="visibility: hidden;" value="newsletters"></input></br>
<button name="news_letters_submit_button" id="news_letters_submit_button" style="font-family: 'Noto Sans', sans-serif; font-size: 17px; color: white; background-color: #27babf; border: none; padding: 5px 15px 5px 15px; cursor: pointer; margin-left: 15px; margin-bottom: 100px;">Send</button>
</form>
</div>
</div>













<div id="alumniprofiles" style="overflow-y: scroll;">
<div style="width: 100%; height: 50px; background-color: #fcfcfc; margin-bottom: 20px; box-shadow: 0px 0px 5px 0px #cfcfcf; ">
<input onkeyup="search_filter(this);" style="font-family: arial; width: 200px; font-size: 14px; border-radius: 2px; margin: 10px 10px 0px 0px; border: 1px solid #d4d4d4; padding: 3px 0px 3px 5px; float: right;" type="text" placeholder="Search by name, Email etc.."></input>
<p style="float: right; margin: 0px; font-family: arial; padding-top: 13px; padding-right: 10px; font-size: 15px;">Filter :</p>


</div>

<div id="alumni_profile_details_display">
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
function search_filter(a)
{
	var search=a.value;
	$.ajax({
	type: 'post',
	 url: 'php/show_alumni_existing_details_filter.php',
	 data : {search:search},
	 success: function(code){
	
	 
		 $('#alumni_profile_details_display').html(code);
	 
	 }
	 });

}
search_filter('');
</script>

</div>

<div id="offers">
<div style="width: 100%; height: 100%; position: relative;">
<div style="width: 100%; height: 100%; position: absolute; top: 0px; overflow-y: scroll;">
<p style="text-align: center; font-family: 'Open Sans', sans-serif; font-size: 15px;">Offers</p>
<div style="width: 100%; height: auto; margin-top: 40px;  text-align: center;">
<div style="width: 400px; position: relative; height: 200px; margin: 0px 50px 0px 50px; border-radius: 10px; display: inline-block; box-shadow: 0px 0px 5px 0px #c6c6c6; background-color: white;">
<div style="width: 100%; height: 100%; position: absolute; top: 0px;">
<div id="piechart" style="width: 400px; height: 200px; border-radius: 10px;"></div>
</div>
</div>
<?php require('php/pie_response.php'); ?>
<script>
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
         <?php echo $array; ?>
        ]);

        var options = {
          title: 'Responses Pie Graph'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }

</script>


<div style="width: 500px;  position: relative; height: 200px; margin: 0px 50px 0px 50px; border-radius: 10px; display: inline-block; box-shadow: 0px 0px 5px 0px #c6c6c6; background-color: white;">
<div style="width: 100%; height: 100%; position: absolute; top: 0px;">
<p style="margin: 0px; padding-top: 10px; font-family: 'Roboto', sans-serif;">Response report requested</p>
<table id="respose_request" style="margin-top: 10px;">
<tr>
    <th>Code</th>
    <th>Email</th>
  <th>Response count</th>
  <th>Download</th>
  <th>Status</th>
  </tr>
<?php
require('php/conn.php');
$get_report_details=mysqli_query($conn,"select * from query where status='Opened' and type='Offers';");
while($get_report=mysqli_fetch_assoc($get_report_details))
{
	$query_code=$get_report['code'];
	$get_c=$get_report['problem'];
	$get_c=explode(' ',$get_c);
	$get_c=(int)$get_c[0];
	$get_count=mysqli_query($conn,"select posted_by from offer where code='$get_c';");
	$get_count=mysqli_fetch_array($get_count);
	$get_email=$get_count[0];
	$get_response=mysqli_query($conn,"select code from offer_response where status='VERIFIED' and response_to='$get_c';");
	$get_response=mysqli_num_rows($get_response);
	echo '
  <tr>
    <td>'.$get_c.'</td>
    <td>'.$get_email.'</td>
    <td>'.$get_response.'</td>
	<td style="text-decoration: none;cursor: pointer;"><a style="text-decoration: none;" href="../cms/php/request_response_data.php?a='.$get_c.'"><p style="color: black;">Click Here</p></a></td>
	<td onclick="query_close('.$query_code.');" style="cursor: pointer; color: red;">Close</td>
  </tr>
';
}

?>
<script>
function query_close(code)
{
	var a=code;
	$.ajax({
	 type: 'post',
	 url: 'php/query_close.php',
	 data : {code:code},
	 success: function(code){
	 alert(code);
	 location.reload();
	 }
	 });
}
</script>
</table>
</div>
</div>
</div>

<p style="text-align: left; margin: 20px 0px 15px 10px; font-family: 'Open Sans', sans-serif; font-size: 15px;">Active offer application</p>

<table id="customers">
<tbody id="display_recent_news_letters">
  <tr style="background-color: red;">
    <th style="background-color: #224162; color: white;">Code</th>
	<th style="background-color: #224162; color: white;">Orgination Name</th>
    <th style="background-color: #224162; color: white;">Job Name</th>
	<th style="background-color: #224162; color: white;">Type</th>
	<th style="background-color: #224162; color: white;">Last date</th>
	<th style="background-color: #224162; color: white;">Posted date</th>
	<th style="background-color: #224162; color: white;">Status</th>
	<th style="background-color: #224162; color: white;">Action</th>
	<th style="background-color: #224162; color: white;">Details</th>
  </tr>
  
  <?php
session_start();
$session=$_SESSION['auth'];
$get_data=mysqli_query($conn,"select * from offer where status='APPROVED';");
while($data=mysqli_fetch_assoc($get_data))
{
	$code=$data['code'];
	$org=$data['organization_name'];
	$jobname=$data['jobname'];
	$jobtype=$data['jobtype'];
	$lastdate=$data['lastdate'];
	$posteddate=$data['timestamp'];
	$status=$data['status'];
	echo '
	 <tr>
    <td>'.$code.'</td>
    <td>'.$org.'</td>
	<td>'.$jobname.'</td>
	<td>'.$jobtype.'</td>
	<td>'.$lastdate.'</td>
	<td>'.$posteddate.'</td>
	<td style="color: #c9e265;">'.$status.'</td>';
	echo'	
	<td><p onclick="offer_verify(0,'.$code.');" style="float: left;cursor: pointer;"> Delete</p></td>
	<td style="color: green; cursor: pointer;"><a style="text-decoration: none;" href="../o_attachment_download.php?code='.$code.'"><p>View</p></a></td>
	</tr>
	';
}
?>
  
  
</tbody>
</table>


<p style="text-align: left; margin: 20px 0px 15px 10px; font-family: 'Open Sans', sans-serif; font-size: 15px;">Awaiting for Approval</p>

<table id="customers">
<tbody id="display_recent_news_letters">
  <tr style="background-color: red;">
    <th style="background-color: #224162; color: white;">Code</th>
	<th style="background-color: #224162; color: white;">Orgination Name</th>
    <th style="background-color: #224162; color: white;">Job Name</th>
	<th style="background-color: #224162; color: white;">Type</th>
	<th style="background-color: #224162; color: white;">Last date</th>
	<th style="background-color: #224162; color: white;">Posted date</th>
	<th style="background-color: #224162; color: white;">Status</th>
	<th style="background-color: #224162; color: white;">Action</th>
	<th style="background-color: #224162; color: white;">Details</th>
  </tr>

<?php
session_start();
$session=$_SESSION['auth'];
$get_data=mysqli_query($conn,"select * from offer where status='Under Verification';");
while($data=mysqli_fetch_assoc($get_data))
{
	$code=$data['code'];
	$org=$data['organization_name'];
	$jobname=$data['jobname'];
	$jobtype=$data['jobtype'];
	$lastdate=$data['lastdate'];
	$posteddate=$data['timestamp'];
	$status=$data['status'];
	echo '
	 <tr>
    <td>'.$code.'</td>
    <td>'.$org.'</td>
	<td>'.$jobname.'</td>
	<td>'.$jobtype.'</td>
	<td>'.$lastdate.'</td>
	<td>'.$posteddate.'</td>';
	if($status=='Under Verification')
	{
		echo '<td style="color: #ffbd59;">'.$status.'</td>';	
	}
	if($status=='SUCESS')
	{
		echo '<td style="color: #c9e265;">'.$status.'</td>';	
	}
	echo'	
	<td><p onclick="offer_verify(1,'.$code.');" style="float: left;cursor: pointer;">Approve </p><p style="float: left;">/</p><p onclick="offer_verify(0,'.$code.');" style="float: left;cursor: pointer;"> Delete</p></td>
	<td style="color: green; cursor: pointer;"><a style="text-decoration: none;" href="../o_attachment_download.php?code='.$code.'"><p>View</p></a></td>
	</tr>
	';
}
?>
</tbody>
</table>
<script src="js/approve_delete_offer.js"></script>
</div>
<!--
<div id="side_nav" style="width: 50%; height: 100%; overflow-y: scroll; background-color: white; box-shadow: 0px 0px 10px 0px #a6a6a6; position: absolute; right: 0px;">
</div>
-->
</div>
</div>

<div  id="studentprojects" style="overflow-y: scroll; position: relative;">
<p style="font-family: 'Lato', sans-serif; font-size: 16px; padding-left: 10px;">New Story</p>
<div  onclick="document.getElementById('new_sucess').style.display='block';" style="width: 200px; cursor: pointer; position: relative; height: 110px; border-radius: 10px; margin-left: 40px; margin-top: 20px; background-color: #f6f6f6;">
<div style="width: 60px; margin: auto; border-radius: 60px; position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px; height: 80px; background-color: #f6f6f6;">
<div style="width: 60px; height: 5px; background-color: #d9d9d9; position: relative; top: 38px;">
</div>
<div style="width: 60px; height: 5px; background-color: #d9d9d9; position: relative; top: 33px; transform: rotate(90deg);">
</div>
</div>
</div>

<div style="width: 100%; height: auto;  margin-top: 40px;">


<?php
require('php/conn.php');
$get_data_sucess=mysqli_query($conn,"select * from success_stories order by timestamp DESC");
while($get_data_s=mysqli_fetch_assoc($get_data_sucess))
{
	$ss_title=$get_data_s['title'];
	$ss_content=$get_data_s['content'];
	$cc=$get_data_s['hash_key'];
	$ss_ext="sucess/".$get_data_s['hash_key'].'.'.$get_data_s['ext'];
	if($get_data_s['ext']=='mp4')
	{
			echo '
	
	<div class="sucess_story_box">
	<div style="width: 100%; height: 100%; position: absolute; top: 0px;">
	<div style="width: 100%; height: 180px;  overflow: hidden;">
	<video width="100%" height="auto" controls>
  <source src="'.$ss_ext.'" type="video/mp4">
  <source src="'.$ss_ext.'" type="video/ogg">
  Your browser does not support the video tag.
   </video>
	</div>
	';
	}
	if($get_data_s['ext']!='mp4')
	{
		echo '
	
	<div class="sucess_story_box">
	<div style="width: 100%; height: 100%; position: absolute; top: 0px;">
	<div style="width: 100%; height: 180px;  overflow: hidden;">
	<img src="'.$ss_ext.'" style="width: 100%; height: auto;" />
	</div>
	';
	}
	
	if(strlen($ss_content)<=75)
	{
	echo '<p style="text-align: left; padding: 10px 20px 0px 20px; font-family: '."'Noto Sans'".', sans-serif; color: #545454; font-size: 14px;">'.$ss_content.'</p>';
	}
	else
	{
		echo '<p style="text-align: left; padding: 10px 20px 0px 20px; font-family: '."'Noto Sans'".', sans-serif; color: #545454; font-size: 14px;">';
	for($i=0;$i<75;$i++)
	{
		echo $ss_content[$i];
	}
	echo ' ...</p>';
	}
	echo'
	<div style="width: 100%: height: auto; text-align: right;">
	<form method="post" action="cms.php">
	<button name="delete_sucess" value="'.$cc.'" style="margin-right: 10px; font-family: arial; cursor: pointer; font-size: 15px; background-color: #ff5757; color: white; border: 1px solid #ff5757;">Delete</button>
	</form>
	</div>
	<div>
	</div>
	</div>
   </div>
	
	';
}
?>

</div>


<?php  require('php/new_success_story.php'); ?>
<div id="new_sucess" style="width: 100%; height: 100%; display: none; background-color: white; position: absolute; top: 0px;">
<p onclick="document.getElementById('new_sucess').style.display='none';" style="text-align: right; font-size: 30px; cursor: pointer; font-family: arial; padding-right: 10px;  padding-top: 10px; margin: 0px;">X</p>
<p style="font-family: arial;  font-size: 15px; padding-left: 10px;">New Story add</p>
<fieldset style="border: none; margin-top: 40px;">
<form action="" method="post" enctype="multipart/form-data">
<legend><p style="margin: 0px; font-family: arial;">Title</p></legend>
<input name="sucess_title" style="border: none; border-bottom: 1px solid black; font-size: 15px;"></input>
</fieldset>
<fieldset style="border: none; margin-top: 40px;">
<legend><p style="margin: 0px; font-family: arial;">Content</p></legend>
<textarea name="sucess_content" style="width: 800px; height: 300px;  font-family: arial; max-height: 300px; border: 1px solid #c2c2c2;"></textarea>
</fieldset>
<fieldset style="border: none; margin-top: 40px;">
<legend><p style="margin: 0px; font-family: arial;">Attachment</p></legend>
<input name="sucess_file" type="file" style="border: none;  font-size: 15px;"></input>
</fieldset>
<div style="width: 100%; height: auto; text-align: right; ">
<button name="sucess_button" style="margin-right: 20px; font-family: arial; color: white; cursor: pointer; font-size: 17px; padding: 2px 15px 2px 15px; background-color: #5271ff; border: 1px solid #5271ff;">Add</button>
</form>
</div>
</div>
</div>

<div id="new_event_create_popup" style="width: 100%; display: none; height: 100%; background-color: rgba(0,0,0,0.5); position: absolute; z-index: 1; top: 0px;">
<div style="width: 100%; height: 100%; position: relative;  background-color: white; overflow-y: scroll;">
<p style="text-align: right; cursor: pointer; margin: 0px; font-size: 30px; font-family: 'Lato', sans-serif; padding-top: 5px;padding-right: 5px;" onclick="document.getElementById('new_event_create_popup').style.display='none';">X</p>
<p style="text-align: center; cursor: pointer; margin: 0px; font-size: 20px; font-family: 'Lato', sans-serif; padding-top: 5px;padding-left: 5px;">Create Event</p>
<div style="width: 100%; height: auto;">
<form action="cms.php" method="post" enctype="multipart/form-data">
<fieldset style=" border: none; margin-top: 30px;">
<legend style="font-family: 'Lato', sans-serif; font-size: 15px;">Event Name</legend>
<input name="event_name_e" id="event_name_e" style="margin-top: 5px; font-size:16px; width: 30%; padding-bottom: 2px; border: none; border-bottom:1px solid black; font-family: 'Lato', sans-serif;"></input>
</fieldset>
<fieldset style=" border: none; margin-top: 30px;">
<legend style="font-family: 'Lato', sans-serif; font-size: 15px;">Event Date</legend>
<input name="event_date_e" type="date" id="event_date_e" style="margin-top: 5px; font-size:16px; width: 30%; padding-bottom: 2px; border: none; border-bottom:1px solid black; font-family: 'Lato', sans-serif;"></input>
</fieldset>
<fieldset style=" border: none; margin-top: 30px;">
<legend style="font-family: 'Lato', sans-serif; font-size: 15px;">Event Venue</legend>
<input name="event_venue_e" id="event_venue_e" style="margin-top: 5px; font-size:16px; width: 30%; padding-bottom: 2px; border: none; border-bottom:1px solid black; font-family: 'Lato', sans-serif;"></input>
</fieldset>
<fieldset style=" border: none; margin-top: 30px;">
<legend style="font-family: 'Lato', sans-serif; font-size: 15px;">Event Location</legend>
<input name="event_location_e" id="event_location_e" style="margin-top: 5px; font-size:16px; width: 30%; padding-bottom: 2px; border: none; border-bottom:1px solid black; font-family: 'Lato', sans-serif;"></input>
</fieldset>
<fieldset style=" border: none; margin-top: 30px;">
<legend style="font-family: 'Lato', sans-serif; font-size: 15px;">Event Duration (In days)</legend>
<input name="event_duration_e" id="event_duration_e" style="margin-top: 5px; font-size:16px; width: 30%; padding-bottom: 2px; border: none; border-bottom:1px solid black; font-family: 'Lato', sans-serif;"></input>
</fieldset>
<fieldset style=" border: none; margin-top: 30px;">
<legend style="font-family: 'Lato', sans-serif; font-size: 15px;">Event Desc</legend>
<textarea name="event_desc_e" id="event_desc_e" style="width: 95%; max-width: 95%; font-family: arial; border: none; background-color: #f4f4f4; height: 160px; max-height: 160px;"></textarea>
</fieldset>

<table style="width: 100%;">
<tr>
<td>
<fieldset style=" border: none; margin-top: 30px;">
<legend style="font-family: 'Lato', sans-serif; font-size: 15px;">Cover photo</legend>
<input name="event_cover_photo_e" id="event_cover_photo_e" type="file" style="margin-top: 5px; font-size:16px; width: 30%; padding-bottom: 2px; border: none; font-family: 'Lato', sans-serif;"></input>
</fieldset>
</td>
<td>
<fieldset style=" border: none; margin-top: 30px;">
<legend style="font-family: 'Lato', sans-serif; font-size: 15px;">Events Photos</legend>
<input name="event_photos_e[]" multiple id="event_photos_e" type="file" style="margin-top: 5px; font-size:16px; width: 30%; padding-bottom: 2px; border: none; font-family: 'Lato', sans-serif;"></input>
</fieldset>
</td>
</tr>
</table>
<button name="event_button_e" id="event_button_e" style="background-color: #38b6ff; font-size: 18px; color: white; cursor: pointer; border-radius: 5px; margin-left: 10px; margin-top: 20px; font-family: arial; padding: 5px 13px 5px 13px; border: none;">Submit</button>
</form>
</div>
</div>
</div>
<div id="events" style="overflow-y: scroll;">
<p style="font-family: 'Lato', sans-serif; font-size: 16px; padding-left: 10px;">New event create</p>
<div onclick="document.getElementById('new_event_create_popup').style.display='block';" style="width: 200px; cursor: pointer; position: relative; height: 110px; border-radius: 10px; margin-left: 40px; margin-top: 20px; background-color: #f6f6f6;">
<div style="width: 60px; margin: auto; border-radius: 60px; position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px; height: 80px; background-color: #f6f6f6;">
<div style="width: 60px; height: 5px; background-color: #d9d9d9; position: relative; top: 38px;">
</div>
<div style="width: 60px; height: 5px; background-color: #d9d9d9; position: relative; top: 33px; transform: rotate(90deg);">
</div>
</div>
</div>
<p style="font-family: 'Lato', sans-serif; font-size: 16px; padding-left: 10px; padding-top: 30px; margin: 0px;">Events</p>
<div style="width: 100%; height: auto; margin-top: 20px;">
<table id="customers">
  <tr>
    <th>Event Id</th>
    <th>Event Name</th>
	<th>Date</th>
	<th>Venue</th>
	<th>Action</th>
  </tr>
  <?php
  require('php/conn.php');
  $get=mysqli_query($conn,"select * from event_name order by e_code DESC;");
  while($k=mysqli_fetch_assoc($get))
  {
	  $event_id=$k['e_code'];
	  $event_name=$k['name'];
	  $event_date=$k['event_date'];
	  $event_desc=$k['event_desc'];
	  $event_venue=$k['venue'];
	  echo '
	    <tr>
    <td>'.$event_id.'</td>
    <td>'.$event_name.'</td>
	<td>'.$event_date.'</td>
	<td>'.$event_venue.'</td>
	<td onclick="delete_event('."'$event_id'".');" style="cursor: pointer; color: red;">Delete</td>
    </tr>

	  ';
  }
  ?>
</table>
</div>

</div>





<div id="news" style="overflow-y: scroll; position: relative;">
<p style="font-family: 'Noto Sans', sans-serif; padding-top: 20px; padding-left: 30px; font-size: 17px;">Add new</p>
<div onclick="document.getElementById('new_news').style.display='block';" style="width: 200px; cursor: pointer; position: relative; height: 110px; border-radius: 10px; margin-left: 40px; margin-top: 20px; background-color: #f6f6f6;">
<div style="width: 60px; margin: auto; border-radius: 60px; position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px; height: 80px; background-color: #f6f6f6;">
<div style="width: 60px; height: 5px; background-color: #d9d9d9; position: relative; top: 38px;">
</div>
<div style="width: 60px; height: 5px; background-color: #d9d9d9; position: relative; top: 33px; transform: rotate(90deg);">
</div>
</div>
</div>

<?php require('php/new_news.php'); ?>

<div id="new_news" style="width: 100%; display: none; height: 100%; background-color: white; position: absolute; top: 0px;">
<p onclick="document.getElementById('new_news').style.display='none';" style="text-align: right; font-size: 30px; cursor: pointer; font-family: arial; padding-right: 10px;  padding-top: 10px; margin: 0px;">X</p>
<p style="font-family: arial;  font-size: 15px; padding-left: 10px;">Add news</p>
<fieldset style="border: none; margin-top: 40px;">
<form action="" method="post" enctype="multipart/form-data">
<legend><p style="margin: 0px; font-family: arial;">Title</p></legend>
<input name="news_title" style="border: none; border-bottom: 1px solid black; font-size: 15px;">
</fieldset>
<fieldset style="border: none; margin-top: 40px;">
<form action="" method="post" enctype="multipart/form-data">
<legend><p style="margin: 0px; font-family: arial;">Url</p></legend>
<input name="news_url" style="border: none; border-bottom: 1px solid black; font-size: 15px;">
</fieldset>
<fieldset style="border: none; margin-top: 40px;">
<legend><p style="margin: 0px; font-family: arial;">Attachment (Only PDF)</p></legend>
<input name="news_file" multiple type="file" style="border: none;  font-size: 15px;">
</fieldset>
<div style="width: 100%; height: auto; text-align: right; ">
<button name="new_news_button" style="margin-right: 20px; font-family: arial; color: white; cursor: pointer; font-size: 17px; padding: 2px 15px 2px 15px; background-color: #5271ff; border: 1px solid #5271ff;">Add</button>
</form>
</div>
</div>
<p style="font-family: 'Noto Sans', sans-serif; padding-top: 20px; padding-left: 30px; font-size: 17px;">Live news</p>
<table id="customers" style="margin-top: 20px;">
<tbody id="display_recent_news_letters">
  <tr style="background-color: red;">
    <th style="background-color: #224162; color: white;">Code</th>
	<th style="background-color: #224162; color: white;">Title</th>
	<th style="background-color: #224162; color: white;">Timestamp</th>
	<th style="background-color: #224162; color: white;">Status</th>
  </tr>
	   <?php
require("php/conn.php");
$get_data=mysqli_query($conn,"select * from news;");
while($get_news_data=mysqli_fetch_assoc($get_data))
{
	$code=$get_news_data['code'];
	$title=$get_news_data['title'];
	$link=$get_news_data['hash_key'];
	$timestamp=$get_news_data['timestamp'];
	echo '
	<tr>
    <td >'.$code.'</td>
	<td>'.$title.'</td>
	<td>'.$timestamp.'</td>
	<td style="color: red; cursor: pointer;" onclick="document.getElementById('."'delete_news'".').submit();">Delete
		<form id="delete_news" method="post" enctype="multipart/form-data" action="">
	<input style="font-size: 2px; visibility: hidden;" type="text" name="delete_news" value="'.$link.'"></input>
	</form>
	</td>
  </tr>
	';
}
?>
	   
	   
	   
	   </tbody>
</table>
</div>

<div id="donation" style="overflow-y: scroll;">
<p style="text-align: center;">donation</p>
</div>

<div id="feedback" style="overflow-y: scroll;">
<div style="width: 100%; height: 100%; position: relative;">
<div style="width: 50%; height: 100%; border-right: 2px solid #f4f4f4; float: left;">
<p style="text-align: center; font-family: 'Open Sans', sans-serif; ">New querys</p>


<?php
require('php/conn.php');
$get_details=mysqli_query($conn,"select * from query where status is NULL;");
while($get_det=mysqli_fetch_assoc($get_details))
{
	$code=$get_det['code'];
	$problem=$get_det['problem'];
	$timestamp=$get_det['timestamp'];
echo '
<div id="'.$code.'" onclick="get_query_details(this);"  style="width: 100%; cursor: pointer; height: auto; margin-top: 10px; background-color: #fafafa; padding-bottom: 10px;">
<p style="font-family: arial; font-size: 14px; padding-top: 5px; margin: 0px; padding-left: 10px;">Ticket Number: '.$code.'</span></p>
<table>
<tr>
<td><p style="font-family: arial; margin: 0px; padding-left: 10px; padding-top: 5px;  font-size: 14px;">Query</p></td>
<td><p style="font-family: arial; margin: 0px; padding-left: 10px; padding-top: 5px;  font-size: 14px; ">'.$problem.'</p></td>
</tr>
</table>
<p style="font-family: arial; font-size: 14px; text-align: right; padding-top: 10px; margin: 0px; padding-right: 10px; padding-left: 10px;">Timestamp: '.$timestamp.'</p>
</div>
';
}
?>

</div>
<div style="width: 49%; height: 100%; float: left;">
<div id="view_ticket_details" style="width: 100%; height: 50%;">

</div>
<div style="width: 100%; height: 50%; border-top: 2px solid #f4f4f4; ">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
function get_query_details(a)
{
	var ticket=a.id;
	$.ajax({
	 type: 'post',
	 url: 'php/get_ticket_details.php',
	 data : {ticket:ticket},
	 success: function(code){
	 document.getElementById('view_ticket_details').innerHTML=code;
	 }
	 });
}

function update_query(a)
{
	var ticket=a.value;
	var content=document.getElementById('query_content').value;
	$.ajax({
	 type: 'post',
	 url: 'php/update_ticket.php',
	 data : {ticket:ticket, content:content},
	 success: function(code){
	 location.reload();
	 }
	 });
}
</script>
<?php
require('php/conn.php');
$get_total_count=mysqli_query($conn,"select * from query;");
$get_deleted=0;
$get_pending=0;
$get_closed=0;
while($get_s=mysqli_fetch_array($get_total_count))
{
	if($get_s['status']=='DELETE')
	{
		$get_deleted++;
	}
	else if($get_s['status']=='CLOSED')
	{
		$get_closed++;
	}
	else if($get_s['status']=='')
	{
		$get_pending++;
	}
}
?>
 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ["Total", "Density", { role: "style" } ],
        ["Deleted ", <?php echo $get_deleted; ?>, "#ff5757"],
        ["Resolved ",<?php echo $get_closed; ?>, "#c9e265"],
        ["Pending", <?php echo $get_pending; ?>, "#ffde59"]
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Query graph",
        width: 500,
        height: 300,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }
  </script>
<div id="columnchart_values" ></div>
</div>
</div>
</div>
</div>

<?php
require('php/conn.php');
$get_data=mysqli_query($conn,"select count(*),gender from profile where gender is not null group by gender;");
$batchwise='';
$c=0;
while($get_data_d=mysqli_fetch_array($get_data))
{
	$temp1=$get_data_d[1];
	$temp2=$get_data_d[0];
	if($c==0)
	{
	$batchwise=$batchwise.','."['".$temp1."',".$temp2.",'#b87333']";
	$c++;
	}
	else
	{
	$batchwise=$batchwise.','."['".$temp1."',".$temp2.",'silver']";	
	}
}
?>
<div id="content" class="content" style="overflow-y: scroll;">
<div style="width: 100%; height: 100%;">
<div style="width: 100%; height: auto; text-align: center;">
<div style="width: 500px; height: 300px; display: inline-block; position: relative; margin: 20px 30px 0px 30px; border-radius: 10px; box-shadow: 0px 0px 5px 0px #d4d4d4;  background-color: white;">
<div style="width: 100%; height: 100%; position: absolute; top: 0px;">
 <script type="text/javascript">
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ["Element", "Count", { role: "style" } ]<?php echo $batchwise; ?>
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Alumni accounts gender wise count",
        width: 500,
        height: 250,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.BarChart(document.getElementById("barchart_values"));
      chart.draw(view, options);
  }
  </script>
<div id="barchart_values" style="margin-top: 20px;"></div>
</div>
</div>

<?php
require('php/conn.php');
$get_data=mysqli_query($conn,"SELECT count(*),year_graduate FROM academics group by year_graduate");
$batchwise='';
while($get_data_d=mysqli_fetch_array($get_data))
{
	$temp1=$get_data_d[1];
	$temp2=$get_data_d[0];
	$batchwise=$batchwise.','."['".$temp1."',".$temp2."]";
}
?>

<div style="width: 500px; height: 300px; position: relative; display: inline-block; margin: 20px 30px 0px 30px; box-shadow: 0px 0px 5px 0px #d4d4d4;  background-color: white;">
<div style="width: 100%; height: 100%; position: absolute; top: 0px;">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day']<?php echo $batchwise; ?>
        ]);

        var options = {
          title: 'Batchwise registered count',
          pieHole: 0,
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);
      }
    </script>
<div id="donutchart" style="width: 500px; height: 300px;"></div>
</div>
</div>
</div>
<div style="width: 100%; height: 100%;">
<div style="width: 100%; height: auto; text-align: center;">
<div style="width: 500px; height: 300px; display: inline-block; position: relative; margin: 20px 30px 0px 30px; border-radius: 10px; box-shadow: 0px 0px 5px 0px #d4d4d4;  background-color: white;">
<div style="width: 100%; height: 100%; position: absolute; top: 0px;">
<div style="width: 100%; height: 70px;">
<fieldset style="border: none; float: left;">
<legend style="text-align: left; font-family: 'Open Sans', sans-serif; padding-top: 20px; font-size: 15px;">Reset password link generator (Alumni)</legend>
<input id="alu_email" type="text" style="float: left; margin-top: 10px; padding: 3px 0px 3px 5px; border: none; border-bottom: 1px solid black; margin-left: 5px; font-family: arial; font-size: 15px;" placeholder="Email"></input>
</fieldset>
<button onclick="forgot_password();" style="float: right; margin: 50px 20px 0px 0px;">Send Link</button>
</div>
<div style="width: 100%; height: 30px; float: left; text-align: center;">
<p id="forgot_password_alumni" style="margin: 0px; font-family: arial; font-size: 13px; color: green; padding-top: 10px;"></p>
</div>
</div>
</div>
<div style="width: 500px; height: 300px; display: inline-block; position: relative; margin: 20px 30px 0px 30px; border-radius: 10px; box-shadow: 0px 0px 5px 0px #d4d4d4;  background-color: white;">
<div style="width: 100%; height: 100%; position: absolute; top: 0px;">
<p style="font-family: arial; margin: 0px; font-size: 15px; text-align: left; padding-left: 20px; padding-top: 10px; padding-bottom: 10px;">Your account last login recrod</p>
<table id="admin_logs" style="font-family: arial;">
  <tr>
    <th>Ip</th>
    <th>Broswer</th>
    <th>Timestamp</th>
  </tr>
<?php
require("php/conn.php");
$mylogs=mysqli_query($conn,"select * from admin where hash_key='$session';");
$mylogs=mysqli_fetch_assoc($mylogs);
$email=$mylogs['email'];
$getmylogs=mysqli_query($conn,"select * from admin_secuirty where email='$email' order by timestamp DESC limit 6;");
while($getmylog=mysqli_fetch_assoc($getmylogs))
{
	$ip=$getmylog['ip'];
	$time=$getmylog['timestamp'];
	$bro=$getmylog['broswer'];
	echo '
	<tr>
	<td>'.$ip.'</td>
	<td>'.$bro.'</td>
	<td>'.$time.'</td>
	</tr>
	';
}
?>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
function forgot_password()
{
	var email=document.getElementById('alu_email').value;
	if(email!='')
	{
		$.ajax({
		 type: 'post',
		 url: 'php/forgot_password.php',
		 data : {email: email},
		 success: function(code){
			 if(code=='ok')
			 {
				 document.getElementById('forgot_password_alumni').innerHTML="Link send sucessfully";
				 document.getElementById('forgot_password_alumni').style.color="green";
			 }
			 else  if(code=='no')
			 {
				 document.getElementById('forgot_password_alumni').innerHTML="No account found";
				 document.getElementById('forgot_password_alumni').style.color="red";
			 }
		 }
		 });
	}
	else  
	{
		 document.getElementById('forgot_password_alumni').innerHTML="Please enter a correct email";
		 document.getElementById('forgot_password_alumni').style.color="red";
	}
}
</script>

<?php
error_reporting(0);
$show=$_GET['show'];
?>
<script>
var show="<?php echo $show; ?>";
if(show=="galleryimage")
{
	document.getElementById('galleryimage').style.display="block";
	document.getElementById('content').style.display="none";
}
else if(show=="alumniprofile_verification")
{
	document.getElementById('alumniprofile_verification').style.display="block";
	document.getElementById('content').style.display="none";
}
else if(show=="alumnistaffrecord")
{
	document.getElementById('alumnistaffrecord').style.display="block";
	document.getElementById('content').style.display="none";
}
else if(show=="newsletters")
{
	document.getElementById('newsletters').style.display="block";
	document.getElementById('content').style.display="none";
}
else if(show=="alumniprofiles")
{
	document.getElementById('alumniprofiles').style.display="block";
	document.getElementById('content').style.display="none";
}
else if(show=="offers")
{
	document.getElementById('offers').style.display="block";
	document.getElementById('content').style.display="none";
}
else if(show=="studentprojects")
{
	document.getElementById('studentprojects').style.display="block";
	document.getElementById('content').style.display="none";
}
else if(show=="events")
{
	document.getElementById('events').style.display="block";
	document.getElementById('content').style.display="none";
}
else if(show=="news")
{
	document.getElementById('news').style.display="block";
	document.getElementById('content').style.display="none";
}
else if(show=="donation")
{
	document.getElementById('donation').style.display="block";
	document.getElementById('content').style.display="none";
}
else if(show=="feedback")
{
	document.getElementById('feedback').style.display="block";
	document.getElementById('content').style.display="none";
}
else
{
	document.getElementById('content').style.display="block";
}

function close_popup(a)
{
	document.getElementById(""+a).style.display='none';
}

function open_popup_verify(name,gender,email,phone,job,company,reg,program,a)
{

	document.getElementById("profile_verify_name").innerHTML=name;
	document.getElementById("profile_verify_gender").innerHTML=gender;
	document.getElementById("profile_verify_email").innerHTML=email;
	document.getElementById("profile_verify_phone").innerHTML=phone;
	document.getElementById("profile_verify_job").innerHTML=job;
	document.getElementById("profile_verify_company").innerHTML=company;
	document.getElementById("profile_verify_regno").innerHTML=reg;
	document.getElementById("profile_verify_program").innerHTML=program;
	document.getElementById(""+a).style.display='block';
}
function verify_profile_verification_button()
{
	var email=document.getElementById("profile_verify_email").innerHTML;
	$.ajax({
	type: 'post',
	 url: 'php/profile_verify.php',
	 data : {email: email},
	 success: function(code){
		 alert(code);
		 location.reload(true);
	 }
	 });
}

</script>


</body>
</html>
