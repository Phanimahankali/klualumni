<html>
<head>
<title>KL Alumni Association | Admin Portal</title>
<?php require('php/admin_login_code.php');?>
<link href="https://fonts.googleapis.com/css?family=Lato|Roboto+Mono" rel="stylesheet">
<style>
body
{
	margin: 0px;
}
.left_photo
{
	width: 60%;
	height: 100%;
	background-image: url('icons/login.jpg');
	background-repeat: no-repeat;
	background-size: auto 100%;
	float: left;
}
.right_login
{
	width: 40%;
	height: 100%;
	float: right;
	text-align: center;
}
.logo
{
	width: 300px;
	height: 100px;
	margin: auto;
	margin-top: 50px;
}
#email
{
	width: 80%;
	font-size: 20px;
	margin: auto;
	margin-top: 40px;
	border: none;
	border-bottom: 1px solid #34313a;
}
button,input:focus
{
	outline: none;
}
#login
{
	width: 80%; 
	padding: 8px 30px 8px 30px;
	font-size: 20px; 
	font-family: 'Noto Sans', sans-serif;
	background: #cc3333;
	border: none;
	border-radius: 10px;
	color: white;
	margin-top: 20px;
	cursor: pointer;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
<div class="left_photo">
</div>
<div class="right_login">
<div class="logo">
<img src="icons/klu.png" style="width: auto; height: 50%; float: left;" />
<p style="margin: 0px; padding-top: 20px; font-family: 'Lato', sans-serif; font-size: 18px;">Alumni Association</p>
</div>
<p style="text-align: center; font-family: 'Lato', sans-serif; font-size: 20px; margin: 0px;"><strong>Admin Portal</strong></p>
<form style="width: 400px; height: auto; margin: auto;" action="login.php" method="post">
<input name="email" placeholder="Email" id="email" type="email"></input>
<input name="password" placeholder="Password" id="email" type="password"></input>
<div style="width: 100%; height: auto;">
<a href="forgot.php" style="text-decoration: none;">
<p style="margin: 0px; cursor: pointer; color: black; font-family: 'Roboto Mono', monospace; font-size: 12px; text-align: right; padding-top: 13px; padding-right: 40px;">Forgot Password ?</p>
</a>
</div>
<button id="login" name="login">LOGIN</button>
<script src="js/ip_broswer.js"></script>
<script type="application/javascript" src="https://api.ipify.org?format=jsonp&callback=getIP"></script>
</form>
</div>
</body>
</html>