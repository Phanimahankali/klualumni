<?php
require('js/php/conn.php');
error_reporting(0);
$code=$_GET['eve'];
if($code=='')
{
	header('Location:events.php');
}
else
{
	$get=mysqli_query($conn,"select * from event_name where e_code='$code';");
	$get=mysqli_fetch_assoc($get);
	$name=$get['name'];
	$cover_photo_format=$get['cover_photo_format'];
	if($name=='')
	{
		header('Location:events.php');
	}
	else
	{
		
	}
	$date=$get['event_date'];
	$event_desc=$get['event_desc'];
	$venue=$get['venue'];
	$event_desc=explode("\n",$event_desc);
}
?>
<html>
<head>
 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">

    <link rel="stylesheet" href="gallery/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="gallery/css/animate.css">
    
    <link rel="stylesheet" href="gallery/css/owl.carousel.min.css">
    <link rel="stylesheet" href="gallery/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="gallery/css/magnific-popup.css">

    <link rel="stylesheet" href="gallery/css/aos.css">
<link rel="stylesheet" type="text/css" href="css/index.css">

 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<script src="js/index_dropdown_menu.js"></script>

    <link rel="stylesheet" href="gallery/css/ionicons.min.css">

    <link rel="stylesheet" href="gallery/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="gallery/css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="gallery/css/flaticon.css">
    <link rel="stylesheet" href="gallery/css/icomoon.css">
    <link rel="stylesheet" href="gallery/css/style.css">
<style>
body
{
	margin: 0px;
	background-color: white;
}
.icon_dis
{
	width: 100%;
	height: 70px;
	max-width: 1800px;
	margin: auto;
}
.main_icon
{
	width: 400px;
	height: 100%;
}
.main_icon img
{
	height: 80%;
	width: auto;
	float: left;
	margin-top: 6px;
	margin-left: 10px;
}
.main_icon p
{
	font-size: 20px;
	padding-top: 35px;
	font-family: 'Lato', sans-serif;
}
.nav_images
{
	width: 100%;
	height: 50px;
	background-color: red;
}
.events_display_box
{
	width: 450px;
	height: 130px;
	cursor: pointer;
	margin: 10px 10px 10px 10px;
	display: inline-block;
	border-radius: 10px;
	position: relative;
	background-color: white;
	overflow: hidden;
	transition: 0.5;
	box-shadow: 0px 0px 5px 0px #d6d6d6;
}
.events_display_box:hover
{
	box-shadow: 0px 0px 1px 0px #d6d6d6;
	transition: 0.5;
}
.dropdownmenu
{
	width: 100%;
	height: 40px;
	background-color: #A81E24;
}
@charset "UTF-8";
.navigation {
  height: 70px;
}

.brand {
  position: absolute;
  padding-left: 20px;
  float: left;
  line-height: 70px;
  text-transform: uppercase;
  font-size: 1.4em;
}
.brand a,
.brand a:visited {
  color: #ffffff;
  text-decoration: none;
}

.nav-container {
  max-width: 1000px;
  margin: 0 auto;
}

nav {
  float: right;
}
nav ul {
  list-style: none;
  margin: 0;
  padding: 0;
}
nav ul li {
  float: left;
  font-family: 'Roboto', sans-serif;
  font-size: 15px;
  position: relative;
}
nav ul li a,
nav ul li a:visited {
  display: block;
  padding: 0 20px;
  line-height: 40px;
  background: #A81E24;
  color: #ffffff;
  text-decoration: none;
}
nav ul li a:hover,
nav ul li a:visited:hover {
  background: #bd3339;
  color: #ffffff;
}
nav ul li a:not(:only-child):after,
nav ul li a:visited:not(:only-child):after {
  padding-left: 4px;
  content: " ▾";
}
nav ul li ul li {
  min-width: 190px;
}
nav ul li ul li a {
  padding: 8px;
  font-size: 14px;
  line-height: 20px;
}

.nav-dropdown {
  position: absolute;
  display: none;
  z-index: 1;
  box-shadow: 0 3px 12px rgba(0, 0, 0, 0.15);
}

/* Mobile navigation */
.nav-mobile {
  display: none;
  position: absolute;
  top: 0;
  right: 0;
  background: #262626;
  height: 70px;
  width: 70px;
}

@media only screen and (max-width: 798px) {
  .nav-mobile {
    display: block;
  }

  nav {
    width: 100%;
    padding: 70px 0 15px;
  }
  nav ul {
    display: none;
  }
  nav ul li {
    float: none;
  }
  nav ul li a {
    padding: 15px;
    line-height: 20px;
  }
  nav ul li ul li a {
    padding-left: 30px;
  }

  .nav-dropdown {
    position: static;
  }
}
@media screen and (min-width: 799px) {
  .nav-list {
    display: block !important;
  }
}
#nav-toggle {
  position: absolute;
  left: 18px;
  top: 22px;
  cursor: pointer;
  padding: 10px 35px 16px 0px;
}
#nav-toggle span,
#nav-toggle span:before,
#nav-toggle span:after {
  cursor: pointer;
  border-radius: 1px;
  height: 5px;
  width: 35px;
  background: #ffffff;
  position: absolute;
  display: block;
  content: "";
  transition: all 300ms ease-in-out;
}
#nav-toggle span:before {
  top: -10px;
}
#nav-toggle span:after {
  bottom: -10px;
}
#nav-toggle.active span {
  background-color: transparent;
}
#nav-toggle.active span:before, #nav-toggle.active span:after {
  top: 0;
}
#nav-toggle.active span:before {
  transform: rotate(45deg);
}
#nav-toggle.active span:after {
  transform: rotate(-45deg);
}

</style>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <script src="js/index_dropdown_menu.js"></script>
</head>
<body>
<div class="icon_dis">
<div class="main_icon" onclick="window.location='index.php'">
<img src="imags/klu.png" />
<strong><p>Alumni Association</p></strong>
</div>
</div>

<div class="dropdownmenu">
 <nav>
      <div class="nav-mobile"><a id="nav-toggle" href="#!"><span></span></a></div>
      <ul class="nav-list" >
        <li>
          <a href="#" style="text-decoration: none;">Carrers</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="careers.php" style="text-decoration: none;">Internship</a>
            </li>
            <li>
              <a href="careers.php" style="text-decoration: none;">Jobs</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="advisoryboard.php" style="text-decoration: none;">Board Members</a>
        </li>
        <li>
          <a href="#!" style="text-decoration: none;">Events</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="events.php" style="text-decoration: none;">Alumni Event Calender</a>
            </li>
			<li><a href="events.php" style="text-decoration: none;">All Events</a></li>
			<li>
			  <a href="https://kluniversity.in/site/acadcal.htm" style="text-decoration: none;">Academic Calender</a></li>
          </ul>
        </li>
		 <li>
              <a href="suess_stories.php" style="text-decoration: none;">Alumni Stories</a>
            </li>
            <li>
              <a href="gallery.php" style="text-decoration: none;">Gallery</a>
            </li>
         <li>
              <a href="updates.php" style="text-decoration: none;">News & Updates</a>
            </li>
        <li>
          <a href="aboutus.html" style="text-decoration: none;">About us</a>
        </li>
      </ul>
    </nav>
</div>
<div style="width: 100%; height: 300px;  position: relative; overflow: hidden;">
<?php
if($cover_photo_format=='')
{
echo '
<img src="imags/slider/our.png" style="width: 100%; height: auto;" />';
}
else
{
	echo '
<img src="cms/event_photos/e_'.$code.'.'.$cover_photo_format.'" style="width: 100%; height: auto;" />';
}
?>
<p style="position: absolute; margin: 0px; bottom: 50px; font-family: 'Roboto', sans-serif;  left: 10px; color: white; font-size: 25px;"><?php echo $name; ?></p>
<div style="position: absolute; margin: 0px; bottom: 5px;  left: 10px; color: white; width: 200px; height: 40px;">
<img src="imags/attendance.png" style="width: 20px; float: left; height: auto;"/>
<p style="float: left; margin: 0px; font-family: 'Roboto', sans-serif; padding-left: 10px;"><?php echo $date; ?></p>
</div>
<div style="position: absolute; margin: 0px; bottom: 5px;  right: 10px; color: white; width: auto; height: 40px;">
<img src="imags/location.png" style="width: 20px; float: left; height: auto;"/>
<p style="float: left; margin: 0px; font-family: 'Roboto', sans-serif; padding-left: 10px;"><?php echo $venue; ?></p>
</div>

</div>
<div style="width: 100%; height: 30px;">
</div>
<?php
for($i=0;$i<count($event_desc);$i++)
{
	echo '
	<p style="font-family: '."'Roboto'".', sans-serif; color: black; line-height: 20px; margin: 0px; padding: 10px 20px 0px 20px;">'.$event_desc[$i].'</p>
	';
}
?>

<div style="width: 100%; height: 100px;">
</div>

<div id="colorlib-page">
		

		<div id="colorlib-main">
		
			<section class="ftco-section-2">
				<div class="photograhy">
					<div class="row no-gutters">
						<?php
						$get=mysqli_query($conn,"select * from event_photos where event_code='$code';");
						while($g=mysqli_fetch_assoc($get))
						{
							$p_code=$g['code'];
							$p_code=$p_code.'.'.$g['image_type'];
							$p_code="cms/event_photos/".$p_code;
						echo '
						<div class="col-md-4 ftco-animate">
							<a href="'.$p_code.'" class="photography-entry img image-popup d-flex justify-content-center align-items-center" style="background-image: url('.$p_code.');">
								
							</a>
						</div>
						
						';
						}
						?>
					</div>
				</div>
			</section>
	    
		</div><!-- END COLORLIB-MAIN -->
	</div><!-- END COLORLIB-PAGE -->

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="gallery/js/jquery.min.js"></script>
  <script src="gallery/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="gallery/js/popper.min.js"></script>
  <script src="gallery/js/bootstrap.min.js"></script>
  <script src="gallery/js/jquery.easing.1.3.js"></script>
  <script src="gallery/js/jquery.waypoints.min.js"></script>
  <script src="gallery/js/jquery.stellar.min.js"></script>
  <script src="gallery/js/owl.carousel.min.js"></script>
  <script src="gallery/js/jquery.magnific-popup.min.js"></script>
  <script src="gallery/js/aos.js"></script>
  <script src="gallery/js/jquery.animateNumber.min.js"></script>
  <script src="gallery/js/bootstrap-datepicker.js"></script>
  <script src="gallery/js/jquery.timepicker.min.js"></script>
  <script src="gallery/js/scrollax.min.js"></script>
 <script src="gallery/js/google-map.js"></script>
  <script src="gallery/js/main.js"></script>

</body>
</html>