<html>
<head>
<?php
error_reporting(0);
session_start();
$get_365mail=$_GET['mail'];
if($_SESSION['alu_auth']!=null)
{
	header('location: news.php');
}
?>
<title>Registration | KL Alumni Asssociation</title>
 <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
 <link href="https://fonts.googleapis.com/css?family=Mukta:400,700" rel="stylesheet">
 <link rel="stylesheet" type="text/css" href="css/register.css">
 <style>
 #login_button
 {
 padding: 3px 20px 3px 20px; 
 margin-top: 20px; 
 font-size: 15px;
 background-color: transparent;
 color: white;
 cursor: pointer;
 border-radius: 10px;
 border: 1.5px solid white;
 font-family: 'Mukta', sans-serif;
 }
 #login_button:hover
 {
 background-color: white;
 color: #224162;
 }
 *:focus
 {
	 outline: none;
 }
 </style>
</head>

<body>
<div class="signup_box">
<a href="index.php"  style="text-decoration: none;">
<div style="width: 100%; height: auto; text-align: center;">
<img src="imags/klu.png" style="width: auto; height: 45px; margin: 0px; margin-top: 15px; display: inline-block;"/>
<p style=" font-family: 'Roboto', sans-serif; color: #8f8f8f; font-size: 18px;  display: inline-block;">Alumni Association</p>
</div>
</a>
<div style="width: 100%; height: 20px;">
<p id="status_div" style="margin: 0px;text-align: center; display: none; font-family: 'Roboto', sans-serif; color: red; padding-top: 20px;">Already Acount Exist</p>
</div>

<?php
if($get_365mail!='')
{
	echo '
	<script>
	document.getElementById("status_div").style.display="block";
	document.getElementById("status_div").innerHTML="No Account found on this Email";
	document.getElementById("status_div").style.color="red";
	</script>
	';
}
?>

<p style="font-family: 'Mukta', sans-serif; color: #343434; margin: 0px; font-size: 22px; padding-left: 20px; padding-top: 60px;"><b>Create an account</b></p>
<p style="font-family: 'Mukta', sans-serif; color: #343434; margin: 0px; line-height: 19px; font-size: 13px; padding-left: 20px; padding-top: 10px;">One of the largest alumni accociation connect with more than 23,000+ members working in different country, fields..</p>
<div style="width: 500px;  height: 390px;  background-color: white;">
<div style="width: 100%; height: auto;">

<form action="register.php" method="post">
<input required type="text" name="first" class="inputfiled_f" placeholder="First name"></input>
<input required type="text" name="last" class="inputfiled_f" style="margin-left: 30px;" placeholder="Last name"></input>
<input required type="email" name="email" id="email"
<?php 
error_reporting(0);
if($get_365mail!='')
{
	echo 'value="'.$get_365mail.'"';
}	
?>
class="inputfiled" placeholder="Email"></input>
<input required type="text" name="mobile" class="inputfiled" placeholder="Mobile"></input>

<p style="font-family: 'Mukta', sans-serif;  color: #343434; margin: 0px; line-height: 19px; font-size: 15px; float: left; padding-left: 20px; padding-top: 20px;"><b>Personal Information</b></p>

<input required type="text" name="company_name" class="inputfiled" placeholder="Company name" ></input>
<input required type="text" name="designation" class="inputfiled" placeholder="Designation" ></input>
<input required type="text" class="inputfiled" name="current_city" placeholder="Current city"></input>

<p style="font-family: 'Mukta', sans-serif;  color: #343434; margin: 0px; line-height: 19px; font-size: 15px; float: left; padding-left: 20px; padding-top: 20px;"><b>Academic Record</b></p>

<input required type="number" name="graduate_year" class="inputfiled" placeholder="Graduated Year" ></input>
<div style="width: 100%; height: 50px;">
<select required onchange="pro(this);" type="text" class="inputfiled_f" name="program"  placeholder="Program (Ex: B.Tech )">
<option value="">Select Program</option>
<option value="B.Tech">B.Tech</option>
<option value="B.Arch">B.Arch</option>
<option value="BHM">BHM</option>
<option value="BFA">BFA</option>
<option value="B.Sc(VC)">B.Sc(VC)</option>
<option value="BCA">BCA</option>
<option value="B.Com(Hons)">B.Com(Hons)</option>
<option value="BBA">BBA</option>
<option value="B.Pharm">B.Pharm</option>
<option value="BA">BA</option>
<option value="M.Com">M.Com</option>
<option value="M.Tech">M.Tech</option>
<option value="MBA">MBA</option>
<option value="MA">MA</option>
<option value="M.Sc">M.Sc</option>
<option value="LLB">LLB</option>
<option value="LLM">LLM</option>
</select>
<select id="stream" name="stream" required type="text" class="inputfiled_f" style="margin-left: 30px;" placeholder="Stream (Ex: CSE )">
</select>
<input type="text" class="inputfiled" name="regid_num" placeholder="University Reg ID (Optional)"></input>

<script>
var p;
function pro(a)
{
	p=a.value;
	
	xmlhttp=new XMLHttpRequest();
	xmlhttp.open("GET","js/php/stream.php?program="+p,false);
	xmlhttp.send(null);
	document.getElementById('stream').innerHTML=xmlhttp.responseText;
}
</script>
</div>
</div>
</div>
<div style="width: 100%; height: 50px; background-color: white;">
<button class="register_button" name="register_button">Create Account</button>
</form>
<?php
require('js/php/conn.php');
error_reporting(0);
if(isset($_REQUEST['register_button']))
{
$first_name=$_REQUEST['first'];
$last_name=$_REQUEST['last'];
$email=$_REQUEST['email'];
$mobile=$_REQUEST['mobile'];
$univer_reg_id=$_REQUEST['regid_num'];
$graduate_year=$_REQUEST['graduate_year'];
$program=$_REQUEST['program'];
$stream=$_REQUEST['stream'];
$company_name=$_REQUEST['company_name'];
$designation=$_REQUEST['designation'];
$current_city=$_REQUEST['current_city'];
if($first_name!='' and $last_name!='' and $email!='' and $mobile!='' and $graduate_year!='' and $program!='' and $stream!='')
{
$check=mysqli_query($conn,"select * from profile where email='$email';");
$key=md5(md5(md5($email)));
$temp=$first_name.','.$last_name;
if(mysqli_num_rows($check)==0)
{
	
	$get_branch=mysqli_query($conn,"select * from programs where sno='$stream';");
	$get_branch=mysqli_fetch_assoc($get_branch);
	$stream=$get_branch['stream'];
	$profile_name=$first_name.' '.$last_name;
	mysqli_query($conn,"insert into academics (email,reg,program,stream,year_graduate) values('$email','$univer_reg_id','$program','$stream','$graduate_year');");
	mysqli_query($conn,"insert into profile (email,name,phone,hash_key,profile_pic,profile_header,profile_name) values ('$email','$temp','$mobile','$key','profile_pics/default.png','profile_banners/default.png','$profile_name');");
	mysqli_query($conn,"insert into auth (email) values ('$email');");
	mysqli_query($conn,"insert into personal (email,job,company,town) values ('$email','$designation','$company_name','$current_city');");
	$hash=md5(uniqid($email,true));
	mysqli_query($conn,"insert into password_reset (email,generated_by,hash_key) values ('$email','User','$hash');");
    include('cms/mailer/new_reset.php');
  echo '
    <script>
 window.location.href = "login.php";
</script>
';
}
else
{
	echo '
	<script>
	document.getElementById("status_div").style.display="block";
	</script>
	';
}
}
}
?>
</div>
</div>
<div class="left_box">
<div style="width: 100px; height: 50px; text-align: center;">
<a href="login.php"><button id="login_button">Login</button></a>
</div>
</div>
</body>
</html>
