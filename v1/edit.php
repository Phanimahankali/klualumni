<html>
<head>
<title>Edit Profile | KL Alumni Association</title>
<?php include('js/php/add_data.php'); ?>
<?php
include('js/php/skills_add_update.php');
?>
<?php
error_reporting(0);
clearstatcache();
session_start();
?>
<?php include('js/php/get_profile_data.php'); ?>
<?php include('profile_image_chage.php'); ?>

<style>
.banner
{
	width: 100%;
	height: 300px;
	max-width: 1800px;
	margin: 0px;
	position: relative;
	background-size: 100% auto;
	background-position: center;
	background-repeat: no-repeat;
}
.nav_icons
{
	width: auto;
	height: 100%;
	float: left;
	padding-right: 10px;
	cursor: pointer;
}
.nav_icons:hover
{
	background-color: #f4f4f4;
}
.update_geo
{
	margin-right: 60px;
	margin-top: 20px;
	background-color: #eff0f2;
	font-family: arial;
	font-size: 15px;
	padding: 3px 10px 3px 10px;
	cursor: pointer;
	border: 1px solid #c2c2c2;
}
.update_geo:hover
{
	background-color: #e4e4e4;
}
</style>
<link rel="stylesheet" href="css/edit.css"/>
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<script src="js/edit.js"></script>
</head>
<body>

<div id="link_emails" style="width: 100%; height: 100%; display: none; z-index: 10; position: fixed; background-color: rgba(0,0,0,0.5);">
<div style="width: 500px; height: 200px; background-color: white; position: absolute; top: 0px; bottom: 0px; left: 0px; right: 0px; margin: auto;">
<p onclick="document.getElementById('link_emails').style.display='none';" style="font-family: arial; font-size: 25px; margin: 0px; position: absolute; top: -30px; color: white; right: 0px; cursor: pointer;">X</p>
<h2 style="font-family: arial; padding-left: 15px; margin: 0px; padding-top: 17px; color: #252525;">Link email</h2>
<div style="width: 100%; height: 70px;  text-align: center; margin-top: 30px;">
<?php 
require('js/php/add_office_mail.php'); 
if($microsoft!='')
{
	echo '<a href="" style="text-decoration: none;">';
}
else
{
	echo '<a href="'.$accessUrl.'" style="text-decoration: none;">';
}
?>
<div style="width: 180px; height: 50px; position: relative; background-color: white; cursor: pointer; box-shadow: 0px 0px 5px 0px #e0e0e0; margin: 0px 10px 0px 10px; display: inline-block;">
<div style="width: 100%; height: 100%; position: absolute; top: 0px;">
<div style="width: 50px; position: relative; height: 50px; float: left;">
<img src="imags/365.png" style="width: 40px; height: 40px; position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px; margin: auto;" />
</div>
<?php
if($microsoft=='')
{
	echo '<p style="float: left; font-family: arial; padding-left: 20px;">Not Linked</p>';
}
else
{
	echo '<p style="float: left; color: black; font-family: arial; padding-left: 20px;">Linked</p>';
}
?>
</div>
</div>
</a>
<?php
require_once('js/php/google_auth.php');
$login_url = 'https://accounts.google.com/o/oauth2/v2/auth?scope=' . urlencode('https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email') . '&redirect_uri=' . urlencode(CLIENT_REDIRECT_URL) . '&response_type=code&client_id=' . CLIENT_ID . '&access_type=online';
if($google!='')
{
	echo '<a href="" style="text-decoration: none;">';
}
else
{
	echo '<a href="'.$login_url.'" style="text-decoration: none;">';
}
?>
<div style="width: 180px; height: 50px; position: relative; background-color: white; cursor: pointer; box-shadow: 0px 0px 5px 0px #e0e0e0; margin: 0px 10px 0px 10px; display: inline-block;">
<div style="width: 100%; height: 100%; position: absolute; top: 0px;">
<div style="width: 50px; position: relative; float: left; height: 50px;">
<img src="imags/google.png" style="width: 40px; height: 40px; position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px; margin: auto;" />
</div>
<?php
if($google=='')
{
	echo '<p style="float: left; font-family: arial; padding-left: 20px;">Not Linked</p>';
}
else
{
	echo '<p style="float: left; font-family: arial; padding-left: 20px;">Linked</p>';
}
?>
</div>
</div>
</a>
</div>
</div>
</div>

<div id="popup_profile_image">
<div class="profile_pic_size">
<p style="font-family: arial; font-size: 25px; text-align: right; margin: 0px; position: absolute; right: 0px; top: -35px; cursor: pointer; color: white;" onclick="photo_change('close')">X</p>
<div style="width: 100%; height: 190px; margin-top: 10px; position: relative;">
<div id="change_profile_pic" class="change_profile_pic">
<div style="width: 100%; height: 100%;">
<img id="change_image_tag" src="imags/camera_w.png" style="position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 20px; width: 30%; height: auto;" />
<p id="change_image_p_tag" style="font-family: arial; padding-top: 110px; color: #666666;">Upload Photo</p>

<form action="?" method="post" enctype="multipart/form-data">
<input type="file" name="profile_pic" id="photo" onchange="show();" style="opacity: 0.0; position: absolute; top:0; left: 0; bottom: 0; right:0; width: 100%; height:100%; cursor: pointer;" />
</div>
</div>
</div>
<div style="margin-top: 20px; width:100%; height: auto; text-align: center;">
<input type="text" name="profile_first_name" placeholder="First Name" style="padding: 10px; font-family: arial; font-size: 17px; margin-left: 20px; margin-right: 20px; display: inline-block; border: none; border-bottom: 1.5px solid black;">
</input>
<input type="text" name="profile_last_name" placeholder="Last Name" style="padding: 10px; font-family: arial; font-size: 17px; margin-left: 20px; margin-right: 20px; display: inline-block; border: none; border-bottom: 1.5px solid black;">
</input>
<p style="font-family: arial; text-align: left; padding-left: 22px;color: #bcbcbc; font-size: 13px;">Shown at top of profile</p>
<button name="change_profile_pic_button" class="change_profile_pic_button">Change</button>
</form>


</div>
</div>
</div>




<div id="education_add_popup">
<div class="education_add">
<p onclick="close_popup('education_add_popup')" style="position: absolute; margin: 0px; top: -35px; right: 0px; color: white; font-size: 30px; font-family: arial; cursor: pointer;">X</p>
<div style="position: relative; width: 100%; height: 80px; background-color: #25293b; margin: 0px; border-top-left-radius: 20px; border-top-right-radius: 20px;">
<div style="overflow: hidden; float: left;width: 60px; height: 60px; background-color: white; border-radius: 60px; position: relative; margin-top: 10px; margin-left: 10px;">
<img src="education_p.png" id="organization_image" style="width: 100%; height: auto; position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px; margin: auto;" />
</div>
<p id="education_add_title_bar" style="float: left; padding-left: 15px; padding-top: 13px; font-family: arial; color: white;">Add / Edit School Details</p>
</div>
<form action="#" method="post"> 
<input type="text" name="add_school_name" id="organization" onkeyup="org(this);" class="input_tags" placeholder="College/School Name"></input>
<div id="display_org" onclick="close(this);" style="width: 270px; height: auto; position: absolute; top: 130px; left: 40px; background-color: white; box-shadow: 0px 0px 2px 0px #a6a6a6;">
</div>
<script>
function close(a)
{
	a.style.display="none";
}
</script>
<input name="add_course_name" id="add_course_name" type="text" class="input_tags" placeholder="Course Name"></input>
<input name="add_stream" id="add_stream" type="text" class="input_tags" placeholder="Stream / Board name"></input>
<input name="add_edu_location" id="add_edu_location" type="text" class="input_tags" placeholder="Location"></input>
<div style="width: 100%; height: 25px; text-align: center;">
<div style="width: 40%; height: 25px; margin: 0px 20px 0px 20px; display: inline-block;">
<p style="margin: 0px; text-align: left; font-family: arial; font-size: 13px;">Form Year</p>
</div>
<div style="width: 40%; height: 25px; margin: 0px 20px 0px 20px; display: inline-block;">
<p style="margin: 0px; text-align: left; font-family: arial; font-size: 13px;">To Year</p>
</div>
</div>
<div style="width: 100%; height: auto; text-align: center;">
<div style="width: 40%; height: auto; margin: 0px 20px 0px 20px; display: inline-block;">
<input name="add_edu_join_month" id="add_edu_join_month" placeholder="MM" list="month" style="width: 30%; margin-left: 5px; margin-right: 5px; height: auto;  padding:5px; font-family: arial; font-size: 15px; border: none; border-bottom: 1.5px solid #25293b;"></input>
<input name="add_edu_join_year" id="add_edu_join_year" placeholder="YYYY" style="width: 60%;margin-left: 5px; margin-right: 5px; height: auto;  padding:5px;  font-family: arial; font-size: 15px; border: none; border-bottom: 1.5px solid #25293b;"></input>
</div>
<div style="width: 40%; height: auto; margin: 0px 20px 0px 20px; display: inline-block;">
<input name="add_edu_l_month" id="add_edu_l_month" placeholder="MM" list="month" style="width: 30%; margin-left: 5px; margin-right: 5px; height: auto;  padding:5px; font-family: arial; font-size: 15px; border: none; border-bottom: 1.5px solid #25293b;"></input>
<input name="add_edu_l_year" id="add_edu_l_year" placeholder="YYYY" style="width: 60%;margin-left: 5px; margin-right: 5px; height: auto;  padding:5px;  font-family: arial; font-size: 15px; border: none; border-bottom: 1.5px solid #25293b;"></input>
</div>
</div>
<button name="add_edu_button" id="add_edu_button" class="change_button">Add</button>
<button name="edit_edu_button" id="edit_edu_button" class="edit_button">Update</button>
</form>
</div>
</div>







<datalist id="month">
<option value="Jan">
<option value="Feb">
<option value="Mar">
<option value="Apr">
<option value="May">
<option value="Jun">
<option value="Jul">
<option value="Aug">
<option value="Sept">
<option value="Oct">
<option value="Nov">
<option value="Dec">
</datalist>

<div id="skills_add_popup">
<div class="skills_add">
<div style="width: 100%; height: 120px;border-top-left-radius: 20px;border-top-right-radius: 20px; overflow: hidden;">
<img src="imags/skills_popup_image.png" style="width: 100%; height: auto;" />
<p onclick="close_popup('skills_add_popup')" style="position: absolute; cursor: pointer; margin: 0px; top: -30px; right: 0px; color: white; font-family: arial; font-size: 25px;">X</p>
</div>
<p style="text-align: left; margin: 0px; padding: 10px; font-family: 'Montserrat', sans-serif;">Add Your Skills</p>
<div style="width: 100%; height: auto; padding-left: 15px;">
<div id="skills_box" style=" float: left; width: auto; height: auto;">
</div>
<input type="text"  id="skills" style=" font-family: arial; font-size: 15px; min-width: 10px; float: left; padding: 5px; border: none; border-bottom: 2px solid #616060;" placeholder="(Eg: C )"></input>
</div>
<p style="font-size: 10px; width: 100%;text-align: left; float: left; margin: 0px; padding: 10px; font-family: 'Montserrat', sans-serif;">Note: Hit enter after adding each skill</p>
<div id="skill_add" onclick="remove(this)" value="ok" style="visibility: hidden; float: left; width: auto; cursor: pointer;  height: 25px; background-color: #f4f4f4; margin: 0px; margin-right: 5px;">
<div style="width: auto; height: 20px; float: left;">
<p style="margin: 0px;  padding-left: 5px; padding-top: 3px; padding-right: 5px; font-family: arial;"></p>
</div>
<div  style="float: left; width: 20px; height: 20px; background-color: #f4f4f4; margin-top: 2.5px; margin-right: 2px; float: right; position: relative;">
<div style="width: 2px; height: 15px; background-color: #a8a8a8; transform: rotate(45deg); position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;">
</div>
<div style="width: 2px; height: 15px; background-color: #a8a8a8; transform: rotate(135deg); position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;">
</div>
</div>
</div>
<script>
var i=0;
var array=[];
var skills_box=document.getElementById('skills_box');
document.getElementById('skills').onkeypress = function(e){
    var e = window.event;
    var keyCode = e.keyCode || e.which;
	if (keyCode == '13'){ 
	 add();
    }
  }
function add()
{
i++;
var skill_adds=document.getElementById('skill_add');
var m=skill_adds.cloneNode(true);
m.id="v"+i;
m.style.visibility="visible";
skills_box.appendChild(m);
document.querySelector('#v'+i+' div p').innerHTML =document.getElementById('skills').value;
document.querySelector('#v'+i).setValue="v"+i;
array.push(document.getElementById('skills').value);
document.getElementById('skills').value="";
document.getElementById('add_skill_button').value=array;
}
function remove(ok)
{
var d=ok.id;
var val=document.querySelector('#'+d+' div p').innerHTML;
array=remove_array_element(array,val);
document.getElementById(''+d).style.display="none";
document.getElementById('add_skill_button').value=array;
}

function remove_array_element(array, n)
{
   var index = array.indexOf(n);
   if (index > -1) {
    array.splice(index, 1);
}
   return array;
 }
</script>
<form action="#" method="post">
<button id="add_skill_button" name="add_skill_button" class="add_skill_button">Add</button>
</form>
</div>
</div>

<div id="experience_add_popup">
<div class="experience_add">
<p onclick="close_popup('experience_add_popup')" style="position: absolute; margin: 0px; top: -35px; right: 0px; color: white; font-size: 30px; font-family: arial; cursor: pointer;">X</p>
<div style="position: relative; width: 100%; height: 80px; background-color: #25293b; margin: 0px; border-top-left-radius: 20px; border-top-right-radius: 20px;">
<div style="overflow: hidden; float: left;width: 60px; height: 60px; background-color: white; border-radius: 60px; position: relative; margin-top: 10px; margin-left: 10px;">
<img src="company_p.png" id="school_image" style="width: 100%; height: auto; position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px; margin: auto;" />
</div>
<p style="float: left; padding-left: 15px; padding-top: 13px; font-family: arial; color: white;">Add | Edit Experience</p>
</div>
<form action="#" method="post">
<input type="text" name="exp_organization" id="exp_organization" onkeyup="school(this);" class="input_tags" placeholder="Company Name"></input>
<div id="display_school" style="width: 270px; height: auto; position: absolute; top: 130px; left: 40px; background-color: white; box-shadow: 0px 0px 2px 0px #a6a6a6;">
</div>
<input id="exp_add_job" name="exp_add_job" type="text" name="job" class="input_tags" placeholder="Job"></input>
<input id="exp_add_town" name="exp_add_town" type="text" name="town" class="input_tags" placeholder="Town"></input>
<select id="exp_add_country" name="exp_add_country" type="text" name="exp_country" class="input_tags" placeholder="Country">
<?php
$host="localhost";
$username="root";
$password="Sairevanth@1999";
$db="klu_alumni";
$conn=mysqli_connect($host,$username,$password,$db);
$country=mysqli_query($conn,'select * from countrys;');
while($country_list=mysqli_fetch_assoc($country))
{
	if($country_list['country_name']=="India")
	{
		echo "<option selected='selected' value=".$country_list['alpha2code'].">".$country_list['country_name']."</option>";

	}
	else
	{
	echo "<option value=".$country_list['alpha2code'].">".$country_list['country_name']."</option>";
	}
}
?>
</select>
<div style="width: 100%; height: 25px; text-align: center;">
<div style="width: 40%; height: 25px; margin: 0px 20px 0px 20px; display: inline-block;">
<p style="margin: 0px; text-align: left; font-family: arial; font-size: 13px;">Form Year</p>
</div>
<div style="width: 40%; height: 25px; margin: 0px 20px 0px 20px; display: inline-block;">
<p style="margin: 0px; text-align: left; font-family: arial; font-size: 13px;">To Year</p>
</div>
</div>
<div style="width: 100%; height: auto; text-align: center;">
<div style="width: 40%; height: auto; margin: 0px 20px 0px 20px; display: inline-block;">
<input id="exp_join_month" name="exp_join_month" placeholder="MM" list="month" style="width: 30%; margin-left: 5px; margin-right: 5px; height: auto;  padding:5px; font-family: arial; font-size: 15px; border: none; border-bottom: 1.5px solid #25293b;"></input>
<input id="exp_join_year" name="exp_join_year" placeholder="YYYY"  maxlength="4"  style="width: 60%;margin-left: 5px; margin-right: 5px; height: auto;  padding:5px;  font-family: arial; font-size: 15px; border: none; border-bottom: 1.5px solid #25293b;"></input>
</div>
<div style="width: 40%; height: auto; margin: 0px 20px 0px 20px; display: inline-block;">
<input id="exp_l_month" name="exp_l_month" placeholder="MM" list="month" style="width: 30%; margin-left: 5px; margin-right: 5px; height: auto;  padding:5px; font-family: arial; font-size: 15px; border: none; border-bottom: 1.5px solid #25293b;"></input>
<input id="exp_l_year" name="exp_l_year" placeholder="YYYY"  maxlength="4" style="width: 60%;margin-left: 5px; margin-right: 5px; height: auto;  padding:5px;  font-family: arial; font-size: 15px; border: none; border-bottom: 1.5px solid #25293b;"></input>
</div>
</div>
<button name="edit_exp_button" id="edit_exp_button"  class="edit_button">Change</button>
<button name="add_experience" id="add_experience" class="change_button">Add</button>
</form>
</div>
</div>

<?php include('js/php/update_data.php'); ?>

<div id="profile_add_popup">
<div class="profile_add">
<p onclick="close_popup('profile_add_popup')" style="position: absolute; margin: 0px; top: -35px; right: 0px; color: white; font-size: 30px; font-family: arial; cursor: pointer;">X</p>
<div style="position: relative; width: 100%; height: 80px; background-color: #25293b; margin: 0px; border-top-left-radius: 20px; border-top-right-radius: 20px;">
<div style="overflow: hidden; float: left;width: 40px; height: 40px; background-color: white; border-radius: 60px; position: relative; margin-top: 18px; margin-left: 15px;">
<img src="<?php echo $profile_pic; ?>" id="school_image" style="width: 100%; height: auto; position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px; margin: auto;" />
</div>
<p style="float: left; padding-left: 15px; padding-top: 13px; font-family: arial; color: white;">Edit Profile Details</p>
</div>
<form enctype="multipart/form-data" method="post">
<input  name="firstname_change" type="text" id="firstname" class="input_tags" placeholder="First Name"></input>
<input  name="lastname_change" type="text" id="lastname" class="input_tags" placeholder="Last Name"></input>
<select  name="gender_change" type="text" id="gender" class="input_tags">
<option>Select Gender</option>
<option value="Male">Male</option>
<option value="Female">Female</option>
<option value="others">Others</option>
</select>
<input  name="dob_change" type="text" id="dob" class="input_tags" placeholder="DOB (DD/MM/YYYY)"></input>
<input  name="fathername_change" type="text" id="fathername" class="input_tags" placeholder="Father Name"></input>
<input  name="phone_change" type="text" id="phone" class="input_tags" placeholder="Phone"></input>
<input  name="hometown_change" type="text" id="hometown" class="input_tags" placeholder="Home Town"></input>
<select name="country_change" type="text" id="country_d" class="input_tags" placeholder="Country">
<?php
$host="localhost";
$username="root";
$password="Sairevanth@1999";
$db="klu_alumni";
$conn=mysqli_connect($host,$username,$password,$db);
$country=mysqli_query($conn,'select * from countrys;');
while($country_list=mysqli_fetch_assoc($country))
{
	
	echo "<option value=".$country_list['alpha2code'].">".$country_list['country_name']."</option>";
	
}
?>
</select>
<script>
document.getElementById('firstname').value="<?php echo $name[0]; ?>";
document.getElementById('lastname').value="<?php echo $name[1]; ?>";
document.querySelector('#profile_add_popup #gender').value="<?php echo $gender; ?>";
document.getElementById('dob').value="<?php echo $dob; ?>";
document.getElementById('fathername').value="<?php echo $fathername; ?>";
document.getElementById('phone').value="<?php echo $phone; ?>";
document.getElementById('hometown').value="<?php echo $hometown; ?>";
</script>
<button name="profile_change" class="change_button">Change</button>
</form>
</div>
</div>



<?php include('js/php/get_profile_data.php'); ?>
<div class="top_nav">
<img src="imags/klu.png" style="height: 90%; width: auto; margin-left: 10px; margin-top: 3px; float: left;" />
<p style="margin: 0px; font-family: 'Lato', sans-serif; font-size: 20px; padding-top: 12px; float: left;"><strong>Alumni Association</strong></p>
<div style="width: auto; height:100%; float: right;">


<div class="icons">
<div style="width: auto; height: 100%;float: left;">
<a href="logout.php">
<div onclick="profile_content_click();" style="width: 30px;  height: 30px; cursor: pointer;  overflow: hidden; border-radius: 30px; margin-top: 10px;">
<img src="logout_c.png" style="width: 100%; height: auto;">
</div>
</a>
</div>
</div>

<div class="icons">
<div style="width: auto; height: 100%;float: left;">
<a href="main_p.php">
<div onclick="profile_content_click();" style="width: 30px;  height: 30px; cursor: pointer; background: pink; overflow: hidden; border-radius: 30px; margin-top: 10px;">
<img src="<?php echo $profile_pic; ?>" style="width: 100%; height: auto;">
</div>
</a>
</div>
</div>


<a href="news.php">
<div class="nav_icons">
<div style="width: auto; height: 100%; float: right;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; margin: 0px; padding-top: 13.5px; color: #8d8d8d;">Home</p>
</div>
<div style="width: 50px; height: 100%; float: right; position: relative;">
<img src="imags/home.png" style="width: 35px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;" />
</div>
</div>
</a>
<a href="mypost.php">
<div class="nav_icons">
<div style="width: auto; height: 100%; float: right;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; margin: 0px; padding-top: 13.5px; color: #8d8d8d;">My Posts</p>
</div>
<div style="width: 50px; height: 100%; float: right; position: relative;">
<img src="imags/my_post.png" style="width: 35px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;" />
</div>
</div>
</a>
<a href="network.php">
<div class="nav_icons">
<div style="width: auto; height: 100%; float: right;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; margin: 0px; padding-top: 13.5px; color: #8d8d8d;">My Network</p>
</div>
<div style="width: 50px; height: 100%; float: right; position: relative;">
<img src="imags/mynetwork.png" style="width: 35px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;" />
</div>
</div>
</a>




</div>
</div>
<div class="banner_main">
<div  id="banner" class="banner">
<div class="banner_change">
<div style="width: 140px; height: 100%;  float: right; margin-right: 40px;">
<form  method="post" enctype="multipart/form-data" action="?"> 
<input onchange="document.getElementById('pdfff').click();" name="profile_header_photo" type="file" style="opacity: 0.0; position: absolute; top:0; left: 0; bottom: 0; right:0; width: 100%; height:100%; cursor: pointer;">
<p style="background-color: rgba(0,0,0,0.4); cursor: pointer; color: white; font-family: arial; font-size: 13px; margin: 0px; border: 1.5px solid white; text-align: center; padding: 5px;">Change Cover Photo</p>
</input>
<button name="ufff" style="display: none;" id="pdfff">ok</button>
</form>
<script>document.getElementById('banner').style.backgroundImage="url('<?php echo $profile_header; ?>')";</script>
</div>
</div>
</div>
<div class="profile_pic" id="profile_pic">
<img src="<?php echo $profile_pic; ?>" style="width: 100%; height: auto;"/>
<div class="change_photo">
<div style="width: 100%; height: 40px; margin-top: 10px;" onclick="photo_change('open');">
<div style="width: 40%; height: 40px; float: left; ">
<img src="imags/camera_w.png" style="width: 30px; height: auto; float: right; margin-top: 7px;" />
</div>
<div style="width: 60%; height: 40px; float: left;">
<p style="color: white; font-family: arial; margin: 0px; padding-top: 10px; padding-left: 10px;">Update</p>
</div>
</div>
</div>
</div>
<div class="banner_bottom_profile" id="banner_bottom_profile">
<div class="name_diplay" id="name_diplay">
<p style="font-family: 'Noto Sans', sans-serif; margin: 0px; font-size: 25px;" id="name"><?php echo $profile_name; ?><img src="imags/edit.png" onclick="photo_change('open');" style="margin-left: 5px;cursor: pointer; width:15px; height: auto; " /></p>
<p style="font-family: 'Noto Sans', sans-serif; margin: 0px; font-size: 15px; color: #989898; padding-top: 2px;">Student, KL University</p>
<p style="font-family: 'Noto Sans', sans-serif; margin: 0px; font-size: 13px; color: #989898;padding-top: 1px;">(2020 Batch)</p>
</div>
</div>
</div>
<div class="profile_edit">
<h2 style="font-family: arial; padding-left: 50px; padding-top: 10px; color: #252525;">Profile Information <img onclick="open_popup('profile_add_popup');" src="imags/edit.png" style=" cursor: pointer; width: 15px; height: auto;" /></h2>
<div class="profile_display" style="width: 100%; height: auto; text-align: center;">
<fieldset style="margin-top: 20px; margin-left: 40px; margin-right: 40px; border: none; width: 40px; font-family: 'Roboto', sans-serif;text-align: left; display: inline-block;">
<legend>First</legend>
<input id="first" value="<?php echo $name[0]; ?>" disabled type="text" style="font-size: 16px; background-color: #ededed; border: none; padding: 7px;"></input>
</fieldset>
<fieldset style="margin-top: 20px; margin-left: 40px; margin-right: 40px; border: none; width: 40px; font-family: 'Roboto', sans-serif;text-align: left; display: inline-block;">
<legend>Last</legend>
<input id="last" value="<?php echo $name[1]; ?>" disabled type="text" style="font-size: 16px; background-color: #ededed; border: none; padding: 7px;"></input>
</fieldset>
<fieldset style="margin-top: 20px; margin-left: 40px; margin-right: 40px; border: none; width: 40px; font-family: 'Roboto', sans-serif;text-align: left; display: inline-block;">
<legend>Gender</legend>
<input id="gender" value="<?php echo $gender; ?>" disabled type="text" style="font-size: 16px; background-color: #ededed; border: none; padding: 7px;"></input>
</fieldset>
<fieldset style="margin-top: 20px; margin-left: 40px; margin-right: 40px; border: none; width: 40px; font-family: 'Roboto', sans-serif;text-align: left; display: inline-block;">
<legend>Date Of Birth</legend>
<input id="dob" value="<?php echo $dob; ?>" disabled type="text" style="font-size: 16px; background-color: #ededed; border: none; padding: 7px;"></input>
</fieldset>
<fieldset style="margin-top: 20px; margin-left: 40px; margin-right: 40px; border: none; width: 40px; font-family: 'Roboto', sans-serif;text-align: left; display: inline-block;">
<legend>Father Name</legend>
<input id="fathername" value="<?php echo $fathername; ?>" disabled type="text" style="font-size: 16px; background-color: #ededed; border: none; padding: 7px;"></input>
</fieldset>
<fieldset style="margin-top: 20px; margin-left: 40px; margin-right: 40px; border: none; width: 40px; font-family: 'Roboto', sans-serif;text-align: left; display: inline-block;">
<legend>Phone</legend>
<input id="phone" value="<?php echo $phone; ?>" disabled type="text" style="font-size: 16px; background-color: #ededed; border: none; padding: 7px;"></input>
</fieldset>
<fieldset style="margin-top: 20px; margin-left: 40px; margin-right: 40px; border: none; width: 40px; font-family: 'Roboto', sans-serif;text-align: left; display: inline-block;">
<legend>Home Town</legend>
<input id="hometown" value="<?php echo $hometown; ?>" disabled type="text" style="font-size: 16px; background-color: #ededed; border: none; padding: 7px;"></input>
</fieldset>
<fieldset style="margin-top: 20px; margin-left: 40px; margin-right: 40px; border: none; width: 40px; font-family: 'Roboto', sans-serif;text-align: left; display: inline-block;">
<legend>Country</legend>
<input id="country" value="<?php echo $country_display; ?>" disabled type="text" style="font-size: 16px; background-color: #ededed; border: none; padding: 7px;"></input>
</fieldset>
</div>
<h2 style="font-family: arial; padding-left: 50px;color: #252525; margin: 0px; padding-bottom: 10px; padding-top: 30px;">Email Addresses</h2>
<p style="font-family: arial; padding-bottom: 20px; font-size: 13px; margin: 0px; padding-left: 50px; color: #474747;">We will never share your email address or display it publicly.</p>
<div style="width: 100%; height: auto;">
<div style="height: auto; width: 100%; display:  inline-block;">
<p style="font-family: arial; float: left; height: auto; margin: 0px; padding-top: 10px; font-size: 15px; padding-left: 50px;"><?php echo $primary_email; ?></p>
<p style=" float: left; margin: 0px; margin-top: 7px; color: white; border-radius: 5px; float: left; margin-left: 20px; font-size: 10px; font-family: arial; padding: 5px; background-color:#99e265;">Verified</p>
<p style=" float: left; margin: 0px; margin-top: 7px; color: white; border-radius: 5px;  margin-left: 20px; font-size: 10px; font-family: arial; padding: 5px; background-color:#ff914d;">Primary</p>
</div>
<?php
for($i=0;$i<sizeof($secondary_email);$i++)
{
	if($secondary_email[$i]==$primary_email or $secondary_email[$i]=="")
	{
		continue;
	}
	else
	{
		echo '<div style="height: auto; width: 100%; display:  inline-block;padding-top: 15px;">
<p style="font-family: arial; float: left;   height: auto; margin: 0px; padding-top: 10px; font-size: 15px; padding-left: 50px;">'.$secondary_email[$i].'</p>
<p style=" float: left; margin: 0px; margin-top: 7px; color: white; border-radius: 5px;  margin-left: 20px; font-size: 10px; font-family: arial; padding: 5px; background-color:#99e265;">Verified</p>
<p style=" float: left; margin: 0px; margin-top: 7px; cursor: pointer; color: black; border-radius: 5px;  margin-left: 20px; font-size: 12px; font-family: arial; padding: 3px;"></p>
</div>';
	}
}
?>
</div>
<div onclick="document.getElementById('link_emails').style.display='block';" class="profile_display" style="width: 100%; height: auto;">
<div onclick="" style="cursor: pointer; width: auto; height: 50px; float: right; background-color: #ebebeb; margin-right: 20px;">
<p style="float: left;font-family: arial; background-color: #ebebeb; font-size: 36px;  margin: 0px; padding-top: 7px; padding-left: 10px; padding-right: 10px;">+</p>
<p style="float: right; margin: 0px; font-family: arial; font-size: 15px; padding-left: 10px; padding-right: 10px; padding-top: 18px;">Add Email address</p>
</div>
</div>

<h2 style="font-family: arial; padding-left: 50px; padding-top: 10px; color: #252525;">Geo Location</h2>
<div style="width: 100%; height: auto; text-align: center;">
<fieldset style="margin-top: 20px; margin-left: 40px; margin-right: 40px; border: none; width: 40px; font-family: 'Roboto', sans-serif;text-align: left; display: inline-block;">
<legend>Latitude</legend>
<input id="latitude" value="<?php echo $geo_location[0]; ?>" disabled type="text" style="font-size: 16px; background-color: #ededed; border: none; padding: 7px;"></input>
</fieldset>
<fieldset style="margin-top: 20px; margin-left: 40px; margin-right: 40px; border: none; width: 40px; font-family: 'Roboto', sans-serif;text-align: left; display: inline-block;">
<legend>Longitude</legend>
<input id="longitude" value="<?php echo $geo_location[1]; ?>" disabled type="text" style="font-size: 16px; background-color: #ededed; border: none; padding: 7px;"></input>
</fieldset>
<fieldset style="margin-top: 20px; margin-left: 40px; margin-right: 40px; border: none; width: 40px; font-family: 'Roboto', sans-serif;text-align: left; display: inline-block;">
<legend>Town</legend>
<input  value="<?php echo $current_town; ?>" id="current_town"  type="text" style="font-size: 16px; background-color: #ededed; border: none; padding: 7px;"></input>
</fieldset>
<fieldset style="margin-top: 20px; margin-left: 40px; margin-right: 40px; border: none; width: 40px; font-family: 'Roboto', sans-serif;text-align: left; display: inline-block;">
<legend>Country</legend>
<input  value="<?php echo $current_country; ?>" id="current_country" type="text" style="font-size: 16px; background-color: #ededed; border: none; padding: 7px;"></input>
</fieldset>
</div>
<div style="width: 100%; height: auto; text-align: right;">
<button class="update_geo" onclick="getLocation();">Update</button>
</div>
<h2 style="font-family: arial; padding-left: 50px; padding-top: 10px; color: #252525;">Personal Information</h2>
<div style="width: 100%; height: auto; text-align: center;">
<fieldset style="margin-top: 20px; margin-left: 40px; margin-right: 40px; border: none; width: 40px; font-family: 'Roboto', sans-serif;text-align: left; display: inline-block;">
<legend>Current working</legend>
<input  value="<?php echo $current_job; ?>" id="current_working" type="text" style="font-size: 16px; background-color: #ededed; border: none; padding: 7px;"></input>
</fieldset>
<fieldset style="margin-top: 20px; margin-left: 40px; margin-right: 40px; border: none; width: 40px; font-family: 'Roboto', sans-serif;text-align: left; display: inline-block;">
<legend>Organization</legend>
<input  value="<?php echo $current_company; ?>" id="current_organization" type="text" style="font-size: 16px; background-color: #ededed; border: none; padding: 7px;"></input>
</fieldset>
<fieldset style="margin-top: 20px; margin-left: 40px; margin-right: 40px; border: none; width: 40px; font-family: 'Roboto', sans-serif;text-align: left; display: inline-block;">
<legend>Higher Education</legend>
<input  value="<?php echo $higher_education; ?>" id="higher_education" type="text" style="font-size: 16px; background-color: #ededed; border: none; padding: 7px;"></input>
</fieldset>
<fieldset style="margin-top: 20px; margin-left: 40px; margin-right: 40px; border: none; width: 40px; font-family: 'Roboto', sans-serif;text-align: left; display: inline-block;">
<legend>University Name</legend>
<input  value="<?php echo $higher_education_completed; ?>" id="university_name" type="text" style="font-size: 16px; background-color: #ededed; border: none; padding: 7px;"></input>
</fieldset>
</div>
<div style="width: 100%; height: auto; text-align: right;">
<button class="update_geo" onclick="personal_update();">Update</button>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);

  } 
}

function showPosition(position) {
	var lat=position.coords.latitude;
	var lon=position.coords.longitude;
	var current_town=document.getElementById('current_town').value;
	var current_country=document.getElementById('current_country').value;

	$.ajax({
	 type: 'post',
	 url: 'js/php/geolocation.php',
	 data : {lat:lat, lon: lon,current_town:current_town,current_country:current_country},
	 success: function(code){
	 document.getElementById('latitude').value=lat;
	document.getElementById('longitude').value=lon;
	location.reload();
	 }
	 });
}
function personal_update()
{
	var current_working=document.getElementById('current_working').value;
	var current_organization=document.getElementById('current_organization').value;
	var higher_education=document.getElementById('higher_education').value;
	var university_name=document.getElementById('university_name').value;
		$.ajax({
	 type: 'post',
	 url: 'js/php/personal_update.php',
	 data : {current_working:current_working, current_organization: current_organization,higher_education:higher_education,university_name:university_name},
	 success: function(code){
	location.reload();
	 }
	 });
}

</script>

<h2 style="font-family: arial; padding-left: 50px;color: #252525; margin: 0px; padding-bottom: 10px; padding-top: 30px;">Education</h2>
<div style="width: 100%; height: 50px;">
<div onclick="open_popup('education_add_popup');" style="cursor: pointer; width: auto; height: 50px; float: right; background-color: #ebebeb; margin-right: 20px;">
<p style="float: left;font-family: arial; background-color: #ebebeb; font-size: 36px;  margin: 0px; padding-top: 7px; padding-left: 10px; padding-right: 10px;">+</p>
<p style="float: right; margin: 0px; font-family: arial; font-size: 15px; padding-left: 10px; padding-right: 10px; padding-top: 18px;">Add School</p>
</div>
</div>

<div style="width: 100%; height: auto;">




<?php
require('js/php/conn.php');
$c=0;
for($i=0;$i<sizeof($education_previous);$i=$i+8)
{
$check=mysqli_query($conn,"select * from organization where organization_name='$education_previous[$i]';");
if(mysqli_num_rows($check)==1)
{
$check=mysqli_fetch_assoc($check);
$temp=$check['code'];
echo '<div style="display: inline-block; margin-top: 20px; width: 500px;  height: 130px; border-radius: 10px;  margin-left: 40px; margin-right: 40px; box-shadow: 0px 0px 2px 0px #d2d2d2;">
<div style="float: left; width: 120px; height: 100%;  text-align: center; position: relative; ">
<img src="organization_images/'.$temp.'.png" style="width: 100px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;" />
</div>
<div style=" width: 380px; height: 100%; float: left; position: relative; text-align: left;">
<p id="edu_get_college_name" style=" width: 100%; font-family: arial; margin: 0px; font-size: 18px; padding-top: 10px; padding-left: 10px; padding-top: 20px;">'.$education_previous[$i].'</p>
<p id="edu_get_course_stream" style="width: 100%; font-size: 13px; font-family: '.'Montserrat'.', sans-serif; margin: 0px; padding-top: 10px; padding-left: 10px;">'.$education_previous[$i+1].' - '.$education_previous[$i+2].'</p>
<p id="edu_get_year_month" style="width: 100%; font-size: 13px; font-family: '.'Montserrat'.', sans-serif; margin: 0px; padding-top: 5px; padding-left: 10px;">'.$education_previous[$i+4].' '.$education_previous[$i+5].' - '.$education_previous[$i+6].' '.$education_previous[$i+7].'</p>
<p id="edu_get_location" style="width: 100%; font-size: 11px; font-family: '.'Montserrat'.', sans-serif; margin: 0px; padding-top: 10px; padding-left: 10px;">'.$education_previous[$i+3].'</p>
<form action='."#".' method="post">
<button class="edit" value='.$c.' name="edit_education">Edit</button>
</form>
<p onclick='."remove_ex('edu',$i);".' class="remove">Remove</p>
</div>
</div>';
$c++;
}
else
{
echo '<div style="display: inline-block; margin-top: 20px; width: 500px;  height: 130px; border-radius: 10px;  margin-left: 40px; margin-right: 40px; box-shadow: 0px 0px 2px 0px #d2d2d2;">
<div style="float: left; width: 120px; height: 100%;  text-align: center; position: relative; ">
<img src="company_p.png" style="width: 100px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;" />
</div>
<div style="width: 380px; height: 100%; float: left; position: relative; text-align: left;">
<p id="edu_get_college_name" style=" width: 100%; font-family: arial; margin: 0px; font-size: 18px; padding-top: 10px; padding-left: 10px; padding-top: 20px;">'.$education_previous[$i].'</p>
<p id="edu_get_course_stream" style="width: 100%; font-size: 13px; font-family: arial; margin: 0px; padding-top: 10px; padding-left: 10px;">'.$education_previous[$i+1].' - '.$education_previous[$i+2].'</p>
<p id="edu_get_year_month" style="width: 100%; font-size: 13px; font-family: arial; margin: 0px; padding-top: 5px; padding-left: 10px;">'.$education_previous[$i+4].' '.$education_previous[$i+5].' - '.$education_previous[$i+6].' '.$education_previous[$i+7].'</p>
<p id="edu_get_location" style="width: 100%; font-size: 11px; font-family: arial; margin: 0px; padding-top: 10px; padding-left: 10px;">'.$education_previous[$i+3].'</p>
<form action='."#".' method="post">
<button class="edit" value='.$c.' name="edit_education">Edit</button>
</form>
<p onclick='."'remove_ex('edu',$i);'".' class="remove">Remove</p>
</div>
</div>';
$c++;
}
}

?>
</div>

<h2 style="font-family: arial; padding-left: 50px;color: #252525; margin: 0px; padding-bottom: 10px; padding-top: 30px;">Experience</h2>
<div style="width: 100%; height: 50px;">
<div onclick="open_popup('experience_add_popup');"  style="cursor: pointer; width: auto; height: 50px; float: right; background-color: #ebebeb; margin-right: 20px;">
<p style="float: left;font-family: arial; background-color: #ebebeb; font-size: 36px;  margin: 0px; padding-top: 7px; padding-left: 10px; padding-right: 10px;">+</p>
<p style="float: right; margin: 0px; font-family: arial; font-size: 15px; padding-left: 10px; padding-right: 10px; padding-top: 18px;">Add Experience</p>
</div>
</div>
<div style="width: 100%; height: auto;">


<?php
require('js/php/conn.php');
$c=0;
for($i=0;$i<sizeof($company_previous);$i=$i+8)
{
$temp=$company_previous[$i+1];
$check=mysqli_query($conn,"select * from organization where organization_name='$temp';");
if(mysqli_num_rows($check)==1)
{
$check=mysqli_fetch_assoc($check);
$temp=$check['code'];
echo '<div style="display: inline-block; margin-top: 20px; width: 500px;  height: 130px; border-radius: 10px;  margin-left: 40px; margin-right: 40px; box-shadow: 0px 0px 2px 0px #d2d2d2;">
<div style="float: left; width: 120px; height: 100%;  text-align: center; position: relative; ">
<img src="organization_images/'.$temp.'.png" style="width: 100px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;" />
</div>
<div style=" width: 380px; height: 100%; float: left; position: relative; text-align: left;">
<p style=" width: 100%; font-family: arial; margin: 0px; font-size: 18px; padding-top: 10px; padding-left: 10px; padding-top: 20px;">'.$company_previous[$i+1].'</p>
<p style="width: 100%; font-size: 13px; font-family: '.'Montserrat'.', sans-serif; margin: 0px; padding-top: 10px; padding-left: 10px;">'.$company_previous[$i].'</p>
<p style="width: 100%; font-size: 13px; font-family: '.'Montserrat'.', sans-serif; margin: 0px; padding-top: 5px; padding-left: 10px;">'.$company_previous[$i+2].' '.$company_previous[$i+3].' - '.$company_previous[$i+4].' '.$company_previous[$i+5].'</p>
<p style="width: 100%; font-size: 11px; font-family: '.'Montserrat'.', sans-serif; margin: 0px; padding-top: 10px; padding-left: 10px;">'.$company_previous[$i+6].','.$company_previous[$i+7].'</p>
<form action="edit.php" method="post">
<button class="edit" value="'.$c.'" name="edit_experience">Edit</button>
</form>
<p onclick="remove_ex('."'exp'".','.$i.');" class="remove">Remove</p>
</div>
</div>';
}
else
{
echo '<div style="display: inline-block; margin-top: 20px; width: 500px;  height: 130px; border-radius: 10px;  margin-left: 40px; margin-right: 40px; box-shadow: 0px 0px 2px 0px #d2d2d2;">
<div style="float: left; width: 120px; height: 100%;  text-align: center; position: relative; ">
<img src="company_p.png" style="width: 100px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;" />
</div>
<div style=" width: 380px; height: 100%; float: left; position: relative; text-align: left;">
<p style=" width: 100%; font-family: arial; margin: 0px; font-size: 18px; padding-top: 10px; padding-left: 10px; padding-top: 20px;">'.$company_previous[$i+1].'</p>
<p style="width: 100%; font-size: 13px; font-family: '.'Montserrat'.', sans-serif; margin: 0px; padding-top: 10px; padding-left: 10px;">'.$company_previous[$i].'</p>
<p style="width: 100%; font-size: 13px; font-family: '.'Montserrat'.', sans-serif; margin: 0px; padding-top: 5px; padding-left: 10px;">'.$company_previous[$i+2].' '.$company_previous[$i+3].' - '.$company_previous[$i+4].' '.$company_previous[$i+5].'</p>
<p style="width: 100%; font-size: 11px; font-family: '.'Montserrat'.', sans-serif; margin: 0px; padding-top: 10px; padding-left: 10px;">'.$company_previous[$i+6].','.$company_previous[$i+7].'</p>
<form action="edit.php" method="post">
<button class="edit" value="'.$c.'" name="edit_experience">Edit</button>
</form>
<p onclick="remove_ex('."'exp'".','.$i.');" class="remove">Remove</p>
</div>
</div>';
}
$c++;
}
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
function remove_ex(a,b)
{
	$.ajax({
	 type: 'post',
	 url: 'js/php/delete_experience_school.php',
	 data : {a:a, b: b},
	 success: function(code){
	 }
	 });
}

</script>
</div>


<h2 style="font-family: arial; padding-left: 50px;color: #252525; margin: 0px; padding-bottom: 10px; padding-top: 30px;">Skills</h2>
<div style="width: 100%; height: 50px;">
<div onclick="open_popup('skills_add_popup');" style="cursor: pointer; width: auto; height: 50px; float: right; background-color: #ebebeb; margin-right: 20px;">
<p style="float: left;font-family: arial; background-color: #ebebeb; font-size: 36px;  margin: 0px; padding-top: 7px; padding-left: 10px; padding-right: 10px;">+</p>
<p style="float: right; margin: 0px; font-family: arial; font-size: 15px; padding-left: 10px; padding-right: 10px; padding-top: 18px;">Add Skills</p>
</div>
</div>
<div style="width: 100%; height: auto; padding-bottom: 30px; padding-left: 30px;">
<?php
for($i=0;$i<sizeof($skills);$i++)
{
	echo '<div style="height: auto; background-color: #ebebeb; display: inline-block; margin-right: 10px;">
<form action="#" method="post">
<p style="font-family: arial;  margin: 0px; padding: 8px;  background-color: #ebebeb; float: left;">'.$skills[$i].'</p>
<button name="remove_skill" value="'.$skills[$i].'" style="font-family: arial; margin:0px;   padding: 9px; background-color: #ebebeb;float: left; cursor: pointer; border: none;">X</button>
</div>
</form>';
}
?>
</div>

<div id="edit_social_media" style="width:100%; display: none; height: 100%; background-color: rgba(0,0,0,0.5); z-index: 2; position: fixed;margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;">
<div style="width: 400px; height: 450px; background-color: white; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;">
<p style="font-family: arial; padding-left: 10px; font-size: 18px;">Social Media account</p>
<p onclick="document.getElementById('edit_social_media').style.display='none';" style="font-family: arial; padding-left: 10px; font-size: 28px; cursor: pointer; color: white; position: absolute; top: -65px; right: 0px;">X</p>
<div style="width: 95%; height: 2px; background-color: #dfdfdf; margin: auto;">
</div>
<fieldset style="border: none;">
<legend><a class="fa fa-facebook" style="float: left;"></a></legend>
<input id="facebook_url" value="<?php echo $social_accounts[0]; ?>" style="width: 95%; font-family: arial; margin-top: 3px; font-size: 15px; height: auto; border: none; border-bottom: 1px solid black;" placeholder="Account URL"></input>
</fieldset>
<fieldset style="border: none;">
<legend><a class="fa fa-twitter" style="float: left;"></a></legend>
<input id="twitter_url" value="<?php echo $social_accounts[2]; ?>" style="width: 95%; font-family: arial; margin-top: 3px; font-size: 15px; height: auto; border: none; border-bottom: 1px solid black;" placeholder="Account URL"></input>
</fieldset>
<fieldset style="border: none;">
<legend><a class="fa fa-linkedin" style="float: left;"></a></legend>
<input id="linkedin_url" value="<?php echo $social_accounts[1]; ?>" style="width: 95%; font-family: arial; margin-top: 3px; font-size: 15px; height: auto; border: none; border-bottom: 1px solid black;" placeholder="Account URL"></input>
</fieldset>
<fieldset style="border: none;">
<legend><a class="fa fa-instagram" style="float: left;"></a></legend>
<input id="instagram_url" value="<?php echo $social_accounts[3]; ?>" style="width: 95%; font-family: arial; margin-top: 3px; font-size: 15px; height: auto; border: none; border-bottom: 1px solid black;" placeholder="Account URL"></input>
</fieldset>
<button onclick="update_social_url();" style="cursor: pointer; border-radius: 5px; background-color: #0d496d; border: 1px solid #0d496d; color: white; padding:3px 10px 3px 10px; position: absolute; bottom: 10px; right: 10px;">Update</button>
</div>
</div>
<script>
function update_social_url()
{
	var instagram=document.getElementById('instagram_url').value;
	var facebook=document.getElementById('facebook_url').value;
	var twitter=document.getElementById('twitter_url').value;
	var linkedin=document.getElementById('linkedin_url').value;
	$.ajax({
	type: 'post',
	 url: 'js/php/social_media_account_update.php',
	 data : {instagram:instagram, facebook:facebook, twitter:twitter,linkedin:linkedin},
	 success: function(code){
		 location.reload();
	 }
	 });
}
</script>

<h2 style="font-family: arial; padding-left: 50px;color: #252525; margin: 0px; padding-bottom: 20px; padding-top: 30px;">Social Media Account <img src="imags/edit.png" onclick="document.getElementById('edit_social_media').style.display='block';" style="width: 18px; margin-left: 5px; cursor: pointer; height: auto;"/></h2>

<div style="width: 100%; height: auto;">
<div style="width: 240px; height: 45px; margin-left: 50px; display: inline-block;">
<a href="<?php echo $social_accounts[0]; ?>" class="fa fa-facebook" style="float: left;"></a>
<p style="font-family: arial; font-size: 13px; margin: 0px; float: left; padding-top: 15px; padding-left: 10px;">Account is <?php if($social_accounts[0]!=null){ echo'Connected';}else{ echo'Not Connected'; } ?></p>
</div>
<div style="width: 240px; height: 45px; margin-left: 50px; display: inline-block;">
<a href="<?php echo $social_accounts[2]; ?>" class="fa fa-twitter" style="float: left;"></a>
<p style="font-family: arial; font-size: 13px; margin: 0px; float: left; padding-top: 15px; padding-left: 10px;">Account is <?php if($social_accounts[2]!=null){ echo'Connected';}else{ echo'Not Connected'; } ?></p>
</div>
<div style="width: 240px; height: 45px; margin-left: 50px; display: inline-block;">
<a href="<?php echo $social_accounts[1]; ?>" class="fa fa-linkedin" style="float: left;"></a>
<p style="font-family: arial; font-size: 13px; margin: 0px; float: left; padding-top: 15px; padding-left: 10px;">Account is <?php if($social_accounts[1]!=null){ echo'Connected';}else{ echo'Not Connected'; } ?></p>
</div>
<div style="width: 240px; height: 45px; margin-left: 50px; display: inline-block;">
<a href="<?php echo $social_accounts[3];?>" class="fa fa-instagram" style="float: left;"></a>
<p style="font-family: arial; font-size: 13px; margin: 0px; float: left; padding-top: 15px; padding-left: 10px;">Account is <?php if($social_accounts[3]!=null){ echo'Connected';}else{ echo'Not Connected'; } ?></p>
</div>
</div>
</div>
</div>
</body>
</html>
	
