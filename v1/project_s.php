﻿<?php

?>
<html>
<head>
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<link rel="stylesheet" href="css/project_s.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="js/index_dropdown_menu.js"></script>
</head>
<body>
<div class="icon_dis" style="cursor: pointer;">
<div class="main_icon" onclick="window.location='index.php'">
<img src="icons/klu.png" />
<strong><p style="text-decoration: none;">Alumni Association</p></strong>
</div>
</div>
<div class="dropdownmenu">
 <nav>
      <div class="nav-mobile"><a id="nav-toggle" href="#!"><span></span></a></div>
      <ul class="nav-list" >
        <li >
          <a href="#" style="text-decoration: none;">Projects</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="#" style="text-decoration: none;">Student Projects</a>
            </li>
            <li>
              <a href="#" style="text-decoration: none;">Industrial Projects</a>
            </li>
            <li>
              <a href="#" style="text-decoration: none;">Project Expo</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="#" style="text-decoration: none;">Carrers</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="careers.php" style="text-decoration: none;">Internship</a>
            </li>
            <li>
              <a href="careers.php" style="text-decoration: none;">Jobs</a>
            </li>
            <li>
              <a href="careers.php" style="text-decoration: none;">Mentorship</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="#!" style="text-decoration: none;">Contribute</a>
        </li>
        <li>
          <a href="#!" style="text-decoration: none;">Events</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="events.php" style="text-decoration: none;">Alumni Event Calender</a>
            </li>
			<li><a href="events.php" style="text-decoration: none;">All Events</a></li>
			<li>
			  <a href="https://kluniversity.in/site/acadcal.htm" style="text-decoration: none;">Academic Calender</a></li>
          </ul>
        </li>
		 <li>
              <a href="#" style="text-decoration: none;">Alumni Stories</a>
            </li>
            <li>
              <a href="gallery.php" style="text-decoration: none;">Gallery</a>
            </li>
         <li>
              <a href="#!" style="text-decoration: none;">News & Updates</a>
            </li>
        <li>
          <a href="#!" style="text-decoration: none;">About us</a>
        </li>
      </ul>
    </nav>
</div>
<div class="">
</div>
</body>
</html>