<html>
<head>
<?php
session_start();
if($_SESSION['alu_auth']==null)
{
	header("location:index.php");
}
?>
<?php require('js/php/get_profile_data.php'); 
$session=$_SESSION['alu_auth'];
$che=mysqli_query($conn,"select * from user_security where hash_key='$session';");
$che=mysqli_fetch_assoc($che);
$session=$che['email'];
date_default_timezone_set('Asia/Kolkata');
$date=date('Y-m-d H:i:s');
mysqli_query($conn,"update profile set active_status='$date' where email='$session';");

?>
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
<style>
body
{
	margin: 0px;
	position: relative;
	background-color:  #ededed;
}
.top_nav
{
	width: 100%;
	height: 50px;
	max-width: 1800px;
	margin: auto;
	text-align: center;
	background: white;
	box-shadow: 0px 1px 5px #cccccc;
}
*:focus
{
	outline: none;
}
.icons
{
	width: 40px;
	height: 100%;
	float: right;
}
.icons:hover > .dropdown_profile
{
	display: block;
}
.dropdown_profile
{
	position: relative; 
	z-index: 1;
	float: right; 
	text-align: left;
	height: auto;
	margin: 0px; 
	display: none;
	cursor:pointer;	
	background-color: white;
}
.icons:hover > .dropdown_profile
{
	display: block;
}
.view_profile
{
	width: 95%; 
	font-size: 15px; 
	color: black;
	margin: 0px;
	text-decoration: none;
	margin: auto; 
	padding: 3px 0px 3px 0px;
	text-align: center; 
	font-family: arial;
	border-radius: 5px;
}
.view_profile:hover
{
	background-color: #f1f1f1;
}
.nav_icons
{
	width: auto;
	height: 100%;
	float: left;
	padding-right: 10px;
	cursor: pointer;
}
.nav_icons:hover
{
	background-color: #f4f4f4;
}
#nav_unread_message_status
{
	width: 15px;
	height: 15px;
	background-color: #ff5757;
	position: absolute;
	top: 8px; 
	right: 5px;
	border-radius: 20px;
}
.statics
{
	width: 99%;
	max-width: 1800px;
	text-align: center;
	height: auto;
	background-color: white;
	margin: auto;
	margin-top: 5px;
	border-radius: 10px;
}
.statics_profile_pic
{
	width: 180px;
	height: 180px;
	top: 10px;
	display: inline-block;
	overflow: hidden;
	margin-left: 20px;
}
.statics_profile_desc
{
	width: 300px;
	height: 200px;
	text-align: left;
	display: inline-block;
}
.static_followers
{
	width: 150px;
	height: 200px;
	margin: 0px 10px 0px 10px;
	display: inline-block;
}
.static_following
{
	width: 150px;
	height: 200px;
	margin: 0px 10px 0px 10px;
	display: inline-block;
}
.static_request
{
	width: 150px;
	height: 200px;
	margin: 0px 10px 0px 10px;
	display: inline-block;
}
.request_accept_box
{
	width: 300px;
	height: 200px;
	margin: 0px 10px 0px 40px;
	display: inline-block;
}
.followerblock
{
	width: 99%;
	max-width: 1800px;
	height: auto;
	margin: auto;
	border-radius: 10px;
	margin-top: 5px;
	text-align: center;
	background-color: white;
}
.followerlist
{
	width: 600px;
	height: 350px;
	display: inline-block;
	margin: 20px 30px 20px 30px;
	
	background-color: red;
}
.followinglist
{
	width: 600px;
	height: 350px;
	display: inline-block;
	margin: 20px 30px 20px 30px;
	background-color: red;
}
.seeall
{
	font-family: 'Noto Sans', sans-serif; 
	color: #224162; 
	background-color:white; 
	font-size: 15px;
	padding: 5px 10px 5px 10px;
	border: 1px solid #224162;
	cursor: pointer;
	transition: 0.3s;
	margin-right: 10px;
	margin-top: 5px;
	border-radius: 10px;
}
.seeall:hover
{
	background-color: #224162;
	color: white;
	transition: 0.3s;
}
.box_button
{
	background-color: #fbc84f;
	font-family: 'Noto Sans', sans-serif; 
	font-size: 15px;
	padding: 6px 13px 6px 13px;
	margin-top: 12px;
	border-radius:10px;
	color: white;
	border: 1px solid #fbc84f;
	cursor: pointer;
}
.box_button:hover
{
	background-color: white;
	color: #fbc84f;
}
.follower_box_show
{
  width: 100%;
  height: 60px;
  background-color: white;	
  border-bottom: 1px solid #d9d9d9;
  cursor: pointer;
}
.follower_box_show:hover
{
	background-color: #f8f8f8;
}
#follw_box_show_pop_up
{
	position: absolute; 
	width: 100%; 
	height: 100%;
	display: none;
	background-color: rgba(0,0,0,0.4);
	z-index: 10;
}
</style>
</head>
<body>

<div id="follw_box_show_pop_up">
<div style="width: 100%; height: 100%; position: relative;">
<div style="width: 500px; height: 500px;  background-color: white; position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px; margin: auto;">
<p onclick="document.getElementById('follw_box_show_pop_up').style.display='none';" style="color: white; font-family: arial; cursor: pointer;	 font-size: 18px; position: absolute; margin: 0px; top: -30px; right: 0px;">X</p>
<div style="width: 100%; height: 50px; background-image: url('imags/follow_popup.jpg');">
<p style="margin: 0px; color: white; font-family: 'Noto Sans', sans-serif; padding: 14px 0px 0px 10px; font-size: 15px;" id="show_display_status_show">Followers</p>
</div>
<div style="width: 100%; height: 450px; overflow-y: scroll;" id="main_follow_status_popup_box">
</div>
</div>
</div>
</div>
<div class="top_nav">
<img src="imags/klu.png" style="height: 90%; width: auto; margin-left: 10px; margin-top: 3px; float: left;">
<p style="margin: 0px; font-family: 'Lato', sans-serif; font-size: 20px; padding-top: 12px; float: left;"><strong>Alumni Association</strong></p>
<div style="width: auto; height:100%; float: right;">
<div class="icons">
<div style="width: auto; height: 100%;float: left;">
<a href="logout.php">
<div onclick="profile_content_click();" style="width: 30px;  height: 30px; cursor: pointer;  overflow: hidden; border-radius: 30px; margin-top: 10px;">
<img src="logout_c.png" style="width: 100%; height: auto;">
</div>
</a>
</div>
</div>
<div class="icons">
<div style="width: auto; height: 100%;float: left;">
<a href="main_p.php">
<div onclick="profile_content_click();" style="width: 30px;  height: 30px; cursor: pointer; background: pink; overflow: hidden; border-radius: 30px; margin-top: 10px;">
<img src="<?php echo $profile_pic; ?>" style="width: 100%; height: auto;">
<ul>
<li>Profile</li>
</ul>
</div>
</a>
</div>
<div class="dropdown_profile">
<div style="width: 250px; height: 60px; background-color: white;">
<div style="width: 50px; margin: 5px; height: 50px; border-radius: 60px; background-color: black; overflow: hidden; float:left;">
<img src="<?php echo $profile_pic; ?>" style="width: 100%; height: auto;">
</div>
<div style="height: 50px; width: auto; float: left;">
<p style=" margin: 0px; padding-left: 5px; padding-top: 10px; font-family: arial; font-size: 15px;"><?php echo $profile_name; ?></p>
</div>
</div>
<a style="text-decoration: none;" href="edit.php"><p class="view_profile">View Profile</p></a>
<div style="width: 100%; height: 0.5px; background-color: #bebebe; margin-top: 5px;">
</div>
<p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; padding: 7px 0px 2px 7px; font-size: 15px; ">News</p>
<p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; padding: 7px 0px 2px 7px; font-size: 15px; ">Job postings</p>
<p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; padding: 7px 0px 2px 7px; font-size: 15px; ">Change Password</p>
<p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; padding: 7px 0px 2px 7px; font-size: 15px;">Privacy policy</p>
<p style="margin: 0px; color: #595959; font-family: 'Roboto', sans-serif; padding: 7px 0px 2px 7px; font-size: 15px;">Settings</p>

</div>
</div>
</div>
<div style="width: auto; height: 100%; float: right; margin-right: 30px;">
<a href="news.php">
<div class="nav_icons">
<div style="width: auto; height: 100%; float: right;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; margin: 0px; padding-top: 13.5px; color: #8d8d8d;">Home</p>
</div>
<div style="width: 50px; height: 100%; float: right; position: relative;">
<img src="imags/home.png" style="width: 35px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;">
</div>
</div>
</a>
<a href="mypost.php">
<div class="nav_icons">
<div style="width: auto; height: 100%; float: right;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; margin: 0px; padding-top: 13.5px; color: #8d8d8d;">My Posts</p>
</div>
<div style="width: 50px; height: 100%; float: right; position: relative;">
<img src="imags/my_post.png" style="width: 35px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;">
</div>
</div>
</a>
<div class="nav_icons">
<div style="width: auto; height: 100%; float: right;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 14px; margin: 0px; padding-top: 13.5px; color: #8d8d8d;">My Network</p>
</div>
<div style="width: 50px; height: 100%; float: right; position: relative;">
<img src="imags/mynetwork.png" style="width: 35px; height: auto; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;">
<div id="nav_unread_message_status">
<p id="nav_unread_message_status_t" style="margin: 0px; text-align: center; font-size: 9px; color: white; font-family: arial; padding-top: 2px;"><b></b></p>
</div>
</div>
</div>

</div>
</div>

<div class="statics">
<div class="statics_profile_pic">
<img src="<?php echo $profile_pic; ?>" style="width: 100%; height: auto;" />
</div>
<div class="statics_profile_desc" style="position: relative;">
<div style="width: 100%; height: 100%; position: absolute; top: 0px;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 18px; padding-top: 10px; padding-left: 10px;"><?php echo $profile_name; ?></p>
<p style="font-family: 'Noto Sans', sans-serif; font-size: 12px; padding-left: 10px; margin: 0px;"><?php echo "Working at  &nbsp;&nbsp;<strong>".$current_job.', '.$current_company.'</strong>'; ?></p>
<p style="font-family: 'Noto Sans', sans-serif; font-size: 12px; padding-left: 10px; padding-top: 1px; margin: 0px;"><?php echo "Location &nbsp;&nbsp;<strong>".$current_town.', '.$current_country.'</strong>'; ?></p>
</div>
</div>
<div class="static_followers" style="position: relative;">
<div style="width: 100%; height: 100%; position: absolute; top: 0px;">
<div style="width: 150px; height: 100px; position: relative;">
<img src="imags/follower.png" style="width: 80px; height: auto; position: absolute; bottom: 0px; left: 0px; right: 0px; margin: auto;" />
</div>
<?php
require('js/php/conn.php');
$c=mysqli_query($conn,"select follower,request,following from profile where email='$session';");
$c=mysqli_fetch_assoc($c);
$follower_c=$c['follower'];
$follower_c=check_count($follower_c);
$request_c=$c['request'];
$request_c=check_count($request_c);
$following_c=$c['following'];
$following_c=check_count($following_c);
function check_count($request)
{
	$request=explode(',',$request);
	$request_count=sizeof($request);
	if($request[0]=='')
	{
		return 0;
	}
	return $request_count;
}

?>
<div style="width:150px; height: 50px;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 13px; margin: 0px; padding-top: 10px; color: #505050;">Followers</p>
<p style="font-family: 'Noto Sans', sans-serif; font-size: 13px; margin: 0px; padding-top: 2px;"><strong id="follower_c"><?php echo $follower_c; ?></strong></p>
</div>
</div>
</div>
<div class="static_following" style="position: relative;">
<div style="width: 100%; height: 100%; position: absolute; top: 0px;">
<div style="width: 150px; height: 100px; position: relative;">
<img src="imags/following.png" style="width: 80px; height: auto; position: absolute; bottom: 0px; left: 0px; right: 0px; margin: auto;" />
</div>
<div style="width:150px; height: 50px;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 13px; margin: 0px; padding-top: 10px; color: #505050;">Following</p>
<p style="font-family: 'Noto Sans', sans-serif; font-size: 13px; margin: 0px; padding-top: 2px;"><strong id="following_c"><?php echo $following_c; ?></strong></p>
</div>
</div>
</div>
<div class="static_request" style="position: relative;">
<div style="width: 100%; height: 100%; position: absolute; top: 0px;">
<div style="width: 150px; height: 100px; position: relative;">
<img src="imags/request.png" style="width: 80px; height: auto; position: absolute; bottom: 0px; left: 0px; right: 0px; margin: auto;" />
</div>
<div style="width:150px; height: 50px;">
<p style="font-family: 'Noto Sans', sans-serif; font-size: 13px; margin: 0px; padding-top: 10px; color: #505050;">Requests</p>
<p style="font-family: 'Noto Sans', sans-serif; font-size: 13px; margin: 0px; padding-top: 2px;"><strong id="request_c"><?php echo $request_c; ?></strong></p>
</div>
</div>
</div>
<div class="request_accept_box" style="position: relative;">
<div style="width: 100%; height: 90%; position: absolute; overflow: hidden;  border-radius: 10px; box-shadow: 0px 0px 5px 0px #d9d9d9; top: 10px;">
<div style="width: 100%; height: 100%; text-align: center;" id="pending_requests">
<img src="imags/pending.png" style="width: 100px; margin-top: 10px; height: auto;" />
<p style="font-family: 'Noto Sans', sans-serif; font-size: 13px;">No Pending Requests</p>
</div>
<div style="width: 100%; height: 100%; background-color: white;" id="approve_request">
<?php 
require('js/php/conn.php');
$request=explode(",",$request);

	for($i=0;$i<sizeof($request);$i++)
{
	$temp=$request[$i];
	$c=mysqli_query($conn,"select profile_name,job,company,town,personal.country,profile_pic from personal inner join profile where personal.email=profile.email and profile.code='$temp';");
	$c=mysqli_fetch_assoc($c);
	$profile_pic=$c['profile_pic'];
	$profile_name=$c['profile_name'];
	$line1=$c['job'].','.$c['company'];
	$line2=$c['town'].','.$c['country'];
	echo '
<div style="width: 100%; height: 100%; background-color: white;" id="approve_request'.$i.'">
<p style="font-family: '."'Noto Sans'".', sans-serif; font-size: 14px; margin: 0px; padding-top: 10px;">Follow Requests</p>
<div style="width: 100%; height: 50px; margin-top: 20px;">
<div style="width: 50px; height: 50px; overflow: hidden; margin-left: 40px; float: left;">
<img src="'.$profile_pic.'" style="width: 100%; height: auto;" />
</div>
<div style="width: auto; height: 50px; float: left;">
<p style="margin: 0px; padding-left: 15px; text-align: left; font-family: '."'Noto Sans'".', sans-serif; font-size: 14px;">'.$profile_name.'</p>
<p style="margin: 0px; padding-left: 15px; padding-top:3px; font-family: '."'Noto Sans'".', sans-serif; font-size: 10px; text-align: left; ">'.$line1.'</p>
<p style="margin: 0px; padding-left: 15px; padding-top:0px; font-family: '."'Noto Sans'".', sans-serif; font-size: 10px; text-align: left; ">'.$line2.'</p>
</div>
</div>
<div style="width: 100%; height: 50px; text-align: center; margin-top: 30px;">
<button onclick="close_approve_div('."'approve_request$i','$temp','app'".')" style="display: inline-block; font-family: '."'Noto Sans'".', sans-serif; margin: 0px 20px 0px 20px; font-size: 15px; padding: 5px 10px 5px 10px; background-color: #224162; color: white; border: 1px solid #224162; cursor: pointer;">Accept</button>
<button onclick="close_approve_div('."'approve_request$i','$temp','ign'".')" style="display: inline-block; font-family: '."'Noto Sans'".', sans-serif; margin: 0px 20px 0px 20px; font-size: 15px; padding: 5px 10px 5px 10px; background-color: white; color: #224162; border: 1px solid #224162; cursor: pointer;">Ignore</button>
</div>
</div>';
}
?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
var approve_count=0;
function close_approve_div(a,co,step)
{

		$.ajax({
		type: 'post',
		 url: 'js/php/approve_follow_request.php',
		 data : {co:co,step:step},
		 success: function(code){
			 var c_status=code;
		 document.getElementById(a).style.display="none";
		 approve_count++;
		 if(approve_count=='<?php echo sizeof($request); ?>')
			 {
				 document.getElementById("pending_requests").style.display="block";
				 document.getElementById("approve_request").style.display="none";
			 }
			c_status=c_status.split('|'); 
			document.getElementById("follower_c").innerHTML=c_status[0];
			document.getElementById("following_c").innerHTML=c_status[1];
			document.getElementById("request_c").innerHTML=c_status[2];
			if(c_status[2]>=1)
			{
			document.getElementById("nav_unread_message_status").style.display="block";
			document.getElementById("nav_unread_message_status_t").innerHTML=c_status[2];
			}
			else
			{
			document.getElementById("nav_unread_message_status").style.display="none";
			document.getElementById("nav_unread_message_status_t").innerHTML=c_status[2];
			}
		 }
		 });	
}
<?php 

if($request_c>=1)
{
	echo '
	document.getElementById("nav_unread_message_status").style.display="block";
	document.getElementById("nav_unread_message_status_t").innerHTML=c_status[2];
	';
}
else
{
	echo '
	document.getElementById("nav_unread_message_status").style.display="none";
	document.getElementById("nav_unread_message_status_t").innerHTML=c_status[2];
	';
}
?>

</script>
</div>
</div>
</div>

<?php
if(sizeof($request)>=1 and $request[0]!='')
{ 
   echo '<script>document.getElementById("pending_requests").style.display="none";
   document.getElementById("approve_request").style.display="block";</script>';
}
else
{
	echo '<script>document.getElementById("pending_requests").style.display="block";
   document.getElementById("approve_request").style.display="none";</script>';
}
?>


<div class="followerblock"> 
<div class="followerlist" style="position: relative;">
<div style="width: 100%; height: 100%; background-color: white; box-shadow: 0px 0px 1.5px 0px #cbcbcb; position: absolute; top: 0px;">
<div style="width: 100%; height: 50px; background-color: #224162; text-align: left;">
<p style="font-family: 'Noto Sans', sans-serif; margin: 0px; color: white; padding-left: 10px; padding-top: 10px; font-size: 20px;">Followers</p>
</div>
<?php
require('js/php/conn.php');
error_reporting(0);
$session=$_SESSION['alu_auth'];
$che=mysqli_query($conn,"select * from user_security where hash_key='$session';");
$che=mysqli_fetch_assoc($che);
$session=$che['email'];
$dis=$_REQUEST['dis'];
$c=mysqli_query($conn,"select follower from profile where email='$session';");
	$c=mysqli_fetch_assoc($c);
	$c=$c['follower'];
	$c=explode(",",$c);
	if(sizeof($c)>=1 and $c[0]!='')
	{
	   if(sizeof($c)>=4 and $c[0]!='')
	   {
		   	for($i=0;$i<4;$i++)
			{
				$temp_code=$c[$i];
				$get_details=mysqli_query($conn,"select profile_name,profile_pic,job,company from profile inner join personal where personal.email=profile.email and code='$temp_code';");
				$get_details=mysqli_fetch_assoc($get_details);	
				$pic=$get_details['profile_pic'];
				$name=$get_details['profile_name'];
				$job=$get_details['job'];
				$company=$get_details['company'];
					$mk=mysqli_query($conn,"select following from profile where email='$session';");
					$mk=mysqli_fetch_assoc($mk);
					$mk=$mk['following'];
					$rt=mysqli_query($conn,"select request_sent from profile where email='$session';");
					$rt=mysqli_fetch_assoc($rt);
					$rt=$rt['request_sent'];
					$m=check($mk,$rt,$temp_code);
				echo '
				
				<div class="follower_box_show">
				<div style="width: 60px; height: 60px; position: relative; margin-left: 20px; float: left;">
				<div style="width: 50px; height: 50px; overflow: hidden; border-radius: 50px; position: absolute; margin: auto; top: 0px ; left: 0px; right: 0px; bottom: 0px;">
				<img src="'.$pic.'" style="width: 100%; height: auto;" />
				</div>
				</div>
				<div style="width: 250px; height: 60px; text-align: left; float: left;">
				<p style="margin: 0px; padding-left: 10px; font-family: '."'Noto Sans'".', sans-serif; padding-top: 8px; font-size: 15px;">'.$name.'</p>
				<p style="margin: 0px; padding-left: 10px; font-family: '."'Noto Sans'".', sans-serif; padding-top: 2px; font-size: 12px;">'.$job.','.$company.'</p>
				</div>
				<div style="width: 130px; height: 60px; float: right;">
				<button onclick="follow_button(this,'."'Following'".');" id="follow_button" value="'.$temp_code.'"  class="box_button">'.$m.'</button>
				</div>
				</div>
				
				';
			}
	   }
	   else
	   {
		   	for($i=0;$i<sizeof($c);$i++)
			{
				$temp_code=$c[$i];
				$get_details=mysqli_query($conn,"select profile_name,profile_pic,job,company from profile inner join personal where personal.email=profile.email and code='$temp_code';");
				$get_details=mysqli_fetch_assoc($get_details);	
				$pic=$get_details['profile_pic'];
				$name=$get_details['profile_name'];
				$job=$get_details['job'];
				$company=$get_details['company'];
					$mk=mysqli_query($conn,"select following from profile where email='$session';");
					$mk=mysqli_fetch_assoc($mk);
					$mk=$mk['following'];
					$rt=mysqli_query($conn,"select request_sent from profile where email='$session';");
					$rt=mysqli_fetch_assoc($rt);
					$rt=$rt['request_sent'];
					$m=check($mk,$rt,$temp_code);
				echo '
				
				<div class="follower_box_show">
				<div style="width: 60px; height: 60px; position: relative; margin-left: 20px; float: left;">
				<div style="width: 50px; height: 50px; overflow: hidden; border-radius: 50px; position: absolute; margin: auto; top: 0px ; left: 0px; right: 0px; bottom: 0px;">
				<img src="'.$pic.'" style="width: 100%; height: auto;" />
				</div>
				</div>
				<div style="width: 250px; height: 60px; text-align: left; float: left;">
				<p style="margin: 0px; padding-left: 10px; font-family: '."'Noto Sans'".', sans-serif; padding-top: 8px; font-size: 15px;">'.$name.'</p>
				<p style="margin: 0px; padding-left: 10px; font-family: '."'Noto Sans'".', sans-serif; padding-top: 2px; font-size: 12px;">'.$job.','.$company.'</p>
				</div>
				<div style="width: 130px; height: 60px; float: right;">
				<button onclick="follow_button(this,'."'Following'".');" id="follow_button" value="'.$temp_code.'"  class="box_button">'.$m.'</button>
				</div>
				</div>
				
				';
			}
	   }
	}
	
	
	function check($mk,$rt,$temp_code)
{
	$mk=explode(',',$mk);
	$rt=explode(',',$rt);
	for($i=0;$i<sizeof($mk);$i++)
	{
		if($mk[$i]==$temp_code)
		{
			return('Following');
			break;
		}
	}
	for($i=0;$i<sizeof($rt);$i++)
	{
		if($rt[$i]==$temp_code)
		{
			return('Requested');
			break;
		}
	}
	return('Follow');
}

?>

<div style="width: 100%; height: 50px; background-color: white; position: absolute; text-align: right; bottom: 0px;">
<button class="seeall" onclick="show_pop_up_box('Followers');">See All</button>
</div>
</div>
</div>
<div class="followinglist" style="position: relative;">
<div style="width: 100%; height: 100%; background-color: white; box-shadow: 0px 0px 1.5px 0px #cbcbcb; position: absolute; top: 0px;">
<div style="width: 100%; height: 50px; background-color: #224162; text-align: left;">
<p style="font-family: 'Noto Sans', sans-serif; margin: 0px; color: white; padding-left: 10px; padding-top: 10px; font-size: 20px;">Following</p>
</div>
<?php
$c=mysqli_query($conn,"select following from profile where email='$session';");
	$c=mysqli_fetch_assoc($c);
	$c=$c['following'];
	$c=explode(",",$c);
	if(sizeof($c)>=1 and $c[0]!='')
	{
       if(sizeof($c)>=4 and $c[0]!='')
	   {
				for($i=0;$i<4;$i++)
				{
					$temp_code=$c[$i];
					$get_details=mysqli_query($conn,"select profile_name,profile_pic,job,company from profile inner join personal where personal.email=profile.email and code='$temp_code';");
					$get_details=mysqli_fetch_assoc($get_details);	
					$pic=$get_details['profile_pic'];
					$name=$get_details['profile_name'];
					$job=$get_details['job'];
					$company=$get_details['company'];
					echo '
					
					<div class="follower_box_show">
			<div style="width: 60px; height: 60px; position: relative; margin-left: 20px; float: left;">
			<div style="width: 50px; height: 50px; overflow: hidden; border-radius: 50px; position: absolute; margin: auto; top: 0px ; left: 0px; right: 0px; bottom: 0px;">
			<img src="'.$pic.'" style="width: 100%; height: auto;" />
			</div>
			</div>
			<div style="width: 250px; height: 60px; text-align: left; float: left;">
			<p style="margin: 0px; padding-left: 10px; font-family: '."'Noto Sans'".', sans-serif; padding-top: 8px; font-size: 15px;">'.$name.'</p>
			<p style="margin: 0px; padding-left: 10px; font-family: '."'Noto Sans'".', sans-serif; padding-top: 2px; font-size: 12px;">'.$job.','.$company.'</p>
			</div>
			<div style="width: 130px; height: 60px; float: right;">
			<button onclick="follow_button(this,'."'Following'".');" id="follow_button" value="'.$temp_code.'"  class="box_button">Following</button>
			</div>
			</div>
					
					';
				}
	   }
	   else
	   {
		   		for($i=0;$i<sizeof($c);$i++)
				{
					$temp_code=$c[$i];
					$get_details=mysqli_query($conn,"select profile_name,profile_pic,job,company from profile inner join personal where personal.email=profile.email and code='$temp_code';");
					$get_details=mysqli_fetch_assoc($get_details);	
					$pic=$get_details['profile_pic'];
					$name=$get_details['profile_name'];
					$job=$get_details['job'];
					$company=$get_details['company'];
					echo '
					
					<div class="follower_box_show">
			<div style="width: 60px; height: 60px; position: relative; margin-left: 20px; float: left;">
			<div style="width: 50px; height: 50px; overflow: hidden; border-radius: 50px; position: absolute; margin: auto; top: 0px ; left: 0px; right: 0px; bottom: 0px;">
			<img src="'.$pic.'" style="width: 100%; height: auto;" />
			</div>
			</div>
			<div style="width: 250px; height: 60px; text-align: left; float: left;">
			<p style="margin: 0px; padding-left: 10px; font-family: '."'Noto Sans'".', sans-serif; padding-top: 8px; font-size: 15px;">'.$name.'</p>
			<p style="margin: 0px; padding-left: 10px; font-family: '."'Noto Sans'".', sans-serif; padding-top: 2px; font-size: 12px;">'.$job.','.$company.'</p>
			</div>
			<div style="width: 130px; height: 60px; float: right;">
			<button onclick="follow_button(this,'."'Following'".');" id="follow_button" value="'.$temp_code.'"  class="box_button">Following</button>
			</div>
			</div>
					
					';
				}
	   }
	}

?>
<div style="width: 100%; height: 50px; background-color: white; position: absolute; text-align: right; bottom: 0px;">
<button class="seeall"  onclick="show_pop_up_box('Following');">See All</button>
</div>
</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
function show_pop_up_box(a)
{
	document.getElementById('follw_box_show_pop_up').style.display='block';
	document.getElementById('show_display_status_show').innerHTML=a;
	var dis=a;
	$.ajax({
	type: 'post',
	 url: 'js/php/follow_popup_show.php',
	 data : {dis:dis},
	 success: function(code){
	 $('#main_follow_status_popup_box').html(code);
	 }
	 });
}
</script>
<script>
function follow_button(v,f)
{
	var value=v.value;
	$.ajax({
type: 'post',
 url: 'js/php/follow_button.php',
 data : {value:value},
 success: function(code){
var m=code;
if(m=="following")
{
	document.getElementById("follow_button").innerHTML="Following";
	document.getElementById("follow_button").style.backgroundColor="#fe6e60";
	document.getElementById("follow_button").style.color="white";
}
else if(m=="Requested")
{
	document.getElementById("follow_button").innerHTML="Requested";
	document.getElementById("follow_button").style.backgroundColor="#fe6e60";
	document.getElementById("follow_button").style.color="white";
}
else if(m=='unfollowed')
{
	document.getElementById("follow_button").innerHTML="Follow";
	document.getElementById("follow_button").style.backgroundColor="white";
	document.getElementById("follow_button").style.color="#fe6e60";
}
else
{
	document.getElementById("follow_button").innerHTML="Requested";
	document.getElementById("follow_button").style.backgroundColor="#fe6e60";
	document.getElementById("follow_button").style.color="white";
}
 }
 });

}
</script>

</body>
</html>