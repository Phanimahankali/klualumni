<html>
<head>
<style>
body
{
	margin: 0px;
	background-color: #f4f4f4;
}
.nav
{
	width: 100%;
	height: 65px;
	box-shadow: 0px 1px 2px 0px #b8b8b8;
	background-color: white;
}
.password_reset
{
	width: 380px;
	height: 350px;
	margin: auto;
	box-shadow: 0px 0px 1px 0px #6d6d6d;
	margin-top: 100px;
	background-color: white;
}
#password
{
	width: 80%;
    margin-top: 3px;
    font-family: 'Lato', sans-serif;
    padding: 10px 5px 10px 5px;
    border-radius: 3px;
    border: 1px solid #bdbdbd;
    background-color: #fefefe;
}
#repassword
{
	width: 80%;
    margin-top: 3px;
    font-family: 'Lato', sans-serif;
    padding: 10px 5px 10px 5px;
    border-radius: 3px;
	margin-top: 20px;
    border: 1px solid #bdbdbd;
    background-color: #fefefe;
}
#enc
{
	font-size: 16px;
	padding: 10px;
	width: 80%;
	padding-top: 15px;
	padding-bottom: 15px;
	background-color: #cc3333;
	border: none;
	color: white;
	cursor: pointer;
	border-radius: 10px;
	margin-top: 20px;
}
</style>
</head>
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<body>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<div class="nav">
<img src="imags/klu.png" style="height: 80%; width: auto; margin-top: 5px; margin-left: 5px; float: left;"/>
<p style="float: left; font-family: arial; padding-top: 20px; font-size: 17px; padding-left: 5px;">Alumni Association</p>
</div>
<div class="password_reset">
<p style="text-align: center; font-family: 'Roboto', sans-serif; margin: 0px; font-size: 18px; padding-top: 45px; ">Reset Password</p>
<div style="width: 100%; height: 40px;">
<p id="status_login" style="margin: 0px; text-align: center; padding-top: 10px;font-family: 'Lato', sans-serif; font-size: 15px; "></p>
</div>
<form style="text-align: center;" method="post" action="password_set.php">
<input type="password" id="password" name="password" placeholder="Password"></input>
<input type="password" id="repassword" name="repassword" placeholder="Re-Password"></input>
<input type="text" value="<?php error_reporting(0); echo $_REQUEST['enc']; ?>" style="visibility: hidden; height: 0px;" name="enc" placeholder="Re-Password"></input>
<button id="enc" name="submit">Set Password</button>
</form>
</div>
<?php
require_once('js/php/conn.php');
error_reporting(0);
if(isset($_REQUEST['submit']))
{
	$hash_key=$_REQUEST['enc'];
	$hash=mysqli_real_escape_string($conn,$hash_key);
	$password=$_REQUEST['password'];
	$password=mysqli_real_escape_string($conn,$password);
	$repassword=$_REQUEST['repassword'];
	$repassword=mysqli_real_escape_string($conn,$repassword);
	if($password!=$repassword and $password!='' and $repassword!='')
	{
		echo '
		<script>
		document.getElementById("status_login").innerHTML="Password Not match";
		document.getElementById("status_login").style.color="red";
		</script>
		';
	}
	else
	{
		$check=mysqli_query($conn,"select * from password_reset where hash_key='$hash' and status is NULL;");
		if(mysqli_num_rows($check)==1)
		{
			$e=mysqli_fetch_assoc($check);
			$email=$e['email'];
			$password=md5(md5($password));
			mysqli_query($conn,"update auth set password='$password' where email='$email';");
			mysqli_query($conn,"update password_reset set status='CLOSED' where hash_key='$hash';");
			echo '
			<script>
			document.getElementById("status_login").innerHTML="Password changed sucessfully";
			document.getElementById("status_login").style.color="green";
			 window.setTimeout(function(){
              window.location.href = "login.php";
           }, 1500);
			</script>
			';
		}
		else
		{
				echo '
			<script>
			document.getElementById("status_login").innerHTML="Link expaired (or) Already used";
			document.getElementById("status_login").style.color="red";
			 window.setTimeout(function(){
              window.location.href = "login.php";
           }, 1500);
			</script>
			';
		}
	}
}
?>
</body>
</html>