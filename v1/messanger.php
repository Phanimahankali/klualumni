<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.js"></script>
<style>
body
{
	margin: 0px;
	position: relative;
}
#messanger_down_box
{
	width:100%;
	height: 10px;
	position: absolute;
	bottom: 0px;
	position: fixed;
}
.message_box
{
	width: auto;
	height: 350px;
	position: absolute;
	bottom: 0px;
	right: 0px;
}
.message_sub_box
{
	width: 280px;
	height: 350px;
	display: inline-block;
	visibility: hidden;
	border-top-left-radius: 10px;
	border-top-right-radius: 10px;
	margin: 0px 20px 0px 20px;
	overflow: hidden;
	background-color: white;
	box-shadow: 0px 0px 5px 1px #d9d9d9;
}
.message_top_details
{
	background-color:#df3a3a;
	width: 100%;
	height: 40px;
}
.message_send_box
{
	width: 280px;
	height: 35px;
	border-top: 1px solid #a6a6a6;
	position: absolute;
	bottom: 0px;
}
.message_active_status
{
	width: 30px; 
	height: 40px;
	float: left;
	position: relative;
}
.message_active_person_name
{
	width: 200px;
	height: 40px;
	float: left;
	overflow: hidden;
}
.message_active_person_name p
{
	font-family: 'Noto Sans', sans-serif;
	font-size: 13px;
	margin: 0px;
	padding-top: 10px;
	color: white;
}
.message_box_close
{
	width: auto;
	height: 40px;
	float: right;
}
.message_box_close p
{
	font-family: 'Noto Sans', sans-serif;
	font-size: 18px;
	margin: 0px;
	cursor: pointer;
	margin: 8px 10px 0px 0px;
    padding: 0px;
	padding: 0px 4px 0px 4px;
	width: auto;
	color: white;
}
.message_box_close p:hover
{
	background-color: #e14848;
}
*:focus
{
	outline: none;
}
.message_display_content
{
	width:280px;
	height: 274px;
	background-color: white;
	position: absolute;
}
</style>
</head>
<body>
<button onclick="get_new_message_box();">Get message box</button>

<div id="messanger_down_box">
<div class="message_box" id="message_box">
<div class="message_sub_box" id="message_sub_box">
<div class="message_top_details">
<div class="message_active_status">
<div style="width: 10px; height: 10px; border-radius: 10px; background-color: #7ed957; position:absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;">
</div>
</div>
<div class="message_active_person_name">
<p>G Sai Revanth</p>
</div>
<div class="message_box_close">
<b><p onclick="close_m(this.closest('[id]'));">X</p></b>
</div>
</div>
<div class="message_display_content">
</div>
<div class="message_send_box">
<div style="width: 85%; height: 100%; background-color: red; float: left;">
<input style="width: 100%; height: 100%; font-family: 'Noto Sans', sans-serif; margin: 0px; border: none; padding-left: 10px;" placeholder="Type a message"></input>
</div>
<div style="width: 15%; height: 100%; float: left;">
<p style="font-family: 'Noto Sans', sans-serif; margin: 0px; font-size: 13px; text-align: center; padding-top: 9px; cursor: pointer;"><b>Send</b></p>
</div>
</div>
</div>
</div>
</div>
<script>
    var h=$(window).height();
	var h=h-10;
document.getElementById('messanger_down_box').style.top=h;
$(window).on('resize', function(){
    var h=$(window).height();
	var h=h-10;
document.getElementById('messanger_down_box').style.top=h;
});
var i=0;
var num_box=0;
function get_new_message_box()
{

		var w=$(window).width();
		num_box=num_box+320;
		if(num_box<=w-320)
		{
			var message_box=document.getElementById('message_sub_box');
			var m=message_box.cloneNode(true);
			m.id="v"+i;
			m.style.visibility="visible";
			i++;
			document.getElementById('message_box').appendChild(m);
		}
		else
		{
			num_box=num_box-320;
			alert("Resolution Not Support");
		}
}
function close_m(m)
{
num_box=num_box-320;
m.remove();
}
</script>
</body>
</html>