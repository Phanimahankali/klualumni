<html>
<head>
<title>Login | KL Alumni Association</title>
<?php
error_reporting(0);
session_start();
$get_365mail=$_GET['mail'];
if($_SESSION['alu_auth']!=null)
{
	header('location: news.php');
}
?>
<link href="https://fonts.googleapis.com/css?family=Lato|Noto+Sans" rel="stylesheet">
<style>
body
{
	margin: 0px;
	background-color: #f7f7f7;
}
.nav
{
	width: 100%;
	height: 70px;
	background-color: white;
	box-shadow: 0px 2px 8px 0px #ebebeb;
}
.nav_icon
{
	list-style-type: none;
}
.nav_icon li
{
	float: right;
	padding-top: 6px;
	font-family: 'Lato', sans-serif;
	padding-bottom: 5px;
	margin-left: 20px;
	cursor: pointer;
	color: black;
	margin-right: 30px;
	transition: 0.2s;
}
.nav_icon li:hover
{
	transition: 0.2s;
	border-bottom: 2px solid #b5b5b5;
}
.login
{
	width: 380px;
	height: 400px;
	margin: auto;
	margin-top: 100px;
	box-shadow: 0px 0px 1px 0px #8d8d8d;
	background-color: white;
}
input:focus,button:focus
{
	outline: none;
}
.signin_button
{
	width: 80%;
	font-family: arial;
	color: white;
	margin: 10px 40px 0px 40px;
	background-color: #cc3333;
	border: none;
	font-size: 18px;
	padding: 10px;
	cursor:pointer;
	
}
.input_fields_style
{
	width: 100%;
	margin-top: 3px;
	font-family: 'Lato', sans-serif;
	padding: 10px 5px 10px 5px;
	border-radius: 3px;
	border: 1px solid #bdbdbd;
	background-color: #fefefe;
}
*:focus
{
	outline: none;
}
</style>
<link href="https://fonts.googleapis.com/css?family=Lato|Roboto" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
<div class="nav">
<a href="index.php">
<img src="imags/klu.png" style="height: 50px; width: auto; margin-top: 10px; margin-left: 10px; float: left;"/>
<p style="margin: 0px; color: black; font-family: arial; padding-top: 38px; font-size: 18px; float:left;">Alumni Association</p>
</a>
<div style="width: auto; height: 70px;  float: right; margin: 0px;">
<ul class="nav_icon">
<a href="register.php"><li>REGISTRATION</li></a>
<a href="careers.php"><li>Carrers</li></a>
<a href="events.php"><li>Events & Calendar </li></a>
<a href="index.php"><li>Home</li></a>
</ul>
</div>
</div>

<div class="login">

<p style="text-align: left; padding-left: 40px; font-family: 'Lato', sans-serif; margin: 0px; font-size: 25px; letter-spacing: 1px; padding-top: 30px;"><strong>Sign In</strong></p>
<div style="width: 100%; height: 10px;">
<p id="not_exit" style="text-align: center; font-family: 'Roboto', cursive; font-size: 15px;  color: red; padding-top: 0px;"></p>

</div>
<div style="width: 100%; height: auto; margin-top: 10px; text-align: center;">
<form action="login.php"  method="post">
<fieldset style="margin: 0px 30px 0px 30px; text-align: left; border: none;">
<legend><p style="font-family: 'Lato', sans-serif; font-size: 13px; margin: 0px;"><b>Username</b></p></legend>
<input name="email" class="input_fields_style" placeholder="e.g., &nbsp; alumni@kluniversity.in"></input>
</fieldset>
<fieldset style="margin: 15px 30px 0px 30px; text-align: left; border: none;">
<legend><p style="font-family: 'Lato', sans-serif; font-size: 13px; margin: 0px;"><b>Password</b></p></legend>
<input name="password" class="input_fields_style" type="password"></input>
</fieldset>
<div style="width: 100%; height: 30px; ">
<a href="forgot.php">
<p style="margin: 0px; color: black;  font-family: arial; font-size: 12px; cursor: pointer; padding-top: 10px; float: right; padding-right: 50px;">Forgot Your Password ?</p>
</a>
</div>
<button name="signin" id="signin" class="signin_button">SIGN IN</button>
<script src="js/get_ip.js"></script>
<script type="application/javascript" src="https://api.ipify.org?format=jsonp&callback=getIP"></script>
</form>

</div>
</div>
<p style="text-align: center; font-family: 'Noto Sans', sans-serif; font-size: 13px; color: #494949;">Copyright &copy;  KL University</p>
<?php
require('js/php/conn.php');
if(isset($_REQUEST['signin']))
{
$conn=mysqli_connect($host,$username,$password,$db);
$password=md5(md5($_REQUEST['password']));
$password=stripslashes($password);
$email=mysqli_real_escape_string($conn,$_REQUEST['email']);
$email=stripslashes($email);
$password=mysqli_real_escape_string($conn,$password);
$ip=$_REQUEST['signin'];
$ip=explode("/",$ip);
$broswer=$ip[0];
$ip=$ip[1];
$q=mysqli_query($conn,"select * from auth where email='$email' and password='$password';");
if(mysqli_num_rows($q)==1)
{
	$key = md5(microtime().rand());
	$c1=0;
	while($c1!=1)
	{
		$check=mysqli_query($conn,"select * from user_security where hash_key='$key' and status='CLOSED';");
		if(mysqli_num_rows($check)==0)
		{
			$c1++;
		}
		else
		{
			$key = md5(microtime().rand());
		}
	}
date_default_timezone_set('Asia/Kolkata');
$date=date('Y-m-d H:i:s');
mysqli_query($conn,"update profile set active_status='$date' where email='$email';");

	mysqli_query($conn,"insert into user_security (email,hash_key,ip,broswer) values ('$email','$key','$ip','$broswer');");
	session_start();
	$_SESSION['alu_auth']=$key;
	include('cms/mailer/new_device.php');
}
else
{
	$ccc=mysqli_query($conn,"select * from auth where email='$email';");
	if(mysqli_num_rows($ccc)==1)
	{
	echo '
	<script>
	document.getElementById("not_exit").innerHTML="Incorrect password";
	document.getElementById("not_exit").style.display="block";
	</script>
	';
	}
	else
	{
	echo '
	<script>
	document.getElementById("not_exit").innerHTML="Account Not Exits";
	document.getElementById("not_exit").style.display="block";
	</script>
	';
	}
	exit;
}
}
?>
</body>
</html>