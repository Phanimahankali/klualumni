<html>
<head>
<title>KL Alumni Association | K L University</title>
<meta http-equiv="expires" content="0">
<?php
clearstatcache();
error_reporting(0);
session_start();
$session=$_SESSION['alu_auth'];
if($session!=null)
{
header("Location:news.php");	
}
?>
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<link rel="stylesheet" type="text/css" href="css/index.css">
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">	
<link href="https://fonts.googleapis.com/css?family=Noto+Sans|Roboto" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<script src="js/index_dropdown_menu.js"></script>
<style>

.explore_map
{
	font-family: 'Open Sans', sans-serif;
	font-size: 18px;
	color: white; 
	background-color: transparent;
	border: 1.5px solid white;
	padding: 3px 15px 3px 15px;
	transform: 5s;
}
.explore_map:hover
{
	background-color: white;
	color: #bbbbbb;
	border-radius: 4px;
	transform: 5s;
}
</style>
</head>
<body>

<div class="icon_dis" style="cursor: pointer; z-index: 10;">
<div class="main_icon" onclick="window.location='index.php'">
<img src="imags/klu.png" />
<strong><p style="text-decoration: none;">Alumni Association</p></strong>
</div>
</div>
<div class="dropdownmenu">
  <nav style="position: relative; z-index: 10;">
      <div class="nav-mobile"><a id="nav-toggle" href="#!"><span></span></a></div>
      <ul class="nav-list" style="z-index: 10;" >
        <li>
          <a href="#" style="text-decoration: none;">Carrers</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="careers.php" style="text-decoration: none;">Internship</a>
            </li>
            <li>
              <a href="careers.php" style="text-decoration: none;">Jobs</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="advisoryboard.php" style="text-decoration: none;">Board Members</a>
        </li>
        <li>
          <a href="#!" style="text-decoration: none;">Events</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="events.php" style="text-decoration: none;">Alumni Event Calender</a>
            </li>
			<li><a href="events.php" style="text-decoration: none;">All Events</a></li>
			<li>
			  <a href="https://kluniversity.in/site/acadcal.htm" style="text-decoration: none;">Academic Calender</a></li>
          </ul>
        </li>
		 <li>
              <a href="suess_stories.php" style="text-decoration: none;">Alumni Stories</a>
            </li>
            <li>
              <a href="gallery.php" style="text-decoration: none;">Gallery</a>
            </li>
         <li>
              <a href="updates.php" style="text-decoration: none;">News & Updates</a>
            </li>
        <li>
          <a href="aboutus.html" style="text-decoration: none;">About us</a>
        </li>
 <li>
          <a href="login.php" style="text-decoration: none;">Login</a>
        </li>
      </ul>
    </nav>
</div>
<div class="slider" style="z-index: 1;">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="imags/slider/aboutus.png" alt="Los Angeles" style="width:100%;">
      </div>

      <div class="item">
        <img src="imags/slider/KLUniversityH.png" alt="Chicago" style="width:100%;">
      </div>
    
      <div class="item">
        <img src="imags/slider/our.png" alt="New york" style="width:100%;">
      </div>
    </div>

 
    <a class="left carousel-control" style="opacity: 0;" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" style="opacity: 0;" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<div style="width: 100%; height: auto; position: relative;">
<div class="working_at">
<strong><p style="text-align: left;">Our students working at</p></strong>
<div class="companys_list" id="companys_list" style="padding-top: 30px;">
   <div class="owl-carousel clients-carousel">
         <img src="imags/working/1.png" style="padding: 0px 15px 0px 15px; height: 50px; width: auto;" alt="">
         <img src="imags/working/2.png" style="padding: 0px 15px 0px 15px; height: 50px; width: auto;" alt="">
		 <img src="imags/working/3.png" style="padding: 0px 15px 0px 15px; height: 50px; width: auto;" alt="">
		 <img src="imags/working/4.png" style="padding: 0px 15px 0px 15px; height: 50px; width: auto;" alt="">
		 <img src="imags/working/5.png" style="padding: 0px 15px 0px 15px; height: 50px; width: auto;" alt="">
		 <img src="imags/working/6.png" style="padding: 0px 15px 0px 15px; height: 50px; width: auto;" alt="">
		 <img src="imags/working/7.png" style="padding: 0px 15px 0px 15px; height: 50px; width: auto;" alt="">
		 <img src="imags/working/8.png" style="padding: 0px 15px 0px 15px; height: 50px; width: auto;" alt="">
		  <img src="imags/working/9.png" style="padding: 0px 15px 0px 15px; height: 50px; width: auto;" alt="">
		 <img src="imags/working/10.png" style="padding: 0px 15px 0px 15px; height: 50px; width: auto;" alt="">
        </div>
</div>
</div>
<div class="signin_box" id="signin_box">
<div class="main_signin_box">
<div style="width:100%; height: 60px; background-image: url('imags/main_loginbox.jpg'); ">
<p style="text-align: left; font-size: 20px; padding-left: 20px; padding-top: 10px; color: white;">Sign in / Register</p>
</div>
<?php
require_once('js/php/google_settings.php');
$login_url = 'https://accounts.google.com/o/oauth2/v2/auth?scope=' . urlencode('https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email') . '&redirect_uri=' . urlencode(CLIENT_REDIRECT_URL) . '&response_type=code&client_id=' . CLIENT_ID . '&access_type=online';
?>
<a href='<?php echo $login_url; ?>' style="text-decoration: none;">
<div class="signin_box_style" >
<img src="imags/working/google_signin.png" style="float: left; height: 90%; margin-top: 1%; float: left; margin-left: 10px; width: auto;">
<p style="margin: 0px; padding-top: 9px; color: black; font-size: 18px; font-family: 'Noto Sans', sans-serif; float: left; padding-left: 30px;">Google</p>
</div>
</a>
<?php require('365.php'); ?>
<a href='<?php echo $accessUrl; ?>' style="text-decoration: none;">
<div class="signin_box_style" id="SignIn">
<img src="imags/working/office.png" style="float: left; height: 90%; float: left; margin-top: 1%; margin-left: 10px; width: auto;">
<p style="margin: 0px; padding-top: 9px; color: black; font-size: 18px; font-family: 'Noto Sans', sans-serif; float: left; padding-left: 30px;">Office 365</p>
</div>
</a>
<div style="width: 90%; height: 1.5px; margin: auto; margin-top: 20px; background-color: #d8d8d8;">
</div>
<div style="width: 100%; height: 50px; margin-top: 10px;">
<table style="margin: auto; margin-top: 20px; margin-left: 30px;">
<tr>
<td>
<a href="login.php" style="text-decoration: none;"><p style="font-size: 13px; color: black; cursor: pointer; padding: 0px; margin: 0px; margin-right: 40px;">Connect with custom mail</p></a>
</td>
<td>
<a href="register.php" style="text-decoration: none;"><p style="font-size: 13px;  color: black; cursor: pointer; padding: 0px; margin: 0px; margin-left: 40px;">New to site</p></a>
</td>
</tr>
</table>
</div>
</div>
</div>
</div>

<div class="updates_news">
</div>



  <div class="container" style="width: 100%; max-width: 1800px; height: auto;">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">



<div class="sucess_storys_box">
<div class="sub_sucess_storys_box">
<div style="width: 100%; height: 50px; background-image: url('imags/main_loginbox.jpg');">
<a href="suess_stories.php"><p>Success Stories</p></a>
</div>
<div style="width: 100%; height: 300px;">

<?php
require('js/php/conn.php');
$get_sucess=mysqli_query($conn,"select * from success_stories order by timestamp DESC limit 3;");
while($get_data_s=mysqli_fetch_assoc($get_sucess))
{
$ss_title=$get_data_s['title'];
$cc=$get_data_s['hash_key'];
$ss_ext="cms/sucess/".$get_data_s['hash_key'].'.'.$get_data_s['ext'];

echo '
<a href="suess_stories_s.php?hash='.$cc.'">
<div class="sucess_stories">
<div style="float: left; width: 100px; position: relative; height: 100%;">
<div style="position: absolute; top: 0px; left: 0px; right: 0px; overflow: hidden; bottom: 0px; margin: auto; width: 65px; height: 65px; border-radius:65px; ">
<img src="'.$ss_ext.'" style="width: 100%; height: auto;" />
</div>
</div>

<div style="float: left; width: 500px; position: relative; height: 100%;">
<p style="border: none; font-size: 15px; font-family: '."'Lato'".', sans-serif; color: #626262;"> ';
if(strlen($ss_title)<=166)
{
	echo $ss_title;
	echo '</p>';
}
else
{
	for($i=0;$i<strlen($ss_title);$i++)
	{
		echo $ss_title[$i];
	}
	echo'...</p>';
}
echo '
</div>
</div>
</a>

';
}
?>


</div>
</div>
</div>



</div>




<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">



<div class="updates">
<div class="update_box">
<div style="width: 100%; height: 50px; background-image: url('imags/main_loginbox.jpg');">
<p>News & Updates</p>
</div>
<div class="update_box_ul">
<ul>
<?php
require('js/php/conn.php');

$get_data=mysqli_query($conn,"select file,title,url from news order by timestamp DESC limit 6;");
while($get_news_data=mysqli_fetch_array($get_data))
{
	$title=$get_news_data[1];
	$file="cms/news/".$get_news_data[0];
	$url=$get_news_data[2];
	if($url=='' or $url==null)
	{
	echo '<a href="'.$file.'"><li style="font-family: arial; font-size: 14px;">'.$title.'</li></a>';
	}
	else
	{
		echo '<a href="'.$url.'"><li style="font-family: arial; font-size: 14px;">'.$title.'</li></a>';
	}
}
?>

</ul>
</div>
</div>
</div>
</div>


</div>
</div>





<div class="social_media">
<div class="social_media_box">
<div class="social_media_box_a">
<div style="width: 100%; height: 50px; background-image: url('imags/box/1.jpg');">
<p>Popular Profiles</p>
</div>
<div class="popular_profile_box">

<?php
require('js/php/conn.php');
$get_popular_profiles=mysqli_query($conn,"select profile_pic,profile_name,town,personal.country,hash_key from profile 
inner join auth on auth.email=profile.email 
inner join personal on personal.email=profile.email 
where auth.status='VERIFIED' order by profile.visitors_count DESC limit 4;");
while($get_popular=mysqli_fetch_array($get_popular_profiles))
{
	$profile_pic=$get_popular[0];
	$get_name=$get_popular[1];
	$get_town=$get_popular[2];
	$get_country=$get_popular[3];
	$get_hash=$get_popular[4];
	echo '
	<a href="profile.php?getdetails='.$get_hash.'">
	<div class="popular_profile_box_profile">
<div style="width: 50px; height: 50px; margin-top: 10px; position: relative; float:left; border-radius: 60px; overflow: hidden; margin-left: 20px;">
<img src="'.$profile_pic.'" style="width: 100%; height: auto; position: absolute; top:0px; left: 0px; right: 0px; bottom: 0px; margin: auto;"/>
</div>
<div style="width:360px; float:left; height: 100%;">
<div style="width: 100%; height: 25px; float: left; margin-top: 10px;">
<p style="font-family: '."'Open Sans'".', sans-serif; float: left; margin: 0px; margin-left: 10px; border: none; color: black; font-size: 16px;">'.$get_name.'</p>
<img src="imags/verify.png" style="width: auto; margin-top: 8px; float: left; height: 60%;" />
</div>
<p style="font-family: '."'Open Sans'".', sans-serif; float: left; margin: 0px; margin-left: 10px; border: none; color: black; font-size: 12px;">'.$get_town.', '.$get_country.'</p>
</div>
</div>
</a>
	';
}
?>
</div>
</div>
</div>

<div class="social_media_box">
<div class="social_media_box_a">
<div style="width: 100%; height: 50px; background-image: url('imags/box/2.jpg');">
<p>Careers</p>
</div>
<?php
require('js/php/conn.php');
$get_carrer=mysqli_query($conn,"select organization_name,jobname,joblocation,hash from offer where status='APPROVED' order by timestamp DESC limit 3;");
while($get_details=mysqli_fetch_assoc($get_carrer))
{
		$orgname=$get_details['organization_name'];
		$job_name=$get_details['jobname'];
		$job_location=$get_details['joblocation'];
		$hash=$get_details['hash'];
		
		echo '
<a href="offer_response.php?h='.$hash.'">
<div class="carrers_box_profile">
<div style="width:70px; height: 70px; position: relative; float: left; margin-left: 10px; float: left;">
<img src="imags/job.png" style="width: 50px; height: 50px; position: absolute; margin: auto; top: 0px; left: 0px; right: 0px; bottom: 0px;" />
</div>
<div style="width: 350px; height: 100%; float: left;">
<p style="font-family: '."'Open Sans'".', sans-serif; color: black; width: 100%; font-size: 14px; margin: 0px; padding: 0px; padding-top: 10px;">'.$job_name.'</p>
<p style="font-family: '."'Open Sans'".', sans-serif; color: black; font-size: 10px;  margin: 0px; padding: 0px; padding-top: 3px;">'.$orgname.' , '.$job_location.'</p>
</div>
</div></a>		
		';	
}
?>




</div>
</div>

<div class="social_media_box">
<div class="social_media_box_a">
<div style="width: 100%; height: 50px; background-image: url('imags/box/3.jpg');">
<p>KLU Social</p>
</div>
<div style="width: 430px; height: 500px;">
<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FKLUniversity%2F&tabs=timeline&width=430&height=350&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId" width="430" height="350" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
</div>
</div>
</div>
</div>
<div style="width: 100%; height: auto; background-image: url('imags/geolocation.jpg'); background-position: center; background-size: 100% auto; padding-bottom: 30px;">
<p style="text-align: center; font-family: 'Noto Sans', sans-serif; color: white; font-size: 30px; padding-top: 20px; word-spacing: 5px; letter-spacing: 1px;"><b>Around The World</b></p>
<div style="width: 100%; max-width: 1800px; margin: auto; height: auto; margin-top: 50px; text-align: center;">
<div style="width: 350px; height: 180px; position: relative; display: inline-block; margin: 5px 30px 5px 30px;">
<div style="width: 100%; height: 100%; position: absolute; top: 0px;">
<div style="width: 130px; height: 130px; margin: auto;">
<img src="imags/1.png" style="height: 130px; width: auto; " />
</div>
<p style="font-size: 18px; padding-top: 10px; color: white; font-family: 'Open Sans', sans-serif;">Connect with more than 25,000+ Alumni</p>
</div>
</div>
<div style="width: 350px; height: 180px;  position: relative; display: inline-block; margin: 5px 30px 5px 30px;">
<div style="width: 100%; height: 100%; position: absolute; top: 0px;">
<div style="width: 130px; height: 130px;  margin: auto;">
<img src="imags/2.png" style="height: 130px; width: auto; " />
</div>
<p style="font-size: 18px; padding-top: 10px; color: white; font-family: 'Open Sans', sans-serif;">Across 25+ countries</p>
</div>
</div>
<div style="width: 350px; height: 180px;  position: relative; display: inline-block; margin: 5px 30px 5px 30px;">
<div style="width: 100%; height: 100%; position: absolute; top: 0px;">
<div style="width: 130px; height: 130px; margin: auto;">
<img src="imags/3.png" style="height: 130px; width: auto; " />
</div>
<p style="font-size: 18px; padding-top: 10px; color: white; font-family: 'Open Sans', sans-serif;">Working in different fields</p>
</div>
</div>
</div>
<div style="width: 100%; height: 45px; margin-top: 30px; text-align: center;">
<a href="map.php">
<button class="explore_map">Explore on map</button>
</a>
<link rel="stylesheet" href="css/bootrap.min.css"/>


    <link rel="stylesheet" href="css/footer.css"/>
</div>
</div>

   <footer class="site-footer">
        <div class="footer-widgets">
            <div class="container" style="width: 100%; max-width: 1800px;">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="foot-about">

							<div style="width: 300px; height: 100px;">
							<a href="https://kluniversity.in/">
							<img src="imags/klu.png" style="height: 70px; width: auto; float: left;" alt="">
							</a>
							</div>
                            <p style="font-family: arial; ">The Alumni Association of K L University (KLUAA) has been functioning from 1985 at the University campus. The main aim of the Association is to maintain the link between University and Alumni and share their details of employment and achievements</p>

                            <p class="copyright" >
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | KL Alumni Association
</p>
                        </div>
                    </div>

                    <div class="col-12 col-md-6 col-lg-4 mt-5 mt-md-0" style="margin: 0px 0px 0px 0px;">
                        <div class="foot-contact">
                            <h2 style="padding-left: 30px;">Contact</h2>

                            <ul class="p-0 m-0">
                                <li style="margin: 0px; padding-left: 30px; font-family: arial; "><span>Address:</span>Alumni Relation Cell ,</li>
								<li style="margin: 0px; padding-left: 30px; font-family: arial; ">3rd Floor, #305,</li>
								<li style="margin: 0px; padding-left: 30px; font-family: arial; ">Administrative Block ( Above Examinations Cell),</li>
						        <li style="margin: 0px; padding-left: 30px; font-family: arial; ">K L University , </li>
								<li style="margin: 0px; padding-left: 30px; font-family: arial; ">Green Fields, Vaddeswaram, </li>
								<li style="margin: 0px; padding-left: 30px; font-family: arial; ">Guntur District , AP., India. </li>
						        <li style="margin: 0px; padding-left: 30px; font-family: arial; ">Pincode : 522 502. </li>								
                                <li  style="margin: 0px; padding-left: 30px; font-family: arial; "><span>Phone:</span>0863-2399999 (Extension-1940) </li>
                                <li style="margin: 0px; padding-left: 30px; font-family: arial; "><span>Email:</span> alumni@kluniveristy.in</li>
                            </ul>
                        </div>
                    </div><!-- .col -->

                    <div class="col-12 col-md-6 col-lg-4 mt-5 mt-md-0">
                        <div class="foot-links">
                            <h2 >Useful Links</h2>

                            <ul class="p-0 m-0">
                                <li style="margin: 0px; font-family: arial; "><a href="map.php">Alumni's Explore om map</a></li>
                                <li style="margin: 0px; font-family: arial; "><a href="aboutus.html">About us</a></li>
                                <li style="margin: 0px; font-family: arial; "><a href="advisoryboard.php">Board members</a></li>
                                <li style="margin: 0px; font-family: arial; "><a href="aboutus.html">Contact</a></li>
                                <li style="margin: 0px; font-family: arial; "><a href="news.html">FAQ</a></li>
                                <li style="margin: 0px; font-family: arial; "><a href="updates.php">News & Updates</a></li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet"> 
<div class="col-12 col-md-12 col-lg-12 mt-12 mt-md-0">
                        <div class="foot-links">
<strong><p style="text-align: center; margin-top: 10px; font-family: arial; font-size: 13px;">Design and developed by Department of Computer Science and Engineering & KLUG @ KL University</p>

</div>
</div>

    </footer>


<script>
var width = $(window).width();
$(window).on('resize', function(){
   if($(this).width() != width){
      width = $(this).width();
      if(width<1315)
	   {
		  document.getElementById('signin_box').style.display="none";
		  document.getElementById('companys_list').style.width="100%";
	   }
	   else
	   {
		  document.getElementById('signin_box').style.display="block";
		  document.getElementById('companys_list').style.width="70%";
	   }
   }
});
 if(width<1315)
	   {
		  document.getElementById('signin_box').style.display="none";
		   document.getElementById('companys_list').style.width="100%";

	   }
	   else
	   {
		  document.getElementById('signin_box').style.display="block";
		   document.getElementById('companys_list').style.width="70%";
	   }
</script>
  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/magnific-popup/magnific-popup.min.js"></script>
  <script src="lib/sticky/sticky.js"></script>
  <script src="js/main.js"></script>

</body>
</html>
