<html>
<head>
<title>KL Alumni Association | K L University</title>
<meta http-equiv="expires" content="0">
<?php
clearstatcache();
?>
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<link rel="stylesheet" type="text/css" href="css/index.css">
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">	
<link href="https://fonts.googleapis.com/css?family=Noto+Sans|Roboto" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Baloo+Chettan|Montserrat" rel="stylesheet">
<script src="js/index_dropdown_menu.js"></script>
</head>
<body>
<div class="icon_dis" style="cursor: pointer;">
<div class="main_icon" onclick="window.location='index.php'">
<img src="imags/klu.png" />
<strong><p style="text-decoration: none;">Alumni Association</p></strong>
</div>
</div>
<div class="dropdownmenu">
  <nav>
      <div class="nav-mobile"><a id="nav-toggle" href="#!"><span></span></a></div>
      <ul class="nav-list" >
        <li>
          <a href="#" style="text-decoration: none;">Carrers</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="careers.php" style="text-decoration: none;">Internship</a>
            </li>
            <li>
              <a href="careers.php" style="text-decoration: none;">Jobs</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="advisoryboard.php" style="text-decoration: none;">Board Members</a>
        </li>
        <li>
          <a href="#!" style="text-decoration: none;">Events</a>
		   <ul class="nav-dropdown">
            <li>
              <a href="events.php" style="text-decoration: none;">Alumni Event Calender</a>
            </li>
			<li><a href="events.php" style="text-decoration: none;">All Events</a></li>
			<li>
			  <a href="https://kluniversity.in/site/acadcal.htm" style="text-decoration: none;">Academic Calender</a></li>
          </ul>
        </li>
		 <li>
              <a href="suess_stories.php" style="text-decoration: none;">Alumni Stories</a>
            </li>
            <li>
              <a href="gallery.php" style="text-decoration: none;">Gallery</a>
            </li>
         <li>
              <a href="updates.php" style="text-decoration: none;">News & Updates</a>
            </li>
        <li>
          <a href="aboutus.html" style="text-decoration: none;">About us</a>
        </li>
      </ul>
    </nav>
 </div>
<p style="font-family: 'Baloo Chettan', cursive; padding-top: 20px; font-size: 20px; padding-left: 20px;">Advisory Board</p>

<div style="max-width: 1400px; width: auto; height: auto; display: inline-block;  margin: auto;">


<?php
require('js/php/conn.php');
$get=mysqli_query($conn,"select * from faculty where alumni_designation='Advisory Board';");
while($g=mysqli_fetch_assoc($get))
{
	$name=$g['name'];
	$department_designation=$g['department_designation'];
	$photo='cms/faculty/'.$g['photo'];
	echo '
	
	<div style="width: 300px; height: 300px; float: left; position: relative; margin: 20px; ">
<div style="width: 120px; height: 120px; overflow: hidden; box-shadow: 0px 3px 10px 0px #adadad; border-radius: 120px; background-color: white; position: absolute; margin: auto; z-index:1; top: 10px; left: 0px; right: 0px; ">
<img src="'.$photo.'" style="width: 100%; height: auto;"/>
</div>
<div style="width: 300px; height: 260px; position: absolute; box-shadow: 0px 0px 2px 0px #adadad; bottom: 0px; overflow: hidden; border-radius: 5px;">
<div style="width: 100%; height: 3px; position: absolute; top: 0px;  background-image: linear-gradient(to right, #5dadec , #3d81c3);">
</div>
<div style="width: 100%; height: 100%; position: relative;">
<div style="width: 100%; height: 150px; margin-top: 90px;">
<p  style="font-family: '."'Montserrat'".', sans-serif; font-size: 18px; text-align: center; padding-top: 25px; color: ">'.$name.'</p>
<p  style="font-family: '."'Montserrat'".', sans-serif; font-size: 13px; text-align: center; padding-top: 10px; color: ">'.$department_designation.'</p>
</div>
</div>
</div>
</div>
	';
}
?>




</div>

<p style="font-family: 'Baloo Chettan', cursive; padding-top: 20px; font-size: 20px; padding-left: 20px;">Organization</p>
<div style="max-width: 1400px; width: auto; height: auto; display: inline-block;  margin: auto;">


<?php
require('js/php/conn.php');
$get=mysqli_query($conn,"select * from faculty where alumni_designation='Organisation';");
while($g=mysqli_fetch_assoc($get))
{
	$name=$g['name'];
	$department_designation=$g['department_designation'];
	$photo='cms/faculty/'.$g['photo'];
	echo '
	
	<div style="width: 300px; height: 300px; float: left; position: relative; margin: 20px; ">
<div style="width: 120px; height: 120px; overflow: hidden; box-shadow: 0px 3px 10px 0px #adadad; border-radius: 120px; background-color: white; position: absolute; margin: auto; z-index:1; top: 10px; left: 0px; right: 0px; ">
<img src="'.$photo.'" style="width: 100%; height: auto;"/>
</div>
<div style="width: 300px; height: 260px; position: absolute; box-shadow: 0px 0px 2px 0px #adadad; bottom: 0px; overflow: hidden; border-radius: 5px;">
<div style="width: 100%; height: 3px; position: absolute; top: 0px;  background-image: linear-gradient(to right, #5dadec , #3d81c3);">
</div>
<div style="width: 100%; height: 100%; position: relative;">
<div style="width: 100%; height: 150px; margin-top: 90px;">
<p  style="font-family: '."'Montserrat'".', sans-serif; font-size: 18px; text-align: center; padding-top: 25px; color: ">'.$name.'</p>
<p  style="font-family: '."'Montserrat'".', sans-serif; font-size: 13px; text-align: center; padding-top: 10px; color: ">'.$department_designation.'</p>
</div>
</div>
</div>
</div>
	';
}
?>

</div>



<p style="font-family: 'Baloo Chettan', cursive; padding-top: 20px; font-size: 20px; padding-left: 20px;">Executive Board</p>
<div style="max-width: 1400px; width: auto; height: auto; display: inline-block;  margin: auto;">



<?php
require('js/php/conn.php');
$get=mysqli_query($conn,"select * from faculty where alumni_designation='Executive Board';");
while($g=mysqli_fetch_assoc($get))
{
	$name=$g['name'];
	$department_designation=$g['department_designation'];
	$photo='cms/faculty/'.$g['photo'];
	echo '
	
	<div style="width: 300px; height: 300px; float: left; position: relative; margin: 20px; ">
<div style="width: 120px; height: 120px; overflow: hidden; box-shadow: 0px 3px 10px 0px #adadad; border-radius: 120px; background-color: white; position: absolute; margin: auto; z-index:1; top: 10px; left: 0px; right: 0px; ">
<img src="'.$photo.'" style="width: 100%; height: auto;"/>
</div>
<div style="width: 300px; height: 260px; position: absolute; box-shadow: 0px 0px 2px 0px #adadad; bottom: 0px; overflow: hidden; border-radius: 5px;">
<div style="width: 100%; height: 3px; position: absolute; top: 0px;  background-image: linear-gradient(to right, #5dadec , #3d81c3);">
</div>
<div style="width: 100%; height: 100%; position: relative;">
<div style="width: 100%; height: 150px; margin-top: 90px;">
<p  style="font-family: '."'Montserrat'".', sans-serif; font-size: 18px; text-align: center; padding-top: 25px; color: ">'.$name.'</p>
<p  style="font-family: '."'Montserrat'".', sans-serif; font-size: 13px; text-align: center; padding-top: 10px; color: ">'.$department_designation.'</p>
</div>
</div>
</div>
</div>
	';
}
?>

</div>



<p style="font-family: 'Baloo Chettan', cursive; padding-top: 20px; font-size: 20px; padding-left: 20px;">Alumni Coordinators</p>
<div style="max-width: 1400px; width: auto; height: auto; display: inline-block;  margin: auto;">



<?php
require('js/php/conn.php');
$get=mysqli_query($conn,"select * from faculty where alumni_designation='Alumni Coordinators';");
while($g=mysqli_fetch_assoc($get))
{
	$name=$g['name'];
	$department_designation=$g['department_designation'];
	$photo='cms/faculty/'.$g['photo'];
	echo '
	
	<div style="width: 300px; height: 300px; float: left; position: relative; margin: 20px; ">
<div style="width: 120px; height: 120px; overflow: hidden; box-shadow: 0px 3px 10px 0px #adadad; border-radius: 120px; background-color: white; position: absolute; margin: auto; z-index:1; top: 10px; left: 0px; right: 0px; ">
<img src="'.$photo.'" style="width: 100%; height: auto;"/>
</div>
<div style="width: 300px; height: 260px; position: absolute; box-shadow: 0px 0px 2px 0px #adadad; bottom: 0px; overflow: hidden; border-radius: 5px;">
<div style="width: 100%; height: 3px; position: absolute; top: 0px;  background-image: linear-gradient(to right, #5dadec , #3d81c3);">
</div>
<div style="width: 100%; height: 100%; position: relative;">
<div style="width: 100%; height: 150px; margin-top: 90px;">
<p  style="font-family: '."'Montserrat'".', sans-serif; font-size: 18px; text-align: center; padding-top: 25px; color: ">'.$name.'</p>
<p  style="font-family: '."'Montserrat'".', sans-serif; font-size: 13px; text-align: center; padding-top: 10px; color: ">'.$department_designation.'</p>
</div>
</div>
</div>
</div>
	';
}
?>

</div>



<p style="font-family: 'Baloo Chettan', cursive; padding-top: 20px; font-size: 20px; padding-left: 20px;">Alumni Chapters & Presidents</p>
<div style="max-width: 1400px; width: auto; height: auto; display: inline-block;  margin: auto;">



<?php
require('js/php/conn.php');
$get=mysqli_query($conn,"select * from faculty where alumni_designation='Alumni Chapters & Presidents';");
while($g=mysqli_fetch_assoc($get))
{
	$name=$g['name'];
	$department_designation=$g['department_designation'];
	$photo='cms/faculty/'.$g['photo'];
	echo '
	
	<div style="width: 300px; height: 300px; float: left; position: relative; margin: 20px; ">
<div style="width: 120px; height: 120px; overflow: hidden; box-shadow: 0px 3px 10px 0px #adadad; border-radius: 120px; background-color: white; position: absolute; margin: auto; z-index:1; top: 10px; left: 0px; right: 0px; ">
<img src="'.$photo.'" style="width: 100%; height: auto;"/>
</div>
<div style="width: 300px; height: 260px; position: absolute; box-shadow: 0px 0px 2px 0px #adadad; bottom: 0px; overflow: hidden; border-radius: 5px;">
<div style="width: 100%; height: 3px; position: absolute; top: 0px;  background-image: linear-gradient(to right, #5dadec , #3d81c3);">
</div>
<div style="width: 100%; height: 100%; position: relative;">
<div style="width: 100%; height: 150px; margin-top: 90px;">
<p  style="font-family: '."'Montserrat'".', sans-serif; font-size: 18px; text-align: center; padding-top: 25px; color: ">'.$name.'</p>
<p  style="font-family: '."'Montserrat'".', sans-serif; font-size: 13px; text-align: center; padding-top: 10px; color: ">'.$department_designation.'</p>
</div>
</div>
</div>
</div>
	';
}
?>

</div>

<p style="font-family: 'Baloo Chettan', cursive; padding-top: 20px; font-size: 20px; padding-left: 20px;">Alumni Staff</p>
<div style="max-width: 1400px; width: auto; height: auto; display: inline-block;  margin: auto;">



<?php
require('js/php/conn.php');
$get=mysqli_query($conn,"select * from faculty where alumni_designation='Alumni Staff';");
while($g=mysqli_fetch_assoc($get))
{
	$name=$g['name'];
	$department_designation=$g['department_designation'];
	$photo='cms/faculty/'.$g['photo'];
	echo '
	
	<div style="width: 300px; height: 300px; float: left; position: relative; margin: 20px; ">
<div style="width: 120px; height: 120px; overflow: hidden; box-shadow: 0px 3px 10px 0px #adadad; border-radius: 120px; background-color: white; position: absolute; margin: auto; z-index:1; top: 10px; left: 0px; right: 0px; ">
<img src="'.$photo.'" style="width: 100%; height: auto;"/>
</div>
<div style="width: 300px; height: 260px; position: absolute; box-shadow: 0px 0px 2px 0px #adadad; bottom: 0px; overflow: hidden; border-radius: 5px;">
<div style="width: 100%; height: 3px; position: absolute; top: 0px;  background-image: linear-gradient(to right, #5dadec , #3d81c3);">
</div>
<div style="width: 100%; height: 100%; position: relative;">
<div style="width: 100%; height: 150px; margin-top: 90px;">
<p  style="font-family: '."'Montserrat'".', sans-serif; font-size: 18px; text-align: center; padding-top: 25px; color: ">'.$name.'</p>
<p  style="font-family: '."'Montserrat'".', sans-serif; font-size: 13px; text-align: center; padding-top: 10px; color: ">'.$department_designation.'</p>
</div>
</div>
</div>
</div>
	';
}
?>

</div>




</body>
</html>